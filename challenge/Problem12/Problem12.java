import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Problem12 {
	static BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));

	private String[] inputs = {"E","F","A","G","B","H","C","I","J","D"};

	public int[] a26 = {4,5,6,7,8,9,10,11};
	public int a307 = 3;
	public String a59 = "e";
	public boolean a373 = false;
	public String a105 = "i";
	public boolean a240 = false;
	public int a302 = 11;
	public boolean a97 = false;
	public int a196 = 16;
	public int[] a109 = {45,46,47,48,49,50};
	public int[] a193 = {51,52,53,54,55,56};
	public int[] a114 = {57,58,59,60,61,62};
	public int[] a3 = a109;
	public boolean a7 = true;
	public int a277 = 85;
	public String a139 = "i";
	public String a312 = "f";
	public int[] a87 = {4,5,6,7,8,9,10,11};
	public int a54 = 149;
	public int a99 = 5;
	public boolean a151 = false;
	public int[] a111 = {44,45,46,47,48,49};
	public int[] a60 = {50,51,52,53,54,55};
	public int[] a188 = {56,57,58,59,60,61};
	public int[] a197 = a60;
	public int a72 = 93;
	public int a36 = 306;
	public int[] a92 = {8,9,10,11,12,13,14,15};
	public int[] a175 = {1,2,3,4,5,6,7,8};
	public int a141 = 12;
	public boolean a260 = false;
	public int a270 = 11;
	public boolean a339 = false;
	public int a397 = 11;
	public String a135 = "h";
	public int a310 = 187;
	public int a230 = 4;
	public boolean a146 = true;
	public String a75 = "f";
	public int a128 = 10;
	public int a359 = 4;
	public int[] a363 = {5,6,7,8,9,10,11,12};
	public String a143 = "h";
	public int a295 = 9;
	public int a125 = 12;
	public boolean a123 = true;
	public int[] a115 = {4,5,6,7,8,9,10,11};
	public boolean a142 = false;
	public int a53 = 116;
	public boolean a378 = false;
	public boolean a210 = false;
	public boolean a300 = false;
	public boolean a395 = false;
	public String a173 = "f";
	public int[] a226 = {4,5,6,7,8,9,10,11};
	public String a361 = "f";
	public int[] a294 = {2,3,4,5,6,7,8,9};
	public int a13 = 338;
	public boolean a221 = false;
	public int[] a68 = {5,6,7,8,9,10};
	public int[] a199 = {11,12,13,14,15,16};
	public int[] a76 = {17,18,19,20,21,22};
	public int[] a25 = a68;
	public int[] a366 = {7,8,9,10,11,12,13,14};
	public int[] a318 = {22,23,24,25,26,27};
	public int[] a285 = {28,29,30,31,32,33};
	public int[] a311 = {34,35,36,37,38,39};
	public int[] a370 = a285;
	public int a1 = 6;
	public int[] a229 = {44,45,46,47,48,49};
	public int[] a264 = {50,51,52,53,54,55};
	public int[] a292 = {56,57,58,59,60,61};
	public int[] a228 = a264;
	public int a19 = 254;
	public int a211 = 2;
	public boolean a16 = false;
	public String a207 = "f";
	public int[] a20 = {76,77,78,79,80,81};
	public int[] a58 = {82,83,84,85,86,87};
	public int[] a85 = {88,89,90,91,92,93};
	public int[] a195 = a58;
	public int[] a37 = {7,8,9,10,11,12,13,14};
	public int a398 = 11;
	public int a179 = -10;
	public int a42 = 2;
	public int[] a205 = {5,6,7,8,9,10,11,12};
	public int[] a242 = {31,32,33,34,35,36};
	public int[] a299 = {37,38,39,40,41,42};
	public int[] a268 = {43,44,45,46,47,48};
	public int[] a239 = a299;
	public int[] a348 = {65,66,67,68,69,70};
	public int[] a351 = {71,72,73,74,75,76};
	public int[] a335 = {77,78,79,80,81,82};
	public int[] a358 = a351;
	public String a155 = "f";
	public int a130 = 127;
	public int[] a245 = {9,10,11,12,13,14};
	public int[] a354 = {15,16,17,18,19,20};
	public int[] a290 = {21,22,23,24,25,26};
	public int[] a274 = a245;
	public int[] a217 = {9,10,11,12,13,14,15,16};
	public int a201 = -151;
	public String a256 = "f";
	public int a278 = 8;
	public int a286 = 3;
	public int a320 = 7;
	public int a71 = 1;
	public String a177 = "f";
	public int a249 = 346;
	public boolean a66 = true;
	public String a329 = "f";
	public String a224 = "f";
	public int[] a79 = {6,7,8,9,10,11,12,13};
	public int a154 = 13;
	public String a218 = "f";
	public int a237 = 4;
	public int[] a157 = {8,9,10,11,12,13,14,15};
	public int a91 = 9;
	public int[] a263 = {1,2,3,4,5,6};
	public int[] a241 = {7,8,9,10,11,12};
	public int[] a399 = {13,14,15,16,17,18};
	public int[] a353 = a241;
	public int a244 = 11;
	public int a14 = 8;
	public boolean a134 = true;
	public int[] a152 = {6,7,8,9,10,11};
	public int[] a144 = {12,13,14,15,16,17};
	public int[] a153 = {18,19,20,21,22,23};
	public int[] a39 = a153;
	public boolean a64 = false;
	public int a324 = 9;
	public boolean cf = true;
	public int[] a326 = {2,3,4,5,6,7,8,9};
	public String a107 = "e";
	public String a183 = "g";
	public int a357 = -187;
	public int[] a148 = {21,22,23,24,25,26};
	public int[] a119 = {27,28,29,30,31,32};
	public int[] a18 = {33,34,35,36,37,38};
	public int[] a46 = a18;
	public int[] a212 = {78,79,80,81,82,83};
	public int[] a384 = {84,85,86,87,88,89};
	public int[] a362 = {90,91,92,93,94,95};
	public int[] a296 = a384;
	public int a206 = 5;
	public int a191 = 14;
	public int[] a227 = {2,3,4,5,6,7,8,9};
	public int[] a262 = {91,92,93,94,95,96};
	public int[] a223 = {97,98,99,100,101,102};
	public int[] a259 = {103,104,105,106,107,108};
	public int[] a275 = a262;
	public int[] a30 = {5,6,7,8,9,10,11,12};
	public int a184 = 14;
	public int a386 = 160;
	public int[] a232 = {8,9,10,11,12,13,14,15};
	public int[] a47 = {8,9,10,11,12,13,14,15};
	public int a158 = 5;
	public int[] a65 = {54,55,56,57,58,59};
	public int[] a83 = {60,61,62,63,64,65};
	public int[] a100 = {66,67,68,69,70,71};
	public int[] a40 = a65;
	public int a382 = -46;
	public boolean a336 = false;
	public String a171 = "e";
	public int a136 = 7;
	public int a202 = 10;
	public int a383 = 5;
	public int a81 = 4;
	public int[] a372 = {7,8,9,10,11,12,13,14};
	public int a172 = 6;
	public int a17 = 257;
	public int a8 = 12;
	public int a102 = 14;
	public int a73 = 153;
	public int[] a127 = {7,8,9,10,11,12,13,14};
	public String a44 = "e";
	public int a57 = 14;
	public int[] a216 = {7,8,9,10,11,12,13,14};
	public int a77 = 12;
	public String a313 = "f";
	public int a243 = -173;
	public int a200 = 9;
	public int a129 = 14;
	public int[] a189 = {19,20,21,22,23,24};
	public int[] a116 = {25,26,27,28,29,30};
	public int[] a117 = {31,32,33,34,35,36};
	public int[] a137 = a189;
	public int a323 = 10;
	public int[] a253 = {96,97,98,99,100,101};
	public int[] a289 = {102,103,104,105,106,107};
	public int[] a250 = {108,109,110,111,112,113};
	public int[] a276 = a289;
	public int a338 = 4;
	public int a234 = 8;
	public int a333 = -26;
	public int a235 = 8;
	public boolean a368 = false;
	public int[] a303 = {42,43,44,45,46,47};
	public int[] a293 = {48,49,50,51,52,53};
	public int[] a376 = {54,55,56,57,58,59};
	public int[] a265 = a293;
	public int[] a208 = {34,35,36,37,38,39};
	public int[] a257 = {40,41,42,43,44,45};
	public int[] a304 = {46,47,48,49,50,51};
	public int[] a392 = a257;
	public String a69 = "f";
	public String a269 = "f";
	public int[] a167 = {2,3,4,5,6,7,8,9};
	public int[] a346 = {10,11,12,13,14,15,16,17};
	public int a170 = 72;
	public boolean a132 = false;
	public int a225 = 6;
	public int a120 = 1;
	public int a131 = -15;
	public int a164 = 1;
	public int a122 = 3;
	public int a165 = 1;
	public int a89 = 1;
	public int a174 = 1;
	public int a63 = 3;
	public int a12 = 1;
	public int a67 = 0;
	public int a169 = 2;
	public int a133 = 1;
	public int a35 = 1;
	public int a50 = 1;
	public int a98 = 1;
	public int a162 = 2;
	public int a56 = -15;
	public int a178 = 1;
	public int a5 = 3;
	public int a90 = 1;
	public int a110 = -15;
	public int a51 = 1;
	public int a156 = -15;
	public int a93 = -15;
	public int a181 = 1;
	public int a94 = 1;
	public int a31 = 1;
	public int a88 = -15;
	public int a161 = 3;
	public int a168 = 1;

	private void errorCheck() {
	    if((((a1 == a87[0] && (a77 == 10)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(0);
	    }
	    if(((((a42 == 1) && a141 == a47[1]) && (a173.equals("i"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(1);
	    }
	    if((((a14 == a79[4] && (a143.equals("f"))) && (a57 == 11)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(2);
	    }
	    if((((a141 == a47[1] && a72 <=  -104) && (a57 == 15)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(3);
	    }
	    if((((a71 == a175[1] && !a134) && (a57 == 14)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(4);
	    }
	    if((((a99 == a26[0] && (a172 == 7)) && (a173.equals("g"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(5);
	    }
	    if(((((22 == a25[5]) && (63 == a40[3])) && (a57 == 17)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(6);
	    }
	    if((((!a66 && ((134 < a130) && (276 >= a130))) && (a173.equals("e"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(7);
	    }
	    if((((a210 && a125 == a30[2]) && (a173.equals("f"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(8);
	    }
	    if((((325 < a13 && 493 < a130) && (a173.equals("e"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(9);
	    }
	    if(((((a136 == 12) && (a77 == 5)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(10);
	    }
	    if((((a17 <=  134 && 258 < a72) && (a57 == 15)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(11);
	    }
	    if((((a129 == a92[3] && (a172 == 1)) && (a173.equals("g"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(12);
	    }
	    if((((a1 == a87[4] && (a91 == 11)) && (a173.equals("h"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(13);
	    }
	    if((((a129 == a92[0] && (a196 == 10)) && a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(14);
	    }
	    if((((a129 == a92[1] && a125 == a30[7]) && (a173.equals("f"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(15);
	    }
	    if(((((a183.equals("i")) && a278 == a326[5]) && (a57 == 16)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(16);
	    }
	    if((((a129 == a92[5] && (a77 == 12)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(17);
	    }
	    if(((((a69.equals("i")) && (a143.equals("i"))) && (a57 == 11)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(18);
	    }
	    if(((((a155.equals("h")) && (a77 == 7)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(19);
	    }
	    if((((a13 <=  -10 && (a158 == 7)) && (a57 == 10)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(20);
	    }
	    if((((a54 <=  164 && (a196 == 12)) && a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(21);
	    }
	    if((((a129 == a92[3] && (a77 == 12)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(22);
	    }
	    if(((((a196 == 11) && a295 == a366[0]) && !a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(23);
	    }
	    if((((a1 == a87[3] && (a91 == 11)) && (a173.equals("h"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(24);
	    }
	    if(((((a105.equals("h")) && (a158 == 6)) && (a57 == 10)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(25);
	    }
	    if(((((9 == a274[0]) && (a172 == 4)) && (a173.equals("g"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(26);
	    }
	    if((((a184 == a157[3] && a295 == a366[4]) && !a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(27);
	    }
	    if((((!a7 && (a172 == 6)) && (a173.equals("g"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(28);
	    }
	    if(((((a8 == 6) && (a172 == 2)) && (a173.equals("g"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(29);
	    }
	    if(((((15 == a39[3]) && a73 <=  167) && !a146) && (a75.equals("i")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(30);
	    }
	    if((((a244 == a363[0] && a141 == a47[2]) && (a173.equals("i"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(31);
	    }
	    if((((a81 == a167[5] && a125 == a30[5]) && (a173.equals("f"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(32);
	    }
	    if((((((-58 < a179) && (23 >= a179)) && a81 == a167[4]) && (a57 == 12)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(33);
	    }
	    if(((((a69.equals("f")) && (a143.equals("i"))) && (a57 == 11)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(34);
	    }
	    if((((((277 < a36) && (307 >= a36)) && ((-104 < a72) && (100 >= a72))) && (a57 == 15)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(35);
	    }
	    if((((a141 == a47[2] && a72 <=  -104) && (a57 == 15)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(36);
	    }
	    if(((((a136 == 5) && (a77 == 5)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(37);
	    }
	    if((((a81 == a167[5] && a278 == a326[7]) && (a57 == 16)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(38);
	    }
	    if((((a81 == a167[0] && a125 == a30[5]) && (a173.equals("f"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(39);
	    }
	    if(((((a173.equals("f")) && (a143.equals("g"))) && (a57 == 11)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(40);
	    }
	    if(((((a105.equals("i")) && (a158 == 6)) && (a57 == 10)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(41);
	    }
	    if((((a81 == a167[5] && (a158 == 4)) && (a57 == 10)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(42);
	    }
	    if((((a141 == a47[7] && a72 <=  -104) && (a57 == 15)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(43);
	    }
	    if(((((a69.equals("h")) && (a143.equals("i"))) && (a57 == 11)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(44);
	    }
	    if(((((a143.equals("g")) && a295 == a366[6]) && !a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(45);
	    }
	    if(((((a158 == 8) && a81 == a167[2]) && (a57 == 12)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(46);
	    }
	    if((((a99 == a26[1] && a81 == a167[0]) && (a57 == 12)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(47);
	    }
	    if(((((a177.equals("g")) && (26 == a137[1])) && a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(48);
	    }
	    if((((a81 == a167[6] && a125 == a30[5]) && (a173.equals("f"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(49);
	    }
	    if((((!a16 && ((167 < a73) && (277 >= a73))) && !a146) && (a75.equals("i")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(50);
	    }
	    if((((a184 == a157[5] && a295 == a366[4]) && !a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(51);
	    }
	    if((((176 < a53 && a295 == a366[5]) && !a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(52);
	    }
	    if(((((a128 == 6) && a141 == a47[6]) && (a173.equals("i"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(53);
	    }
	    if((((((26 < a170) && (82 >= a170)) && a81 == a167[5]) && (a57 == 12)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(54);
	    }
	    if(((((5 == a25[0]) && (63 == a40[3])) && (a57 == 17)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(55);
	    }
	    if(((((23 == a274[2]) && (a172 == 4)) && (a173.equals("g"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(56);
	    }
	    if((((a200 == a115[6] && (a158 == 5)) && (a57 == 10)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(57);
	    }
	    if(((((a155.equals("f")) && (a77 == 7)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(58);
	    }
	    if(((((a44.equals("f")) && (a77 == 8)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(59);
	    }
	    if((((((306 < a19) && (405 >= a19)) && a278 == a326[0]) && (a57 == 16)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(60);
	    }
	    if((((a129 == a92[7] && (a196 == 10)) && a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(61);
	    }
	    if(((((a69.equals("g")) && (a143.equals("i"))) && (a57 == 11)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(62);
	    }
	    if(((((a139.equals("e")) && ((276 < a130) && (493 >= a130))) && (a173.equals("e"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(63);
	    }
	    if((((!a64 && 402 < a73) && !a146) && (a75.equals("i")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(64);
	    }
	    if((((a141 == a47[5] && a72 <=  -104) && (a57 == 15)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(65);
	    }
	    if((((a179 <=  -162 && a81 == a167[4]) && (a57 == 12)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(66);
	    }
	    if((((a99 == a26[2] && a81 == a167[0]) && (a57 == 12)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(67);
	    }
	    if((((a141 == a47[0] && a72 <=  -104) && (a57 == 15)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(68);
	    }
	    if((((a170 <=  26 && a295 == a366[3]) && !a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(69);
	    }
	    if((((a81 == a167[3] && (a158 == 8)) && (a57 == 10)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(70);
	    }
	    if(((((55 == a40[1]) && a278 == a326[2]) && (a57 == 16)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(71);
	    }
	    if(((((a183.equals("h")) && a278 == a326[5]) && (a57 == 16)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(72);
	    }
	    if((((a1 == a87[3] && (a77 == 10)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(73);
	    }
	    if((((a210 && (a143.equals("h"))) && (a57 == 11)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(74);
	    }
	    if(((((86 == a195[4]) && (a91 == 13)) && (a173.equals("h"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(75);
	    }
	    if(((((a107.equals("f")) && a278 == a326[4]) && (a57 == 16)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(76);
	    }
	    if((((a141 == a47[6] && (55 == a40[1])) && (a57 == 17)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(77);
	    }
	    if(((((60 == a3[3]) && (a158 == 9)) && (a57 == 10)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(78);
	    }
	    if((((a191 == a37[5] && a130 <=  134) && (a173.equals("e"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(79);
	    }
	    if((((a81 == a167[3] && a125 == a30[5]) && (a173.equals("f"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(80);
	    }
	    if(((((45 == a197[1]) && a81 == a167[1]) && (a57 == 12)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(81);
	    }
	    if(((((a155.equals("g")) && (a77 == 7)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(82);
	    }
	    if((((a1 == a87[2] && (a77 == 10)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(83);
	    }
	    if(((((a196 == 13) && a295 == a366[0]) && !a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(84);
	    }
	    if(((((a107.equals("g")) && (a172 == 8)) && (a173.equals("g"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(85);
	    }
	    if((((a123 && (31 == a137[0])) && a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(86);
	    }
	    if(((((a8 == 12) && a125 == a30[1]) && (a173.equals("f"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(87);
	    }
	    if(((((6 == a39[0]) && a73 <=  167) && !a146) && (a75.equals("i")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(88);
	    }
	    if((((((-46 < a53) && (74 >= a53)) && a295 == a366[5]) && !a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(89);
	    }
	    if((((a134 && a278 == a326[6]) && (a57 == 16)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(90);
	    }
	    if(((((a42 == 7) && a141 == a47[1]) && (a173.equals("i"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(91);
	    }
	    if((((((82 < a170) && (121 >= a170)) && a295 == a366[3]) && !a134) && (a75.equals("h")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(92);
	    }
	    if((((a99 == a26[3] && (a77 == 11)) && !a132) && (a75.equals("e")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(93);
	    }
	    if(((((a139.equals("h")) && ((276 < a130) && (493 >= a130))) && (a173.equals("e"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(94);
	    }
	    if(((((a397 == 11) && a125 == a30[3]) && (a173.equals("f"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(95);
	    }
	    if((((a184 == a157[2] && a141 == a47[3]) && (a173.equals("i"))) && (a75.equals("f")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(96);
	    }
	    if((((a71 == a175[4] && !a134) && (a57 == 14)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(97);
	    }
	    if((((258 < a72 && (21 == a46[0])) && a146) && (a75.equals("i")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(98);
	    }
	    if((((a141 == a47[0] && (55 == a40[1])) && (a57 == 17)) && (a75.equals("g")))){
	    	cf = false;
	    	Errors.__VERIFIER_error(99);
	    }
	}private  void calculateOutputm20(String input) {
    if(((input.equals(inputs[4]) && ((((a218.equals("e")) && ((a201 <=  -199 && a249 <=  152) && (a75.equals("e")))) && a132) && a395)) && (((a359 == 3) && ((26 == a137[1]) && ((a177.equals("f")) && cf))) && a260))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a169 -= (a169 - 20) < a169 ? 4 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if((178 < a201 && ((a333 <=  -47 && !(55 == a40[1])) || !(a307 == a227[3])))) {
    	a333 = (((3 / 5) - -3707) * 5);
    	a57 = ((a270 / a206) + 10);
    	a336 = false;
    	a296 = a212;
    	a358 = a348;
    	a230 = 6;
    	a353 = a263;
    	a382 = ((((a382 % 14911) + 15087) - -4378) - -5003);
    	a312 = "e";
    	a368 = false;
    	a243 = ((((a243 % 14910) + -179) * 1) - 4354);
    	a269 = "e";
    	a234 = a372[2];
    	a224 = "f";
    	a398 = 14;
    	a361 = "e";
    	a239 = a299;
    	a75 = "g";
    	a256 = "h";
    	a313 = "f";
    	a320 = 7;
    	a277 = ((((a277 % 14830) - -15169) - -12115) / 5);
    	a235 = a216[3];
    	a228 = a229;
    	a225 = a205[7];
    	a240 = false;
    	a270 = 16;
    	a207 = "g";
    	a260 = true;
    	a392 = a208;
    	a237 = 9;
    	a338 = 4;
    	a211 = 2;
    	a265 = a293;
    	a378 = false;
    	a324 = a232[0];
    	a386 = (((52 - -140) - 25948) + 25841);
    	a373 = false;
    	a249 = ((((a249 - 0) / 5) % 101) + 254);
    	a218 = "e";
    	a307 = a227[5];
    	a201 = (((((a201 % 14900) - 199) - 14263) + 26038) + -22726);
    	a395 = true;
    	a339 = true;
    	a370 = a285;
    	a329 = "i";
    	a383 = a226[3];
    	a206 = 11;
    	a81 = a167[(a359 + -1)];
    	a158 = ((a57 / a57) - -4);
    	a359 = 3; 
    	}else {
    	a224 = "h";
    	a243 = ((((a243 / -5) - 27808) + 19465) * -3);
    	a57 = 12;
    	a75 = "g";
    	a368 = false;
    	a338 = 9;
    	a296 = a212;
    	a324 = a232[1];
    	a378 = false;
    	a339 = true;
    	a333 = (((39 + 26488) / 5) - 5159);
    	a392 = a304;
    	a218 = "i";
    	a201 = ((((((a201 % 14900) + -199) * 1) - -25366) * -1) / 10);
    	a206 = 5;
    	a270 = 14;
    	a370 = a318;
    	a359 = 8;
    	a358 = a351;
    	a211 = 6;
    	a277 = ((((a277 % 14995) - 10) + -3277) + -6232);
    	a361 = "f";
    	a307 = a227[5];
    	a207 = "e";
    	a240 = false;
    	a81 = a167[(a57 - 5)];
    	a353 = a241;
    	a398 = 10;
    	a329 = "e";
    	a237 = 4;
    	a225 = a205[6];
    	a312 = "h";
    	a269 = "i";
    	a235 = a216[3];
    	a395 = true;
    	a382 = ((((a382 - 0) % 14967) - 65) - 7499);
    	a202 = a217[0];
    	a260 = true;
    	a230 = 9;
    	a228 = a264;
    	a239 = a299;
    	a249 = ((((a249 % 14750) - -15249) - -1) * 1);
    	a386 = (((81 - -27546) / 5) + 1048);
    	a336 = false;
    	a323 = (a57 + -3);
    	}System.out.println("S");
    } 
    if(((input.equals(inputs[2]) && ((a338 == 3) && a243 <=  -179)) && ((a177.equals("f")) && (((a224.equals("e")) && ((a313.equals("e")) && ((a75.equals("e")) && ((81 == a296[3]) && ((26 == a137[1]) && (a132 && cf)))))) && a234 == a372[0])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	a75 = "g";
    	a40 = a83;
    	a57 = (a398 + 7);
    	a25 = a68; 
    	System.out.println("S");
    } 
    if((((a378 && (((a75.equals("e")) && a201 <=  -199) && (26 == a137[1]))) && a336) && ((37 == a392[3]) && ((a361.equals("e")) && (a339 && ((a177.equals("f")) && ((cf && input.equals(inputs[0])) && a132))))))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a169 -= (a169 - 20) < a169 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a230 = 10;
    	a243 = ((((a243 - -5349) % 14910) - 15088) - 3);
    	a383 = a226[5];
    	a307 = a227[3];
    	a357 = (((63 + -20909) - 3828) + -1167);
    	a224 = "g";
    	a249 = (((((a249 % 14750) - -15249) - -1) + -23170) - -23170);
    	a269 = "h";
    	a286 = a294[0];
    	a398 = 12;
    	a339 = true;
    	a173 = "f";
    	a276 = a253;
    	a239 = a268;
    	a392 = a304;
    	a202 = a217[2];
    	a107 = "e";
    	a382 = ((((a382 % 107) + 115) - -26297) + -26263);
    	a324 = a232[6];
    	a235 = a216[7];
    	a234 = a372[0];
    	a75 = "f";
    	a256 = "e";
    	a265 = a303;
    	a329 = "i";
    	a300 = true;
    	a211 = 7;
    	a270 = 12;
    	a225 = a205[2];
    	a218 = "g";
    	a361 = "e";
    	a201 = (((((a201 % 94) + 93) - -1451) * 1) / 10);
    	a240 = true;
    	a320 = 12;
    	a302 = a346[3];
    	a353 = a399;
    	a368 = true;
    	a313 = "i";
    	a359 = 3;
    	a228 = a292;
    	a312 = "h";
    	a125 = a30[(a206 - -2)];
    	a277 = (((a277 + 20397) / 5) + -23039);
    	a310 = ((((98 / 5) - 9818) * -1) / 10);
    	a296 = a384;
    	a338 = 7;
    	a260 = true;
    	a336 = false;
    	a358 = a348;
    	a370 = a318;
    	a373 = true;
    	a395 = true;
    	a206 = 9; 
    	System.out.println("S");
    } 
    if((((a329.equals("e")) && (((a256.equals("e")) && (a240 && (a249 <=  152 && (26 == a137[1])))) && (a211 == 1))) && ((24 == a370[2]) && ((input.equals(inputs[7]) && (((a75.equals("e")) && cf) && a132)) && (a177.equals("f")))))) {
    	a164 += (a164 + 20) > a164 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a35 += (a35 + 20) > a35 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	if(a368) {
    	a225 = a205[1];
    	a201 = (((((a201 % 14900) + -199) * 10) / 9) - 11958);
    	a276 = a253;
    	a42 = (a320 + 2);
    	a256 = "h";
    	a277 = (((((a277 % 14995) + -10) * 10) / 9) - 11159);
    	a392 = a304;
    	a310 = ((((38 - -13771) - 13486) - 14022) + 14022);
    	a260 = true;
    	a239 = a299;
    	a357 = (((25 - 24868) - -24822) - 19);
    	a324 = a232[5];
    	a300 = false;
    	a338 = 7;
    	a218 = "h";
    	a234 = a372[4];
    	a240 = true;
    	a383 = a226[2];
    	a398 = 13;
    	a269 = "i";
    	a312 = "f";
    	a313 = "g";
    	a75 = "f";
    	a228 = a264;
    	a230 = 10;
    	a286 = a294[4];
    	a329 = "e";
    	a270 = 15;
    	a395 = true;
    	a353 = a241;
    	a211 = 4;
    	a224 = "g";
    	a141 = a47[(a359 - 2)];
    	a307 = a227[2];
    	a237 = 5;
    	a206 = 8;
    	a320 = 6;
    	a173 = "i";
    	a333 = ((((73 + -44) / 5) - -20578) + -20459);
    	a378 = true;
    	a373 = false;
    	a359 = 9; 
    	}else {
    	a378 = true;
    	a310 = (((((73 - -12756) + -20474) / 5) * -3) / 10);
    	a392 = a304;
    	a134 = true;
    	a357 = (((98 - -12406) * 2) + -25008);
    	a329 = "g";
    	a75 = "h";
    	a333 = (((((29 - -5647) * 10) / 9) * 10) / 9);
    	a395 = true;
    	a237 = 9;
    	a307 = a227[2];
    	a324 = a232[5];
    	a386 = ((((94 + -29583) + 21864) * -1) / 10);
    	a276 = a250;
    	a256 = "h";
    	a196 = 10;
    	a207 = "f";
    	a224 = "g";
    	a201 = ((((a201 - 0) % 14900) + -199) * 1);
    	a286 = a294[5];
    	a129 = a92[(a196 - 8)];
    	}System.out.println("S");
    } 
    if(((((a359 == 3) && ((((a132 && ((26 == a137[1]) && cf)) && input.equals(inputs[1])) && (a177.equals("f"))) && a234 == a372[0])) && a201 <=  -199) && ((((67 == a358[2]) && (a75.equals("e"))) && a395) && a324 == a232[0]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a178 += (a178 + 20) > a178 ? 4 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	a77 = ((a359 - a206) - -6);
    	a132 = false;
    	a136 = a77; 
    	System.out.println("X");
    } 
    if(((a382 <=  -65 && (a202 == a217[0] && ((((input.equals(inputs[5]) && (a361.equals("e"))) && (a359 == 3)) && (a75.equals("e"))) && a132))) && (a277 <=  -10 && ((((a177.equals("f")) && cf) && (26 == a137[1])) && (a312.equals("e")))))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a35 -= (a35 - 20) < a35 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	if(((!(a320 == 12) || (!(a143.equals("i")) && (a134 && a146))) && !(a177.equals("g")))) {
    	a146 = false;
    	a16 = false;
    	a75 = "i";
    	a73 = (((34 + 175) * 5) / 5); 
    	}else {
    	a170 = ((((((a201 * a249) % 14999) / 5) - 2558) / 5) - 14824);
    	a134 = false;
    	a75 = "h";
    	a295 = a366[((a211 + a338) - 1)];
    	}System.out.println("Z");
    } 
    if((((((a269.equals("e")) && ((a202 == a217[0] && ((a320 == 6) && (a202 == a217[0] && (a359 == 3)))) && a132)) && (26 == a137[1])) && input.equals(inputs[6])) && ((a359 == 3) && ((a177.equals("f")) && ((a75.equals("e")) && cf))))) {
    	a164 += (a164 + 20) > a164 ? 3 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a173 = "e";
    	a75 = "f";
    	a130 = (((35 * 5) - -13355) - -12833);
    	a13 = (((((a130 * a130) % 14999) - -1643) - -2410) * 1); 
    	System.out.println("Y");
    } 
    if((((a320 == 6) && (26 == a137[1])) && (a202 == a217[0] && ((((((a75.equals("e")) && (a132 && ((input.equals(inputs[3]) && cf) && (a177.equals("f"))))) && (a218.equals("e"))) && a368) && (a313.equals("e"))) && (47 == a265[5]))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a67 -= (a67 - 20) < a67 ? 4 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a207 = "i";
    	a373 = true;
    	a243 = ((((a243 % 11) + -157) + -4) - -1);
    	a237 = 7;
    	a235 = a216[0];
    	a286 = a294[2];
    	a307 = a227[1];
    	a240 = false;
    	a295 = a366[((a230 * a270) - 30)];
    	a234 = a372[3];
    	a270 = 13;
    	a206 = 7;
    	a196 = 16;
    	a324 = a232[1];
    	a313 = "e";
    	a382 = (((((a382 % 12) - 48) - 22186) + -582) + 22764);
    	a296 = a212;
    	a75 = "h";
    	a249 = (((((a249 % 101) + 254) + -1) / 5) - -130);
    	a134 = false;
    	a333 = ((((48 * 10) / 9) - 41) + -5);
    	a370 = a318;
    	a276 = a250;
    	a383 = a226[6];
    	a310 = (((21 - 24207) * 1) + -5378);
    	a386 = (((94 - 24626) * 1) + 24705);
    	a230 = 3; 
    	System.out.println("R");
    } 
    if(((input.equals(inputs[8]) && (a249 <=  152 && (a324 == a232[0] && ((a75.equals("e")) && ((cf && (a177.equals("f"))) && a132))))) && ((((3 == a353[2]) && ((24 == a370[2]) && a378)) && a307 == a227[0]) && (26 == a137[1])))) {
    	a164 += (a164 + 20) > a164 ? 3 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	cf = false;
    	a57 = (a320 + 10);
    	a278 = a326[(a270 + -10)];
    	a75 = "g";
    	a19 = ((((91 - -3835) - -23987) + 1979) + -29503); 
    	System.out.println("T");
    } 
    if(((input.equals(inputs[9]) && (a373 && (a225 == a205[0] && a260))) && ((a177.equals("f")) && (((26 == a137[1]) && ((47 == a265[5]) && ((a132 && (cf && (a75.equals("e")))) && a395))) && a383 == a226[0])))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	cf = false;
    	a57 = ((a230 / a338) - -14);
    	a75 = "g";
    	a72 = (((94 * 5) * 5) + 2414);
    	a17 = ((((((a72 * a72) % 14999) - 22025) * 10) / 9) - -3963); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm1(String input) {
    if((((a235 == a216[0] && a243 <=  -179) && a383 == a226[0]) && ((a312.equals("e")) && (a286 == a294[0] && ((cf && (26 == a137[1])) && a324 == a232[0]))))) {
    	if(((((36 == a239[5]) && ((a269.equals("e")) && (a256.equals("e")))) && a395) && ((67 == a358[2]) && (((a177.equals("f")) && cf) && (45 == a228[1]))))) {
    		calculateOutputm20(input);
    	} 
    } 
}
private  void calculateOutputm29(String input) {
    if((((a320 == 6) && ((((((cf && (a44.equals("g"))) && (a75.equals("e"))) && a240) && a383 == a226[0]) && !a132) && input.equals(inputs[4]))) && (a249 <=  152 && ((a339 && a277 <=  -10) && (a77 == 8))))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	if((!(a312.equals("g")) && a134)) {
    	a307 = a227[7];
    	a392 = a304;
    	a383 = a226[3];
    	a357 = ((((a357 / 5) / 5) / 5) + 21511);
    	a206 = 9;
    	a239 = a299;
    	a201 = ((((a201 * -9) / 10) * 1) + 2004);
    	a378 = true;
    	a361 = "i";
    	a333 = ((((74 + 4991) * 10) / 9) - -8579);
    	a310 = ((((a310 - 0) - -20950) % 15080) - 14919);
    	a373 = true;
    	a225 = a205[7];
    	a249 = ((((a249 % 14750) - -15249) - 0) * 1);
    	a228 = a292;
    	a202 = a217[4];
    	a218 = "e";
    	a358 = a348;
    	a386 = ((((a386 % 61) + 262) + 0) + 0);
    	a256 = "g";
    	a269 = "g";
    	a370 = a318;
    	a224 = "h";
    	a296 = a384;
    	a320 = 13;
    	a382 = ((((a382 * 1) % 14967) - 65) * 1);
    	a240 = true;
    	a339 = true;
    	a270 = 10;
    	a395 = true;
    	a353 = a399;
    	a75 = "f";
    	a235 = a216[2];
    	a173 = "h";
    	a265 = a303;
    	a207 = "h";
    	a102 = a127[((a77 / a77) + 4)];
    	a211 = 4;
    	a286 = a294[6];
    	a336 = true;
    	a276 = a289;
    	a368 = true;
    	a91 = ((a237 - a230) + 8);
    	a243 = ((((45 + 9004) + -9133) - -2713) + -2710);
    	a260 = true;
    	a313 = "h";
    	a277 = (((((a277 * 1) - 0) - -26733) % 14995) - 15004);
    	a359 = 6;
    	a302 = a346[3];
    	a338 = 7;
    	a230 = 9;
    	a237 = 7; 
    	}else {
    	a75 = "g";
    	a278 = a326[((a270 - a77) - 2)];
    	a19 = ((((63 - -245) / 5) * 55) / 10);
    	a57 = ((a77 / a359) - -14);
    	}System.out.println("T");
    } 
    if(((a373 && ((a44.equals("g")) && (a368 && ((47 == a265[5]) && ((((a77 == 8) && cf) && input.equals(inputs[7])) && (a75.equals("e"))))))) && ((a395 && (!a132 && (24 == a370[2]))) && a302 == a346[0]))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	a168 -= (a168 - 20) < a168 ? 1 : 0;
    	cf = false;
    	if(a64) {
    	a75 = "g";
    	a333 = ((((20 / 5) + -801) + -12738) + 13663);
    	a395 = false;
    	a296 = a384;
    	a382 = (((((a382 - -5362) % 12) + -52) - -26377) + -26376);
    	a383 = a226[0];
    	a302 = a346[6];
    	a224 = "i";
    	a256 = "f";
    	a300 = false;
    	a202 = a217[7];
    	a207 = "h";
    	a320 = 9;
    	a260 = true;
    	a361 = "g";
    	a211 = 1;
    	a277 = (((((a277 % 14995) + -10) / 5) * 10) / 2);
    	a235 = a216[6];
    	a368 = false;
    	a398 = 11;
    	a336 = false;
    	a240 = false;
    	a307 = a227[3];
    	a329 = "h";
    	a378 = false;
    	a265 = a303;
    	a386 = (((((a386 % 61) + 138) * 5) % 61) + 115);
    	a81 = a167[(a230 + -1)];
    	a225 = a205[0];
    	a230 = 8;
    	a392 = a257;
    	a370 = a318;
    	a249 = (((((a249 + 0) + 0) - 0) % 101) + 253);
    	a276 = a289;
    	a234 = a372[4];
    	a158 = (a206 - -7);
    	a313 = "h";
    	a243 = (((((83 * 10) / -5) * 1) / 5) - 137);
    	a338 = 9;
    	a201 = (((((a201 + 16904) % 93) + -104) + 15673) - 15673);
    	a286 = a294[4];
    	a57 = (a270 - -2);
    	a373 = true;
    	a228 = a264;
    	a353 = a241;
    	a358 = a335;
    	a310 = (((((a310 / 5) + -3541) - 4829) % 77) + 270);
    	a312 = "i";
    	a339 = true;
    	a218 = "f";
    	a239 = a242;
    	a269 = "e";
    	a359 = 4;
    	a206 = 11;
    	a357 = (((a357 / 5) - 4255) / 5);
    	a237 = 3;
    	a324 = a232[6];
    	a270 = 13; 
    	}else {
    	a125 = a30[(a359 - -2)];
    	a173 = "f";
    	a75 = "f";
    	a81 = a167[a359];
    	}System.out.println("P");
    } 
    if(((((a269.equals("e")) && (a339 && (a230 == 3))) && a286 == a294[0]) && (((a211 == 1) && ((a77 == 8) && (input.equals(inputs[9]) && (!a132 && ((a75.equals("e")) && ((a44.equals("g")) && cf)))))) && a225 == a205[0]))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a333 = (((49 / 5) * 5) - -122);
    	a177 = "f";
    	a329 = "i";
    	a357 = (((((a357 % 65) + -89) / 5) - 11577) - -11521);
    	a207 = "e";
    	a302 = a346[1];
    	a132 = true;
    	a386 = ((((a386 % 14837) - -15162) * 1) - -1);
    	a276 = a289;
    	a234 = a372[5];
    	a237 = 7;
    	a300 = false;
    	a310 = (((((a310 % 14821) - -15178) - 9852) + 7889) + 1965);
    	a398 = 17;
    	a137 = a116; 
    	System.out.println("S");
    } 
    if((((((input.equals(inputs[1]) && (81 == a296[3])) && (67 == a358[2])) && !a132) && (a361.equals("e"))) && ((a77 == 8) && ((98 == a276[2]) && ((45 == a228[1]) && (a382 <=  -65 && (((a44.equals("g")) && cf) && (a75.equals("e"))))))))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a98 -= (a98 - 20) < a98 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a69 = "g";
    	a75 = "g";
    	a143 = "i";
    	a57 = (a77 + 3); 
    	System.out.println("S");
    } 
    if((((a235 == a216[0] && (((a44.equals("g")) && ((a338 == 3) && (a211 == 1))) && !a132)) && (37 == a392[3])) && ((a224.equals("e")) && (a395 && ((a75.equals("e")) && (input.equals(inputs[6]) && (cf && (a77 == 8)))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a122 -= (a122 - 20) < a122 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a75 = "h";
    	a134 = false;
    	a184 = a157[(a77 + -5)];
    	a295 = a366[(a77 - 4)]; 
    	System.out.println("T");
    } 
    if(((((98 == a276[2]) && ((((a75.equals("e")) && cf) && input.equals(inputs[2])) && (45 == a228[1]))) && a201 <=  -199) && (((((a386 <=  77 && (a77 == 8)) && !a132) && a324 == a232[0]) && (a44.equals("g"))) && (a256.equals("e"))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	if(cf) {
    	a295 = a366[((a211 + a206) - 1)];
    	a75 = "h";
    	a134 = false;
    	a184 = a157[((a77 / a77) + 2)]; 
    	}else {
    	a155 = "f";
    	a77 = (a359 - -4);
    	}System.out.println("R");
    } 
    if((((a270 == 10) && (a235 == a216[0] && ((a75.equals("e")) && (input.equals(inputs[5]) && ((a77 == 8) && (cf && !a132)))))) && (a235 == a216[0] && ((((a361.equals("e")) && a357 <=  -188) && (a44.equals("g"))) && (36 == a239[5]))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a234 = a372[1];
    	a324 = a232[1];
    	a310 = ((((a310 - 0) % 15080) - 14919) - 1);
    	a243 = ((((99 + -265) * 5) - -27325) - 26671);
    	a237 = 6;
    	a286 = a294[3];
    	a196 = ((a359 + a359) + 10);
    	a249 = ((((a249 * 1) % 14750) + 15249) - -1);
    	a398 = 11;
    	a313 = "f";
    	a270 = 16;
    	a370 = a318;
    	a382 = ((((a382 - -5487) - 2157) % 14911) - -15087);
    	a75 = "h";
    	a240 = false;
    	a302 = a346[5];
    	a207 = "e";
    	a383 = a226[3];
    	a300 = false;
    	a134 = false;
    	a373 = true;
    	a296 = a384;
    	a206 = 10;
    	a357 = (((((a357 + 0) * 1) * 1) * -2) / 10);
    	a329 = "i";
    	a307 = a227[6];
    	a230 = 7;
    	a235 = a216[4];
    	a295 = a366[(a196 - 16)]; 
    	System.out.println("X");
    } 
    if(((a310 <=  160 && (((3 == a353[2]) && a201 <=  -199) && (a230 == 3))) && (!a132 && ((a302 == a346[0] && ((a77 == 8) && (input.equals(inputs[8]) && ((a44.equals("g")) && ((a75.equals("e")) && cf))))) && a240)))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a155 = "f";
    	a77 = (a237 + 4); 
    	System.out.println("R");
    } 
    if(((((a359 == 3) && ((a211 == 1) && (a338 == 3))) && (a338 == 3)) && (((a270 == 10) && ((a77 == 8) && ((((a44.equals("g")) && (cf && !a132)) && a378) && input.equals(inputs[3])))) && (a75.equals("e"))))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a51 += (a51 + 20) > a51 ? 2 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a66 = false;
    	a173 = "e";
    	a75 = "f";
    	a130 = (((10 + -27931) + 28146) - 26); 
    	System.out.println("Y");
    } 
    if((((((a269.equals("e")) && ((cf && (a75.equals("e"))) && (a44.equals("g")))) && (a211 == 1)) && (a361.equals("e"))) && (((a338 == 3) && (input.equals(inputs[0]) && (((a77 == 8) && a249 <=  152) && !a132))) && a310 <=  160))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a63 -= (a63 - 20) < a63 ? 4 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	a77 = (a237 + 8);
    	a99 = a26[(a77 - 8)]; 
    	System.out.println("R");
    } 
}
private  void calculateOutputm30(String input) {
    if((((a320 == 6) && (((a270 == 10) && (3 == a353[2])) && a201 <=  -199)) && ((((a77 == 9) && (a339 && ((a207.equals("e")) && ((!a132 && cf) && input.equals(inputs[2]))))) && a16) && (a75.equals("e"))))) {
    	a164 += (a164 + 20) > a164 ? 3 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a75 = "h";
    	a134 = false;
    	a184 = a157[((a270 * a320) - 57)];
    	a295 = a366[a206]; 
    	System.out.println("Y");
    } 
    if(((((((a77 == 9) && cf) && (a75.equals("e"))) && a260) && a16) && (input.equals(inputs[0]) && ((!a132 && (a386 <=  77 && (a286 == a294[0] && ((a312.equals("e")) && a395)))) && a310 <=  160)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	cf = false;
    	a57 = ((a398 / a320) - -16);
    	a75 = "g";
    	a40 = a65;
    	a141 = a47[((a57 + a237) - 20)]; 
    	System.out.println("R");
    } 
    if((((a361.equals("e")) && ((a234 == a372[0] && (input.equals(inputs[8]) && ((cf && !a132) && (a77 == 9)))) && (a75.equals("e")))) && (a16 && (a357 <=  -188 && (a357 <=  -188 && (a234 == a372[0] && a202 == a217[0])))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a67 -= (a67 - 20) < a67 ? 4 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	cf = false;
    	a395 = true;
    	a234 = a372[5];
    	a313 = "i";
    	a184 = a157[(a211 + 5)];
    	a207 = "i";
    	a392 = a304;
    	a338 = 3;
    	a333 = ((((a333 + 18742) % 14911) + 15088) - -1);
    	a378 = false;
    	a302 = a346[2];
    	a269 = "i";
    	a141 = a47[(a77 + -6)];
    	a206 = 10;
    	a296 = a384;
    	a398 = 12;
    	a173 = "i";
    	a368 = true;
    	a230 = 7;
    	a75 = "f";
    	a224 = "g";
    	a260 = true;
    	a237 = 5;
    	a383 = a226[6];
    	a307 = a227[4];
    	a249 = ((((a249 % 15076) - 14923) - 2) - 0);
    	a201 = (((((a201 % 94) - -135) + -37) * 10) / 9);
    	a336 = true;
    	a276 = a253;
    	a228 = a264;
    	a300 = false;
    	a382 = (((60 + 16003) + -35870) / 5);
    	a320 = 13;
    	a373 = true;
    	a240 = true;
    	a353 = a263;
    	a357 = ((((((a357 * 1) % 38) - -4) * 5) % 38) + -17);
    	a270 = 12;
    	a286 = a294[0];
    	a310 = (((((a310 + 7283) / 5) / 5) % 20) + 337);
    	a277 = ((((12 * 10) / 9) + 13194) - 13208);
    	a370 = a311;
    	a243 = (((a243 - -10576) + 8625) + 10770);
    	a218 = "e";
    	a211 = 7; 
    	System.out.println("R");
    } 
    if(((((a206 == 4) && (((a249 <=  152 && input.equals(inputs[1])) && (a338 == 3)) && (a359 == 3))) && (24 == a370[2])) && ((24 == a370[2]) && ((a75.equals("e")) && (a16 && (((a77 == 9) && cf) && !a132)))))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a67 -= (a67 - 20) < a67 ? 4 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a57 = (a237 - -9);
    	a75 = "g";
    	a81 = a167[((a338 + a338) + -6)];
    	a99 = a26[(a57 + -10)]; 
    	System.out.println("R");
    } 
    if(((((a339 && ((((a224.equals("e")) && ((cf && !a132) && (a77 == 9))) && a336) && a243 <=  -179)) && (a75.equals("e"))) && input.equals(inputs[5])) && ((a218.equals("e")) && (a16 && (24 == a370[2]))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a196 = ((a320 / a230) + 8);
    	a134 = true;
    	a75 = "h";
    	a129 = a92[(a237 - -4)]; 
    	System.out.println("P");
    } 
    if(((a260 && ((((a75.equals("e")) && ((a77 == 9) && (cf && !a132))) && a16) && a307 == a227[0])) && (a201 <=  -199 && ((a395 && ((a211 == 1) && input.equals(inputs[9]))) && (a313.equals("e")))))) {
    	a164 += (a164 + 20) > a164 ? 3 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a206 = 4;
    	a361 = "i";
    	a75 = "g";
    	a338 = 7;
    	a307 = a227[3];
    	a224 = "h";
    	a357 = ((((a357 * -2) / 10) - -7884) * 2);
    	a353 = a241;
    	a57 = (a211 - -11);
    	a243 = ((((a243 % 11) + -165) + 1) * 1);
    	a218 = "h";
    	a249 = ((((a249 * 1) % 14750) - -15249) * 1);
    	a392 = a257;
    	a201 = (((((a201 % 14900) - 199) * 10) / 9) + -7917);
    	a378 = false;
    	a310 = ((((a310 % 15080) - 14919) + -1) + -1);
    	a230 = 10;
    	a81 = a167[((a57 / a77) - -1)];
    	a158 = (a398 + -2); 
    	System.out.println("S");
    } 
    if(((((a16 && (a357 <=  -188 && (a230 == 3))) && (a398 == 10)) && a249 <=  152) && ((a256.equals("e")) && ((((a75.equals("e")) && (((a77 == 9) && cf) && !a132)) && input.equals(inputs[7])) && a373)))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a75 = "g";
    	a81 = a167[(a211 - -3)];
    	a57 = (a206 + 8);
    	a179 = ((((60 - 19756) * 1) * 10) / 9); 
    	System.out.println("R");
    } 
    if(((((a207.equals("e")) && ((3 == a353[2]) && a16)) && (a206 == 4)) && (((a320 == 6) && (!a132 && ((a269.equals("e")) && (((a77 == 9) && (cf && input.equals(inputs[6]))) && (a75.equals("e")))))) && a234 == a372[0]))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	cf = false;
    	a353 = a399;
    	a368 = true;
    	a225 = a205[3];
    	a339 = true;
    	a312 = "e";
    	a398 = 10;
    	a129 = a92[(a77 + -8)];
    	a224 = "h";
    	a235 = a216[0];
    	a172 = (a359 - 2);
    	a313 = "i";
    	a260 = true;
    	a243 = ((((a243 * 1) + 15996) % 14910) + -15088);
    	a239 = a299;
    	a370 = a318;
    	a320 = 9;
    	a276 = a253;
    	a383 = a226[0];
    	a207 = "i";
    	a361 = "g";
    	a296 = a212;
    	a230 = 9;
    	a386 = ((((a386 * 1) % 61) + 263) + -1);
    	a357 = ((((a357 % 38) + -15) - 2) - 1);
    	a269 = "e";
    	a392 = a208;
    	a218 = "i";
    	a310 = (((((a310 * 1) + 14109) - -12457) % 14821) + 15178);
    	a333 = ((((a333 + 0) % 14976) + -47) - 14214);
    	a302 = a346[3];
    	a338 = 10;
    	a173 = "g";
    	a336 = true;
    	a307 = a227[5];
    	a237 = 8;
    	a395 = true;
    	a249 = (((((a249 / 5) - -1686) / 5) % 71) + 426);
    	a75 = "f";
    	a270 = 15;
    	a206 = 10;
    	a324 = a232[7];
    	a256 = "g";
    	a378 = true;
    	a240 = true;
    	a286 = a294[3];
    	a201 = ((((a201 + 11667) % 14900) + -15098) + 0);
    	a359 = 9; 
    	System.out.println("X");
    } 
    if(((((a225 == a205[0] && (a16 && (cf && (a77 == 9)))) && (a75.equals("e"))) && (a270 == 10)) && (a368 && ((!a132 && ((3 == a353[2]) && (a286 == a294[0] && input.equals(inputs[4])))) && (a270 == 10))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	a94 += (a94 + 20) > a94 ? 4 : 0;
    	cf = false;
    	a201 = (((((a201 % 14900) + -199) / 5) / 5) - 20677);
    	a286 = a294[5];
    	a312 = "i";
    	a333 = (((((a333 % 14911) + 15088) * 1) - 23363) - -34206);
    	a357 = ((((a357 % 38) + 18) - 20973) - -20949);
    	a243 = (((a243 / 5) - 6617) - 8977);
    	a218 = "h";
    	a234 = a372[3];
    	a224 = "h";
    	a270 = 15;
    	a256 = "h";
    	a383 = a226[5];
    	a339 = true;
    	a307 = a227[2];
    	a276 = a253;
    	a313 = "h";
    	a310 = ((((a310 % 15080) + -14919) * 1) + -1);
    	a329 = "h";
    	a358 = a348;
    	a237 = 9;
    	a206 = 10;
    	a228 = a229;
    	a378 = true;
    	a225 = a205[4];
    	a207 = "e";
    	a359 = 9;
    	a173 = "h";
    	a368 = true;
    	a361 = "g";
    	a239 = a242;
    	a373 = true;
    	a395 = true;
    	a338 = 10;
    	a260 = true;
    	a302 = a346[5];
    	a386 = ((((a386 - 0) % 14837) - -15162) * 1);
    	a296 = a212;
    	a75 = "f";
    	a91 = (a398 - -1);
    	a249 = ((((a249 * 1) + 18129) % 14750) - -15249);
    	a320 = 10;
    	a336 = true;
    	a235 = a216[5];
    	a1 = a87[((a230 - a91) - -14)];
    	a353 = a399;
    	a392 = a208;
    	a269 = "h";
    	a211 = 8;
    	a230 = 9;
    	a277 = (((70 + 23821) - -2533) + 1144);
    	a240 = true;
    	a370 = a311;
    	a202 = a217[7];
    	a398 = 10; 
    	System.out.println("R");
    } 
    if(((input.equals(inputs[3]) && ((a320 == 6) && (a339 && (81 == a296[3])))) && (((a338 == 3) && (((a225 == a205[0] && (!a132 && (cf && (a77 == 9)))) && (a75.equals("e"))) && a16)) && (a218.equals("e"))))) {
    	a120 -= (a120 - 20) < a120 ? 2 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a235 = a216[6];
    	a378 = true;
    	a75 = "f";
    	a173 = "e";
    	a269 = "h";
    	a276 = a253;
    	a206 = 6;
    	a392 = a304;
    	a359 = 10;
    	a313 = "e";
    	a310 = ((((a310 % 15080) - 14919) + -1) + -1);
    	a270 = 16;
    	a339 = true;
    	a240 = true;
    	a224 = "h";
    	a357 = (((a357 - -30106) + -7172) + 7218);
    	a225 = a205[6];
    	a191 = a37[(a77 - 4)];
    	a277 = (((64 + 9859) * 3) + 148);
    	a243 = (((a243 + 0) - -29967) - -55);
    	a239 = a242;
    	a130 = (((62 + -5887) - -763) / 5); 
    	System.out.println("T");
    } 
}
private  void calculateOutputm34(String input) {
    if(((a1 == a87[6] && ((a333 <=  -47 && (a313.equals("e"))) && (a359 == 3))) && (a235 == a216[0] && (a201 <=  -199 && (a373 && ((((a75.equals("e")) && (cf && input.equals(inputs[4]))) && (a77 == 10)) && !a132)))))) {
    	cf = false;
    	a239 = a268;
    	a230 = (a338 - 1);
    	a269 = "g";
    	a276 = a250;
    	a154 = ((a320 * a237) + -3);
    	a395 = false;
    	a370 = a285;
    	a392 = a257;
    	a218 = "g";
    	a57 = (a154 - 2);
    	a359 = ((a77 - a77) + 5);
    	a228 = a292;
    	a75 = "g";
    	a201 = (((((((a201 * a310) % 14999) % 93) - 104) + -22040) * 1) + 22039);
    	a137 = a189;
    	a260 = false;
    	a277 = (((((((a277 * a386) % 14999) % 78) + 70) / 5) + -15866) - -15901);
    	a310 = (((((((a310 * a382) % 14999) % 77) + 238) / 5) - -28276) + -28041);
    	a353 = a399;
    	a333 = (((((((a333 * a357) % 14999) + -10066) % 14) + 163) + -26196) + 26195); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm36(String input) {
    if((((((a237 == 3) && ((a361.equals("e")) && ((a77 == 11) && (cf && (a75.equals("e")))))) && input.equals(inputs[4])) && (47 == a265[5])) && ((a99 == a26[7] && (a368 && ((a256.equals("e")) && !a132))) && (67 == a358[2])))) {
    	a169 -= (a169 - 20) < a169 ? 2 : 0;
    	cf = false;
    	a57 = ((a320 / a270) - -13);
    	a359 = ((a270 + a237) - 8);
    	a300 = true;
    	a368 = true;
    	a353 = a399;
    	a307 = a227[(a77 - 9)];
    	a276 = a250;
    	a310 = ((((((((a310 * a243) % 14999) % 77) - -238) + -1) / 5) * 51) / 10);
    	a338 = (a230 + 2);
    	a270 = (a338 + 6);
    	a154 = ((a211 + a57) - -1);
    	a75 = "g";
    	a260 = false;
    	a224 = "f";
    	a137 = a189;
    	a211 = a230;
    	a239 = a268;
    	a218 = "g";
    	a269 = "g";
    	a228 = a292;
    	a395 = false;
    	a312 = "g";
    	a370 = a285;
    	a240 = true;
    	a230 = (a359 - 1); 
    	System.out.println("X");
    } 
    if((((a395 && ((((a75.equals("e")) && cf) && input.equals(inputs[8])) && a99 == a26[7])) && a235 == a216[0]) && (((a300 && (a202 == a217[0] && (a307 == a227[0] && !a132))) && (a77 == 11)) && a286 == a294[0]))) {
    	cf = false;
    	if((!a300 || ((23 == a274[2]) && a184 == a157[0]))) {
    	a368 = true;
    	a307 = a227[((a230 * a338) + -7)];
    	a353 = a399;
    	a230 = (a237 - -1);
    	a312 = "g";
    	a228 = a292;
    	a395 = false;
    	a239 = a268;
    	a260 = false;
    	a300 = true;
    	a137 = a189;
    	a218 = "g";
    	a240 = true;
    	a154 = (a359 - -12);
    	a276 = a250;
    	a211 = (a270 - 7);
    	a57 = ((a338 - a359) - -13);
    	a370 = a285;
    	a338 = ((a270 * a270) - 95);
    	a310 = (((((((a310 * a382) % 14999) / 5) * 5) * 2) % 77) + 239);
    	a75 = "g";
    	a224 = "f";
    	a269 = "g";
    	a359 = ((a270 - a320) + 1);
    	a270 = a77; 
    	}else {
    	a260 = true;
    	a75 = "g";
    	a358 = a335;
    	a398 = ((a338 + a359) - -6);
    	a339 = true;
    	a361 = "g";
    	a240 = true;
    	a270 = ((a359 * a237) + 3);
    	a378 = true;
    	a300 = true;
    	a265 = a376;
    	a224 = "f";
    	a320 = ((a77 + a211) - 4);
    	a234 = a372[((a359 * a237) + -7)];
    	a143 = "g";
    	a230 = (a359 + 2);
    	a370 = a285;
    	a296 = a212;
    	a201 = ((((((a382 * a382) % 14999) * 2) % 94) + 82) + 2);
    	a307 = a227[((a359 * a270) + -34)];
    	a202 = a217[((a270 + a359) + -13)];
    	a57 = ((a359 + a359) + 5);
    	a173 = "e";
    	a373 = false;
    	a218 = "g";
    	a329 = "f";
    	a353 = a399;
    	a395 = false;
    	a310 = ((((((a310 * a357) % 14999) / 5) * 5) % 77) + 239);
    	}System.out.println("X");
    } 
}
private  void calculateOutputm2(String input) {
    if(((a260 && (a373 && a378)) && (a339 && (((cf && (a77 == 8)) && (a270 == 10)) && (a211 == 1))))) {
    	if((((a44.equals("g")) && cf) && ((a237 == 3) && ((((a260 && (a313.equals("e"))) && (a211 == 1)) && a225 == a205[0]) && (47 == a265[5]))))) {
    		calculateOutputm29(input);
    	} 
    } 
    if((((a338 == 3) && ((a77 == 9) && cf)) && (((((a207.equals("e")) && a333 <=  -47) && a386 <=  77) && a383 == a226[0]) && (3 == a353[2])))) {
    	if((((a218.equals("e")) && ((24 == a370[2]) && (37 == a392[3]))) && ((67 == a358[2]) && ((a260 && (cf && a16)) && (a207.equals("e")))))) {
    		calculateOutputm30(input);
    	} 
    } 
    if(((((a359 == 3) && (((a77 == 10) && cf) && a395)) && (a218.equals("e"))) && (a277 <=  -10 && (a277 <=  -10 && a234 == a372[0])))) {
    	if(((a378 && (a1 == a87[6] && cf)) && (a373 && ((a395 && ((a218.equals("e")) && a225 == a205[0])) && (37 == a392[3]))))) {
    		calculateOutputm34(input);
    	} 
    } 
    if(((a243 <=  -179 && ((a320 == 6) && (a211 == 1))) && ((a224.equals("e")) && (a368 && (((a77 == 11) && cf) && (a359 == 3)))))) {
    	if(((a240 && (a307 == a227[0] && (a300 && ((a99 == a26[7] && cf) && a378)))) && (a249 <=  152 && (a312.equals("e"))))) {
    		calculateOutputm36(input);
    	} 
    } 
}
private  void calculateOutputm40(String input) {
    if(((a235 == a216[1] && (((input.equals(inputs[6]) && (a191 == a37[6] && (cf && (a173.equals("e"))))) && !a260) && (a207.equals("f")))) && ((((((77 < a386) && (201 >= a386)) && a130 <=  134) && (a75.equals("f"))) && (a218.equals("f"))) && ((77 < a386) && (201 >= a386))))) {
    	a122 -= (a122 - 20) < a122 ? 3 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a75 = "g";
    	a141 = a47[(a211 - 2)];
    	a57 = ((a320 - a230) + 14);
    	a40 = a65; 
    	System.out.println("P");
    } 
    if(((((77 < a386) && (201 >= a386)) && ((a359 == 4) && (!a240 && (a312.equals("f"))))) && (a286 == a294[1] && ((a75.equals("f")) && ((a173.equals("e")) && (input.equals(inputs[9]) && (((a130 <=  134 && cf) && a191 == a37[6]) && a235 == a216[1]))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a67 += (a67 + 20) > a67 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a173 = "f";
    	a397 = (a230 - -7);
    	a125 = a30[((a397 / a397) + 2)]; 
    	System.out.println("X");
    } 
    if(((((a218.equals("f")) && ((a269.equals("f")) && (a173.equals("e")))) && !a395) && ((a312.equals("f")) && (((76 == a358[5]) && (((-65 < a382) && (-39 >= a382)) && ((a75.equals("f")) && ((cf && a130 <=  134) && a191 == a37[6])))) && input.equals(inputs[3]))))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a169 -= (a169 - 20) < a169 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	a66 = false;
    	a130 = ((((a130 / 5) + -12683) % 70) - -239); 
    	System.out.println("S");
    } 
    if(((a270 == 11) && (input.equals(inputs[0]) && ((((-65 < a382) && (-39 >= a382)) && (((((((cf && a191 == a37[6]) && a130 <=  134) && (a173.equals("e"))) && a383 == a226[1]) && a307 == a227[1]) && (a75.equals("f"))) && ((-179 < a243) && (-156 >= a243)))) && ((77 < a386) && (201 >= a386)))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	a173 = "i";
    	a141 = a47[(a230 + -1)];
    	a184 = a157[2]; 
    	System.out.println("T");
    } 
    if((((!a378 && ((a130 <=  134 && ((a320 == 7) && (a338 == 4))) && ((-188 < a357) && (-57 >= a357)))) && input.equals(inputs[5])) && ((((a173.equals("e")) && ((a191 == a37[6] && cf) && (a75.equals("f")))) && (89 == a296[5])) && (a211 == 2)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a31 -= (a31 - 20) < a31 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a218 = "h";
    	a132 = false;
    	a228 = a292;
    	a243 = (((((a243 % 11) - 159) + -4) + -16589) + 16595);
    	a75 = "e";
    	a211 = 8;
    	a224 = "f";
    	a230 = 9;
    	a239 = a268;
    	a155 = "h";
    	a339 = false;
    	a302 = a346[4];
    	a310 = (((((a310 * 5) + -28155) + -936) * -1) / 10);
    	a240 = false;
    	a269 = "h";
    	a260 = false;
    	a368 = false;
    	a370 = a285;
    	a358 = a335;
    	a77 = (a270 - 4); 
    	System.out.println("V");
    } 
    if((((a359 == 4) && (((a218.equals("f")) && !a339) && (a338 == 4))) && (((a270 == 11) && (a191 == a37[6] && ((a75.equals("f")) && ((a130 <=  134 && (input.equals(inputs[2]) && cf)) && (a173.equals("e")))))) && (40 == a239[3])))) {
    	a164 += (a164 + 20) > a164 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a307 = a227[1];
    	a228 = a292;
    	a353 = a241;
    	a172 = ((a211 - a237) + 9);
    	a358 = a335;
    	a329 = "i";
    	a173 = "g";
    	a225 = a205[2];
    	a99 = a26[(a206 - 3)]; 
    	System.out.println("Z");
    } 
    if(((((-10 < a277) && (148 >= a277)) && ((28 == a370[0]) && (a234 == a372[1] && a191 == a37[6]))) && (((a237 == 4) && ((a173.equals("e")) && ((a75.equals("f")) && ((a130 <=  134 && (cf && input.equals(inputs[1]))) && a302 == a346[1])))) && ((-65 < a382) && (-39 >= a382))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a5 -= (a5 - 20) < a5 ? 3 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a221 = true;
    	a373 = true;
    	a329 = "g";
    	a173 = "i";
    	a333 = ((((84 / 5) - 5769) * 10) / -9);
    	a353 = a263;
    	a386 = ((((a386 % 61) + 85) - 2) - -6);
    	a211 = 5;
    	a237 = 4;
    	a256 = "i";
    	a207 = "h";
    	a276 = a253;
    	a141 = a47[((a359 / a270) + 4)]; 
    	System.out.println("Z");
    } 
    if((((a191 == a37[6] && ((((a218.equals("f")) && (89 == a296[5])) && (a173.equals("e"))) && (a75.equals("f")))) && !a368) && (a286 == a294[1] && (((a230 == 4) && ((cf && input.equals(inputs[8])) && a130 <=  134)) && a234 == a372[1])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a63 -= (a63 - 20) < a63 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	if((a1 == a87[5] || !(a338 == 9))) {
    	a107 = "g";
    	a173 = "g";
    	a172 = ((a230 * a206) + -12); 
    	}else {
    	a75 = "g";
    	a57 = 17;
    	a40 = a83;
    	a25 = a68;
    	}System.out.println("Z");
    } 
    if((((((((a270 == 11) && a191 == a37[6]) && (a75.equals("f"))) && !a339) && (a237 == 4)) && a286 == a294[1]) && (((-65 < a382) && (-39 >= a382)) && ((a130 <=  134 && ((a173.equals("e")) && (cf && input.equals(inputs[4])))) && !a368)))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a98 -= (a98 - 20) < a98 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	a173 = "g";
    	a373 = true;
    	a276 = a253;
    	a234 = a372[6];
    	a239 = a242;
    	a211 = 3;
    	a99 = a26[((a206 * a359) + -19)];
    	a324 = a232[3];
    	a336 = true;
    	a358 = a351;
    	a228 = a292;
    	a277 = (((a277 - -3539) - -5461) - -14502);
    	a329 = "i";
    	a353 = a399;
    	a383 = a226[3];
    	a172 = ((a359 - a206) - -8); 
    	System.out.println("S");
    } 
    if(((((a307 == a227[1] && (((a173.equals("e")) && cf) && input.equals(inputs[7]))) && a235 == a216[1]) && (50 == a228[0])) && ((((a75.equals("f")) && ((a324 == a232[1] && ((-10 < a277) && (148 >= a277))) && a130 <=  134)) && a225 == a205[1]) && a191 == a37[6]))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	a132 = false;
    	a77 = ((a270 + a359) + -3);
    	a75 = "e";
    	a129 = a92[(a77 - 9)]; 
    	System.out.println("R");
    } 
}
private  void calculateOutputm44(String input) {
    if((((((a359 == 4) && (((a173.equals("e")) && cf) && ((203 < a13) && (325 >= a13)))) && ((77 < a386) && (201 >= a386))) && (a230 == 4)) && (a235 == a216[1] && ((input.equals(inputs[1]) && (((a75.equals("f")) && (a398 == 11)) && 493 < a130)) && a235 == a216[1])))) {
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a31 += (a31 + 20) > a31 ? 3 : 0;
    	cf = false;
    	a75 = "h";
    	a196 = ((a270 / a206) + 9);
    	a134 = false;
    	a295 = a366[((a196 / a196) - 1)]; 
    	System.out.println("T");
    } 
    if(((((((-179 < a243) && (-156 >= a243)) && (((40 == a239[3]) && ((a313.equals("f")) && ((77 < a386) && (201 >= a386)))) && ((203 < a13) && (325 >= a13)))) && (a207.equals("f"))) && (a173.equals("e"))) && ((a398 == 11) && (((cf && input.equals(inputs[9])) && (a75.equals("f"))) && 493 < a130)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if(((60 == a3[3]) || ((a171.equals("g")) || (a184 == a157[4] && ((a136 == 7) || a141 == a47[2]))))) {
    	a302 = a346[3];
    	a202 = a217[4];
    	a382 = ((((75 - 116) - 26037) / 5) + 5156);
    	a269 = "i";
    	a57 = ((a230 - a230) + 10);
    	a357 = (((((a357 % 65) + -63) * 1) * 10) / 9);
    	a207 = "f";
    	a286 = a294[4];
    	a338 = 6;
    	a312 = "g";
    	a310 = ((((a310 + 23056) - 37547) * 10) / -9);
    	a201 = (((((73 * 10) / 9) / 5) + -29551) + 29524);
    	a234 = a372[1];
    	a339 = true;
    	a277 = (((a277 - -15514) + 1898) * 1);
    	a235 = a216[2];
    	a270 = 10;
    	a218 = "h";
    	a230 = 9;
    	a240 = false;
    	a155 = "i";
    	a249 = ((((a249 * -5) * 5) + 6916) + -17975);
    	a368 = true;
    	a276 = a253;
    	a324 = a232[5];
    	a358 = a351;
    	a320 = 9;
    	a296 = a362;
    	a206 = 8;
    	a398 = 16;
    	a300 = false;
    	a260 = false;
    	a307 = a227[5];
    	a211 = 6;
    	a224 = "h";
    	a313 = "h";
    	a243 = (((a243 + -4607) * 5) * 1);
    	a333 = ((((92 - -3238) / 5) - -2348) + -2854);
    	a336 = true;
    	a370 = a318;
    	a378 = true;
    	a239 = a299;
    	a329 = "e";
    	a361 = "g";
    	a353 = a241;
    	a392 = a208;
    	a386 = ((((a386 - 15631) * 10) / 9) - 12574);
    	a228 = a264;
    	a265 = a293;
    	a359 = 7;
    	a237 = 10;
    	a75 = "g";
    	a373 = true;
    	a158 = (a57 - -1); 
    	}else {
    	a234 = a372[0];
    	a256 = "e";
    	a353 = a263;
    	a320 = 10;
    	a395 = true;
    	a172 = ((a359 - a359) - -1);
    	a173 = "g";
    	a336 = true;
    	a228 = a264;
    	a277 = (((a277 / 5) - -29824) + -29794);
    	a333 = ((((76 + -24159) * -1) / 10) - -1271);
    	a225 = a205[5];
    	a129 = a92[(a206 - 3)];
    	}System.out.println("T");
    } 
    if((((a173.equals("e")) && ((a269.equals("f")) && ((493 < a130 && ((a75.equals("f")) && ((((203 < a13) && (325 >= a13)) && cf) && input.equals(inputs[2])))) && !a260))) && ((a312.equals("f")) && ((a237 == 4) && (!a339 && !a378))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a239 = a299;
    	a358 = a351;
    	a75 = "h";
    	a296 = a384;
    	a398 = 13;
    	a392 = a257;
    	a359 = 4;
    	a134 = false;
    	a218 = "f";
    	a211 = 8;
    	a224 = "f";
    	a339 = false;
    	a202 = a217[1];
    	a373 = false;
    	a361 = "g";
    	a228 = a264;
    	a234 = a372[1];
    	a53 = ((((((a130 * a130) % 14999) / 5) / 5) % 59) - -14);
    	a269 = "i";
    	a276 = a289;
    	a295 = a366[5]; 
    	System.out.println("T");
    } 
    if((((a313.equals("f")) && (((((-188 < a357) && (-57 >= a357)) && (!a300 && (a361.equals("f")))) && (a398 == 11)) && (a75.equals("f")))) && (((203 < a13) && (325 >= a13)) && ((!a373 && (493 < a130 && (cf && input.equals(inputs[3])))) && (a173.equals("e")))))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a174 += (a174 + 20) > a174 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	if((59 == a228[3])) {
    	a276 = a250;
    	a234 = a372[6];
    	a359 = 4;
    	a361 = "f";
    	a296 = a384;
    	a392 = a304;
    	a134 = false;
    	a269 = "i";
    	a224 = "h";
    	a339 = false;
    	a228 = a292;
    	a75 = "h";
    	a218 = "i";
    	a358 = a351;
    	a295 = a366[(a338 + 1)];
    	a398 = 14;
    	a239 = a268;
    	a202 = a217[1];
    	a373 = false;
    	a211 = 4;
    	a53 = ((((((a310 * a13) % 14999) % 59) - 44) / 5) / 5); 
    	}else {
    	a386 = ((((a386 * 5) % 61) + 258) + 4);
    	a313 = "e";
    	a296 = a384;
    	a218 = "i";
    	a237 = 7;
    	a378 = false;
    	a336 = false;
    	a243 = (((a243 / 5) * 5) + 44);
    	a202 = a217[1];
    	a240 = false;
    	a207 = "f";
    	a370 = a311;
    	a329 = "e";
    	a201 = (((((64 * 28) / 10) * 5) * 10) / 9);
    	a260 = false;
    	a320 = 13;
    	a359 = 4;
    	a269 = "g";
    	a382 = (((90 - -11265) + -38923) * 1);
    	a368 = false;
    	a75 = "g";
    	a230 = 10;
    	a270 = 11;
    	a324 = a232[3];
    	a302 = a346[5];
    	a228 = a292;
    	a206 = 7;
    	a353 = a263;
    	a239 = a268;
    	a307 = a227[3];
    	a105 = "g";
    	a277 = ((((a277 - -4988) + -20341) % 78) + 79);
    	a338 = 4;
    	a310 = ((((a310 - -17283) + 4067) % 77) + 231);
    	a358 = a351;
    	a392 = a304;
    	a361 = "i";
    	a312 = "i";
    	a339 = true;
    	a398 = 15;
    	a276 = a289;
    	a235 = a216[2];
    	a357 = ((((a357 - 13003) + 149) * 10) / 9);
    	a211 = 8;
    	a373 = false;
    	a57 = 10;
    	a158 = (a57 + -4);
    	}System.out.println("X");
    } 
    if(((((a173.equals("e")) && (!a368 && ((a359 == 4) && a324 == a232[1]))) && ((203 < a13) && (325 >= a13))) && ((((11 == a353[4]) && ((a75.equals("f")) && ((input.equals(inputs[8]) && cf) && 493 < a130))) && !a378) && ((160 < a310) && (316 >= a310))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a5 -= (a5 - 20) < a5 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a278 = a326[7];
    	a57 = (a206 - -11);
    	a75 = "g";
    	a81 = a167[((a57 / a57) - -4)]; 
    	System.out.println("T");
    } 
    if(((((((203 < a13) && (325 >= a13)) && (!a373 && 493 < a130)) && (28 == a370[0])) && (a329.equals("f"))) && ((89 == a296[5]) && (((a329.equals("f")) && (input.equals(inputs[5]) && (((a75.equals("f")) && cf) && (a173.equals("e"))))) && ((160 < a310) && (316 >= a310)))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a162 -= (a162 - 20) < a162 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	if((!(a154 == 12) || ((201 < a386) && (325 >= a386)))) {
    	a324 = a232[7];
    	a228 = a229;
    	a237 = 7;
    	a357 = ((((a357 % 65) - 93) + -7) + -21);
    	a392 = a208;
    	a202 = a217[7];
    	a207 = "h";
    	a81 = a167[((a338 + a359) + -6)];
    	a206 = 9;
    	a249 = ((((((a249 * 5) % 101) - -243) / 5) * 39) / 10);
    	a265 = a293;
    	a234 = a372[6];
    	a218 = "h";
    	a158 = a270;
    	a201 = (((((36 * 10) / -1) * 5) + 3003) - 15794);
    	a269 = "i";
    	a382 = (((((91 + 23090) * -1) / 10) + 14454) - 27585);
    	a243 = (((((a243 + 9948) * 10) / 9) * 10) / 9);
    	a359 = 7;
    	a225 = a205[7];
    	a333 = ((((65 + 8843) * 10) / 9) + 8996);
    	a386 = (((a386 / 5) - 7064) * 4);
    	a270 = 14;
    	a310 = ((((a310 * 5) * 5) / 5) * -5);
    	a240 = false;
    	a373 = true;
    	a312 = "h";
    	a320 = 13;
    	a286 = a294[6];
    	a370 = a318;
    	a302 = a346[1];
    	a368 = false;
    	a296 = a384;
    	a239 = a242;
    	a230 = 10;
    	a57 = (a158 + 1);
    	a339 = true;
    	a277 = ((((a277 - -5007) * 10) / 9) * 5);
    	a336 = false;
    	a307 = a227[4];
    	a398 = 10;
    	a300 = false;
    	a378 = false;
    	a224 = "e";
    	a75 = "g";
    	a353 = a263;
    	a211 = 7;
    	a235 = a216[1];
    	a329 = "f";
    	a276 = a289;
    	a338 = 3; 
    	}else {
    	a143 = "h";
    	a210 = true;
    	a75 = "g";
    	a57 = 11;
    	}System.out.println("S");
    } 
    if(((493 < a130 && !a300) && ((((53 == a265[5]) && (input.equals(inputs[0]) && ((((a75.equals("f")) && (((203 < a13) && (325 >= a13)) && ((a173.equals("e")) && cf))) && (76 == a358[5])) && (a207.equals("f"))))) && a286 == a294[1]) && a286 == a294[1]))) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	if(a368) {
    	a57 = (a359 - -11);
    	a141 = a47[((a57 - a270) + -4)];
    	a75 = "g";
    	a72 = (((((a130 * a13) % 14999) + -25309) - 2308) - 1175); 
    	}else {
    	a378 = false;
    	a240 = false;
    	a235 = a216[3];
    	a329 = "i";
    	a230 = 7;
    	a206 = 7;
    	a357 = ((((a357 * -4) / 10) - -15960) + 8701);
    	a277 = (((a277 + 28572) - 27728) + 20150);
    	a202 = a217[7];
    	a398 = 13;
    	a218 = "g";
    	a146 = false;
    	a302 = a346[7];
    	a225 = a205[3];
    	a270 = 15;
    	a320 = 11;
    	a234 = a372[7];
    	a361 = "h";
    	a249 = (((a249 * -5) - -6105) + -16926);
    	a243 = (((a243 * -5) + 28522) + 392);
    	a260 = false;
    	a324 = a232[5];
    	a224 = "h";
    	a276 = a253;
    	a395 = false;
    	a312 = "h";
    	a383 = a226[4];
    	a228 = a292;
    	a286 = a294[4];
    	a353 = a241;
    	a368 = false;
    	a256 = "i";
    	a239 = a299;
    	a359 = 6;
    	a386 = (((a386 + 2558) + 11847) / 5);
    	a75 = "i";
    	a300 = false;
    	a237 = 6;
    	a373 = false;
    	a338 = 5;
    	a265 = a293;
    	a16 = true;
    	a207 = "h";
    	a358 = a335;
    	a296 = a362;
    	a339 = false;
    	a73 = (((((((a130 * a13) % 14999) % 54) + 214) * 5) % 54) + 172);
    	}System.out.println("R");
    } 
    if(((((((103 == a276[1]) && input.equals(inputs[6])) && 493 < a130) && ((-188 < a357) && (-57 >= a357))) && (a206 == 5)) && ((((((((203 < a13) && (325 >= a13)) && cf) && (a173.equals("e"))) && a202 == a217[1]) && !a378) && (a75.equals("f"))) && a202 == a217[1]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a174 -= (a174 - 20) < a174 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a35 += (a35 + 20) > a35 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a333 = ((((16 * 10) / -3) + -7855) + -12401);
    	a173 = "h";
    	a91 = ((a230 + a359) + 3);
    	a383 = a226[0];
    	a243 = (((((a243 * 10) / 8) * 5) - -6523) - 10112);
    	a296 = a212;
    	a207 = "f";
    	a395 = true;
    	a235 = a216[0];
    	a336 = true;
    	a320 = 6;
    	a312 = "h";
    	a286 = a294[6];
    	a329 = "i";
    	a338 = 4;
    	a1 = a87[(a206 + 1)]; 
    	System.out.println("R");
    } 
    if((((103 == a276[1]) && (((152 < a249) && (355 >= a249)) && (((203 < a13) && (325 >= a13)) && ((a173.equals("e")) && (a269.equals("f")))))) && ((a307 == a227[1] && ((((-188 < a357) && (-57 >= a357)) && ((cf && input.equals(inputs[7])) && (a75.equals("f")))) && 493 < a130)) && (a207.equals("f"))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a51 += (a51 + 20) > a51 ? 3 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	if(((!(a184 == a157[7]) || ((-156 < a243) && (-68 >= a243))) && !(a200 == 8))) {
    	a173 = "f";
    	a81 = a167[(a270 + -11)];
    	a125 = a30[(a338 + 1)]; 
    	}else {
    	a91 = (a237 - -9);
    	a173 = "h";
    	a195 = a58;
    	}System.out.println("R");
    } 
    if((((!a378 && (input.equals(inputs[4]) && ((!a300 && (a361.equals("f"))) && a235 == a216[1]))) && (a398 == 11)) && (((11 == a353[4]) && ((a173.equals("e")) && ((cf && ((203 < a13) && (325 >= a13))) && (a75.equals("f"))))) && 493 < a130))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a162 += (a162 + 20) > a162 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	cf = false;
    	if(((a278 == 5) || ((((233 < a17) && (414 >= a17)) || a71 == a175[6]) && !(31 == a137[0])))) {
    	a81 = a167[(a338 + -4)];
    	a75 = "g";
    	a57 = (a206 - -7);
    	a99 = a26[(a57 + -11)]; 
    	}else {
    	a77 = (a270 - 6);
    	a132 = false;
    	a75 = "e";
    	a136 = ((a211 * a211) - -8);
    	}System.out.println("X");
    } 
}
private  void calculateOutputm3(String input) {
    if((((((a224.equals("f")) && ((43 == a392[3]) && (a206 == 5))) && a225 == a205[1]) && (a206 == 5)) && ((40 == a239[3]) && (cf && a130 <=  134)))) {
    	if((((a320 == 7) && (a235 == a216[1] && !a368)) && (!a368 && ((a286 == a294[1] && (a191 == a37[6] && cf)) && a302 == a346[1])))) {
    		calculateOutputm40(input);
    	} 
    } 
    if((((a224.equals("f")) && ((493 < a130 && cf) && (a270 == 11))) && ((a224.equals("f")) && (!a378 && ((40 == a239[3]) && (a218.equals("f"))))))) {
    	if(((((-188 < a357) && (-57 >= a357)) && (cf && ((203 < a13) && (325 >= a13)))) && (a234 == a372[1] && ((11 == a353[4]) && ((a235 == a216[1] && ((152 < a249) && (355 >= a249))) && (11 == a353[4])))))) {
    		calculateOutputm44(input);
    	} 
    } 
}
private  void calculateOutputm46(String input) {
    if((((a286 == a294[1] && (a202 == a217[1] && ((a173.equals("f")) && ((cf && 402 < a54) && input.equals(inputs[2]))))) && ((a398 == 11) && (!a378 && ((a75.equals("f")) && (((a329.equals("f")) && ((-47 < a333) && (147 >= a333))) && a125 == a30[0]))))) && a169 >= 2)) {
    	a164 += (a164 + 20) > a164 ? 1 : 0;
    	cf = false;
    	a211 = (a359 - 1);
    	a276 = a250;
    	a57 = (a320 + 10);
    	a75 = "g";
    	a40 = a83;
    	a382 = ((((((a382 * a201) % 107) - -7) * 10) / 9) + -21);
    	a357 = ((((((a357 * a310) % 14999) + 27436) * 1) % 38) - 32);
    	a218 = "g";
    	a202 = a217[(a398 + -9)];
    	a307 = a227[(a338 + -2)];
    	a336 = true;
    	a378 = true;
    	a320 = (a237 - -4);
    	a386 = ((((((a386 * a333) / 5) / 5) + 11356) % 61) - -256);
    	a302 = a346[(a270 - 9)];
    	a239 = a268;
    	a269 = "g";
    	a240 = true;
    	a265 = a376;
    	a333 = (((((((a333 * a277) % 14999) * 2) / 5) / 5) % 14) - -161);
    	a358 = a335;
    	a25 = a76; 
    	System.out.println("R");
    } 
    if(((((!a339 && ((a237 == 4) && ((a125 == a30[0] && ((a338 == 4) && (a338 == 4))) && (a75.equals("f"))))) && (76 == a358[5])) && ((a270 == 11) && ((((a173.equals("f")) && cf) && 402 < a54) && input.equals(inputs[6])))) && a67 >= -2)) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	cf = false;
    	a201 = ((((((a201 * a386) % 14999) % 94) - -83) + 19492) + -19491);
    	a313 = "g";
    	a382 = ((((((a382 * a201) * 2) % 107) + 68) - -6308) + -6306);
    	a240 = true;
    	a75 = "g";
    	a69 = "f";
    	a357 = (((((((a357 * a243) % 14999) % 38) - 25) + -17) * 9) / 10);
    	a224 = "g";
    	a57 = 11;
    	a378 = true;
    	a143 = "i";
    	a202 = a217[((a237 * a270) - 42)];
    	a218 = "g";
    	a368 = true;
    	a358 = a335;
    	a265 = a376;
    	a230 = (a398 + -6);
    	a307 = a227[a211];
    	a333 = ((((((a333 * a201) / 5) / 5) / 5) % 14) - -163);
    	a386 = (((((((a386 * a54) % 14999) / 5) % 61) + 252) - -10419) + -10466); 
    	System.out.println("S");
    } 
    if((((!a395 && ((a313.equals("f")) && (a125 == a30[0] && ((((402 < a54 && cf) && input.equals(inputs[0])) && (43 == a392[3])) && a307 == a227[1])))) && ((89 == a296[5]) && ((a75.equals("f")) && ((a173.equals("f")) && (a270 == 11))))) && a50 == 82)) {
    	a5 -= (a5 - 20) < a5 ? 2 : 0;
    	cf = false;
    	a107 = "g";
    	a173 = "g";
    	a172 = 8; 
    	System.out.println("V");
    } 
    if((((a230 == 4) && ((a206 == 5) && (a202 == a217[1] && ((402 < a54 && (cf && (a173.equals("f")))) && (a75.equals("f")))))) && ((!a339 && (!a300 && (input.equals(inputs[4]) && (a329.equals("f"))))) && a125 == a30[0]))) {
    	a161 -= (a161 - 20) < a161 ? 4 : 0;
    	cf = false;
    	a361 = "f";
    	a395 = false;
    	a249 = ((((((((a277 * a386) % 14999) + 1596) % 71) - -372) - 96) * 13) / 10);
    	a373 = false;
    	a240 = true;
    	a260 = false;
    	a81 = a167[(a237 + -2)];
    	a206 = ((a270 - a270) - -6);
    	a243 = (((((((a249 * a201) % 14999) % 43) + -112) + 11002) + 15016) - 26018);
    	a358 = a335;
    	a158 = ((a211 + a359) + 3);
    	a368 = true;
    	a336 = true;
    	a269 = "g";
    	a386 = ((((((a277 * a249) % 14999) - -5707) * 1) % 61) + 81);
    	a300 = false;
    	a211 = ((a338 + a270) - 14);
    	a359 = (a206 - 3);
    	a392 = a304;
    	a225 = a205[(a270 - 10)];
    	a296 = a384;
    	a329 = "f";
    	a202 = a217[(a270 + -9)];
    	a312 = "f";
    	a57 = (a158 + 3);
    	a339 = true;
    	a207 = "e";
    	a276 = a253;
    	a357 = (((((a357 * a243) % 38) - 25) - 16009) + 16002);
    	a333 = ((((((a333 * a382) * 3) % 14) - -161) + -4505) + 4507);
    	a382 = ((((((a333 * a243) % 12) - 51) / 5) * 49) / 10);
    	a378 = true;
    	a370 = a311;
    	a235 = a216[(a338 + -2)];
    	a224 = "g";
    	a398 = (a338 - -8);
    	a313 = "f";
    	a320 = a206;
    	a234 = a372[(a206 - 4)];
    	a302 = a346[((a270 - a206) + -3)];
    	a218 = "g";
    	a239 = a242;
    	a310 = ((((((a310 * a201) % 14999) % 20) + 336) * 1) + 0);
    	a75 = "g";
    	a230 = ((a270 / a206) - -4);
    	a237 = (a338 + 1);
    	a201 = ((((((a243 * a333) / 5) % 93) + -72) * 10) / 9);
    	a256 = "f";
    	a307 = a227[(a338 - 2)];
    	a324 = a232[(a270 + -11)];
    	a265 = a293;
    	a286 = a294[(a57 + -11)];
    	a338 = ((a270 / a398) - -3);
    	a270 = (a158 - -2); 
    	System.out.println("R");
    } 
    if(((((((a173.equals("f")) && a202 == a217[1]) && (a75.equals("f"))) && ((-179 < a243) && (-156 >= a243))) && (a237 == 4)) && (!a395 && ((a256.equals("f")) && ((28 == a370[0]) && (input.equals(inputs[5]) && (a125 == a30[0] && (402 < a54 && cf)))))))) {
    	a122 += (a122 + 20) > a122 ? 2 : 0;
    	a63 += (a63 + 20) > a63 ? 1 : 0;
    	a51 -= (a51 - 20) < a51 ? 4 : 0;
    	a161 += (a161 + 20) > a161 ? 2 : 0;
    	a168 -= (a168 - 20) < a168 ? 4 : 0;
    	cf = false;
    	a172 = ((a338 - a320) + 4);
    	a225 = a205[(a359 + -3)];
    	a173 = "g";
    	a383 = a226[(a172 + 1)];
    	a224 = "f";
    	a129 = a92[(a172 - -1)]; 
    	System.out.println("Y");
    } 
    if(((((!a240 && ((a302 == a346[1] && ((a173.equals("f")) && (402 < a54 && ((a125 == a30[0] && cf) && input.equals(inputs[7]))))) && ((160 < a310) && (316 >= a310)))) && ((160 < a310) && (316 >= a310))) && (((a75.equals("f")) && a235 == a216[1]) && ((-188 < a357) && (-57 >= a357)))) && a12 == 5636)) {
    	a120 -= (a120 - 20) < a120 ? 4 : 0;
    	cf = false;
    	a125 = a30[5];
    	a81 = a167[(a206 - 2)]; 
    	System.out.println("X");
    } 
    if(((!a378 && (((77 < a386) && (201 >= a386)) && (((a313.equals("f")) && a125 == a30[0]) && (a173.equals("f"))))) && ((76 == a358[5]) && (((((cf && input.equals(inputs[8])) && (a75.equals("f"))) && !a336) && 402 < a54) && ((-65 < a382) && (-39 >= a382)))))) {
    	a122 -= (a122 - 20) < a122 ? 4 : 0;
    	cf = false;
    	a358 = a348;
    	a75 = "e";
    	a339 = true;
    	a269 = "e";
    	a240 = true;
    	a239 = a242;
    	a155 = "h";
    	a77 = (a206 - -2);
    	a211 = ((a338 / a398) - -1);
    	a218 = "e";
    	a368 = true;
    	a260 = true;
    	a373 = true;
    	a310 = (((((((a310 * a54) % 14999) + -19667) * 10) / 9) * 10) / 9);
    	a276 = a253;
    	a132 = false;
    	a370 = a318;
    	a329 = "e";
    	a230 = ((a270 + a270) + -19);
    	a243 = (((((a243 * a357) % 14999) / 5) + -14274) + -1066);
    	a353 = a263;
    	a302 = a346[(a237 - 4)]; 
    	System.out.println("Y");
    } 
    if(((((a218.equals("f")) && ((((a173.equals("f")) && (input.equals(inputs[3]) && !a300)) && !a260) && (40 == a239[3]))) && (402 < a54 && (((a398 == 11) && (((a75.equals("f")) && cf) && a125 == a30[0])) && ((152 < a249) && (355 >= a249))))) && a133 == 2186)) {
    	a67 += (a67 + 20) > a67 ? 1 : 0;
    	cf = false;
    	a75 = "h";
    	a54 = (((((a54 * a243) % 14999) + -10519) + -2630) - 1155);
    	a134 = true;
    	a196 = ((a359 / a270) - -12); 
    	System.out.println("V");
    } 
    if(((((160 < a310) && (316 >= a310)) && (402 < a54 && (!a373 && (((a75.equals("f")) && cf) && a125 == a30[0])))) && ((((a235 == a216[1] && (!a368 && (103 == a276[1]))) && (a338 == 4)) && (a173.equals("f"))) && input.equals(inputs[9])))) {
    	a5 -= (a5 - 20) < a5 ? 4 : 0;
    	cf = false;
    	if((a129 == a92[0] || a278 == a326[1])) {
    	a373 = true;
    	a359 = ((a230 + a230) - 5);
    	a141 = a47[(a237 - 3)];
    	a237 = ((a230 - a230) + 3);
    	a333 = ((((((a333 * a54) % 14999) % 14976) - 15022) + -3) * 1);
    	a224 = "f";
    	a383 = a226[((a398 - a206) - 6)];
    	a398 = (a338 - -6);
    	a225 = a205[(a211 - 2)];
    	a277 = (((((((a201 * a386) % 14999) - -24997) % 78) - 1) + 21879) + -21844);
    	a228 = a229;
    	a218 = "e";
    	a353 = a263;
    	a312 = "e";
    	a211 = (a338 - 3);
    	a234 = a372[((a320 - a338) - 3)];
    	a239 = a242;
    	a42 = (a338 - -4);
    	a300 = true;
    	a338 = (a270 - 8);
    	a173 = "i";
    	a230 = 3; 
    	}else {
    	a260 = true;
    	a386 = ((((((a333 * a357) % 15038) - 14960) - -511) + 24773) + -25286);
    	a339 = true;
    	a353 = a263;
    	a373 = true;
    	a368 = true;
    	a230 = ((a270 * a206) - 52);
    	a277 = ((((((a333 * a382) - 16543) + 17340) * 3) % 14995) - 15004);
    	a196 = ((a270 - a270) + 10);
    	a129 = a92[((a270 - a270) - -2)];
    	a240 = true;
    	a249 = (((((a310 * a386) % 14999) - 14992) * 1) * 1);
    	a237 = (a320 + -4);
    	a359 = ((a230 + a230) - 3);
    	a320 = (a359 - -3);
    	a313 = "e";
    	a312 = "e";
    	a206 = (a237 - -1);
    	a276 = a253;
    	a269 = "e";
    	a75 = "h";
    	a207 = "e";
    	a333 = (((((((a333 * a249) % 14999) % 14976) + -15022) - 2) / 5) - 11856);
    	a134 = true;
    	a224 = "f";
    	a338 = (a398 - 8);
    	a270 = (a359 + 7);
    	a398 = ((a211 / a359) - -10);
    	}System.out.println("S");
    } 
    if((((((-179 < a243) && (-156 >= a243)) && (((-47 < a333) && (147 >= a333)) && ((53 == a265[5]) && a302 == a346[1]))) && (((((a329.equals("f")) && (a235 == a216[1] && ((cf && input.equals(inputs[1])) && (a75.equals("f"))))) && a125 == a30[0]) && (a173.equals("f"))) && 402 < a54)) && a35 == 3324)) {
    	a122 -= (a122 - 20) < a122 ? 3 : 0;
    	cf = false;
    	a75 = "g";
    	a57 = ((a230 + a398) - -2);
    	a141 = a47[((a320 / a237) + -1)];
    	a40 = a65; 
    	System.out.println("V");
    } 
}
private  void calculateOutputm47(String input) {
    if(((((a8 == 10) && ((a173.equals("f")) && ((103 == a276[1]) && ((77 < a386) && (201 >= a386))))) && (11 == a353[4])) && (((89 == a296[5]) && (((77 < a386) && (201 >= a386)) && ((a125 == a30[1] && (cf && input.equals(inputs[8]))) && (a361.equals("f"))))) && (a75.equals("f"))))) {
    	a174 -= (a174 - 20) < a174 ? 2 : 0;
    	a67 += (a67 + 20) > a67 ? 1 : 0;
    	cf = false;
    	a357 = (((((((a249 * a201) % 14999) + 724) % 38) - 18) + 11850) + -11850);
    	a296 = a362;
    	a373 = false;
    	a359 = (a8 + -5);
    	a320 = ((a359 * a8) + -42);
    	a370 = a311;
    	a310 = (((((((a357 * a201) % 20) - -336) * 5) - 6114) % 20) + 337);
    	a333 = ((((((a243 * a310) % 14999) + 9569) / 5) % 14) + 161);
    	a230 = (a320 - 3);
    	a269 = "g";
    	a276 = a250;
    	a211 = (a8 + -7);
    	a339 = false;
    	a358 = a335;
    	a206 = (a8 + -4);
    	a353 = a399;
    	a228 = a229;
    	a249 = ((((((a357 * a357) % 71) - -426) * 1) - -12240) + -12237);
    	a260 = true;
    	a278 = a326[((a398 + a338) - 12)];
    	a224 = "g";
    	a75 = "g";
    	a361 = "g";
    	a368 = true;
    	a277 = (((((a277 * a386) % 95) + 244) + -22886) + 22885);
    	a378 = true;
    	a336 = true;
    	a184 = a157[((a270 * a270) + -119)];
    	a57 = ((a237 * a8) - 24);
    	a237 = ((a359 - a359) - -5);
    	a234 = a372[(a359 - 3)];
    	a382 = ((((((a382 * a243) % 107) + 69) * 5) % 107) - -58);
    	a302 = a346[(a206 + -4)];
    	a270 = ((a8 * a338) + -28);
    	a307 = a227[(a8 + -8)];
    	a338 = ((a359 - a8) - -10);
    	a386 = (((((((a357 * a357) - -13936) % 61) + 235) * 5) % 61) + 253);
    	a398 = (a359 + 7);
    	a312 = "g";
    	a202 = a217[(a359 - 4)];
    	a207 = "e";
    	a240 = true;
    	a243 = (((((a357 * a357) * 5) + 8100) % 43) + -147);
    	a225 = a205[(a211 - 1)]; 
    	System.out.println("Y");
    } 
    if((((((a8 == 10) && a302 == a346[1]) && ((-179 < a243) && (-156 >= a243))) && (!a368 && (((160 < a310) && (316 >= a310)) && (((a173.equals("f")) && ((input.equals(inputs[4]) && (a125 == a30[1] && (cf && (a75.equals("f"))))) && (a224.equals("f")))) && (a313.equals("f")))))) && (a131 % 2==0))) {
    	a164 += (a164 + 20) > a164 ? 2 : 0;
    	cf = false;
    	a134 = true;
    	a57 = (a270 - -5);
    	a75 = "g";
    	a278 = a326[((a57 + a8) - 20)]; 
    	System.out.println("R");
    } 
    if((((input.equals(inputs[7]) && ((76 == a358[5]) && ((77 < a386) && (201 >= a386)))) && (((-199 < a201) && (-12 >= a201)) && ((a324 == a232[1] && (((a225 == a205[1] && (((a75.equals("f")) && cf) && (a8 == 10))) && (a173.equals("f"))) && !a260)) && a125 == a30[1]))) && a122 >= 3)) {
    	cf = false;
    	a295 = a366[(a320 + -2)];
    	a359 = (a237 - 1);
    	a361 = "e";
    	a339 = true;
    	a398 = ((a206 * a206) + -15);
    	a358 = a348;
    	a134 = false;
    	a296 = a212;
    	a224 = "e";
    	a276 = a253;
    	a75 = "h";
    	a218 = "e";
    	a269 = "e";
    	a239 = a242;
    	a320 = ((a359 / a230) + 6);
    	a392 = a208;
    	a373 = true;
    	a228 = a229;
    	a53 = ((((37 + 27184) / 5) - -14483) + -19925); 
    	System.out.println("Y");
    } 
    if(((((a75.equals("f")) && ((((a270 == 11) && (28 == a370[0])) && a235 == a216[1]) && input.equals(inputs[1]))) && (103 == a276[1])) && ((a324 == a232[1] && (((-65 < a382) && (-39 >= a382)) && ((a8 == 10) && (cf && a125 == a30[1])))) && (a173.equals("f"))))) {
    	cf = false;
    	a218 = "f";
    	a225 = a205[(a270 - 11)];
    	a228 = a229;
    	a395 = false;
    	a286 = a294[(a320 / a237)];
    	a125 = a30[(a8 - 10)];
    	a265 = a293;
    	a207 = "f";
    	a277 = (((((a277 * a201) % 95) + 244) + 22070) - 22069);
    	a224 = "e";
    	a256 = "f";
    	a361 = "f";
    	a336 = false;
    	a202 = a217[((a270 - a270) - -1)];
    	a329 = "f";
    	a54 = (((7 / 5) - -15826) + 1120);
    	a333 = ((((((a243 * a201) % 14999) % 96) - 30) - 4) + 44);
    	a239 = a299;
    	a300 = false;
    	a211 = (a359 - 2);
    	a353 = a399;
    	a383 = a226[((a237 + a206) + -9)];
    	a234 = a372[(a230 / a211)]; 
    	System.out.println("V");
    } 
    if((((!a240 && (!a260 && !a339)) && (((a302 == a346[1] && (a125 == a30[1] && (((a173.equals("f")) && (input.equals(inputs[0]) && (cf && (a75.equals("f"))))) && ((152 < a249) && (355 >= a249))))) && !a378) && (a8 == 10))) && a164 <= 3)) {
    	a168 += (a168 + 20) > a168 ? 3 : 0;
    	cf = false;
    	a239 = a299;
    	a130 = (((((a201 * a277) % 15067) + -14932) - 2) - 0);
    	a173 = "e";
    	a191 = a37[(a8 - 5)]; 
    	System.out.println("V");
    } 
    if(((((((-199 < a201) && (-12 >= a201)) && ((((a75.equals("f")) && (!a378 && a307 == a227[1])) && (50 == a228[0])) && (a237 == 4))) && !a373) && ((((a173.equals("f")) && (cf && input.equals(inputs[3]))) && a125 == a30[1]) && (a8 == 10))) && a120 >= 3)) {
    	cf = false;
    	a277 = (((((((a277 * a249) % 14999) % 95) + 244) / 5) + -16846) + 16993);
    	a158 = (a270 - 6);
    	a200 = a115[(a206 - -1)];
    	a243 = ((((((((a310 * a277) % 14999) % 43) - 120) + 3) / 5) * 59) / 10);
    	a307 = a227[(a398 - 9)];
    	a320 = (a237 + 4);
    	a324 = a232[(a8 - 8)];
    	a75 = "g";
    	a392 = a304;
    	a240 = true;
    	a339 = false;
    	a225 = a205[(a206 + -3)];
    	a57 = ((a8 - a8) - -10);
    	a338 = (a230 + 1);
    	a353 = a399;
    	a357 = (((((((a357 * a243) + 830) % 38) + -32) * 5) % 38) + -18); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm48(String input) {
    if(((a383 == a226[1] && ((a75.equals("f")) && ((((11 == a353[4]) && a125 == a30[1]) && (43 == a392[3])) && (a8 == 11)))) && ((a338 == 4) && ((a237 == 4) && (((input.equals(inputs[5]) && cf) && (a173.equals("f"))) && a324 == a232[1]))))) {
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	cf = false;
    	a75 = "g";
    	a324 = a232[((a211 + a211) + -2)];
    	a239 = a268;
    	a234 = a372[(a270 + -11)];
    	a240 = true;
    	a276 = a250;
    	a313 = "e";
    	a357 = (((((((a357 * a243) % 14999) % 38) + -55) * 5) % 38) - 15);
    	a137 = a189;
    	a225 = a205[(a211 + -2)];
    	a296 = a362;
    	a57 = (a359 - -9);
    	a353 = a399;
    	a359 = (a237 - -1);
    	a382 = (((((a382 * a386) / 5) % 107) - -96) + 14);
    	a338 = (a230 - -1);
    	a386 = ((((((a386 * a277) - -222) % 61) + 262) + -3940) + 3942);
    	a339 = false;
    	a154 = (a57 - -2);
    	a228 = a292;
    	a211 = ((a206 * a237) + -17);
    	a206 = (a8 - 7);
    	a237 = (a338 + -2); 
    	System.out.println("X");
    } 
    if((((a211 == 2) && (a202 == a217[1] && !a339)) && ((40 == a239[3]) && (a324 == a232[1] && (!a240 && (input.equals(inputs[6]) && ((a125 == a30[1] && ((a75.equals("f")) && (cf && (a173.equals("f"))))) && (a8 == 11)))))))) {
    	a178 += (a178 + 20) > a178 ? 1 : 0;
    	cf = false;
    	a230 = (a8 - 6);
    	a373 = false;
    	a357 = ((((((a243 * a382) + 16910) * 1) / 5) % 38) + -29);
    	a339 = true;
    	a265 = a376;
    	a202 = a217[(a230 + -3)];
    	a368 = true;
    	a57 = (a206 + 6);
    	a313 = "e";
    	a320 = (a230 + 3);
    	a225 = a205[(a206 - 5)];
    	a143 = "g";
    	a173 = "e";
    	a398 = (a237 - -8);
    	a234 = a372[(a338 + -2)];
    	a276 = a253;
    	a329 = "f";
    	a240 = true;
    	a338 = (a237 + -1);
    	a312 = "e";
    	a260 = true;
    	a324 = a232[(a359 - 2)];
    	a361 = "g";
    	a296 = a212;
    	a358 = a335;
    	a228 = a229;
    	a359 = ((a230 + a237) + -6);
    	a353 = a399;
    	a211 = ((a230 + a320) - 12);
    	a201 = ((((((a201 * a357) * 2) % 94) + 84) + 12128) - 12128);
    	a75 = "g";
    	a270 = a398;
    	a382 = ((((((a382 * a333) * 2) * 1) + -1106) % 107) - -155);
    	a206 = a237;
    	a237 = (a230 + -2); 
    	System.out.println("X");
    } 
    if((((a383 == a226[1] && (a286 == a294[1] && ((a8 == 11) && a225 == a205[1]))) && (89 == a296[5])) && (!a240 && ((((a125 == a30[1] && ((a75.equals("f")) && cf)) && (a313.equals("f"))) && input.equals(inputs[0])) && (a173.equals("f")))))) {
    	a162 -= (a162 - 20) < a162 ? 1 : 0;
    	cf = false;
    	a339 = true;
    	a320 = (a206 - -3);
    	a382 = (((((a382 * a201) % 107) + 58) + 2) + -82);
    	a225 = a205[(a211 / a320)];
    	a57 = a8;
    	a234 = a372[(a359 - 2)];
    	a75 = "g";
    	a368 = true;
    	a296 = a212;
    	a353 = a399;
    	a270 = ((a237 - a237) - -12);
    	a211 = (a206 + -4);
    	a329 = "f";
    	a357 = ((((((((a357 * a243) % 14999) - -14894) % 38) + -35) * 5) % 38) + -17);
    	a338 = (a270 - 9);
    	a173 = "e";
    	a359 = ((a206 * a270) - 57);
    	a228 = a229;
    	a230 = ((a237 - a320) - -9);
    	a361 = "g";
    	a201 = ((((((a201 * a277) % 94) + 83) + 2) + -12577) + 12574);
    	a324 = a232[(a237 + -2)];
    	a237 = ((a270 + a206) + -14);
    	a240 = true;
    	a276 = a253;
    	a398 = (a206 + 7);
    	a143 = "g";
    	a265 = a376;
    	a260 = true;
    	a202 = a217[(a270 - 10)];
    	a358 = a335;
    	a312 = "e";
    	a313 = "e";
    	a373 = false;
    	a206 = ((a320 - a398) - -8); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm52(String input) {
    if(((a383 == a226[1] && (!a368 && ((a207.equals("f")) && ((((a173.equals("f")) && (cf && a125 == a30[4])) && !a210) && (a313.equals("f")))))) && (((((-188 < a357) && (-57 >= a357)) && (a75.equals("f"))) && (a206 == 5)) && input.equals(inputs[3])))) {
    	a31 += (a31 + 20) > a31 ? 2 : 0;
    	cf = false;
    	a75 = "e";
    	a395 = true;
    	a370 = a318;
    	a359 = (a398 - 8);
    	a361 = "e";
    	a234 = a372[(a359 - 3)];
    	a77 = ((a338 * a320) + -18);
    	a132 = false;
    	a276 = a253;
    	a310 = (((((a310 * a277) % 14999) - 4302) + -1566) * 1);
    	a320 = (a359 - -3);
    	a218 = "e";
    	a230 = (a338 - 1);
    	a1 = a87[(a77 + -10)]; 
    	System.out.println("P");
    } 
    if((((a361.equals("f")) && (((a398 == 11) && ((!a210 && ((a75.equals("f")) && ((28 == a370[0]) && a234 == a372[1]))) && !a378)) && (a173.equals("f")))) && ((a125 == a30[4] && (input.equals(inputs[9]) && cf)) && !a368))) {
    	cf = false;
    	if(((!(a207.equals("i")) && ((a171.equals("e")) || !(a383 == 7))) && (a324 == 15))) {
    	a339 = true;
    	a370 = a318;
    	a310 = ((((a382 * a382) / -5) * 5) - 6813);
    	a338 = (a270 + -8);
    	a357 = (((((((a249 * a382) % 14999) + 584) - 8261) * 1) % 14906) - 15093);
    	a202 = a217[(a320 + -7)];
    	a383 = a226[(a338 - 3)];
    	a295 = a366[a230];
    	a307 = a227[((a270 + a230) - 13)];
    	a240 = true;
    	a277 = (((((((a249 * a249) % 14999) % 78) + 68) - 17204) * 1) + 17205);
    	a353 = a399;
    	a235 = a216[(a230 - 3)];
    	a333 = (((((a310 * a357) % 14999) / 5) - 15709) + -1033);
    	a239 = a268;
    	a75 = "h";
    	a313 = "e";
    	a218 = "e";
    	a201 = (((((((a201 * a249) % 14999) - 11272) % 14900) - 15098) / 5) + -23975);
    	a224 = "e";
    	a320 = (a230 + 2);
    	a398 = (a230 - -6);
    	a312 = "e";
    	a373 = true;
    	a237 = (a206 + -2);
    	a296 = a212;
    	a336 = true;
    	a276 = a253;
    	a184 = a157[(a359 + 3)];
    	a206 = ((a270 - a338) + -4);
    	a359 = (a270 - 8);
    	a134 = false;
    	a302 = a346[(a398 - 9)]; 
    	}else {
    	a358 = a335;
    	a357 = (((((a357 * a243) % 38) + -55) / 5) - -21);
    	a201 = (((((((a357 * a382) % 94) + 82) + 0) * 5) % 94) + 83);
    	a202 = a217[(a270 + -9)];
    	a338 = (a230 - -1);
    	a206 = (a230 - -2);
    	a361 = "g";
    	a359 = a338;
    	a260 = false;
    	a239 = a268;
    	a277 = ((((((a243 * a357) - 7735) * 2) + 27084) % 95) - -233);
    	a228 = a292;
    	a269 = "g";
    	a320 = (a338 - -3);
    	a218 = "g";
    	a398 = (a320 + 4);
    	a224 = "g";
    	a310 = (((((((a310 * a201) % 14999) * 2) - 0) - -2) % 20) + 336);
    	a75 = "g";
    	a383 = a226[(a237 + -3)];
    	a392 = a304;
    	a270 = (a320 - -4);
    	a230 = (a206 - 1);
    	a143 = "f";
    	a382 = ((((((a382 * a277) / 5) + -10956) + 13231) % 107) - -68);
    	a57 = 11;
    	a240 = true;
    	a276 = a250;
    	a307 = a227[(a338 - 3)];
    	a353 = a399;
    	a14 = a79[(a57 + -9)];
    	}System.out.println("T");
    } 
}
private  void calculateOutputm54(String input) {
    if(((((a361.equals("f")) && (((a218.equals("f")) && (input.equals(inputs[0]) && (a125 == a30[5] && cf))) && (a75.equals("f")))) && (89 == a296[5])) && (a81 == a167[2] && ((((103 == a276[1]) && ((-179 < a243) && (-156 >= a243))) && a202 == a217[1]) && (a173.equals("f")))))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	if(a243 <=  -179) {
    	a81 = a167[((a270 / a206) + 3)]; 
    	}else {
    	a307 = a227[7];
    	a218 = "h";
    	a202 = a217[0];
    	a359 = 10;
    	a357 = ((((a357 - -26996) + -53642) + -497) - -39829);
    	a75 = "g";
    	a240 = false;
    	a201 = ((((a201 / 5) * 5) + 11605) + -11844);
    	a69 = "h";
    	a313 = "i";
    	a382 = ((((a382 * 17) / 10) * 5) + -1006);
    	a270 = 15;
    	a296 = a384;
    	a143 = "i";
    	a249 = ((((a249 * -5) - -18696) * 1) + -20439);
    	a230 = 4;
    	a57 = 11;
    	}System.out.println("R");
    } 
    if(((((input.equals(inputs[7]) && ((a173.equals("f")) && (((a81 == a167[2] && cf) && a125 == a30[5]) && (a218.equals("f"))))) && !a368) && !a240) && ((((40 == a239[3]) && (a75.equals("f"))) && (a398 == 11)) && (a329.equals("f"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a35 += (a35 + 20) > a35 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	a161 -= (a161 - 20) < a161 ? 1 : 0;
    	cf = false;
    	a239 = a242;
    	a270 = 11;
    	a265 = a376;
    	a333 = ((((a333 % 96) - -50) - -1) - -1);
    	a359 = 8;
    	a225 = a205[6];
    	a383 = a226[1];
    	a358 = a351;
    	a57 = 13;
    	a313 = "f";
    	a312 = "e";
    	a320 = 13;
    	a206 = 9;
    	a302 = a346[5];
    	a237 = 10;
    	a361 = "f";
    	a249 = (((a249 / 5) - 13198) * 2);
    	a228 = a264;
    	a300 = false;
    	a307 = a227[3];
    	a310 = ((((a310 - -7812) * 10) / 9) * 3);
    	a296 = a384;
    	a336 = false;
    	a286 = a294[4];
    	a382 = ((((a382 * 10) / 6) - 1919) + -816);
    	a207 = "e";
    	a260 = false;
    	a137 = a116;
    	a339 = true;
    	a386 = (((a386 * 5) / 5) + 23345);
    	a378 = false;
    	a398 = 14;
    	a368 = false;
    	a357 = ((((a357 % 65) + -62) + 5) + -65);
    	a75 = "g";
    	a230 = 9;
    	a295 = a366[(a338 + -2)];
    	a240 = false;
    	a256 = "e";
    	a373 = false;
    	a324 = a232[6];
    	a202 = a217[6];
    	a211 = 2;
    	a218 = "h";
    	a243 = ((((a243 * 12) / 10) + -11071) + -10724);
    	a329 = "h";
    	a338 = 7; 
    	System.out.println("V");
    } 
    if(((((a81 == a167[2] && ((76 == a358[5]) && a202 == a217[1])) && (a173.equals("f"))) && (a270 == 11)) && (((152 < a249) && (355 >= a249)) && (((((cf && (a75.equals("f"))) && a125 == a30[5]) && !a300) && input.equals(inputs[3])) && a324 == a232[1])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 3 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	if(((a44.equals("g")) && (a234 == a372[3] || ((67 == a40[1]) && (a373 && (a14 == 8)))))) {
    	a249 = (((((a249 * 5) + -18960) / 5) * -2) / 10);
    	a353 = a399;
    	a361 = "g";
    	a218 = "f";
    	a359 = 5;
    	a196 = (a211 - -13);
    	a134 = true;
    	a373 = false;
    	a237 = 9;
    	a240 = false;
    	a206 = 5;
    	a296 = a384;
    	a135 = "g";
    	a256 = "h";
    	a224 = "g";
    	a368 = false;
    	a320 = 12;
    	a234 = a372[7];
    	a75 = "h";
    	a313 = "i";
    	a276 = a289;
    	a333 = ((((((a333 + -18287) % 14) - -172) * 5) % 14) - -157);
    	a338 = 7;
    	a235 = a216[3];
    	a265 = a293;
    	a228 = a292;
    	a382 = (((((a382 * 6) / 10) - -21) + 28356) - 28184);
    	a211 = 2; 
    	}else {
    	a57 = (a338 - -6);
    	a75 = "g";
    	a81 = a167[((a230 - a320) + 6)];
    	a158 = ((a338 / a57) + 8);
    	}System.out.println("R");
    } 
    if(((((input.equals(inputs[9]) && (a81 == a167[2] && (a307 == a227[1] && (28 == a370[0])))) && (a359 == 4)) && !a395) && ((((((a173.equals("f")) && cf) && a125 == a30[5]) && (a75.equals("f"))) && (50 == a228[0])) && !a336))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	if((((-39 < a382) && (176 >= a382)) || (a14 == 8))) {
    	a75 = "g";
    	a239 = a299;
    	a218 = "e";
    	a240 = false;
    	a373 = true;
    	a386 = (((a386 - -26914) - -2859) + 13);
    	a398 = 14;
    	a336 = false;
    	a368 = false;
    	a211 = 5;
    	a295 = a366[a237];
    	a225 = a205[7];
    	a224 = "g";
    	a256 = "i";
    	a392 = a208;
    	a313 = "i";
    	a361 = "f";
    	a338 = 6;
    	a339 = true;
    	a307 = a227[6];
    	a302 = a346[1];
    	a57 = 13;
    	a300 = false;
    	a357 = ((((a357 * 33) / 10) / 5) + -20718);
    	a270 = 16;
    	a286 = a294[1];
    	a329 = "i";
    	a249 = (((a249 - 20912) - -16665) * 5);
    	a359 = 8;
    	a310 = (((a310 - -27913) / 5) + -5377);
    	a333 = ((((a333 % 96) + 50) / 5) * 5);
    	a207 = "h";
    	a243 = ((((((a243 % 11) + -166) * 1) / 5) * 49) / 10);
    	a370 = a318;
    	a277 = ((((6 * 567) / 10) + -18261) - -38904);
    	a395 = false;
    	a260 = false;
    	a358 = a351;
    	a276 = a289;
    	a137 = a116;
    	a230 = 10;
    	a324 = a232[1];
    	a320 = 13;
    	a228 = a229;
    	a235 = a216[7];
    	a206 = 4;
    	a383 = a226[3];
    	a382 = ((((a382 - 8430) - 941) - -38843) + -38868);
    	a237 = 7; 
    	}else {
    	a141 = a47[(a230 - -1)];
    	a72 = (((((a357 * a333) - 1550) - 893) % 14948) - 15051);
    	a75 = "g";
    	a57 = (a338 - -11);
    	}System.out.println("V");
    } 
    if(((a125 == a30[5] && ((a230 == 4) && a302 == a346[1])) && ((((-188 < a357) && (-57 >= a357)) && (!a240 && (((a398 == 11) && ((a320 == 7) && ((a75.equals("f")) && (a81 == a167[2] && cf)))) && (a173.equals("f"))))) && input.equals(inputs[5])))) {
    	a122 -= (a122 - 20) < a122 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	a81 = a167[(a398 + -6)]; 
    	System.out.println("Y");
    } 
    if(((((-188 < a357) && (-57 >= a357)) && (a324 == a232[1] && (a235 == a216[1] && ((input.equals(inputs[2]) && !a260) && a125 == a30[5])))) && (((77 < a386) && (201 >= a386)) && (a81 == a167[2] && ((a398 == 11) && ((cf && (a75.equals("f"))) && (a173.equals("f")))))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a5 -= (a5 - 20) < a5 ? 3 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a276 = a253;
    	a202 = a217[3];
    	a382 = ((((a382 * 5) * 10) / 9) * 5);
    	a235 = a216[0];
    	a239 = a299;
    	a329 = "e";
    	a373 = false;
    	a249 = (((a249 / -5) * 5) - 22678);
    	a234 = a372[6];
    	a228 = a264;
    	a302 = a346[7];
    	a353 = a399;
    	a173 = "g";
    	a105 = "g";
    	a312 = "g";
    	a269 = "i";
    	a339 = true;
    	a225 = a205[4];
    	a224 = "h";
    	a307 = a227[0];
    	a265 = a303;
    	a243 = (((a243 / 5) - -19101) + -19232);
    	a383 = a226[1];
    	a172 = (a398 - 6); 
    	System.out.println("Y");
    } 
    if(((((a329.equals("f")) && a383 == a226[1]) && (a75.equals("f"))) && (!a336 && ((40 == a239[3]) && ((76 == a358[5]) && ((a173.equals("f")) && ((a125 == a30[5] && ((a81 == a167[2] && cf) && input.equals(inputs[8]))) && (a329.equals("f"))))))))) {
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	if(((((a141 == 10) || (!a378 && a260)) || 325 < a13) && (103 == a275[0]))) {
    	a201 = (((a201 / 5) - -16357) + -9520);
    	a373 = true;
    	a382 = (((((a382 % 12) + -49) * 5) % 12) - 44);
    	a239 = a242;
    	a260 = false;
    	a75 = "g";
    	a158 = ((a270 * a270) - 113);
    	a243 = ((((a243 * 5) - -10835) - 22028) - -35515);
    	a225 = a205[7];
    	a383 = a226[7];
    	a235 = a216[3];
    	a324 = a232[1];
    	a357 = (((a357 + -4522) / 5) - 25588);
    	a230 = 3;
    	a240 = false;
    	a207 = "f";
    	a307 = a227[0];
    	a378 = false;
    	a57 = (a158 + 2);
    	a338 = 4;
    	a218 = "e";
    	a237 = 7;
    	a202 = a217[3];
    	a336 = false;
    	a395 = false;
    	a81 = a167[((a57 / a158) + 4)]; 
    	}else {
    	a129 = a92[((a237 - a398) + 9)];
    	a224 = "h";
    	a338 = 8;
    	a383 = a226[5];
    	a230 = 8;
    	a249 = ((((a249 - -13990) * 10) / 9) - -11139);
    	a276 = a250;
    	a373 = false;
    	a228 = a292;
    	a206 = 11;
    	a368 = false;
    	a240 = false;
    	a260 = false;
    	a196 = ((a237 - a237) - -10);
    	a237 = 9;
    	a386 = ((((((a386 * 5) % 61) - -133) * 5) % 61) + 133);
    	a320 = 8;
    	a207 = "f";
    	a359 = 4;
    	a313 = "f";
    	a277 = ((((89 - 25) - -4251) - -1938) - 6163);
    	a270 = 12;
    	a269 = "f";
    	a333 = (((((a333 / 5) + -23187) - -1508) % 14) - -168);
    	a75 = "h";
    	a134 = true;
    	a353 = a241;
    	a398 = 16;
    	}System.out.println("R");
    } 
    if(((((77 < a386) && (201 >= a386)) && (((-65 < a382) && (-39 >= a382)) && ((a302 == a346[1] && (input.equals(inputs[4]) && ((cf && a125 == a30[5]) && a81 == a167[2]))) && !a373))) && ((((a75.equals("f")) && (a230 == 4)) && (a173.equals("f"))) && (a206 == 5)))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a162 -= (a162 - 20) < a162 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	if((a196 == 15)) {
    	a81 = a167[5]; 
    	}else {
    	a132 = false;
    	a75 = "e";
    	a77 = (a359 - -6);
    	a1 = a87[((a230 - a320) + 5)];
    	}System.out.println("T");
    } 
    if(((((-199 < a201) && (-12 >= a201)) && ((76 == a358[5]) && ((a173.equals("f")) && ((a75.equals("f")) && (a359 == 4))))) && ((a237 == 4) && (!a395 && (((a81 == a167[2] && (a125 == a30[5] && cf)) && ((-65 < a382) && (-39 >= a382))) && input.equals(inputs[1])))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a256 = "f";
    	a225 = a205[2];
    	a383 = a226[6];
    	a333 = ((((a333 * 5) - 2890) - -15856) - 18991);
    	a302 = a346[5];
    	a224 = "h";
    	a398 = 11;
    	a312 = "g";
    	a368 = false;
    	a228 = a229;
    	a265 = a376;
    	a358 = a348;
    	a239 = a242;
    	a234 = a372[7];
    	a296 = a384;
    	a249 = (((a249 - -23439) + 3664) * 1);
    	a339 = true;
    	a125 = a30[(a270 - 4)];
    	a202 = a217[6];
    	a307 = a227[5];
    	a218 = "h";
    	a129 = a92[7]; 
    	System.out.println("R");
    } 
    if(((((((a75.equals("f")) && cf) && a125 == a30[5]) && (a173.equals("f"))) && ((-65 < a382) && (-39 >= a382))) && ((a237 == 4) && (((input.equals(inputs[6]) && (((77 < a386) && (201 >= a386)) && (((-199 < a201) && (-12 >= a201)) && (89 == a296[5])))) && a81 == a167[2]) && (a270 == 11))))) {
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	if((((a278 == 6) || !(a278 == a326[4])) || !(60 == a3[3]))) {
    	a312 = "h";
    	a269 = "i";
    	a207 = "h";
    	a296 = a212;
    	a329 = "i";
    	a224 = "e";
    	a265 = a303;
    	a286 = a294[6];
    	a339 = true;
    	a338 = 3;
    	a353 = a263;
    	a243 = (((a243 * 5) * 5) * -5);
    	a256 = "f";
    	a91 = (a230 + 7);
    	a173 = "h";
    	a225 = a205[3];
    	a235 = a216[3];
    	a234 = a372[3];
    	a1 = a87[((a91 / a91) - -5)]; 
    	}else {
    	a57 = 17;
    	a40 = a65;
    	a75 = "g";
    	a141 = a47[((a57 - a57) + 6)];
    	}System.out.println("T");
    } 
}
private  void calculateOutputm58(String input) {
    if((((a75.equals("f")) && (((a269.equals("f")) && (a125 == a30[5] && ((cf && (a173.equals("f"))) && input.equals(inputs[8])))) && a286 == a294[1])) && (a81 == a167[7] && ((!a240 && ((11 == a353[4]) && ((-188 < a357) && (-57 >= a357)))) && ((-179 < a243) && (-156 >= a243)))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a169 -= (a169 - 20) < a169 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	if((!a210 || a134)) {
    	a234 = a372[3];
    	a277 = (((a277 * 5) * 5) + 10038);
    	a353 = a241;
    	a361 = "e";
    	a228 = a292;
    	a172 = (a206 / a230);
    	a358 = a348;
    	a224 = "i";
    	a302 = a346[6];
    	a383 = a226[3];
    	a173 = "g";
    	a129 = a92[(a172 + a172)]; 
    	}else {
    	a132 = false;
    	a75 = "e";
    	a77 = 10;
    	a1 = a87[(a270 + -9)];
    	}System.out.println("V");
    } 
    if(((((43 == a392[3]) && ((a173.equals("f")) && (a81 == a167[7] && cf))) && !a395) && (((-47 < a333) && (147 >= a333)) && (!a373 && (input.equals(inputs[6]) && ((((a211 == 2) && a125 == a30[5]) && (a313.equals("f"))) && (a75.equals("f")))))))) {
    	a120 -= (a120 - 20) < a120 ? 4 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	if((!(a313.equals("f")) || ((a183.equals("e")) || (18 == a39[0])))) {
    	a75 = "g";
    	a57 = 10;
    	a158 = (a206 - -4);
    	a3 = a114; 
    	}else {
    	a75 = "h";
    	a134 = true;
    	a196 = ((a206 * a270) + -45);
    	a129 = a92[((a196 + a196) + -13)];
    	}System.out.println("V");
    } 
    if(((a286 == a294[1] && (((input.equals(inputs[1]) && (((a173.equals("f")) && ((-65 < a382) && (-39 >= a382))) && ((-47 < a333) && (147 >= a333)))) && (a75.equals("f"))) && (a218.equals("f")))) && (((77 < a386) && (201 >= a386)) && ((a125 == a30[5] && (cf && a81 == a167[7])) && ((-10 < a277) && (148 >= a277)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	cf = false;
    	if((((a244 == 5) && (!(a191 == a37[7]) && a7)) || !a378)) {
    	a398 = 16;
    	a202 = a217[2];
    	a296 = a384;
    	a218 = "h";
    	a211 = 5;
    	a239 = a299;
    	a373 = false;
    	a295 = a366[5];
    	a228 = a264;
    	a336 = false;
    	a276 = a289;
    	a392 = a304;
    	a359 = 4;
    	a339 = false;
    	a320 = 11;
    	a269 = "g";
    	a134 = false;
    	a75 = "h";
    	a333 = ((((a333 * 5) % 96) - -50) + 2);
    	a53 = ((((63 + 19014) - 23799) - 18445) - -23186); 
    	}else {
    	a75 = "e";
    	a132 = false;
    	a77 = (a211 - -8);
    	a1 = a87[a211];
    	}System.out.println("R");
    } 
    if((((a312.equals("f")) && ((((a75.equals("f")) && (a202 == a217[1] && ((a125 == a30[5] && (a81 == a167[7] && cf)) && input.equals(inputs[9])))) && (a237 == 4)) && (a398 == 11))) && (((a173.equals("f")) && (a329.equals("f"))) && (43 == a392[3])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a122 -= (a122 - 20) < a122 ? 2 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	if(a132) {
    	a143 = "f";
    	a57 = ((a230 / a270) + 11);
    	a75 = "g";
    	a14 = a79[a359]; 
    	}else {
    	a57 = ((a270 - a320) - -8);
    	a81 = a167[((a359 + a57) + -16)];
    	a75 = "g";
    	a99 = a26[((a57 - a338) - 7)];
    	}System.out.println("Z");
    } 
    if(((((a75.equals("f")) && ((a225 == a205[1] && !a378) && a81 == a167[7])) && (11 == a353[4])) && (((((a125 == a30[5] && (input.equals(inputs[0]) && cf)) && (a173.equals("f"))) && (103 == a276[1])) && !a378) && a235 == a216[1]))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a98 -= (a98 - 20) < a98 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	if((76 == a358[5])) {
    	a276 = a250;
    	a324 = a232[7];
    	a370 = a311;
    	a196 = (a270 + 1);
    	a218 = "h";
    	a312 = "g";
    	a296 = a362;
    	a357 = (((((a357 / 5) * 9) / 10) - 5177) - -5171);
    	a202 = a217[2];
    	a211 = 7;
    	a277 = (((a277 - 3031) / 5) - -15904);
    	a237 = 7;
    	a310 = (((((a310 % 77) + 192) / 5) - 25669) + 25873);
    	a339 = false;
    	a333 = ((((a333 - -15362) * 10) / 9) * 1);
    	a373 = false;
    	a395 = false;
    	a368 = false;
    	a329 = "h";
    	a353 = a399;
    	a249 = (((a249 - -28373) / 5) - 5453);
    	a228 = a264;
    	a300 = false;
    	a230 = 8;
    	a336 = false;
    	a398 = 11;
    	a75 = "h";
    	a54 = (((((((68 * 36) / 10) * 10) / 9) + -1697) * -2) / 10);
    	a240 = false;
    	a378 = false;
    	a313 = "f";
    	a338 = 10;
    	a386 = (((a386 - -22549) * 1) - -2110);
    	a256 = "f";
    	a207 = "h";
    	a260 = false;
    	a383 = a226[1];
    	a206 = 7;
    	a307 = a227[7];
    	a243 = (((((a243 % 11) - 159) - -23025) * 1) - 23028);
    	a392 = a304;
    	a359 = 4;
    	a239 = a299;
    	a225 = a205[1];
    	a201 = (((a201 + 28950) / 5) - -13724);
    	a235 = a216[1];
    	a269 = "g";
    	a320 = 7;
    	a265 = a376;
    	a382 = (((a382 - -12945) + 6073) + 7638);
    	a134 = true;
    	a270 = 16; 
    	}else {
    	a302 = a346[3];
    	a243 = (((((a243 * 5) + -19460) * 1) % 11) + -166);
    	a296 = a212;
    	a373 = true;
    	a234 = a372[7];
    	a353 = a241;
    	a260 = false;
    	a239 = a299;
    	a307 = a227[2];
    	a225 = a205[4];
    	a333 = (((a333 / 5) / 5) - 30);
    	a249 = (((((a249 * 10) / 3) * 5) * 10) / 9);
    	a269 = "f";
    	a228 = a292;
    	a256 = "h";
    	a210 = false;
    	a336 = true;
    	a211 = 4;
    	a286 = a294[7];
    	a224 = "g";
    	a277 = ((((a277 % 78) + 69) - 1) + 1);
    	a312 = "i";
    	a361 = "g";
    	a339 = true;
    	a235 = a216[0];
    	a237 = 9;
    	a125 = a30[(a206 - 1)];
    	}System.out.println("R");
    } 
    if((((((28 == a370[0]) && (a81 == a167[7] && (((a75.equals("f")) && ((a173.equals("f")) && cf)) && (a312.equals("f"))))) && (a359 == 4)) && ((-199 < a201) && (-12 >= a201))) && ((a125 == a30[5] && (((77 < a386) && (201 >= a386)) && (11 == a353[4]))) && input.equals(inputs[4])))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	a161 += (a161 + 20) > a161 ? 2 : 0;
    	cf = false;
    	a146 = false;
    	a395 = false;
    	a202 = a217[6];
    	a75 = "i";
    	a218 = "h";
    	a296 = a384;
    	a324 = a232[4];
    	a333 = (((((a333 + -6828) % 14) - -164) - -4682) - 4673);
    	a240 = false;
    	a64 = false;
    	a230 = 6;
    	a338 = 7;
    	a237 = 7;
    	a398 = 15;
    	a378 = false;
    	a339 = false;
    	a357 = (((a357 - -29703) + 142) - -111);
    	a276 = a289;
    	a239 = a268;
    	a260 = false;
    	a73 = ((((94 / 5) * 224) / 10) * 5); 
    	System.out.println("R");
    } 
    if(((!a373 && ((((input.equals(inputs[7]) && ((-188 < a357) && (-57 >= a357))) && a125 == a30[5]) && (a173.equals("f"))) && (28 == a370[0]))) && (((a312.equals("f")) && (((a75.equals("f")) && (a81 == a167[7] && cf)) && (89 == a296[5]))) && ((-188 < a357) && (-57 >= a357))))) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	cf = false;
    	if(((26 < a170) && (82 >= a170))) {
    	a225 = a205[4];
    	a361 = "h";
    	a234 = a372[4];
    	a353 = a241;
    	a302 = a346[4];
    	a224 = "h";
    	a339 = false;
    	a358 = a348;
    	a265 = a303;
    	a269 = "e";
    	a312 = "f";
    	a81 = a167[(a359 - 2)]; 
    	}else {
    	a40 = a65;
    	a75 = "g";
    	a57 = ((a338 + a230) - -9);
    	a141 = a47[((a57 / a57) + -1)];
    	}System.out.println("X");
    } 
    if((((a307 == a227[1] && ((!a339 && ((152 < a249) && (355 >= a249))) && (43 == a392[3]))) && a225 == a205[1]) && (input.equals(inputs[5]) && (((a235 == a216[1] && ((cf && (a173.equals("f"))) && a125 == a30[5])) && a81 == a167[7]) && (a75.equals("f")))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a122 += (a122 + 20) > a122 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	if(((((152 < a249) && (355 >= a249)) && (a307 == 5)) && (a75.equals("e")))) {
    	a228 = a264;
    	a310 = (((((a310 - 12251) % 77) + 300) / 5) - -194);
    	a320 = 9;
    	a286 = a294[1];
    	a202 = a217[2];
    	a386 = ((((a386 / 5) * -5) * 10) / 9);
    	a256 = "g";
    	a338 = 10;
    	a307 = a227[1];
    	a75 = "e";
    	a265 = a376;
    	a240 = false;
    	a357 = (((a357 / 5) * 5) - 26037);
    	a230 = 6;
    	a296 = a384;
    	a225 = a205[4];
    	a211 = 4;
    	a359 = 7;
    	a132 = false;
    	a368 = false;
    	a339 = true;
    	a395 = false;
    	a235 = a216[7];
    	a269 = "f";
    	a77 = (a237 - -7);
    	a373 = false;
    	a243 = (((a243 - 594) / 5) - -9101);
    	a313 = "f";
    	a276 = a289;
    	a260 = false;
    	a382 = (((a382 + 20310) / 5) + -4108);
    	a336 = false;
    	a300 = false;
    	a249 = ((((a249 / 5) / 5) / 5) - -351);
    	a206 = 9;
    	a333 = ((((a333 - -20795) * 1) * 1) + -31709);
    	a270 = 17;
    	a370 = a285;
    	a218 = "g";
    	a329 = "i";
    	a312 = "f";
    	a237 = 7;
    	a239 = a299;
    	a378 = false;
    	a353 = a241;
    	a324 = a232[0];
    	a99 = a26[(a77 + -4)]; 
    	}else {
    	a397 = a398;
    	a125 = a30[(a397 - 8)];
    	}System.out.println("X");
    } 
    if((((a75.equals("f")) && ((a81 == a167[7] && (input.equals(inputs[3]) && cf)) && ((-65 < a382) && (-39 >= a382)))) && ((a237 == 4) && ((103 == a276[1]) && ((!a368 && (((50 == a228[0]) && (a173.equals("f"))) && (11 == a353[4]))) && a125 == a30[5]))))) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a98 -= (a98 - 20) < a98 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	a69 = "i";
    	a75 = "g";
    	a143 = "i";
    	a57 = 11; 
    	System.out.println("P");
    } 
    if((((!a339 && ((28 == a370[0]) && ((a398 == 11) && ((a218.equals("f")) && (!a336 && (((152 < a249) && (355 >= a249)) && a81 == a167[7])))))) && (a75.equals("f"))) && (input.equals(inputs[2]) && (a125 == a30[5] && (cf && (a173.equals("f"))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	a81 = a167[(a230 + -1)]; 
    	System.out.println("R");
    } 
}
private  void calculateOutputm59(String input) {
    if((((((77 < a386) && (201 >= a386)) && ((a107.equals("e")) && (cf && (a75.equals("f"))))) && input.equals(inputs[8])) && ((a230 == 4) && ((a173.equals("f")) && ((((77 < a386) && (201 >= a386)) && ((103 == a276[1]) && (((-47 < a333) && (147 >= a333)) && ((-179 < a243) && (-156 >= a243))))) && a125 == a30[6]))))) {
    	a122 -= (a122 - 20) < a122 ? 4 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	if((!(98 == a276[2]) || cf)) {
    	a57 = (a338 - -8);
    	a75 = "g";
    	a81 = a167[((a338 * a320) + -23)];
    	a170 = ((((53 * 9) / 10) * 5) - 203); 
    	}else {
    	a72 = ((((51 / 5) * -104) / 10) + -19128);
    	a141 = a47[(a398 + -6)];
    	a75 = "g";
    	a57 = (a237 + 11);
    	}System.out.println("R");
    } 
    if(((((11 == a353[4]) && ((a75.equals("f")) && ((cf && (a173.equals("f"))) && input.equals(inputs[6])))) && (50 == a228[0])) && ((a237 == 4) && (!a368 && (((a125 == a30[6] && (28 == a370[0])) && (a107.equals("e"))) && (a398 == 11)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	a75 = "e";
    	a123 = true;
    	a132 = true;
    	a137 = a117; 
    	System.out.println("R");
    } 
    if(((((a173.equals("f")) && ((!a368 && (((76 == a358[5]) && !a368) && a302 == a346[1])) && (a107.equals("e")))) && (103 == a276[1])) && (((a125 == a30[6] && (cf && input.equals(inputs[2]))) && (a75.equals("f"))) && a225 == a205[1]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	a168 += (a168 + 20) > a168 ? 2 : 0;
    	cf = false;
    	if((!(a211 == 8) || (a81 == a167[1] || 20 < a357))) {
    	a173 = "h";
    	a91 = ((a359 + a359) + 3);
    	a1 = a87[((a270 * a359) + -41)]; 
    	}else {
    	a373 = false;
    	a324 = a232[1];
    	a300 = false;
    	a57 = ((a359 - a398) + 20);
    	a357 = (((a357 + -15353) + -3816) + -1171);
    	a276 = a253;
    	a206 = 5;
    	a154 = (a338 + 11);
    	a228 = a229;
    	a218 = "f";
    	a249 = ((((a249 % 71) - -425) - 10662) + 10598);
    	a382 = (((a382 + 24628) - -2381) / 5);
    	a75 = "g";
    	a225 = a205[3];
    	a353 = a263;
    	a256 = "f";
    	a361 = "i";
    	a313 = "f";
    	a234 = a372[2];
    	a239 = a242;
    	a240 = false;
    	a137 = a189;
    	a269 = "h";
    	a333 = ((((a333 + -13515) + -8091) * 10) / 9);
    	a211 = 2;
    	a235 = a216[5];
    	a265 = a376;
    	a307 = a227[5];
    	a339 = true;
    	a386 = ((((a386 * 5) * 5) % 61) + 101);
    	a368 = false;
    	a320 = 10;
    	a237 = 9;
    	a359 = 7;
    	a338 = 8;
    	}System.out.println("Y");
    } 
    if((((((a218.equals("f")) && a286 == a294[1]) && ((-179 < a243) && (-156 >= a243))) && (a270 == 11)) && (((43 == a392[3]) && ((a107.equals("e")) && ((a173.equals("f")) && (a125 == a30[6] && (((a75.equals("f")) && cf) && input.equals(inputs[9])))))) && !a300))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a67 -= (a67 - 20) < a67 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	cf = false;
    	a132 = true;
    	a123 = true;
    	a75 = "e";
    	a137 = a117; 
    	System.out.println("R");
    } 
    if((((((152 < a249) && (355 >= a249)) && ((a206 == 5) && (a107.equals("e")))) && (a398 == 11)) && (!a339 && ((a75.equals("f")) && ((a237 == 4) && (((a173.equals("f")) && ((cf && input.equals(inputs[7])) && a125 == a30[6])) && a324 == a232[1])))))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a35 += (a35 + 20) > a35 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	cf = false;
    	a107 = "g";
    	a173 = "g";
    	a172 = (a338 - -4); 
    	System.out.println("R");
    } 
    if((((53 == a265[5]) && ((a75.equals("f")) && ((a107.equals("e")) && (cf && input.equals(inputs[1]))))) && (!a373 && ((a230 == 4) && (!a339 && (a125 == a30[6] && ((103 == a276[1]) && (!a395 && (a173.equals("f")))))))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a169 -= (a169 - 20) < a169 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if((((355 < a249) && (499 >= a249)) || (!(40 == a239[3]) && (a71 == a175[2] && !(a1 == 9))))) {
    	a207 = "f";
    	a395 = false;
    	a383 = a226[3];
    	a277 = (((a277 * 5) + -6109) * 4);
    	a378 = true;
    	a353 = a241;
    	a202 = a217[0];
    	a260 = false;
    	a256 = "i";
    	a398 = 11;
    	a218 = "h";
    	a339 = true;
    	a206 = 11;
    	a201 = ((((((a201 * 166) / 10) * 10) / 9) + 19066) + -44987);
    	a235 = a216[5];
    	a307 = a227[0];
    	a240 = false;
    	a357 = (((a357 / 5) - -21640) + 4577);
    	a75 = "g";
    	a239 = a299;
    	a324 = a232[1];
    	a225 = a205[3];
    	a158 = (a270 + -6);
    	a386 = (((((a386 * 5) + 8186) - -13985) % 61) + 97);
    	a234 = a372[5];
    	a329 = "g";
    	a200 = a115[(a338 - -3)];
    	a358 = a348;
    	a57 = (a211 - -8);
    	a338 = 8; 
    	}else {
    	a357 = ((((a357 + -28061) - -31451) + -9035) + 29443);
    	a260 = false;
    	a75 = "g";
    	a207 = "e";
    	a218 = "f";
    	a57 = ((a230 + a230) + 2);
    	a324 = a232[3];
    	a237 = 4;
    	a240 = false;
    	a382 = ((((((a382 % 12) - 44) * 10) / 9) + -16664) + 16666);
    	a158 = ((a338 + a398) + -7);
    	a378 = false;
    	a395 = false;
    	a353 = a241;
    	a383 = a226[1];
    	a201 = ((((((a201 + -21532) % 93) + -74) * 5) % 93) - 58);
    	a235 = a216[7];
    	a81 = a167[(a57 - 5)];
    	a230 = 9;
    	a225 = a205[0];
    	a265 = a303;
    	a373 = true;
    	a202 = a217[6];
    	a307 = a227[4];
    	a243 = (((((a243 % 11) + -167) + 12987) / 5) - 2724);
    	a239 = a299;
    	a338 = 6;
    	}System.out.println("R");
    } 
    if((((((a218.equals("f")) && a235 == a216[1]) && (a206 == 5)) && (a107.equals("e"))) && (a125 == a30[6] && (((76 == a358[5]) && (((-65 < a382) && (-39 >= a382)) && (((input.equals(inputs[3]) && cf) && (a173.equals("f"))) && a383 == a226[1]))) && (a75.equals("f")))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if((((a141 == 10) && (24 == a370[2])) || a373)) {
    	a353 = a263;
    	a300 = false;
    	a392 = a257;
    	a256 = "h";
    	a324 = a232[4];
    	a57 = 13;
    	a333 = (((a333 / 5) * 5) - -15195);
    	a240 = false;
    	a234 = a372[1];
    	a218 = "h";
    	a302 = a346[1];
    	a211 = 4;
    	a361 = "i";
    	a398 = 13;
    	a312 = "g";
    	a357 = (((((a357 * 5) % 65) - 87) * 10) / 9);
    	a270 = 15;
    	a373 = true;
    	a339 = true;
    	a260 = false;
    	a239 = a299;
    	a276 = a289;
    	a368 = false;
    	a382 = (((a382 + 2623) + -6698) + 8985);
    	a386 = ((((a386 + 6751) * 10) / 9) / 5);
    	a265 = a303;
    	a235 = a216[7];
    	a383 = a226[7];
    	a296 = a212;
    	a338 = 10;
    	a137 = a116;
    	a320 = 7;
    	a237 = 9;
    	a277 = (((((a277 * 5) % 78) + 68) + 16374) + -16371);
    	a358 = a348;
    	a359 = 9;
    	a378 = true;
    	a370 = a318;
    	a269 = "h";
    	a395 = false;
    	a310 = (((((a310 + -9959) % 77) - -240) / 5) + 252);
    	a313 = "h";
    	a243 = (((((a243 % 11) + -159) + -19274) - 8966) + 28232);
    	a249 = ((((a249 % 101) - -170) + -9860) - -9894);
    	a207 = "f";
    	a75 = "g";
    	a206 = 10;
    	a286 = a294[3];
    	a307 = a227[3];
    	a225 = a205[3];
    	a228 = a229;
    	a230 = 3;
    	a295 = a366[(a57 + -9)]; 
    	}else {
    	a383 = a226[6];
    	a358 = a351;
    	a224 = "i";
    	a336 = false;
    	a206 = 5;
    	a228 = a292;
    	a260 = false;
    	a230 = 10;
    	a225 = a205[4];
    	a338 = 7;
    	a361 = "g";
    	a16 = true;
    	a239 = a299;
    	a75 = "i";
    	a312 = "i";
    	a386 = ((((a386 / 5) + 225) * 10) / 9);
    	a359 = 6;
    	a329 = "i";
    	a324 = a232[2];
    	a256 = "i";
    	a265 = a293;
    	a353 = a399;
    	a240 = false;
    	a237 = 10;
    	a270 = 14;
    	a302 = a346[5];
    	a398 = 16;
    	a146 = false;
    	a249 = (((((a249 / -5) * 10) / 9) * 10) / 9);
    	a277 = (((((a277 + -804) + -10045) + -11581) % 78) - -136);
    	a235 = a216[6];
    	a218 = "f";
    	a286 = a294[3];
    	a333 = ((((a333 % 96) - -51) - -1) - 2);
    	a300 = false;
    	a276 = a289;
    	a373 = false;
    	a243 = (((a243 * 5) + 25508) * 1);
    	a202 = a217[3];
    	a296 = a384;
    	a368 = false;
    	a357 = (((a357 - -21632) / 5) + 522);
    	a395 = false;
    	a234 = a372[6];
    	a207 = "g";
    	a339 = false;
    	a73 = (((((65 * 29) / 10) + 81) + 13232) + -13233);
    	}System.out.println("X");
    } 
    if((((a107.equals("e")) && ((50 == a228[0]) && !a240)) && ((a173.equals("f")) && ((((-65 < a382) && (-39 >= a382)) && ((103 == a276[1]) && ((((a75.equals("f")) && (cf && a125 == a30[6])) && input.equals(inputs[4])) && ((-65 < a382) && (-39 >= a382))))) && !a300)))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a174 += (a174 + 20) > a174 ? 4 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	if((((a338 == 10) || (!(a99 == a26[6]) || (a230 == 4))) && !a151)) {
    	a172 = (a359 - -3);
    	a312 = "h";
    	a173 = "g";
    	a336 = true;
    	a353 = a241;
    	a296 = a362;
    	a329 = "g";
    	a378 = true;
    	a228 = a292;
    	a99 = a26[(a359 - -2)]; 
    	}else {
    	a72 = ((((38 * 5) * 5) - -423) - 5276);
    	a141 = a47[(a230 - -1)];
    	a75 = "g";
    	a57 = (a237 + 11);
    	}System.out.println("R");
    } 
    if((((input.equals(inputs[0]) && (((a75.equals("f")) && !a240) && a234 == a372[1])) && a234 == a372[1]) && (((-10 < a277) && (148 >= a277)) && (!a240 && (((a125 == a30[6] && (cf && (a173.equals("f")))) && (a359 == 4)) && (a107.equals("e"))))))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a63 -= (a63 - 20) < a63 ? 4 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	a173 = "e";
    	a66 = false;
    	a130 = ((((78 + 152) + 27059) / 5) - 5269); 
    	System.out.println("V");
    } 
    if((((a269.equals("f")) && (a125 == a30[6] && (!a368 && (((a75.equals("f")) && (cf && input.equals(inputs[5]))) && (a173.equals("f")))))) && (a202 == a217[1] && (!a240 && (((a206 == 5) && (a107.equals("e"))) && a286 == a294[1]))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a174 += (a174 + 20) > a174 ? 4 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	a173 = "h";
    	a91 = a398;
    	a1 = a87[a359]; 
    	System.out.println("V");
    } 
}
private  void calculateOutputm61(String input) {
    if((((a286 == a294[1] && ((a173.equals("f")) && (a202 == a217[1] && (76 == a358[5])))) && input.equals(inputs[2])) && ((a75.equals("f")) && ((((a312.equals("f")) && (a129 == a92[2] && (a125 == a30[7] && cf))) && (89 == a296[5])) && (a206 == 5))))) {
    	a178 += (a178 + 20) > a178 ? 4 : 0;
    	cf = false;
    	a125 = a30[((a237 + a237) + -8)];
    	a54 = ((((50 - 15819) * 1) - 2572) + 40357);
    	a361 = "f";
    	a225 = a205[(a398 - 11)];
    	a383 = a226[((a320 + a230) + -11)]; 
    	System.out.println("V");
    } 
}
private  void calculateOutputm62(String input) {
    if((((((a173.equals("f")) && ((a302 == a346[1] && ((cf && a125 == a30[7]) && a129 == a92[5])) && (a75.equals("f")))) && (a338 == 4)) && ((((a302 == a346[1] && (a269.equals("f"))) && (a338 == 4)) && input.equals(inputs[8])) && !a260)) && a174 <= 3)) {
    	cf = false;
    	a129 = a92[(a206 + -4)]; 
    	System.out.println("S");
    } 
    if((((a320 == 7) && ((a324 == a232[1] && ((a125 == a30[7] && (((a75.equals("f")) && (cf && a129 == a92[5])) && input.equals(inputs[6]))) && (a173.equals("f")))) && !a240)) && ((a270 == 11) && ((a329.equals("f")) && (a207.equals("f")))))) {
    	a94 += (a94 + 20) > a94 ? 2 : 0;
    	cf = false;
    	a134 = false;
    	a207 = "e";
    	a378 = true;
    	a269 = "e";
    	a339 = true;
    	a307 = a227[(a359 + -4)];
    	a228 = a229;
    	a202 = a217[(a270 + -11)];
    	a358 = a348;
    	a276 = a253;
    	a184 = a157[((a230 / a237) + 2)];
    	a336 = true;
    	a312 = "e";
    	a201 = (((((a201 * a249) % 14999) + -10585) - -7017) - 7345);
    	a218 = "e";
    	a373 = true;
    	a320 = ((a230 - a338) - -6);
    	a75 = "h";
    	a295 = a366[((a338 / a359) - -3)];
    	a333 = (((((((a333 * a249) % 14999) - -7315) * 1) + -8548) % 14976) - 15022);
    	a310 = ((((a310 * a382) + 15730) / 5) + -2814);
    	a359 = (a270 - 8); 
    	System.out.println("V");
    } 
    if((((((a312.equals("f")) && (a75.equals("f"))) && (a173.equals("f"))) && !a300) && ((!a368 && ((a359 == 4) && (((input.equals(inputs[5]) && (a125 == a30[7] && cf)) && a129 == a92[5]) && ((152 < a249) && (355 >= a249))))) && !a378))) {
    	a131 += (a131 + 20) > a131 ? 2 : 0;
    	a169 += (a169 + 20) > a169 ? 1 : 0;
    	a35 += (a35 + 20) > a35 ? 2 : 0;
    	a90 += (a90 + 20) > a90 ? 6 : 0;
    	a110 += (a110 + 20) > a110 ? 2 : 0;
    	a31 -= (a31 - 20) < a31 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 2 : 0;
    	cf = false;
    	a359 = (a398 - 7);
    	a269 = "f";
    	a237 = ((a338 / a206) - -5);
    	a218 = "g";
    	a230 = (a338 - -1);
    	a339 = false;
    	a312 = "g";
    	a75 = "g";
    	a235 = a216[((a338 / a359) - -1)];
    	a395 = true;
    	a277 = (((((((a310 * a310) % 14999) + -3778) / 5) - -15211) % 95) + 164);
    	a202 = a217[(a359 - 3)];
    	a296 = a362;
    	a333 = (((((((a201 * a201) % 14999) % 14) + 157) * 5) % 14) - -160);
    	a137 = a189;
    	a357 = (((((((a357 * a249) % 14999) - 3739) % 38) - -20) - 18043) + 18020);
    	a260 = true;
    	a336 = true;
    	a329 = "g";
    	a225 = a205[((a398 * a398) + -142)];
    	a392 = a304;
    	a361 = "g";
    	a358 = a335;
    	a300 = false;
    	a228 = a292;
    	a207 = "g";
    	a302 = a346[((a398 / a398) - -1)];
    	a286 = a294[((a230 - a237) - -2)];
    	a383 = a226[(a230 + -3)];
    	a324 = a232[(a398 - 10)];
    	a276 = a250;
    	a320 = (a237 - -3);
    	a270 = ((a398 - a206) + 5);
    	a382 = ((((((a382 * a333) % 107) - -170) - -29684) * 1) + -29724);
    	a368 = true;
    	a211 = (a359 + -2);
    	a57 = (a206 - -8);
    	a313 = "g";
    	a243 = ((((((a386 * a386) % 14999) + 7528) % 11) - 166) * 1);
    	a338 = ((a211 / a270) + 5);
    	a240 = true;
    	a206 = (a359 + 1);
    	a249 = ((((((a249 * a386) % 14999) + -15641) * 1) % 71) - -431);
    	a370 = a311;
    	a386 = ((((((a386 * a310) % 14999) % 61) - -239) + 3) - -8);
    	a154 = ((a57 * a57) + -153);
    	a224 = "g";
    	a256 = "f";
    	a373 = false;
    	a307 = a227[((a398 * a359) + -58)]; 
    	System.out.println("Z");
    } 
    if((((((76 == a358[5]) && (76 == a358[5])) && (a312.equals("f"))) && a202 == a217[1]) && (a302 == a346[1] && ((a125 == a30[7] && ((a173.equals("f")) && ((((a75.equals("f")) && cf) && a129 == a92[5]) && input.equals(inputs[9])))) && (103 == a276[1]))))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 4 : 0;
    	a56 += (a56 + 20) > a56 ? 2 : 0;
    	a5 += (a5 + 20) > a5 ? 2 : 0;
    	a93 += (a93 + 20) > a93 ? 2 : 0;
    	a94 -= (a94 - 20) < a94 ? 4 : 0;
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	cf = false;
    	a361 = "e";
    	a224 = "g";
    	a260 = true;
    	a333 = ((((((a333 * a249) % 14999) * 2) % 14976) - 15022) * 1);
    	a243 = (((((a382 * a201) - -10673) * -1) / 10) / 5);
    	a398 = (a320 + 3);
    	a395 = true;
    	a218 = "e";
    	a373 = true;
    	a336 = true;
    	a353 = a263;
    	a237 = (a359 - 1);
    	a16 = true;
    	a228 = a229;
    	a230 = (a206 - 2);
    	a329 = "e";
    	a202 = a217[(a359 - 3)];
    	a146 = false;
    	a256 = "g";
    	a324 = a232[((a359 + a398) - 14)];
    	a270 = (a359 - -6);
    	a286 = a294[((a230 / a230) + 1)];
    	a302 = a346[((a338 / a230) - -1)];
    	a206 = (a237 - -1);
    	a249 = ((((((a333 * a310) % 14999) - 2958) + -524) % 71) + 447);
    	a339 = true;
    	a338 = (a211 - -1);
    	a277 = ((((((a243 * a357) % 14999) - -6675) - 28838) + 33595) - 28609);
    	a73 = ((((2 + 23998) + -23825) * 5) + -627);
    	a75 = "i";
    	a386 = (((((a386 * a243) % 14999) - 11640) - 2220) + -142);
    	a312 = "e";
    	a276 = a250;
    	a383 = a226[(a359 + -4)];
    	a234 = a372[((a237 * a237) - 7)];
    	a358 = a348;
    	a235 = a216[(a211 + -2)];
    	a368 = true;
    	a207 = "e";
    	a300 = true;
    	a378 = true;
    	a225 = a205[(a211 - 2)];
    	a240 = true;
    	a239 = a242;
    	a265 = a376;
    	a357 = (((((a357 * a277) % 14999) / 5) / 5) + -16271);
    	a359 = ((a398 * a230) + -25); 
    	System.out.println("T");
    } 
    if((((((a75.equals("f")) && ((((77 < a386) && (201 >= a386)) && ((a218.equals("f")) && (a129 == a92[5] && ((a173.equals("f")) && cf)))) && (103 == a276[1]))) && a125 == a30[7]) && (input.equals(inputs[4]) && (((50 == a228[0]) && (28 == a370[0])) && a225 == a205[1]))) && a89 == 9776)) {
    	a169 -= (a169 - 20) < a169 ? 3 : 0;
    	cf = false;
    	a8 = (a359 - -2);
    	a173 = "g";
    	a172 = ((a8 * a8) + -34); 
    	System.out.println("R");
    } 
    if(((((a173.equals("f")) && (a125 == a30[7] && (input.equals(inputs[0]) && (a129 == a92[5] && ((a75.equals("f")) && cf))))) && ((a320 == 7) && ((((!a339 && (a329.equals("f"))) && (a313.equals("f"))) && (50 == a228[0])) && (a359 == 4)))) && a63 >= 3)) {
    	a5 -= (a5 - 20) < a5 ? 3 : 0;
    	cf = false;
    	a218 = "g";
    	a392 = a304;
    	a312 = "g";
    	a329 = "g";
    	a310 = ((((((a310 * a386) % 14999) % 20) - -334) * 5) / 5);
    	a357 = ((((((a357 * a382) % 38) + -47) / 5) + 6071) + -6106);
    	a370 = a311;
    	a395 = true;
    	a307 = a227[(a320 + -5)];
    	a333 = ((((((a333 * a382) / 5) / 5) * 5) % 14) - -163);
    	a224 = "g";
    	a240 = true;
    	a75 = "g";
    	a336 = true;
    	a57 = (a270 + 5);
    	a228 = a292;
    	a183 = "h";
    	a206 = (a359 - -2);
    	a278 = a326[(a359 - -1)]; 
    	System.out.println("Z");
    } 
    if(((((a230 == 4) && (((-47 < a333) && (147 >= a333)) && ((a211 == 2) && ((a230 == 4) && (!a373 && (a129 == a92[5] && (input.equals(inputs[1]) && ((a173.equals("f")) && (a211 == 2))))))))) && ((a75.equals("f")) && (cf && a125 == a30[7]))) && a165 == 4790)) {
    	a162 -= (a162 - 20) < a162 ? 2 : 0;
    	cf = false;
    	a75 = "g";
    	a57 = (a206 + 10);
    	a72 = (((((19 * 10) / -1) - -5491) * -1) / 10);
    	a141 = a47[(a57 + -13)]; 
    	System.out.println("Y");
    } 
    if((((a237 == 4) && (a324 == a232[1] && (a361.equals("f")))) && (!a336 && (!a260 && ((a125 == a30[7] && (((a75.equals("f")) && ((cf && (a173.equals("f"))) && a129 == a92[5])) && input.equals(inputs[2]))) && !a300))))) {
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a178 += (a178 + 20) > a178 ? 2 : 0;
    	cf = false;
    	a378 = true;
    	a240 = true;
    	a336 = true;
    	a312 = "g";
    	a313 = "g";
    	a333 = (((32 - 14748) - 1869) - -16753);
    	a228 = a229;
    	a307 = a227[((a270 * a270) + -119)];
    	a249 = ((((((((a333 * a333) % 14999) % 71) - -385) - 12) * 5) % 71) - -394);
    	a386 = ((((((a333 * a333) % 14999) % 61) - -238) + -1) + 28);
    	a224 = "g";
    	a269 = "g";
    	a158 = (a359 - -1);
    	a202 = a217[(a158 - 3)];
    	a392 = a304;
    	a338 = a158;
    	a329 = "g";
    	a368 = true;
    	a235 = a216[(a270 + -9)];
    	a310 = (((((((a310 * a386) % 14999) - 23298) + -6426) + -77) % 20) + 343);
    	a359 = (a398 + -7);
    	a296 = a362;
    	a373 = false;
    	a320 = (a338 - -3);
    	a370 = a318;
    	a300 = true;
    	a382 = ((((((a382 * a249) % 14999) % 107) - -69) + -1) / 5);
    	a286 = a294[((a158 - a211) - 1)];
    	a211 = (a270 - 8);
    	a57 = ((a206 + a270) - 6);
    	a237 = a158;
    	a357 = (((((((a357 * a277) % 14999) % 38) - 17) - 20972) / 5) + 4187);
    	a206 = (a398 - 6);
    	a324 = a232[((a158 / a398) + 2)];
    	a302 = a346[((a398 + a398) - 22)];
    	a230 = a158;
    	a218 = "g";
    	a207 = "g";
    	a225 = a205[(a270 - 9)];
    	a200 = a115[(a57 - 5)];
    	a339 = false;
    	a75 = "g";
    	a260 = true;
    	a395 = true;
    	a276 = a253;
    	a361 = "g";
    	a358 = a335;
    	a201 = (((((((a201 * a243) % 14999) / 5) % 94) + 76) - 13495) + 13499);
    	a270 = (a158 - -7); 
    	System.out.println("V");
    } 
}
private  void calculateOutputm63(String input) {
    if((((a361.equals("f")) && (a270 == 11)) && ((a129 == a92[6] && ((((a173.equals("f")) && ((((input.equals(inputs[5]) && cf) && a125 == a30[7]) && (a359 == 4)) && (a75.equals("f")))) && !a378) && (53 == a265[5]))) && (a270 == 11)))) {
    	cf = false;
    	a125 = a30[(a359 - 3)];
    	a395 = true;
    	a218 = "g";
    	a256 = "g";
    	a239 = a268;
    	a8 = 10;
    	a333 = ((((((a333 * a201) % 14976) - 15022) + 7494) * 1) + -7495);
    	a286 = a294[(a270 - 9)];
    	a300 = true;
    	a202 = a217[((a359 * a230) - 16)];
    	a211 = (a8 + -9);
    	a329 = "g";
    	a207 = "g";
    	a336 = true;
    	a265 = a376;
    	a234 = a372[(a359 + -4)];
    	a383 = a226[(a8 - 8)]; 
    	System.out.println("R");
    } 
    if(((((a234 == a372[1] && (((a270 == 11) && (a173.equals("f"))) && (a312.equals("f")))) && (a230 == 4)) && ((-47 < a333) && (147 >= a333))) && (((a75.equals("f")) && ((28 == a370[0]) && (a125 == a30[7] && (a129 == a92[6] && cf)))) && input.equals(inputs[2])))) {
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	cf = false;
    	a296 = a362;
    	a256 = "g";
    	a353 = a399;
    	a218 = "g";
    	a302 = a346[(a230 + -2)];
    	a202 = a217[((a359 * a338) - 14)];
    	a358 = a335;
    	a239 = a268;
    	a398 = (a320 + 5);
    	a383 = a226[(a270 - 9)];
    	a249 = (((((a243 * a310) % 14999) - 193) + -5892) / 5);
    	a368 = true;
    	a228 = a292;
    	a307 = a227[(a320 - 5)];
    	a269 = "g";
    	a333 = (((((a333 * a382) / 5) * 5) % 14) - -161);
    	a129 = a92[(a359 + 3)]; 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm64(String input) {
    if(((((((-188 < a357) && (-57 >= a357)) && (((a75.equals("f")) && cf) && a129 == a92[7])) && (a173.equals("f"))) && !a395) && (a225 == a205[1] && ((a211 == 2) && (((!a260 && input.equals(inputs[4])) && a125 == a30[7]) && (a237 == 4)))))) {
    	cf = false;
    	a154 = (a211 + 9);
    	a324 = a232[a211];
    	a137 = a189;
    	a329 = "g";
    	a57 = (a359 - -9);
    	a260 = true;
    	a225 = a205[(a320 - 5)];
    	a230 = ((a270 / a211) - 2);
    	a237 = (a320 - 2);
    	a310 = ((((((((a310 * a277) % 14999) % 20) + 337) / 5) * 5) % 20) + 322);
    	a338 = (a270 + -6);
    	a382 = ((((((a382 * a201) + -14060) / 5) / 5) % 107) + 165);
    	a392 = a304;
    	a240 = true;
    	a75 = "g";
    	a276 = a250;
    	a300 = true;
    	a277 = ((((((a277 * a333) % 95) - -244) * 5) % 95) - -214);
    	a211 = (a320 + -4);
    	a357 = (((((((a357 * a201) % 14999) % 38) - 27) + -20) * 10) / 9);
    	a339 = false;
    	a336 = true;
    	a312 = "g";
    	a320 = (a270 + -3);
    	a359 = (a398 - 7); 
    	System.out.println("Z");
    } 
    if((((((((a173.equals("f")) && (a329.equals("f"))) && !a395) && a129 == a92[7]) && !a378) && !a339) && ((((((a75.equals("f")) && cf) && input.equals(inputs[2])) && a125 == a30[7]) && ((-188 < a357) && (-57 >= a357))) && (a320 == 7)))) {
    	a5 += (a5 + 20) > a5 ? 1 : 0;
    	cf = false;
    	a398 = ((a237 + a237) + 3);
    	a225 = a205[(a230 - 3)];
    	a339 = false;
    	a260 = true;
    	a210 = false;
    	a373 = false;
    	a336 = false;
    	a125 = a30[(a206 + -1)];
    	a353 = a263;
    	a307 = a227[((a237 + a398) + -15)];
    	a312 = "g";
    	a243 = (((((a310 * a382) % 43) + -68) + -5310) + 5276);
    	a256 = "g";
    	a269 = "e";
    	a358 = a348;
    	a302 = a346[((a320 + a320) - 12)];
    	a202 = a217[(a359 + -3)];
    	a286 = a294[(a237 - 3)];
    	a296 = a362;
    	a239 = a242;
    	a277 = ((((a277 * a201) / 5) - 14331) - 8887);
    	a383 = a226[(a237 + -3)];
    	a228 = a229;
    	a333 = ((((((((a357 * a357) % 14999) % 14) + 157) * 5) - 10748) % 14) - -172);
    	a235 = a216[((a211 - a320) - -7)];
    	a218 = "f";
    	a237 = ((a398 * a211) + -17);
    	a368 = false;
    	a211 = (a270 + -9); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm4(String input) {
    if((((((a206 == 5) && (a125 == a30[0] && cf)) && ((-199 < a201) && (-12 >= a201))) && !a373) && ((a312.equals("f")) && ((a207.equals("f")) && ((-188 < a357) && (-57 >= a357)))))) {
    	if((((((a312.equals("f")) && (cf && 402 < a54)) && (a269.equals("f"))) && (53 == a265[5])) && (!a378 && ((a338 == 4) && (a237 == 4))))) {
    		calculateOutputm46(input);
    	} 
    } 
    if(((((cf && a125 == a30[1]) && a225 == a205[1]) && ((-10 < a277) && (148 >= a277))) && ((((-10 < a277) && (148 >= a277)) && ((89 == a296[5]) && ((-65 < a382) && (-39 >= a382)))) && a302 == a346[1]))) {
    	if((((((a270 == 11) && ((-199 < a201) && (-12 >= a201))) && (50 == a228[0])) && a302 == a346[1]) && ((a312.equals("f")) && (((a8 == 10) && cf) && a307 == a227[1])))) {
    		calculateOutputm47(input);
    	} 
    	if(((a234 == a372[1] && ((a224.equals("f")) && (((a8 == 11) && cf) && (50 == a228[0])))) && (((-179 < a243) && (-156 >= a243)) && ((76 == a358[5]) && ((77 < a386) && (201 >= a386)))))) {
    		calculateOutputm48(input);
    	} 
    } 
    if((((a218.equals("f")) && ((cf && a125 == a30[4]) && a202 == a217[1])) && ((((a329.equals("f")) && !a300) && (103 == a276[1])) && (a218.equals("f"))))) {
    	if(((!a378 && ((a224.equals("f")) && (!a395 && (!a210 && cf)))) && (((160 < a310) && (316 >= a310)) && (((-188 < a357) && (-57 >= a357)) && (53 == a265[5]))))) {
    		calculateOutputm52(input);
    	} 
    } 
    if((((50 == a228[0]) && ((152 < a249) && (355 >= a249))) && (a286 == a294[1] && (a286 == a294[1] && ((a270 == 11) && ((a125 == a30[5] && cf) && !a260)))))) {
    	if(((a383 == a226[1] && ((cf && a81 == a167[2]) && (a256.equals("f")))) && (((((-199 < a201) && (-12 >= a201)) && ((-47 < a333) && (147 >= a333))) && (a329.equals("f"))) && (a207.equals("f"))))) {
    		calculateOutputm54(input);
    	} 
    	if((((40 == a239[3]) && (((-199 < a201) && (-12 >= a201)) && (53 == a265[5]))) && ((((-65 < a382) && (-39 >= a382)) && (((-10 < a277) && (148 >= a277)) && (a81 == a167[7] && cf))) && (50 == a228[0])))) {
    		calculateOutputm58(input);
    	} 
    } 
    if(((a286 == a294[1] && (((a256.equals("f")) && ((152 < a249) && (355 >= a249))) && (a206 == 5))) && ((((152 < a249) && (355 >= a249)) && (a125 == a30[6] && cf)) && (a398 == 11)))) {
    	if((((43 == a392[3]) && (((152 < a249) && (355 >= a249)) && (a224.equals("f")))) && ((!a368 && (((-10 < a277) && (148 >= a277)) && ((a107.equals("e")) && cf))) && (76 == a358[5])))) {
    		calculateOutputm59(input);
    	} 
    } 
    if(((a286 == a294[1] && (a235 == a216[1] && (a361.equals("f")))) && ((a338 == 4) && ((a230 == 4) && ((a338 == 4) && (cf && a125 == a30[7])))))) {
    	if((((a129 == a92[2] && cf) && (a320 == 7)) && ((a398 == 11) && (((-188 < a357) && (-57 >= a357)) && (((89 == a296[5]) && !a339) && (a398 == 11)))))) {
    		calculateOutputm61(input);
    	} 
    	if(((!a240 && ((cf && a129 == a92[5]) && (50 == a228[0]))) && ((a312.equals("f")) && ((50 == a228[0]) && ((a320 == 7) && (a338 == 4)))))) {
    		calculateOutputm62(input);
    	} 
    	if((((((-65 < a382) && (-39 >= a382)) && !a373) && ((-179 < a243) && (-156 >= a243))) && ((!a373 && (!a336 && (a129 == a92[6] && cf))) && a307 == a227[1]))) {
    		calculateOutputm63(input);
    	} 
    	if(((((!a300 && ((-10 < a277) && (148 >= a277))) && (a230 == 4)) && (a312.equals("f"))) && ((a270 == 11) && ((a129 == a92[7] && cf) && !a336)))) {
    		calculateOutputm64(input);
    	} 
    } 
}
private  void calculateOutputm65(String input) {
    if(((((a172 == 1) && (((53 == a265[5]) && (((-188 < a357) && (-57 >= a357)) && ((cf && a129 == a92[1]) && input.equals(inputs[7])))) && (a329.equals("f")))) && ((a75.equals("f")) && ((a225 == a205[1] && (((-65 < a382) && (-39 >= a382)) && (a173.equals("g")))) && (a398 == 11)))) && a122 >= 3)) {
    	a162 -= (a162 - 20) < a162 ? 3 : 0;
    	cf = false;
    	a224 = "e";
    	a361 = "e";
    	a336 = true;
    	a320 = (a237 + 2);
    	a239 = a242;
    	a339 = true;
    	a295 = a366[(a230 - -1)];
    	a134 = false;
    	a218 = "e";
    	a276 = a253;
    	a359 = ((a320 / a320) - -2);
    	a296 = a212;
    	a75 = "h";
    	a269 = "e";
    	a392 = a208;
    	a398 = ((a359 - a359) - -10);
    	a333 = (((((a333 * a357) % 14976) - 15022) + -1) * 1);
    	a53 = ((((83 * -1) / 10) / 5) - 9); 
    	System.out.println("S");
    } 
    if((((!a378 && ((28 == a370[0]) && ((a302 == a346[1] && ((input.equals(inputs[0]) && cf) && a129 == a92[1])) && (a75.equals("f"))))) && ((a172 == 1) && (((152 < a249) && (355 >= a249)) && ((a173.equals("g")) && (!a339 && ((160 < a310) && (316 >= a310))))))) && a164 <= 3)) {
    	cf = false;
    	a277 = ((((((a310 * a382) / 5) + 26863) / 5) % 78) - 2);
    	a191 = a37[((a172 * a230) + 1)];
    	a324 = a232[((a338 + a320) - 10)];
    	a173 = "e";
    	a239 = a299;
    	a130 = ((((7 / 5) - 12888) - -26632) + -19006); 
    	System.out.println("R");
    } 
    if((((!a300 && (((((-47 < a333) && (147 >= a333)) && (((-179 < a243) && (-156 >= a243)) && ((a173.equals("g")) && ((cf && a129 == a92[1]) && input.equals(inputs[4]))))) && (a75.equals("f"))) && (a172 == 1))) && (((-47 < a333) && (147 >= a333)) && ((11 == a353[4]) && a225 == a205[1]))) && (a131 % 2==0))) {
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	cf = false;
    	a278 = a326[(a172 + 5)];
    	a75 = "g";
    	a134 = true;
    	a57 = (a172 - -15); 
    	System.out.println("R");
    } 
    if((((a338 == 4) && (a359 == 4)) && ((((a75.equals("f")) && ((a172 == 1) && (((a173.equals("g")) && (((a129 == a92[1] && cf) && input.equals(inputs[1])) && (a207.equals("f")))) && (43 == a392[3])))) && (89 == a296[5])) && ((152 < a249) && (355 >= a249))))) {
    	a162 -= (a162 - 20) < a162 ? 1 : 0;
    	cf = false;
    	a173 = "f";
    	a353 = a399;
    	a373 = false;
    	a202 = a217[(a398 - 10)];
    	a383 = a226[(a320 + -7)];
    	a234 = a372[(a237 - 2)];
    	a125 = a30[(a172 + -1)];
    	a224 = "e";
    	a239 = a299;
    	a225 = a205[(a230 + -4)];
    	a361 = "f";
    	a358 = a351;
    	a324 = a232[(a270 + -10)];
    	a211 = ((a338 + a359) + -6);
    	a54 = (((51 - -19348) * 1) + -10525); 
    	System.out.println("V");
    } 
    if((((((a302 == a346[1] && (a398 == 11)) && (89 == a296[5])) && (a172 == 1)) && (a307 == a227[1] && (((a173.equals("g")) && ((((cf && (a75.equals("f"))) && input.equals(inputs[3])) && a129 == a92[1]) && (a269.equals("f")))) && (a361.equals("f"))))) && a120 >= 3)) {
    	a122 -= (a122 - 20) < a122 ? 2 : 0;
    	cf = false;
    	a225 = a205[((a230 / a237) + 1)];
    	a158 = ((a270 - a172) - 5);
    	a353 = a399;
    	a57 = ((a230 - a230) - -10);
    	a357 = ((((((a357 * a386) % 14999) / 5) - -11951) % 38) + -22);
    	a218 = "g";
    	a320 = ((a57 + a237) + -6);
    	a256 = "g";
    	a243 = (((((((a243 * a310) % 14999) % 43) + -111) - 23244) * 1) - -23243);
    	a240 = true;
    	a207 = "g";
    	a392 = a304;
    	a338 = (a237 + 1);
    	a75 = "g";
    	a339 = false;
    	a200 = a115[(a158 + 1)];
    	a395 = true;
    	a307 = a227[(a57 - 8)]; 
    	System.out.println("V");
    } 
    if(((!a240 && (a302 == a346[1] && ((a173.equals("g")) && ((a172 == 1) && ((a224.equals("f")) && ((((cf && (a75.equals("f"))) && a129 == a92[1]) && input.equals(inputs[8])) && !a336)))))) && (((77 < a386) && (201 >= a386)) && (a320 == 7)))) {
    	a174 -= (a174 - 20) < a174 ? 2 : 0;
    	a67 += (a67 + 20) > a67 ? 1 : 0;
    	a122 -= (a122 - 20) < a122 ? 4 : 0;
    	cf = false;
    	if(((a71 == 2) && (a172 == 4))) {
    	a224 = "g";
    	a201 = ((((((((a277 * a277) % 14999) % 94) + 56) * 9) / 10) - -5632) - 5665);
    	a228 = a292;
    	a310 = ((((((a201 * a201) % 14999) * 2) % 20) + 337) - -1);
    	a336 = true;
    	a358 = a335;
    	a206 = ((a270 + a270) - 16);
    	a386 = ((((((a249 * a310) % 14999) % 61) + 238) + -10469) + 10483);
    	a202 = a217[(a398 + -10)];
    	a230 = (a270 - 6);
    	a137 = a117;
    	a339 = false;
    	a307 = a227[((a270 / a172) - 9)];
    	a368 = true;
    	a320 = (a230 - -1);
    	a378 = true;
    	a313 = "g";
    	a324 = a232[a172];
    	a296 = a212;
    	a243 = (((((((a243 * a386) % 14999) % 43) + -111) - -1) + 26436) - 26437);
    	a269 = "g";
    	a357 = ((((((a357 * a333) % 38) + -17) * 5) % 38) + -17);
    	a211 = (a237 + -1);
    	a333 = ((((((a310 * a310) % 14999) % 14) - -154) * 5) / 5);
    	a276 = a250;
    	a312 = "e";
    	a57 = (a172 + 12);
    	a338 = (a206 + -1);
    	a353 = a399;
    	a75 = "g";
    	a234 = a372[(a398 - 9)];
    	a329 = "g";
    	a323 = (a172 - -6);
    	a300 = true;
    	a249 = (((((a249 * a382) / 5) + 17758) % 71) - -426);
    	a218 = "g";
    	a373 = false;
    	a392 = a304;
    	a240 = true;
    	a265 = a376;
    	a359 = ((a237 / a172) - -1);
    	a260 = true;
    	a256 = "g";
    	a207 = "g";
    	a398 = (a237 - -8);
    	a237 = (a211 + 2);
    	a270 = (a323 - -5); 
    	}else {
    	a239 = a299;
    	a202 = a217[(a320 + -6)];
    	a243 = (((((((a243 * a357) % 14999) % 43) - 123) * 5) % 43) + -106);
    	a277 = ((((((a386 * a249) % 14999) - -14158) % 78) + -9) + 0);
    	a269 = "e";
    	a228 = a264;
    	a324 = a232[(a398 - 10)];
    	a353 = a399;
    	a383 = a226[(a206 + -4)];
    	a129 = a92[((a237 * a359) - 9)];
    	a373 = false;
    	a339 = true;
    	a234 = a372[((a270 * a338) - 42)];
    	a358 = a351;
    	a211 = (a230 + -1);
    	}System.out.println("Y");
    } 
}
private  void calculateOutputm66(String input) {
    if(((((a75.equals("f")) && ((103 == a276[1]) && (((((a172 == 1) && (input.equals(inputs[3]) && cf)) && (a207.equals("f"))) && (a173.equals("g"))) && a225 == a205[1]))) && a129 == a92[2]) && (((a329.equals("f")) && (a230 == 4)) && (a211 == 2)))) {
    	a51 -= (a51 - 20) < a51 ? 2 : 0;
    	cf = false;
    	a173 = "f";
    	a224 = "e";
    	a383 = a226[((a398 / a338) - 2)];
    	a125 = a30[(a172 - 1)];
    	a361 = "f";
    	a225 = a205[(a338 + -4)];
    	a54 = (((17 - 8191) - 3009) - -25081); 
    	System.out.println("V");
    } 
    if(((((((-179 < a243) && (-156 >= a243)) && ((160 < a310) && (316 >= a310))) && a129 == a92[2]) && input.equals(inputs[4])) && ((a361.equals("f")) && (((a173.equals("g")) && ((76 == a358[5]) && (((160 < a310) && (316 >= a310)) && (((a75.equals("f")) && cf) && (a172 == 1))))) && !a395)))) {
    	a165 += (a165 + 20) > a165 ? 4 : 0;
    	a98 += (a98 + 20) > a98 ? 6 : 0;
    	a94 += (a94 + 20) > a94 ? 1 : 0;
    	cf = false;
    	a353 = a241;
    	a324 = a232[a211];
    	a129 = a92[(a206 - 4)];
    	a234 = a372[(a398 + -11)];
    	a211 = ((a230 + a172) + -4);
    	a358 = a348;
    	a373 = true;
    	a239 = a268;
    	a202 = a217[((a398 * a398) - 121)]; 
    	System.out.println("R");
    } 
}
private  void calculateOutputm68(String input) {
    if(((!a373 && (!a240 && (((a75.equals("f")) && a235 == a216[1]) && !a300))) && (((a172 == 1) && ((((a173.equals("g")) && (a129 == a92[7] && cf)) && !a373) && input.equals(inputs[9]))) && (76 == a358[5])))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 4 : 0;
    	a56 += (a56 + 20) > a56 ? 2 : 0;
    	a5 += (a5 + 20) > a5 ? 2 : 0;
    	a93 += (a93 + 20) > a93 ? 2 : 0;
    	a94 -= (a94 - 20) < a94 ? 4 : 0;
    	a161 -= (a161 - 20) < a161 ? 4 : 0;
    	cf = false;
    	a368 = true;
    	a312 = "e";
    	a383 = a226[((a320 * a398) - 77)];
    	a243 = (((((a310 * a310) % 14999) + 12236) + -41334) / 5);
    	a211 = (a172 + 1);
    	a269 = "f";
    	a240 = true;
    	a239 = a242;
    	a207 = "e";
    	a270 = (a237 + 6);
    	a206 = (a398 - 7);
    	a300 = true;
    	a329 = "e";
    	a361 = "e";
    	a324 = a232[(a230 + -4)];
    	a146 = false;
    	a338 = ((a230 * a270) + -37);
    	a333 = (((((a333 * a277) + -22052) * 1) - -29374) - 30340);
    	a235 = a216[(a270 - 10)];
    	a302 = a346[(a398 - 9)];
    	a73 = ((((((51 * 10) / 3) - -14) * 5) * 2) / 10);
    	a336 = true;
    	a357 = (((((a357 * a310) % 14999) + -4466) - 6371) - 3684);
    	a265 = a376;
    	a373 = true;
    	a353 = a263;
    	a296 = a212;
    	a16 = true;
    	a386 = (((((a386 * a249) % 14999) - 20028) * 1) * 1);
    	a225 = a205[((a172 + a230) - 5)];
    	a249 = (((((((a243 * a243) % 14999) + -20214) - 2084) + 30808) % 71) + 381);
    	a277 = (((((a243 * a310) % 14999) + -9328) + 22102) - 26900);
    	a224 = "g";
    	a237 = ((a398 - a206) + -4);
    	a256 = "g";
    	a286 = a294[(a270 + -8)];
    	a398 = (a359 + 6);
    	a359 = (a338 + 2);
    	a228 = a229;
    	a75 = "i";
    	a260 = true;
    	a378 = true;
    	a358 = a348;
    	a234 = a372[((a338 / a270) + 2)];
    	a276 = a250;
    	a218 = "e";
    	a395 = true;
    	a230 = (a172 + 2);
    	a202 = a217[(a338 + -2)]; 
    	System.out.println("T");
    } 
    if(((((50 == a228[0]) && ((a237 == 4) && (!a260 && (input.equals(inputs[2]) && ((a173.equals("g")) && (a129 == a92[7] && cf)))))) && (a75.equals("f"))) && (((a172 == 1) && ((a256.equals("f")) && (a338 == 4))) && ((-47 < a333) && (147 >= a333))))) {
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a164 += (a164 + 20) > a164 ? 3 : 0;
    	cf = false;
    	a361 = "g";
    	a235 = a216[((a237 - a211) - -1)];
    	a265 = a376;
    	a270 = (a338 + 8);
    	a225 = a205[((a211 / a320) - -2)];
    	a228 = a229;
    	a386 = ((((((a243 * a243) % 61) + 252) * 5) % 61) - -238);
    	a395 = true;
    	a398 = (a237 - -8);
    	a307 = a227[((a211 + a320) - 8)];
    	a339 = false;
    	a269 = "g";
    	a378 = true;
    	a383 = a226[((a270 + a359) - 14)];
    	a57 = ((a172 - a320) - -16);
    	a358 = a335;
    	a239 = a268;
    	a359 = (a320 + -2);
    	a206 = (a211 + 3);
    	a240 = true;
    	a200 = a115[(a320 + -2)];
    	a312 = "g";
    	a224 = "g";
    	a368 = true;
    	a260 = true;
    	a202 = a217[(a211 + -1)];
    	a370 = a318;
    	a382 = (((((a243 * a243) % 107) - 4) - 7) + 34);
    	a158 = (a57 - 5);
    	a296 = a362;
    	a201 = ((((((a243 * a243) + -25483) - 6659) * 1) % 94) - -148);
    	a324 = a232[(a230 + -2)];
    	a218 = "g";
    	a75 = "g";
    	a302 = a346[(a211 + -1)];
    	a333 = (((((((a333 * a310) % 14999) % 14) - -161) + 0) + 9652) - 9649);
    	a357 = (((((((a201 * a201) % 14999) % 38) - 18) + -28701) * 1) + 28700);
    	a207 = "g";
    	a338 = (a320 + -2);
    	a249 = ((((((a249 * a386) % 14999) - 22008) % 71) - -478) + -26);
    	a277 = (((((a277 * a382) % 95) + 244) - -23732) - 23731);
    	a237 = (a320 - 2);
    	a286 = a294[(a270 + -10)];
    	a392 = a304;
    	a373 = false;
    	a329 = "g";
    	a313 = "g";
    	a256 = "g";
    	a336 = true;
    	a230 = (a211 + 2);
    	a310 = ((((((a357 * a357) % 20) - -337) + 1) / 5) + 259);
    	a300 = true;
    	a276 = a253;
    	a320 = (a398 + -4); 
    	System.out.println("V");
    } 
    if(((((input.equals(inputs[4]) && (!a368 && (((a172 == 1) && (cf && (a75.equals("f")))) && (50 == a228[0])))) && (a207.equals("f"))) && (a129 == a92[7] && (((-10 < a277) && (148 >= a277)) && (((a173.equals("g")) && a383 == a226[1]) && (89 == a296[5]))))) && a89 == 9776)) {
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	cf = false;
    	a8 = (a237 + 2);
    	a172 = (a320 - 5); 
    	System.out.println("R");
    } 
    if((((a129 == a92[7] && ((a398 == 11) && ((a359 == 4) && (!a260 && ((a270 == 11) && (a172 == 1)))))) && ((!a378 && (((cf && (a75.equals("f"))) && input.equals(inputs[8])) && !a336)) && (a173.equals("g")))) && a174 <= 3)) {
    	cf = false;
    	a129 = a92[((a359 + a270) + -14)];
    	a173 = "f";
    	a125 = a30[(a237 - -3)]; 
    	System.out.println("V");
    } 
    if(((((a320 == 7) && ((50 == a228[0]) && a129 == a92[7])) && !a240) && (!a240 && ((a172 == 1) && ((((77 < a386) && (201 >= a386)) && (((cf && input.equals(inputs[5])) && (a173.equals("g"))) && (a75.equals("f")))) && (53 == a265[5])))))) {
    	a131 += (a131 + 20) > a131 ? 2 : 0;
    	a169 += (a169 + 20) > a169 ? 1 : 0;
    	a35 += (a35 + 20) > a35 ? 2 : 0;
    	a90 += (a90 + 20) > a90 ? 6 : 0;
    	a110 += (a110 + 20) > a110 ? 2 : 0;
    	a31 -= (a31 - 20) < a31 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 2 : 0;
    	a63 -= (a63 - 20) < a63 ? 4 : 0;
    	cf = false;
    	a313 = "g";
    	a239 = a268;
    	a154 = ((a270 * a172) + 5);
    	a57 = (a154 + -3);
    	a333 = (((((((a333 * a310) % 14999) * 2) % 14) + 163) / 5) - -123);
    	a206 = (a57 + -7);
    	a307 = a227[((a237 / a237) + 1)];
    	a383 = a226[(a154 - 14)];
    	a202 = a217[(a237 - 2)];
    	a339 = false;
    	a302 = a346[(a154 + -14)];
    	a373 = false;
    	a240 = true;
    	a265 = a376;
    	a358 = a335;
    	a320 = (a237 + a230);
    	a276 = a250;
    	a338 = ((a206 + a398) + -12);
    	a359 = (a211 + 2);
    	a324 = a232[(a206 + -4)];
    	a249 = (((((((a249 * a386) % 14999) / 5) - -8639) - -11158) % 71) + 374);
    	a218 = "g";
    	a137 = a189;
    	a256 = "f";
    	a230 = (a57 - 8);
    	a336 = true;
    	a392 = a304;
    	a395 = true;
    	a235 = a216[(a320 + -7)];
    	a207 = "g";
    	a260 = true;
    	a382 = (((((((a333 * a333) % 14999) % 107) + 31) - -1854) / 5) - 343);
    	a329 = "g";
    	a312 = "g";
    	a361 = "g";
    	a243 = ((((((a277 * a386) / 5) % 11) + -166) - 23028) - -23027);
    	a300 = false;
    	a286 = a294[(a398 - 9)];
    	a270 = (a206 - -6);
    	a228 = a292;
    	a75 = "g";
    	a386 = (((((((a386 * a357) % 14999) % 61) + 262) + 0) - 16513) + 16514);
    	a398 = ((a57 - a237) - -3);
    	a368 = true;
    	a225 = a205[(a154 - 14)];
    	a357 = (((((a357 * a277) + 9573) % 38) - 17) + -1);
    	a296 = a362;
    	a277 = (((((((a277 * a249) % 14999) % 95) + 243) + 0) - -20331) - 20330);
    	a224 = "g";
    	a269 = "f";
    	a370 = a311;
    	a237 = ((a320 / a338) - -4); 
    	System.out.println("Z");
    } 
    if(((((43 == a392[3]) && (!a300 && (((a173.equals("g")) && (((a172 == 1) && (cf && (a75.equals("f")))) && a129 == a92[7])) && ((-47 < a333) && (147 >= a333))))) && ((a312.equals("f")) && (!a378 && (!a336 && input.equals(inputs[1]))))) && a165 == 4790)) {
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	cf = false;
    	a141 = a47[(a320 + -5)];
    	a72 = (((((80 * 5) + -24070) + 48533) * -1) / 10);
    	a75 = "g";
    	a57 = ((a270 * a172) + 4); 
    	System.out.println("R");
    } 
    if(((((a173.equals("g")) && ((40 == a239[3]) && ((cf && (a75.equals("f"))) && (a172 == 1)))) && ((((a312.equals("f")) && ((!a240 && (input.equals(inputs[0]) && (a313.equals("f")))) && a129 == a92[7])) && (a359 == 4)) && (a359 == 4))) && a63 >= 3)) {
    	a162 -= (a162 - 20) < a162 ? 3 : 0;
    	cf = false;
    	a183 = "h";
    	a329 = "g";
    	a312 = "g";
    	a224 = "g";
    	a357 = (((((((a249 * a277) % 14999) - 5355) % 38) + -18) - -27153) + -27152);
    	a370 = a311;
    	a240 = true;
    	a392 = a304;
    	a278 = a326[((a237 / a206) + 5)];
    	a310 = (((((a357 * a357) % 20) + 336) - 10752) + 10753);
    	a333 = ((((((a333 * a310) % 14999) - 10182) + -2893) % 14) + 163);
    	a75 = "g";
    	a239 = a268;
    	a228 = a292;
    	a336 = true;
    	a256 = "g";
    	a218 = "g";
    	a57 = (a230 - -12);
    	a398 = (a320 - -5);
    	a206 = (a359 - -2);
    	a395 = true;
    	a307 = a227[(a270 + -9)]; 
    	System.out.println("T");
    } 
    if(((((152 < a249) && (355 >= a249)) && (((89 == a296[5]) && a324 == a232[1]) && (a230 == 4))) && ((a129 == a92[7] && (((a206 == 5) && (input.equals(inputs[6]) && ((a75.equals("f")) && ((a173.equals("g")) && cf)))) && (a172 == 1))) && (40 == a239[3])))) {
    	a63 -= (a63 - 20) < a63 ? 1 : 0;
    	cf = false;
    	a184 = a157[(a172 + 2)];
    	a358 = a348;
    	a336 = true;
    	a310 = ((((((a310 * a249) % 14999) - 18907) * 1) * 10) / 9);
    	a218 = "e";
    	a276 = a253;
    	a228 = a229;
    	a75 = "h";
    	a333 = (((((a333 * a386) % 14976) - 15022) - 2) - 1);
    	a295 = a366[a359];
    	a307 = a227[((a398 - a230) + -7)];
    	a320 = (a270 + -5);
    	a373 = true;
    	a202 = a217[((a359 + a206) - 9)];
    	a224 = "e";
    	a378 = true;
    	a359 = ((a206 - a338) + 2);
    	a207 = "e";
    	a312 = "e";
    	a296 = a212;
    	a134 = false;
    	a201 = ((((a277 * a277) + -25161) - -22330) + -21383); 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm72(String input) {
    if((((a307 == a227[1] && ((a234 == a372[1] && (((a173.equals("g")) && ((a218.equals("f")) && (89 == a296[5]))) && a235 == a216[1])) && (a75.equals("f")))) && (a105.equals("e"))) && (((cf && (a172 == 5)) && input.equals(inputs[2])) && !a395))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a67 -= (a67 - 20) < a67 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	cf = false;
    	if((405 < a19 && ((a71 == a175[5] && (a102 == 12)) && !(a1 == 6)))) {
    	a75 = "g";
    	a107 = "f";
    	a57 = (a172 + 11);
    	a278 = a326[((a57 / a57) - -3)]; 
    	}else {
    	a386 = (((((a386 * 5) / 5) + -324) % 61) + 142);
    	a202 = a217[0];
    	a256 = "h";
    	a207 = "f";
    	a211 = 8;
    	a237 = 3;
    	a353 = a263;
    	a302 = a346[2];
    	a230 = 7;
    	a358 = a348;
    	a383 = a226[6];
    	a312 = "e";
    	a221 = true;
    	a173 = "i";
    	a243 = ((((82 + -15026) * 10) / 9) - 10923);
    	a336 = false;
    	a141 = a47[a338];
    	}System.out.println("X");
    } 
    if(((((a206 == 5) && (((a313.equals("f")) && (a218.equals("f"))) && !a260)) && !a300) && ((a75.equals("f")) && ((a172 == 5) && ((((cf && (a105.equals("e"))) && (a173.equals("g"))) && (a237 == 4)) && input.equals(inputs[9])))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a295 = a366[((a172 * a237) - 15)];
    	a75 = "h";
    	a134 = false;
    	a53 = (((22 / 5) + -13468) + 17354); 
    	System.out.println("V");
    } 
    if((((a172 == 5) && ((a75.equals("f")) && (((a105.equals("e")) && ((77 < a386) && (201 >= a386))) && (a329.equals("f"))))) && (!a336 && (((!a339 && (input.equals(inputs[3]) && (cf && (a173.equals("g"))))) && (28 == a370[0])) && (a224.equals("f")))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	a161 -= (a161 - 20) < a161 ? 3 : 0;
    	cf = false;
    	if((a383 == 11)) {
    	a125 = a30[(a320 - 6)];
    	a173 = "f";
    	a8 = (a172 - -7); 
    	}else {
    	a234 = a372[6];
    	a269 = "f";
    	a302 = a346[7];
    	a382 = (((((a382 / 5) - -26748) + -43333) * -1) / 10);
    	a320 = 6;
    	a211 = 4;
    	a339 = false;
    	a392 = a257;
    	a310 = ((((a310 + 12610) % 77) + 196) - -5);
    	a228 = a229;
    	a81 = a167[((a359 - a270) - -9)];
    	a57 = ((a237 + a172) + 3);
    	a357 = (((((a357 * 10) / 3) - 6970) * 10) / 9);
    	a300 = false;
    	a270 = 11;
    	a276 = a289;
    	a329 = "h";
    	a256 = "h";
    	a358 = a351;
    	a353 = a241;
    	a158 = (a57 + -8);
    	a307 = a227[1];
    	a370 = a285;
    	a277 = (((3 + 20348) * 1) - -4048);
    	a378 = false;
    	a243 = ((((38 - 17724) / 5) / 5) * -5);
    	a359 = 8;
    	a336 = false;
    	a398 = 10;
    	a239 = a242;
    	a207 = "e";
    	a313 = "h";
    	a249 = (((a249 - -20824) - 22615) / 5);
    	a296 = a384;
    	a338 = 10;
    	a225 = a205[3];
    	a224 = "f";
    	a235 = a216[0];
    	a361 = "e";
    	a206 = 9;
    	a324 = a232[3];
    	a386 = ((((a386 + -16466) * 10) / 9) * 1);
    	a240 = false;
    	a230 = 8;
    	a265 = a293;
    	a373 = true;
    	a333 = ((((59 / 5) + 21053) - 42683) + 21645);
    	a368 = false;
    	a75 = "g";
    	a218 = "h";
    	a312 = "f";
    	a201 = (((91 + -29535) + 29286) - -21);
    	a237 = 7;
    	}System.out.println("S");
    } 
    if(((((152 < a249) && (355 >= a249)) && (((a172 == 5) && (a269.equals("f"))) && (a105.equals("e")))) && (((103 == a276[1]) && ((a207.equals("f")) && ((((a173.equals("g")) && (cf && (a75.equals("f")))) && (a218.equals("f"))) && input.equals(inputs[0])))) && a225 == a205[1]))) {
    	a122 -= (a122 - 20) < a122 ? 4 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if(((!(a338 == 9) || (a339 || (a211 == 1))) && a307 == a227[4])) {
    	a134 = false;
    	a75 = "h";
    	a196 = (a398 - -2);
    	a295 = a366[(a172 + -5)]; 
    	}else {
    	a313 = "f";
    	a224 = "i";
    	a235 = a216[1];
    	a276 = a250;
    	a218 = "i";
    	a336 = false;
    	a75 = "i";
    	a277 = (((66 + 6653) + 19131) - -2574);
    	a339 = false;
    	a324 = a232[6];
    	a249 = ((((a249 + 16072) % 71) - -423) + 2);
    	a286 = a294[2];
    	a240 = false;
    	a329 = "g";
    	a237 = 7;
    	a310 = (((((a310 + 386) + 24916) * 1) % 20) - -319);
    	a370 = a311;
    	a382 = ((((a382 - -27892) - 27735) - 25475) + 25496);
    	a386 = ((((a386 / 5) * 5) % 61) + 129);
    	a358 = a348;
    	a338 = 6;
    	a260 = false;
    	a300 = false;
    	a201 = (((56 / 5) - 23) - 56);
    	a207 = "f";
    	a395 = false;
    	a373 = false;
    	a269 = "i";
    	a239 = a299;
    	a228 = a264;
    	a398 = 15;
    	a307 = a227[2];
    	a206 = 5;
    	a256 = "i";
    	a270 = 15;
    	a46 = a18;
    	a265 = a293;
    	a368 = false;
    	a392 = a257;
    	a357 = ((((a357 / 5) - 87) * 10) / 9);
    	a333 = (((52 + 5786) - 5819) * 5);
    	a383 = a226[5];
    	a353 = a399;
    	a202 = a217[7];
    	a211 = 3;
    	a320 = 11;
    	a146 = true;
    	a359 = 9;
    	a234 = a372[1];
    	a13 = ((((31 + -2719) / 5) + 28608) + -27964);
    	}System.out.println("R");
    } 
    if(((((a398 == 11) && ((a75.equals("f")) && (((input.equals(inputs[7]) && cf) && (a105.equals("e"))) && (a172 == 5)))) && !a260) && (a234 == a372[1] && ((a218.equals("f")) && ((a235 == a216[1] && a225 == a205[1]) && (a173.equals("g"))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a168 += (a168 + 20) > a168 ? 4 : 0;
    	cf = false;
    	if(((a300 || (((24 == a370[2]) || a260) && !(a329.equals("i")))) && !(a155.equals("e")))) {
    	a173 = "i";
    	a141 = a47[(a172 + -4)];
    	a42 = (a211 - 1); 
    	}else {
    	a302 = a346[7];
    	a353 = a399;
    	a256 = "g";
    	a296 = a384;
    	a228 = a264;
    	a368 = false;
    	a286 = a294[6];
    	a307 = a227[1];
    	a395 = false;
    	a359 = 6;
    	a234 = a372[1];
    	a75 = "e";
    	a361 = "f";
    	a339 = false;
    	a206 = 10;
    	a370 = a285;
    	a270 = 11;
    	a357 = ((((a357 - -25182) - 38704) % 65) + -85);
    	a201 = (((42 * 5) + -388) - -8);
    	a237 = 7;
    	a320 = 8;
    	a338 = 7;
    	a277 = (((57 / 5) - -23643) - -6152);
    	a16 = true;
    	a249 = ((((a249 % 101) + 187) * 5) / 5);
    	a336 = false;
    	a269 = "i";
    	a386 = (((a386 + 14074) * 2) - -958);
    	a240 = false;
    	a313 = "h";
    	a224 = "h";
    	a207 = "g";
    	a392 = a304;
    	a202 = a217[1];
    	a132 = false;
    	a260 = false;
    	a211 = 6;
    	a239 = a299;
    	a225 = a205[1];
    	a398 = 14;
    	a378 = false;
    	a235 = a216[6];
    	a218 = "g";
    	a373 = false;
    	a310 = (((((a310 - -8145) + -2088) - 12762) * -1) / 10);
    	a276 = a250;
    	a383 = a226[6];
    	a77 = (a172 + 4);
    	}System.out.println("P");
    } 
    if(((!a373 && ((a75.equals("f")) && ((a256.equals("f")) && !a395))) && (((((50 == a228[0]) && ((input.equals(inputs[8]) && ((a173.equals("g")) && cf)) && (a172 == 5))) && (a105.equals("e"))) && (a329.equals("f"))) && (a237 == 4)))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a63 -= (a63 - 20) < a63 ? 4 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	if((((100 < a72) && (258 >= a72)) && ((a277 <=  -10 || !(a269.equals("g"))) && !(a71 == a175[2])))) {
    	a235 = a216[4];
    	a211 = 5;
    	a382 = ((((a382 + 23047) - 27584) % 12) - 43);
    	a338 = 7;
    	a276 = a289;
    	a359 = 9;
    	a368 = false;
    	a373 = false;
    	a361 = "i";
    	a296 = a362;
    	a240 = false;
    	a228 = a292;
    	a206 = 11;
    	a230 = 5;
    	a135 = "g";
    	a249 = ((((a249 * 33) / 10) - -19315) + 6543);
    	a256 = "f";
    	a339 = false;
    	a218 = "f";
    	a134 = true;
    	a237 = 6;
    	a234 = a372[1];
    	a225 = a205[3];
    	a265 = a376;
    	a75 = "h";
    	a320 = 13;
    	a313 = "h";
    	a196 = ((a172 - a172) + 15); 
    	}else {
    	a228 = a292;
    	a260 = false;
    	a218 = "i";
    	a310 = ((((a310 * 5) - 18381) * -1) / 10);
    	a329 = "g";
    	a373 = false;
    	a224 = "f";
    	a155 = "h";
    	a276 = a289;
    	a269 = "f";
    	a368 = false;
    	a353 = a241;
    	a132 = false;
    	a240 = false;
    	a370 = a285;
    	a211 = 4;
    	a339 = false;
    	a75 = "e";
    	a239 = a299;
    	a77 = (a172 - -2);
    	}System.out.println("R");
    } 
    if(((((input.equals(inputs[1]) && ((a173.equals("g")) && ((a105.equals("e")) && cf))) && (28 == a370[0])) && (a75.equals("f"))) && ((a218.equals("f")) && ((43 == a392[3]) && (((a172 == 5) && ((a338 == 4) && (a359 == 4))) && (a211 == 2)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a174 += (a174 + 20) > a174 ? 4 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a132 = false;
    	a75 = "e";
    	a77 = a172;
    	a136 = ((a270 * a77) - 50); 
    	System.out.println("X");
    } 
    if(((!a336 && (!a395 && ((a105.equals("e")) && ((a329.equals("f")) && ((a270 == 11) && (((a172 == 5) && cf) && input.equals(inputs[6]))))))) && ((a173.equals("g")) && ((11 == a353[4]) && ((a75.equals("f")) && (89 == a296[5])))))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a67 -= (a67 - 20) < a67 ? 1 : 0;
    	a35 -= (a35 - 20) < a35 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a286 = a294[0];
    	a225 = a205[5];
    	a256 = "e";
    	a206 = 4;
    	a211 = 2;
    	a75 = "g";
    	a353 = a263;
    	a333 = ((((85 - -13180) - 13284) * 10) / 9);
    	a329 = "i";
    	a137 = a189;
    	a359 = 7;
    	a300 = false;
    	a201 = ((((80 + 15218) + -15386) * 10) / 9);
    	a320 = 6;
    	a336 = false;
    	a235 = a216[3];
    	a296 = a384;
    	a395 = false;
    	a368 = false;
    	a57 = (a172 - -8);
    	a239 = a299;
    	a386 = (((a386 - 26919) * 1) * 1);
    	a370 = a285;
    	a277 = (((77 / 5) / 5) + -27457);
    	a398 = 11;
    	a207 = "e";
    	a357 = (((((a357 % 65) + -105) * 5) % 65) + -66);
    	a358 = a351;
    	a260 = false;
    	a276 = a253;
    	a265 = a293;
    	a270 = 11;
    	a361 = "e";
    	a339 = true;
    	a373 = true;
    	a240 = false;
    	a310 = (((a310 + 29371) - 16409) + 292);
    	a224 = "h";
    	a307 = a227[7];
    	a383 = a226[5];
    	a312 = "h";
    	a269 = "h";
    	a228 = a264;
    	a324 = a232[4];
    	a234 = a372[0];
    	a230 = 3;
    	a302 = a346[1];
    	a382 = (((((a382 * 5) / 5) + 9319) % 12) - 60);
    	a338 = 8;
    	a237 = 6;
    	a243 = ((((20 * 5) * 5) * 10) / -31);
    	a218 = "i";
    	a154 = ((a57 * a57) + -159); 
    	System.out.println("P");
    } 
    if((((a105.equals("e")) && ((11 == a353[4]) && (((a218.equals("f")) && ((a207.equals("f")) && (a75.equals("f")))) && (a313.equals("f"))))) && ((a237 == 4) && ((53 == a265[5]) && (((cf && (a172 == 5)) && input.equals(inputs[5])) && (a173.equals("g"))))))) {
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	a1 = a87[(a172 - 2)];
    	a173 = "h";
    	a91 = a270; 
    	System.out.println("V");
    } 
    if((((a269.equals("f")) && ((a218.equals("f")) && (!a240 && ((a173.equals("g")) && ((43 == a392[3]) && (input.equals(inputs[4]) && (cf && (a105.equals("e"))))))))) && (((-65 < a382) && (-39 >= a382)) && (((a172 == 5) && a235 == a216[1]) && (a75.equals("f")))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	if((((a1 == 5) || ((a230 == 6) && !(a383 == a226[5]))) && a36 <=  155)) {
    	a307 = a227[6];
    	a310 = (((a310 * -5) + -21076) / 5);
    	a202 = a217[0];
    	a260 = false;
    	a230 = 10;
    	a359 = 6;
    	a137 = a117;
    	a302 = a346[6];
    	a240 = false;
    	a329 = "f";
    	a218 = "e";
    	a57 = ((a211 - a320) + 18);
    	a333 = (((13 + -26148) - 1921) - 343);
    	a338 = 8;
    	a276 = a289;
    	a239 = a299;
    	a368 = false;
    	a386 = (((a386 + 4113) - -17560) - 43948);
    	a234 = a372[4];
    	a378 = false;
    	a313 = "f";
    	a300 = false;
    	a323 = (a206 + 2);
    	a75 = "g";
    	a270 = 13;
    	a320 = 10;
    	a237 = 9;
    	a373 = true;
    	a353 = a241;
    	a243 = (((13 + 29829) + 76) - -1);
    	a357 = ((((a357 - 24946) * 10) / 9) / 5);
    	a339 = true;
    	a256 = "f";
    	a336 = false;
    	a277 = (((37 + -22867) / 5) - -4561);
    	a249 = ((((a249 - 25942) * 1) + 44138) + -25851);
    	a224 = "h";
    	a358 = a351;
    	a398 = 16;
    	a269 = "e";
    	a296 = a384;
    	a265 = a293;
    	a392 = a257;
    	a206 = 4;
    	a228 = a264;
    	a207 = "f";
    	a201 = (((78 - -23929) * 1) - -5203);
    	a211 = 6; 
    	}else {
    	a57 = (a338 - -12);
    	a278 = a326[(a172 + a211)];
    	a392 = a257;
    	a329 = "f";
    	a320 = 10;
    	a333 = (((19 - 7418) / 5) + 17769);
    	a378 = false;
    	a256 = "h";
    	a361 = "h";
    	a228 = a292;
    	a201 = ((((10 * -91) / 10) + 9) + -24);
    	a373 = true;
    	a336 = false;
    	a224 = "f";
    	a234 = a372[0];
    	a225 = a205[4];
    	a207 = "f";
    	a249 = ((((((a249 + 465) * 10) / 9) / 5) * 37) / 10);
    	a313 = "i";
    	a260 = false;
    	a398 = 14;
    	a395 = false;
    	a75 = "g";
    	a357 = ((((a357 + 10966) + -6827) * 10) / 9);
    	a368 = false;
    	a206 = 7;
    	a286 = a294[5];
    	a310 = (((a310 / 5) + 14874) * 2);
    	a81 = a167[(a172 - a172)];
    	a237 = 8;
    	a243 = (((95 * 5) + -12773) + -5358);
    	a265 = a293;
    	a300 = false;
    	a307 = a227[3];
    	a218 = "i";
    	a277 = (((81 / 5) * 5) / 5);
    	a269 = "f";
    	a240 = false;
    	a324 = a232[4];
    	a239 = a242;
    	a339 = false;
    	a359 = 3;
    	a353 = a263;
    	a302 = a346[5];
    	a270 = 15;
    	a386 = ((((a386 / 5) + 26185) + -17279) + -38222);
    	a235 = a216[5];
    	a230 = 6;
    	a338 = 6;
    	a296 = a384;
    	a211 = 2;
    	}System.out.println("Y");
    } 
}
private  void calculateOutputm73(String input) {
    if((((a312.equals("f")) && (((a173.equals("g")) && cf) && input.equals(inputs[3]))) && (((a75.equals("f")) && (((77 < a386) && (201 >= a386)) && (((a105.equals("g")) && ((!a368 && (a338 == 4)) && (a269.equals("f")))) && (a172 == 5)))) && ((-47 < a333) && (147 >= a333))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 2 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if((a310 <=  160 && 307 < a36)) {
    	a77 = ((a230 + a270) + -6);
    	a243 = ((((64 + 29132) / 5) + -2912) - 3096);
    	a211 = 2;
    	a206 = 11;
    	a370 = a285;
    	a225 = a205[7];
    	a398 = 11;
    	a382 = ((((43 / 5) / 5) / 5) - -17999);
    	a386 = ((((((a386 % 61) - -112) + -7) * 5) % 61) + 103);
    	a230 = 5;
    	a239 = a268;
    	a368 = false;
    	a329 = "i";
    	a237 = 8;
    	a234 = a372[1];
    	a256 = "f";
    	a260 = false;
    	a339 = false;
    	a353 = a241;
    	a312 = "g";
    	a201 = ((((a201 % 93) + -41) - 60) + -4);
    	a358 = a351;
    	a235 = a216[0];
    	a336 = false;
    	a392 = a304;
    	a240 = false;
    	a202 = a217[7];
    	a383 = a226[7];
    	a277 = ((((a277 + 4639) * 10) / -9) - 23475);
    	a307 = a227[4];
    	a320 = 8;
    	a333 = ((((a333 % 14) + 162) + 2) + -2);
    	a359 = 7;
    	a338 = 10;
    	a276 = a289;
    	a310 = ((((a310 % 77) + 230) - 9683) - -9639);
    	a361 = "f";
    	a16 = true;
    	a269 = "g";
    	a224 = "g";
    	a395 = false;
    	a207 = "g";
    	a357 = (((((a357 - 1699) * 5) + -19791) % 65) + -57);
    	a378 = false;
    	a218 = "f";
    	a249 = ((((81 * 5) + -18) * 10) / 9);
    	a286 = a294[4];
    	a132 = false;
    	a296 = a362;
    	a313 = "f";
    	a75 = "e";
    	a270 = 16; 
    	}else {
    	a225 = a205[1];
    	a207 = "e";
    	a158 = (a206 - -1);
    	a313 = "e";
    	a312 = "e";
    	a270 = 10;
    	a218 = "i";
    	a240 = false;
    	a357 = ((((a357 * 5) % 65) + -67) + -24);
    	a57 = ((a206 * a320) + -25);
    	a206 = 11;
    	a256 = "e";
    	a105 = "i";
    	a353 = a263;
    	a338 = 7;
    	a75 = "g";
    	a395 = false;
    	a224 = "i";
    	a320 = 11;
    	}System.out.println("V");
    } 
    if(((!a378 && (((-47 < a333) && (147 >= a333)) && ((a218.equals("f")) && input.equals(inputs[9])))) && (((a173.equals("g")) && ((76 == a358[5]) && ((!a240 && ((a75.equals("f")) && (cf && (a105.equals("g"))))) && (a172 == 5)))) && (43 == a392[3])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a169 -= (a169 - 20) < a169 ? 4 : 0;
    	a35 -= (a35 - 20) < a35 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	if((((!(a177.equals("g")) && a54 <=  164) && !a132) && !a123)) {
    	a302 = a346[6];
    	a336 = false;
    	a386 = ((((a386 + -10390) / 5) * 10) / -9);
    	a173 = "h";
    	a228 = a264;
    	a333 = (((a333 - -17140) - 22431) + 23164);
    	a368 = false;
    	a358 = a348;
    	a256 = "h";
    	a373 = true;
    	a320 = 13;
    	a296 = a384;
    	a329 = "e";
    	a383 = a226[2];
    	a249 = ((((94 + -27851) + -1179) * -1) / 10);
    	a202 = a217[5];
    	a59 = "e";
    	a312 = "h";
    	a265 = a376;
    	a234 = a372[6];
    	a353 = a263;
    	a91 = ((a359 * a230) + -9); 
    	}else {
    	a307 = a227[5];
    	a370 = a311;
    	a132 = false;
    	a339 = true;
    	a300 = false;
    	a77 = (a172 + 6);
    	a395 = false;
    	a276 = a289;
    	a386 = (((a386 + -9079) / 5) + 1940);
    	a240 = false;
    	a361 = "f";
    	a202 = a217[6];
    	a75 = "e";
    	a218 = "g";
    	a378 = false;
    	a358 = a351;
    	a230 = 10;
    	a336 = false;
    	a256 = "f";
    	a260 = false;
    	a206 = 8;
    	a286 = a294[7];
    	a224 = "i";
    	a312 = "i";
    	a338 = 6;
    	a269 = "g";
    	a270 = 12;
    	a99 = a26[(a172 + a211)];
    	a225 = a205[6];
    	a353 = a241;
    	a357 = (((a357 / 5) - -20905) * 1);
    	a359 = 9;
    	a296 = a212;
    	a320 = 11;
    	a333 = (((a333 + -4412) * 5) - 1588);
    	a249 = ((((24 / 5) + 26329) * 10) / 9);
    	a368 = false;
    	a265 = a293;
    	a234 = a372[5];
    	a313 = "h";
    	a239 = a268;
    	a235 = a216[4];
    	a243 = (((((66 * 10) / -4) + -4) - 15499) + 15493);
    	a310 = ((((a310 % 77) - -186) - 11098) - -11091);
    	a237 = 5;
    	a324 = a232[1];
    	a211 = 6;
    	}System.out.println("R");
    } 
    if(((((89 == a296[5]) && (a237 == 4)) && (a173.equals("g"))) && (((a172 == 5) && ((a218.equals("f")) && (((((input.equals(inputs[8]) && cf) && (a105.equals("g"))) && (a312.equals("f"))) && (a75.equals("f"))) && (a230 == 4)))) && ((-188 < a357) && (-57 >= a357))))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	if(((!(60 == a197[4]) && (!(60 == a3[3]) || a339)) || !(20 == a137[1]))) {
    	a173 = "i";
    	a184 = a157[(a172 - 3)];
    	a141 = a47[(a206 + -2)]; 
    	}else {
    	a336 = false;
    	a353 = a263;
    	a382 = (((73 * 5) / 5) + 9927);
    	a202 = a217[6];
    	a172 = (a359 + 3);
    	a307 = a227[4];
    	a276 = a289;
    	a239 = a268;
    	a358 = a335;
    	a225 = a205[3];
    	a302 = a346[0];
    	a383 = a226[4];
    	a329 = "i";
    	a243 = ((((83 / 5) * -112) / 10) - 19987);
    	a99 = a26[(a172 - 5)];
    	}System.out.println("Y");
    } 
    if(((((a172 == 5) && ((((a237 == 4) && (a105.equals("g"))) && (a173.equals("g"))) && (a207.equals("f")))) && !a260) && (!a260 && (((a338 == 4) && ((cf && input.equals(inputs[1])) && (a75.equals("f")))) && (11 == a353[4]))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a234 = a372[5];
    	a239 = a242;
    	a243 = ((((99 + 27478) - -1223) * -1) / 10);
    	a228 = a292;
    	a173 = "f";
    	a383 = a226[2];
    	a269 = "h";
    	a202 = a217[4];
    	a81 = a167[((a211 * a172) - 8)];
    	a373 = true;
    	a276 = a253;
    	a265 = a303;
    	a224 = "i";
    	a329 = "h";
    	a307 = a227[0];
    	a312 = "f";
    	a339 = false;
    	a382 = ((((24 * 74) / 10) + -26453) - -32952);
    	a225 = a205[1];
    	a353 = a263;
    	a302 = a346[0];
    	a125 = a30[(a230 - -1)]; 
    	System.out.println("X");
    } 
    if((((a173.equals("g")) && ((a218.equals("f")) && (((a313.equals("f")) && (((-10 < a277) && (148 >= a277)) && (a224.equals("f")))) && input.equals(inputs[0])))) && (((a172 == 5) && ((a338 == 4) && ((cf && (a105.equals("g"))) && (a75.equals("f"))))) && (a218.equals("f"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	a94 += (a94 + 20) > a94 ? 2 : 0;
    	cf = false;
    	if((414 < a17 && (!a132 && a336))) {
    	a132 = false;
    	a44 = "f";
    	a75 = "e";
    	a77 = (a398 - 3); 
    	}else {
    	a75 = "i";
    	a146 = true;
    	a46 = a148;
    	a72 = (((39 - -27656) * 1) + 952);
    	}System.out.println("X");
    } 
    if((((a75.equals("f")) && ((input.equals(inputs[2]) && ((a320 == 7) && ((a270 == 11) && ((-10 < a277) && (148 >= a277))))) && (a105.equals("g")))) && (((((cf && (a173.equals("g"))) && (a172 == 5)) && (a313.equals("f"))) && (a256.equals("f"))) && (a218.equals("f"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a94 += (a94 + 20) > a94 ? 3 : 0;
    	cf = false;
    	a243 = (((40 / 5) - 183) + 8);
    	a240 = false;
    	a313 = "i";
    	a368 = false;
    	a228 = a264;
    	a201 = ((((a201 * 5) % 94) - -149) + -27);
    	a395 = false;
    	a218 = "e";
    	a207 = "e";
    	a392 = a208;
    	a256 = "f";
    	a211 = 6;
    	a370 = a311;
    	a398 = 16;
    	a202 = a217[3];
    	a378 = false;
    	a296 = a212;
    	a336 = false;
    	a357 = (((((a357 / 5) - 24290) / 5) * -1) / 10);
    	a237 = 8;
    	a75 = "g";
    	a310 = ((((((a310 % 77) - -206) + 1465) * 5) % 77) + 208);
    	a230 = 6;
    	a373 = false;
    	a276 = a250;
    	a57 = (a338 + 6);
    	a260 = false;
    	a239 = a299;
    	a206 = 8;
    	a353 = a263;
    	a359 = 6;
    	a386 = (((a386 / 5) / 5) + 311);
    	a324 = a232[4];
    	a339 = true;
    	a270 = 11;
    	a269 = "g";
    	a333 = ((((a333 % 14) - -163) + 1) - 1);
    	a235 = a216[2];
    	a320 = 7;
    	a312 = "f";
    	a225 = a205[7];
    	a158 = ((a57 - a172) + 1);
    	a277 = (((a277 - 19458) - 10123) + 51819);
    	a358 = a351;
    	a361 = "h";
    	a338 = 3; 
    	System.out.println("Z");
    } 
    if((((a206 == 5) && (((160 < a310) && (316 >= a310)) && ((((a172 == 5) && ((cf && (a75.equals("f"))) && input.equals(inputs[4]))) && (a320 == 7)) && !a368))) && ((a105.equals("g")) && ((a173.equals("g")) && (((-10 < a277) && (148 >= a277)) && !a339))))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a358 = a348;
    	a75 = "g";
    	a57 = ((a320 - a211) - -12);
    	a357 = (((a357 / 5) - -5861) / 5);
    	a353 = a241;
    	a40 = a83;
    	a269 = "i";
    	a378 = false;
    	a218 = "e";
    	a336 = false;
    	a277 = (((((a277 + 18746) + 1964) / 5) * -1) / 10);
    	a211 = 8;
    	a265 = a303;
    	a320 = 13;
    	a386 = (((((a386 % 61) - -103) - 24748) + 17440) - -7322);
    	a333 = ((((a333 / 5) - -16733) + -34455) - -29145);
    	a240 = false;
    	a25 = a76; 
    	System.out.println("T");
    } 
    if((((((-10 < a277) && (148 >= a277)) && input.equals(inputs[7])) && (28 == a370[0])) && (((a269.equals("f")) && (((((a173.equals("g")) && ((a75.equals("f")) && (cf && (a172 == 5)))) && (a105.equals("g"))) && (a237 == 4)) && !a395)) && !a339))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a51 += (a51 + 20) > a51 ? 4 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if(((a221 && a277 <=  -10) || !a221)) {
    	a173 = "e";
    	a139 = "h";
    	a130 = ((((35 / 5) + 25539) - -858) + -26061); 
    	}else {
    	a143 = "h";
    	a210 = true;
    	a75 = "g";
    	a57 = ((a270 / a206) + 9);
    	}System.out.println("Y");
    } 
    if(((((a286 == a294[1] && ((((a173.equals("g")) && (a256.equals("f"))) && a234 == a372[1]) && !a368)) && (a105.equals("g"))) && (28 == a370[0])) && ((a75.equals("f")) && (a225 == a205[1] && ((cf && input.equals(inputs[6])) && (a172 == 5)))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a178 += (a178 + 20) > a178 ? 3 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	if((!a123 && ((a269.equals("h")) || (55 == a197[5])))) {
    	a75 = "g";
    	a141 = a47[((a398 * a270) - 119)];
    	a57 = (a338 + 11);
    	a72 = ((((((58 * -18) / 10) + 12121) / 5) * -1) / 10); 
    	}else {
    	a57 = (a270 - -4);
    	a141 = a47[((a270 + a57) - 25)];
    	a75 = "g";
    	a72 = (((87 - 15128) - -1435) * 2);
    	}System.out.println("S");
    } 
    if(((((((a269.equals("f")) && (!a378 && ((a173.equals("g")) && (cf && (a172 == 5))))) && (a105.equals("g"))) && !a260) && (a361.equals("f"))) && ((((a75.equals("f")) && ((-199 < a201) && (-12 >= a201))) && input.equals(inputs[5])) && a324 == a232[1]))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a67 -= (a67 - 20) < a67 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	if(((a206 == 5) && (a333 <=  -47 || (31 == a137[0])))) {
    	a373 = true;
    	a302 = a346[2];
    	a276 = a253;
    	a202 = a217[0];
    	a224 = "g";
    	a382 = (((((40 + 16401) + -40966) / 5) * -1) / 10);
    	a277 = (((a277 + -27876) + -402) / 5);
    	a239 = a268;
    	a125 = a30[(a398 - 4)];
    	a353 = a241;
    	a234 = a372[0];
    	a307 = a227[0];
    	a173 = "f";
    	a243 = (((50 / 5) + 10770) / 5);
    	a329 = "i";
    	a129 = a92[((a172 * a172) - 23)]; 
    	}else {
    	a361 = "i";
    	a256 = "e";
    	a230 = 6;
    	a57 = (a172 - -5);
    	a276 = a250;
    	a237 = 10;
    	a378 = false;
    	a75 = "g";
    	a339 = true;
    	a386 = ((((a386 * 5) / 5) % 61) + 81);
    	a228 = a264;
    	a312 = "f";
    	a353 = a241;
    	a358 = a348;
    	a320 = 12;
    	a373 = true;
    	a211 = 5;
    	a313 = "f";
    	a218 = "e";
    	a207 = "f";
    	a324 = a232[1];
    	a300 = false;
    	a158 = (a398 + -7);
    	a398 = 11;
    	a338 = 4;
    	a206 = 7;
    	a357 = (((a357 + 23090) / 5) * 5);
    	a265 = a293;
    	a370 = a318;
    	a277 = (((((a277 + -15131) * 1) + 1531) % 78) + 113);
    	a234 = a372[3];
    	a336 = false;
    	a270 = 13;
    	a269 = "e";
    	a333 = ((((a333 - 5876) / 5) * 10) / 9);
    	a368 = false;
    	a392 = a257;
    	a240 = false;
    	a359 = 9;
    	a296 = a212;
    	a249 = ((((55 - 13111) * 2) - 2832) - -29258);
    	a395 = false;
    	a225 = a205[7];
    	a81 = a167[(a57 - 8)];
    	}System.out.println("X");
    } 
}
private  void calculateOutputm76(String input) {
    if(((((a224.equals("f")) && (a202 == a217[1] && ((a75.equals("f")) && ((103 == a276[1]) && (a173.equals("g")))))) && (a207.equals("f"))) && (!a368 && ((a172 == 7) && ((a230 == 4) && ((input.equals(inputs[6]) && cf) && a99 == a26[1])))))) {
    	a122 -= (a122 - 20) < a122 ? 2 : 0;
    	cf = false;
    	a202 = a217[(a270 - 11)];
    	a172 = (a320 - 6);
    	a373 = true;
    	a129 = a92[((a172 + a172) - 1)]; 
    	System.out.println("R");
    } 
}
private  void calculateOutputm77(String input) {
    if(((((!a395 && (a173.equals("g"))) && a235 == a216[1]) && (a269.equals("f"))) && (((input.equals(inputs[7]) && ((a256.equals("f")) && ((((a172 == 7) && cf) && (a75.equals("f"))) && a99 == a26[2]))) && a234 == a372[1]) && (43 == a392[3])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a169 -= (a169 - 20) < a169 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	a75 = "h";
    	a249 = (((a249 - -28201) - -1332) * 1);
    	a239 = a299;
    	a398 = 15;
    	a134 = false;
    	a256 = "f";
    	a240 = false;
    	a310 = (((a310 / 5) + 247) - -4);
    	a370 = a285;
    	a357 = ((((a357 * 5) * 10) / -9) / 5);
    	a206 = 7;
    	a224 = "i";
    	a295 = a366[(a172 - 3)];
    	a307 = a227[4];
    	a218 = "g";
    	a269 = "g";
    	a225 = a205[4];
    	a201 = ((((a201 + 3041) - -26239) % 93) + -152);
    	a359 = 6;
    	a339 = false;
    	a202 = a217[3];
    	a320 = 12;
    	a333 = (((a333 + 28109) - -354) * 1);
    	a313 = "i";
    	a237 = 5;
    	a312 = "h";
    	a338 = 10;
    	a383 = a226[2];
    	a296 = a362;
    	a260 = false;
    	a184 = a157[a172]; 
    	System.out.println("X");
    } 
    if((((a237 == 4) && ((a359 == 4) && (((!a339 && ((-199 < a201) && (-12 >= a201))) && (43 == a392[3])) && (a172 == 7)))) && (((a173.equals("g")) && ((((a75.equals("f")) && cf) && a99 == a26[2]) && ((-199 < a201) && (-12 >= a201)))) && input.equals(inputs[3])))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a174 += (a174 + 20) > a174 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	cf = false;
    	a382 = (((a382 + 29703) * 1) - -233);
    	a207 = "i";
    	a395 = false;
    	a277 = (((((a277 % 78) - -70) - 2) + -10515) - -10515);
    	a302 = a346[7];
    	a211 = 4;
    	a386 = (((a386 - 27105) + -2525) - -54634);
    	a201 = ((((a201 * 5) % 93) - 14) + 2);
    	a320 = 12;
    	a276 = a289;
    	a336 = false;
    	a137 = a189;
    	a300 = false;
    	a359 = 3;
    	a286 = a294[5];
    	a370 = a318;
    	a235 = a216[1];
    	a243 = (((a243 - 7493) - -15513) * 3);
    	a269 = "i";
    	a260 = false;
    	a338 = 9;
    	a368 = false;
    	a218 = "i";
    	a225 = a205[4];
    	a312 = "f";
    	a357 = (((a357 - -24262) - -4838) - -476);
    	a307 = a227[0];
    	a240 = false;
    	a339 = true;
    	a237 = 3;
    	a296 = a212;
    	a234 = a372[1];
    	a154 = (a230 - -6);
    	a265 = a293;
    	a324 = a232[1];
    	a373 = true;
    	a333 = (((a333 + 22856) * 1) - 24949);
    	a228 = a229;
    	a75 = "g";
    	a224 = "f";
    	a57 = (a230 + 9);
    	a398 = 13;
    	a256 = "i";
    	a239 = a299;
    	a202 = a217[3];
    	a310 = (((a310 * 5) - 3196) / 5);
    	a358 = a351;
    	a361 = "i";
    	a206 = 10;
    	a329 = "i";
    	a270 = 15;
    	a230 = 10; 
    	System.out.println("T");
    } 
    if(((((a269.equals("f")) && ((((a172 == 7) && (cf && (a75.equals("f")))) && (a269.equals("f"))) && ((-179 < a243) && (-156 >= a243)))) && input.equals(inputs[1])) && (a99 == a26[2] && ((a173.equals("g")) && ((!a339 && (40 == a239[3])) && ((-199 < a201) && (-12 >= a201))))))) {
    	a174 += (a174 + 20) > a174 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	if((!(3 == a353[2]) || (!(a139.equals("f")) && ((!(a324 == 8) && 121 < a170) && !(5 == a25[0]))))) {
    	a57 = (a172 + 9);
    	a75 = "g";
    	a278 = a326[((a211 * a320) - 14)];
    	a19 = (((((36 / 5) * 439) / 10) / 5) - -272); 
    	}else {
    	a378 = false;
    	a383 = a226[0];
    	a359 = 6;
    	a313 = "g";
    	a361 = "h";
    	a240 = true;
    	a207 = "g";
    	a357 = (((((a357 + -26727) % 38) + 9) - 13781) + 13783);
    	a398 = 14;
    	a296 = a362;
    	a230 = 10;
    	a339 = false;
    	a370 = a311;
    	a243 = ((((a243 / 5) / 5) * 9) / 10);
    	a235 = a216[2];
    	a256 = "f";
    	a206 = 7;
    	a224 = "i";
    	a171 = "i";
    	a201 = (((a201 - 17744) - 9967) * 1);
    	a395 = false;
    	a134 = false;
    	a286 = a294[7];
    	a277 = (((a277 + 169) - 567) - -560);
    	a269 = "i";
    	a307 = a227[6];
    	a386 = (((a386 - 2691) - 19378) / 5);
    	a392 = a208;
    	a249 = ((((a249 * 10) / 3) / 5) + 27819);
    	a239 = a299;
    	a373 = true;
    	a300 = false;
    	a202 = a217[3];
    	a265 = a293;
    	a260 = true;
    	a218 = "g";
    	a310 = ((((a310 % 20) - -335) + 27625) - 27627);
    	a237 = 9;
    	a211 = 3;
    	a270 = 17;
    	a382 = ((((a382 - 8270) - -35983) * -1) / 10);
    	a234 = a372[4];
    	a333 = ((((a333 / 5) % 14) - -162) + 1);
    	a353 = a399;
    	a302 = a346[0];
    	a75 = "h";
    	a295 = a366[(a338 + -2)];
    	a312 = "i";
    	a368 = true;
    	a324 = a232[2];
    	a225 = a205[5];
    	a329 = "h";
    	a320 = 13;
    	a338 = 7;
    	}System.out.println("V");
    } 
    if(((((((77 < a386) && (201 >= a386)) && ((((cf && a99 == a26[2]) && (a172 == 7)) && (a173.equals("g"))) && (a75.equals("f")))) && a383 == a226[1]) && (53 == a265[5])) && (((!a395 && a235 == a216[1]) && input.equals(inputs[2])) && (89 == a296[5])))) {
    	a120 -= (a120 - 20) < a120 ? 2 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if((((-199 < a201) && (-12 >= a201)) && (a200 == 6))) {
    	a172 = 1;
    	a225 = a205[7];
    	a383 = a226[5];
    	a336 = true;
    	a239 = a242;
    	a211 = 4;
    	a324 = a232[5];
    	a276 = a253;
    	a234 = a372[7];
    	a353 = a263;
    	a307 = a227[5];
    	a202 = a217[5];
    	a277 = (((((a277 + -13325) * -1) / 10) * 10) / 9);
    	a129 = a92[(a172 / a172)]; 
    	}else {
    	a173 = "e";
    	a361 = "h";
    	a256 = "h";
    	a201 = (((a201 - 20548) - -49572) - 28836);
    	a307 = a227[4];
    	a202 = a217[6];
    	a336 = false;
    	a225 = a205[0];
    	a329 = "i";
    	a265 = a303;
    	a300 = true;
    	a191 = a37[(a172 + -1)];
    	a333 = (((((a333 % 14) - -162) + -1) + 16612) - 16609);
    	a249 = (((((a249 * 5) * 10) / 9) / 5) + 24344);
    	a398 = 12;
    	a358 = a335;
    	a353 = a399;
    	a228 = a292;
    	a130 = (((38 / -5) * 5) / 5);
    	}System.out.println("X");
    } 
    if((((!a378 && (((152 < a249) && (355 >= a249)) && (!a378 && ((160 < a310) && (316 >= a310))))) && (a173.equals("g"))) && (((a75.equals("f")) && ((((160 < a310) && (316 >= a310)) && ((a99 == a26[2] && cf) && (a172 == 7))) && !a260)) && input.equals(inputs[4])))) {
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a202 = a217[7];
    	a312 = "i";
    	a333 = ((((a333 % 96) + 49) - 15076) - -15077);
    	a310 = ((((a310 - 11383) * -1) / 10) * 5);
    	a296 = a362;
    	a339 = false;
    	a75 = "h";
    	a359 = 6;
    	a320 = 10;
    	a134 = false;
    	a184 = a157[((a172 / a172) - -2)];
    	a218 = "f";
    	a224 = "g";
    	a378 = false;
    	a201 = ((((a201 * 5) % 94) + 137) + 27);
    	a269 = "i";
    	a207 = "i";
    	a295 = a366[((a398 - a338) + -3)]; 
    	System.out.println("Y");
    } 
    if(((input.equals(inputs[9]) && ((((-188 < a357) && (-57 >= a357)) && a324 == a232[1]) && (a172 == 7))) && ((a398 == 11) && ((a320 == 7) && ((a224.equals("f")) && (!a260 && ((a75.equals("f")) && ((a99 == a26[2] && cf) && (a173.equals("g")))))))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a169 -= (a169 - 20) < a169 ? 4 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	cf = false;
    	a73 = (((65 / 5) - 19868) - 4330);
    	a146 = false;
    	a75 = "i";
    	a39 = a152; 
    	System.out.println("T");
    } 
    if((((((a224.equals("f")) && ((((a173.equals("g")) && (cf && a99 == a26[2])) && !a378) && (a312.equals("f")))) && (a172 == 7)) && (a75.equals("f"))) && (((-65 < a382) && (-39 >= a382)) && ((a207.equals("f")) && ((a207.equals("f")) && input.equals(inputs[5])))))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a162 -= (a162 - 20) < a162 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	cf = false;
    	if(a130 <=  134) {
    	a172 = ((a206 - a206) + 1);
    	a129 = a92[((a230 / a172) - 1)]; 
    	}else {
    	a75 = "e";
    	a132 = false;
    	a77 = (a172 + 3);
    	a1 = a87[(a77 - 8)];
    	}System.out.println("Y");
    } 
    if(((((((-10 < a277) && (148 >= a277)) && (!a300 && ((cf && (a173.equals("g"))) && (a172 == 7)))) && (a75.equals("f"))) && input.equals(inputs[8])) && ((a324 == a232[1] && (a99 == a26[2] && ((a361.equals("f")) && (a398 == 11)))) && (a361.equals("f"))))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a5 -= (a5 - 20) < a5 ? 2 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a211 = 4;
    	a329 = "h";
    	a228 = a229;
    	a154 = (a172 - -8);
    	a256 = "f";
    	a57 = (a237 - -9);
    	a339 = true;
    	a312 = "i";
    	a75 = "g";
    	a368 = false;
    	a333 = ((((a333 + -13429) - -18367) * 10) / -9);
    	a307 = a227[1];
    	a300 = false;
    	a137 = a189;
    	a269 = "i";
    	a296 = a384;
    	a357 = ((((((a357 % 65) + -63) + -46) * 5) % 65) - 99);
    	a218 = "i";
    	a382 = (((((a382 % 12) - 43) * 10) / 9) - 3);
    	a239 = a299;
    	a324 = a232[5];
    	a338 = 7;
    	a265 = a376;
    	a235 = a216[7];
    	a386 = (((a386 / -5) - -28877) / -5);
    	a206 = 6;
    	a276 = a253;
    	a361 = "i";
    	a320 = 13;
    	a359 = 8;
    	a240 = false;
    	a234 = a372[3];
    	a336 = false;
    	a249 = ((((a249 * 5) * 5) - 13903) - -32825);
    	a313 = "g";
    	a237 = 10; 
    	System.out.println("R");
    } 
    if((((a256.equals("f")) && (((-47 < a333) && (147 >= a333)) && ((((a172 == 7) && (a230 == 4)) && (a173.equals("g"))) && (a211 == 2)))) && ((a312.equals("f")) && (((a75.equals("f")) && (input.equals(inputs[6]) && (a99 == a26[2] && cf))) && (a211 == 2))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a122 -= (a122 - 20) < a122 ? 4 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	if((!(a191 == a37[2]) && ((((a234 == 11) || a324 == a232[6]) && !a132) && !(a295 == 13)))) {
    	a44 = "f";
    	a132 = false;
    	a75 = "e";
    	a77 = (a398 + -3); 
    	}else {
    	a296 = a384;
    	a91 = (a230 - -4);
    	a277 = (((((a277 * 5) - -19723) - 35236) * -1) / 10);
    	a228 = a229;
    	a333 = (((a333 / 5) - 20) + 22);
    	a173 = "h";
    	a225 = a205[4];
    	a324 = a232[5];
    	a312 = "i";
    	a202 = a217[3];
    	a239 = a299;
    	a373 = true;
    	a265 = a303;
    	a336 = true;
    	a358 = a335;
    	a353 = a399;
    	a361 = "f";
    	a307 = a227[5];
    	a276 = a253;
    	a102 = a127[((a206 + a91) - 8)];
    	}System.out.println("T");
    } 
    if((((a172 == 7) && (input.equals(inputs[0]) && (28 == a370[0]))) && (((a173.equals("g")) && ((((a320 == 7) && ((((a75.equals("f")) && cf) && a99 == a26[2]) && (a329.equals("f")))) && a383 == a226[1]) && ((-47 < a333) && (147 >= a333)))) && (a206 == 5)))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a206 = 4;
    	a286 = a294[1];
    	a57 = (a172 + 5);
    	a207 = "i";
    	a269 = "f";
    	a382 = (((a382 / 5) / 5) + -1971);
    	a224 = "i";
    	a339 = true;
    	a270 = 13;
    	a237 = 9;
    	a265 = a303;
    	a235 = a216[7];
    	a81 = a167[(a57 - 10)];
    	a329 = "e";
    	a230 = 6;
    	a249 = ((((a249 * 5) * 5) * 10) / 9);
    	a218 = "h";
    	a383 = a226[5];
    	a75 = "g";
    	a277 = (((a277 - -28711) - 57611) + -267);
    	a276 = a253;
    	a240 = false;
    	a324 = a232[1];
    	a395 = false;
    	a234 = a372[6];
    	a373 = true;
    	a310 = (((a310 * 5) + 11669) / 5);
    	a359 = 9;
    	a228 = a264;
    	a243 = ((((a243 + -26870) - -8504) % 11) + -162);
    	a358 = a335;
    	a386 = (((((a386 * 5) + -3419) - 6339) * -1) / 10);
    	a320 = 12;
    	a211 = 4;
    	a336 = false;
    	a392 = a257;
    	a225 = a205[0];
    	a357 = (((a357 * 5) + 997) * 5);
    	a312 = "i";
    	a378 = false;
    	a256 = "f";
    	a338 = 9;
    	a202 = a217[3];
    	a398 = 16;
    	a368 = false;
    	a201 = (((a201 / 5) + -34) + 8);
    	a307 = a227[3];
    	a300 = false;
    	a296 = a212;
    	a333 = ((((a333 + -11249) - 12864) * 10) / 9);
    	a302 = a346[5];
    	a370 = a318;
    	a239 = a242;
    	a158 = (a172 - -4); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm78(String input) {
    if((((a75.equals("f")) && (53 == a265[5])) && ((a237 == 4) && (((a398 == 11) && (((((-10 < a277) && (148 >= a277)) && ((input.equals(inputs[9]) && (cf && (a173.equals("g")))) && (a172 == 7))) && a99 == a26[5]) && ((77 < a386) && (201 >= a386)))) && !a260)))) {
    	cf = false;
    	if((a141 == a47[4] || !(a143.equals("g")))) {
    	a302 = a346[(a320 + -5)];
    	a218 = "g";
    	a383 = a226[((a206 + a398) - 14)];
    	a173 = "f";
    	a129 = a92[((a172 / a270) - -7)];
    	a256 = "g";
    	a358 = a335;
    	a398 = (a359 - -8);
    	a353 = a399;
    	a333 = ((((((a333 * a310) % 14999) % 14) - -162) + -1) - 0);
    	a125 = a30[(a359 + 3)];
    	a368 = true;
    	a202 = a217[(a320 + -5)]; 
    	}else {
    	a260 = true;
    	a310 = (((((a357 * a243) % 14999) - 27594) - 2247) - -1946);
    	a256 = "g";
    	a300 = true;
    	a134 = false;
    	a295 = a366[((a230 / a237) - 1)];
    	a228 = a229;
    	a353 = a263;
    	a358 = a348;
    	a276 = a253;
    	a201 = (((((((a310 * a277) % 14999) * 2) - 1) * 1) % 14900) - 15098);
    	a320 = (a398 + -5);
    	a269 = "e";
    	a373 = true;
    	a339 = true;
    	a196 = ((a359 / a206) - -17);
    	a336 = true;
    	a359 = (a320 - 3);
    	a383 = a226[((a211 * a398) + -20)];
    	a338 = ((a398 * a398) - 118);
    	a202 = a217[(a211 / a398)];
    	a368 = true;
    	a302 = a346[(a398 - 11)];
    	a357 = ((((((a357 * a310) % 14999) / 5) % 38) + -17) + -1);
    	a75 = "h";
    	a265 = a303;
    	a230 = ((a398 * a398) - 118);
    	a382 = ((((a243 * a277) - 3503) + -86) - 51);
    	a392 = a208;
    	a240 = true;
    	a324 = a232[(a338 - 1)];
    	a224 = "e";
    	a237 = (a398 + -6);
    	a218 = "e";
    	a225 = a205[(a172 - 7)];
    	a312 = "e";
    	a333 = (((((a333 * a386) + 27) % 14976) - 15022) + -3);
    	a277 = ((((((a310 * a310) % 14999) - 11180) % 95) - -243) * 1);
    	a395 = true;
    	a207 = "e";
    	a398 = (a211 + 10);
    	a361 = "e";
    	a211 = ((a270 - a270) - -3);
    	a370 = a318;
    	a329 = "e";
    	a234 = a372[(a320 / a270)];
    	}System.out.println("Y");
    } 
    if(((a302 == a346[1] && ((a211 == 2) && ((input.equals(inputs[1]) && ((a75.equals("f")) && (a224.equals("f")))) && (a256.equals("f"))))) && ((a224.equals("f")) && ((a173.equals("g")) && ((76 == a358[5]) && ((a172 == 7) && (cf && a99 == a26[5]))))))) {
    	a161 -= (a161 - 20) < a161 ? 2 : 0;
    	cf = false;
    	a172 = (a320 - 3);
    	a274 = a290; 
    	System.out.println("V");
    } 
}
private  void calculateOutputm79(String input) {
    if((((a313.equals("f")) && ((!a368 && ((a172 == 7) && ((a173.equals("g")) && (((a75.equals("f")) && cf) && input.equals(inputs[2]))))) && !a378)) && (((!a395 && (76 == a358[5])) && a99 == a26[6]) && !a240))) {
    	cf = false;
    	a225 = a205[(a359 - 4)];
    	a173 = "f";
    	a224 = "e";
    	a234 = a372[(a398 - 9)];
    	a125 = a30[(a172 - a172)];
    	a277 = ((((((a277 * a382) % 95) + 244) + 1) - -28385) - 28386);
    	a383 = a226[(a338 - 4)];
    	a361 = "f";
    	a54 = ((((35 / 5) + 18222) * 10) / 9); 
    	System.out.println("V");
    } 
}
private  void calculateOutputm5(String input) {
    if(((!a395 && ((a256.equals("f")) && a286 == a294[1])) && (((((77 < a386) && (201 >= a386)) && (cf && (a172 == 1))) && ((152 < a249) && (355 >= a249))) && (a320 == 7)))) {
    	if((((a270 == 11) && (((-65 < a382) && (-39 >= a382)) && ((-179 < a243) && (-156 >= a243)))) && ((a270 == 11) && ((a224.equals("f")) && (a235 == a216[1] && (cf && a129 == a92[1])))))) {
    		calculateOutputm65(input);
    	} 
    	if(((((89 == a296[5]) && (((a361.equals("f")) && !a336) && (a320 == 7))) && (a206 == 5)) && (!a336 && (cf && a129 == a92[2])))) {
    		calculateOutputm66(input);
    	} 
    	if(((a129 == a92[7] && cf) && ((a329.equals("f")) && (((89 == a296[5]) && ((103 == a276[1]) && (((152 < a249) && (355 >= a249)) && a324 == a232[1]))) && ((-47 < a333) && (147 >= a333)))))) {
    		calculateOutputm68(input);
    	} 
    } 
    if((((a269.equals("f")) && !a260) && ((((53 == a265[5]) && (((a172 == 5) && cf) && (28 == a370[0]))) && !a336) && (a361.equals("f"))))) {
    	if((((a206 == 5) && ((50 == a228[0]) && ((28 == a370[0]) && (a270 == 11)))) && ((((a105.equals("e")) && cf) && (40 == a239[3])) && (a207.equals("f"))))) {
    		calculateOutputm72(input);
    	} 
    	if(((a324 == a232[1] && ((a286 == a294[1] && ((((a105.equals("g")) && cf) && ((160 < a310) && (316 >= a310))) && (a359 == 4))) && (a320 == 7))) && !a300)) {
    		calculateOutputm73(input);
    	} 
    } 
    if((((a398 == 11) && (a359 == 4)) && (!a378 && (((a361.equals("f")) && ((cf && (a172 == 7)) && a202 == a217[1])) && ((-47 < a333) && (147 >= a333)))))) {
    	if((((103 == a276[1]) && (a302 == a346[1] && (((a99 == a26[1] && cf) && (a270 == 11)) && (a359 == 4)))) && ((a398 == 11) && !a373))) {
    		calculateOutputm76(input);
    	} 
    	if(((a383 == a226[1] && (((cf && a99 == a26[2]) && ((152 < a249) && (355 >= a249))) && (43 == a392[3]))) && ((!a260 && (a207.equals("f"))) && !a260))) {
    		calculateOutputm77(input);
    	} 
    	if(((((-179 < a243) && (-156 >= a243)) && ((cf && a99 == a26[5]) && a383 == a226[1])) && (((a302 == a346[1] && (a312.equals("f"))) && ((-47 < a333) && (147 >= a333))) && (11 == a353[4])))) {
    		calculateOutputm78(input);
    	} 
    	if((((((((-10 < a277) && (148 >= a277)) && !a339) && !a395) && (43 == a392[3])) && (a359 == 4)) && ((cf && a99 == a26[6]) && a234 == a372[1]))) {
    		calculateOutputm79(input);
    	} 
    } 
}
private  void calculateOutputm81(String input) {
    if((((a173.equals("h")) && (((cf && input.equals(inputs[8])) && (a75.equals("f"))) && (a230 == 4))) && (((((a91 == 7) && ((a59.equals("e")) && ((a359 == 4) && !a240))) && !a373) && a383 == a226[1]) && !a395))) {
    	a5 -= (a5 - 20) < a5 ? 1 : 0;
    	cf = false;
    	a276 = a253;
    	a225 = a205[(a230 - 4)];
    	a357 = (((((((a333 * a333) % 14999) + -28967) / 5) - 7297) % 38) - -13);
    	a211 = (a206 + -4);
    	a312 = "e";
    	a206 = a230;
    	a320 = (a237 + a237);
    	a313 = "e";
    	a201 = ((((((a201 * a357) / 5) % 94) + 84) + -23012) + 23011);
    	a359 = ((a230 - a230) + 3);
    	a300 = true;
    	a338 = ((a91 * a320) - 53);
    	a218 = "g";
    	a173 = "e";
    	a398 = (a237 + 8);
    	a324 = a232[(a270 + -9)];
    	a378 = true;
    	a361 = "g";
    	a260 = true;
    	a235 = a216[((a320 - a91) - 1)];
    	a57 = ((a237 - a270) - -18);
    	a368 = true;
    	a240 = true;
    	a339 = true;
    	a270 = (a237 + 8);
    	a265 = a376;
    	a296 = a212;
    	a230 = (a398 + -7);
    	a75 = "g";
    	a228 = a229;
    	a234 = a372[(a237 + -2)];
    	a373 = false;
    	a143 = "g";
    	a237 = 3; 
    	System.out.println("X");
    } 
    if(((a225 == a205[1] && (((a75.equals("f")) && (!a300 && (a173.equals("h")))) && !a260)) && ((a329.equals("f")) && (!a378 && ((a91 == 7) && ((input.equals(inputs[4]) && (cf && (a59.equals("e")))) && a302 == a346[1])))))) {
    	a169 -= (a169 - 20) < a169 ? 2 : 0;
    	cf = false;
    	a57 = ((a91 * a91) + -36);
    	a324 = a232[((a91 + a91) + -12)];
    	a137 = a189;
    	a300 = true;
    	a154 = ((a57 * a206) + -50);
    	a339 = false;
    	a218 = "g";
    	a75 = "g";
    	a235 = a216[(a359 - 4)];
    	a269 = "g";
    	a329 = "g";
    	a225 = a205[((a206 + a211) + -7)];
    	a357 = ((((((a357 * a310) % 14999) % 38) + -17) + -2) - 0);
    	a240 = true;
    	a211 = (a230 + -1);
    	a338 = a206;
    	a361 = "e";
    	a359 = (a237 + 1);
    	a373 = true;
    	a206 = ((a237 * a91) - 24);
    	a313 = "e";
    	a237 = (a57 + -10); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm82(String input) {
    if(((((77 < a386) && (201 >= a386)) && ((((a173.equals("h")) && (a269.equals("f"))) && (a91 == 8)) && a307 == a227[1])) && (((160 < a310) && (316 >= a310)) && (((((a75.equals("f")) && (a102 == a127[5] && cf)) && input.equals(inputs[5])) && (76 == a358[5])) && ((160 < a310) && (316 >= a310)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a162 -= (a162 - 20) < a162 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	if((!(a99 == a26[3]) && (a237 == 6))) {
    	a75 = "g";
    	a240 = false;
    	a269 = "e";
    	a57 = ((a270 + a359) + 2);
    	a320 = 13;
    	a336 = false;
    	a40 = a83;
    	a378 = false;
    	a353 = a241;
    	a302 = a346[6];
    	a211 = 7;
    	a358 = a348;
    	a307 = a227[1];
    	a357 = ((((a357 % 65) + -108) + 40) + 7);
    	a386 = (((((a386 - 7262) % 61) - -162) - -21898) - 21878);
    	a218 = "e";
    	a382 = ((((((a382 % 12) + -41) + -6) / 5) * 49) / 10);
    	a25 = a76; 
    	}else {
    	a139 = "e";
    	a173 = "e";
    	a130 = (((9 - 24174) * 1) + 24580);
    	}System.out.println("R");
    } 
    if(((((77 < a386) && (201 >= a386)) && ((((a102 == a127[5] && (((a91 == 8) && (cf && (a75.equals("f")))) && (11 == a353[4]))) && input.equals(inputs[8])) && a286 == a294[1]) && a307 == a227[1])) && ((((-199 < a201) && (-12 >= a201)) && (28 == a370[0])) && (a173.equals("h"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	a161 -= (a161 - 20) < a161 ? 3 : 0;
    	cf = false;
    	a75 = "h";
    	a134 = true;
    	a129 = a92[((a211 * a91) + -16)];
    	a196 = (a230 - -6); 
    	System.out.println("T");
    } 
    if((((!a395 && ((input.equals(inputs[4]) && ((a173.equals("h")) && (((a75.equals("f")) && cf) && a102 == a127[5]))) && (a237 == 4))) && ((152 < a249) && (355 >= a249))) && ((((152 < a249) && (355 >= a249)) && (!a368 && (a359 == 4))) && (a91 == 8)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a35 -= (a35 - 20) < a35 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	a57 = ((a91 + a91) + -1);
    	a72 = ((((((a243 * a243) % 14999) % 101) + -9) - -8) / 5);
    	a75 = "g";
    	a36 = ((((((a72 * a72) % 14) + 292) * 5) % 14) + 293); 
    	System.out.println("R");
    } 
    if(((((-65 < a382) && (-39 >= a382)) && (input.equals(inputs[3]) && (cf && a102 == a127[5]))) && ((43 == a392[3]) && (((((a234 == a372[1] && ((43 == a392[3]) && a234 == a372[1])) && (76 == a358[5])) && (a91 == 8)) && (a173.equals("h"))) && (a75.equals("f")))))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a5 -= (a5 - 20) < a5 ? 4 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	if(((a59.equals("f")) || (a333 <=  -47 || a81 == a167[5]))) {
    	a75 = "h";
    	a134 = true;
    	a196 = (a320 - -5);
    	a54 = (((54 - 25552) * 1) * 1); 
    	}else {
    	a75 = "h";
    	a295 = a366[(a338 - 1)];
    	a134 = false;
    	a170 = ((((52 / 5) * -5) + 26377) + -45037);
    	}System.out.println("R");
    } 
    if(((((a234 == a372[1] && a286 == a294[1]) && (a91 == 8)) && input.equals(inputs[1])) && ((a237 == 4) && (((50 == a228[0]) && (((11 == a353[4]) && ((cf && (a173.equals("h"))) && a102 == a127[5])) && (a75.equals("f")))) && (a224.equals("f")))))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a98 += (a98 + 20) > a98 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a75 = "g";
    	a71 = a175[(a91 - 4)];
    	a134 = false;
    	a57 = ((a359 / a398) - -14); 
    	System.out.println("Y");
    } 
    if((((((((input.equals(inputs[7]) && (a102 == a127[5] && cf)) && !a378) && ((160 < a310) && (316 >= a310))) && (a173.equals("h"))) && (a237 == 4)) && (a269.equals("f"))) && (((50 == a228[0]) && ((43 == a392[3]) && (a91 == 8))) && (a75.equals("f"))))) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a370 = a318;
    	a302 = a346[5];
    	a243 = (((((a243 / -5) * 10) / 9) * 10) / 9);
    	a260 = true;
    	a235 = a216[7];
    	a353 = a263;
    	a339 = true;
    	a307 = a227[4];
    	a91 = ((a206 / a338) - -11);
    	a277 = ((((82 * 5) - 387) + 5579) + -5519);
    	a276 = a253;
    	a359 = 9;
    	a398 = 15;
    	a358 = a335;
    	a230 = 3;
    	a324 = a232[0];
    	a296 = a362;
    	a265 = a303;
    	a202 = a217[3];
    	a329 = "g";
    	a239 = a242;
    	a286 = a294[3];
    	a312 = "e";
    	a373 = true;
    	a142 = false;
    	a228 = a264;
    	a333 = ((((((26 * 69) / 10) * 5) - 6712) * -1) / 10);
    	a201 = (((a201 + 28572) * 1) + -25774);
    	a336 = true;
    	a382 = (((a382 / 5) + 4152) + 10014);
    	a207 = "i";
    	a206 = 4; 
    	System.out.println("R");
    } 
    if((((!a336 && (((a338 == 4) && ((a173.equals("h")) && (((cf && a102 == a127[5]) && (a75.equals("f"))) && (a91 == 8)))) && a286 == a294[1])) && !a368) && (input.equals(inputs[6]) && (a383 == a226[1] && (a207.equals("f")))))) {
    	a120 -= (a120 - 20) < a120 ? 4 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	if((!(a234 == 9) && ((a323 == 8) && (!(a398 == 13) && (a323 == 10))))) {
    	a237 = 3;
    	a383 = a226[0];
    	a75 = "g";
    	a218 = "h";
    	a357 = ((((a357 - -18367) * 10) / 9) * 1);
    	a370 = a318;
    	a313 = "f";
    	a329 = "i";
    	a310 = (((a310 - -15663) / 5) + 17785);
    	a234 = a372[1];
    	a300 = false;
    	a230 = 10;
    	a378 = false;
    	a324 = a232[3];
    	a339 = true;
    	a392 = a257;
    	a312 = "f";
    	a158 = ((a338 + a398) + -10);
    	a224 = "f";
    	a382 = (((a382 + -9241) - -20470) + 587);
    	a249 = ((((a249 / 5) + -2617) * 10) / 9);
    	a240 = false;
    	a359 = 9;
    	a57 = (a211 + 8);
    	a256 = "e";
    	a358 = a348;
    	a200 = a115[((a206 + a158) + -8)];
    	a228 = a264;
    	a269 = "f";
    	a395 = false;
    	a270 = 16;
    	a336 = false;
    	a353 = a241;
    	a276 = a289;
    	a225 = a205[7];
    	a302 = a346[3];
    	a201 = ((((a201 % 93) - 23) - -29514) + -29586);
    	a243 = (((((a243 * 10) / 8) / 5) * 46) / 10);
    	a206 = 8;
    	a235 = a216[3];
    	a211 = 2;
    	a307 = a227[5];
    	a286 = a294[4];
    	a265 = a376;
    	a338 = 10;
    	a320 = 13;
    	a207 = "i";
    	a368 = false;
    	a398 = 13; 
    	}else {
    	a353 = a263;
    	a324 = a232[5];
    	a382 = (((a382 + 5870) / 5) + -1218);
    	a211 = 7;
    	a313 = "e";
    	a307 = a227[1];
    	a370 = a285;
    	a392 = a257;
    	a260 = false;
    	a359 = 10;
    	a358 = a351;
    	a225 = a205[1];
    	a277 = ((((7 - -11700) * 10) / -9) - -8132);
    	a302 = a346[3];
    	a57 = (a91 + 5);
    	a237 = 10;
    	a249 = (((a249 / 5) - -12867) * -2);
    	a243 = ((((a243 - 26456) * 10) / 9) + -255);
    	a368 = false;
    	a206 = 7;
    	a336 = false;
    	a357 = ((((a357 * 10) / -9) + 19942) - -4815);
    	a339 = true;
    	a230 = 8;
    	a207 = "f";
    	a395 = false;
    	a312 = "f";
    	a269 = "e";
    	a240 = false;
    	a286 = a294[7];
    	a234 = a372[1];
    	a256 = "e";
    	a386 = ((((a386 / 5) + 116) * 9) / 10);
    	a398 = 13;
    	a300 = true;
    	a270 = 16;
    	a75 = "g";
    	a228 = a264;
    	a224 = "e";
    	a218 = "h";
    	a320 = 11;
    	a338 = 8;
    	a154 = (a91 - -8);
    	a235 = a216[5];
    	a383 = a226[6];
    	a329 = "e";
    	a137 = a189;
    	}System.out.println("R");
    } 
    if((((((a329.equals("f")) && (a256.equals("f"))) && ((160 < a310) && (316 >= a310))) && (a269.equals("f"))) && (a286 == a294[1] && ((((((a75.equals("f")) && (a102 == a127[5] && cf)) && (a91 == 8)) && (a173.equals("h"))) && input.equals(inputs[9])) && (76 == a358[5]))))) {
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	if((cf || !(a206 == 7))) {
    	a265 = a303;
    	a276 = a253;
    	a333 = ((((54 + -9226) - -9324) - -5090) + -5087);
    	a378 = false;
    	a361 = "i";
    	a107 = "e";
    	a329 = "h";
    	a202 = a217[4];
    	a277 = (((((36 * 5) * 10) / 5) - 28137) + 56674);
    	a336 = false;
    	a324 = a232[4];
    	a239 = a242;
    	a373 = true;
    	a312 = "e";
    	a173 = "f";
    	a125 = a30[(a230 + 2)]; 
    	}else {
    	a136 = (a270 + -6);
    	a132 = false;
    	a75 = "e";
    	a77 = (a237 + 1);
    	}System.out.println("P");
    } 
    if(((a383 == a226[1] && ((input.equals(inputs[0]) && ((((a173.equals("h")) && (cf && a102 == a127[5])) && (a211 == 2)) && (a75.equals("f")))) && (a329.equals("f")))) && ((((a91 == 8) && a286 == a294[1]) && (a269.equals("f"))) && !a339))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a169 -= (a169 - 20) < a169 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	if((!(92 == a296[2]) || ((a206 == 8) && (a130 <=  134 || a191 == a37[4])))) {
    	a75 = "e";
    	a129 = a92[(a320 - 4)];
    	a132 = false;
    	a77 = (a398 + 1); 
    	}else {
    	a75 = "g";
    	a81 = a167[((a270 + a359) + -10)];
    	a57 = (a91 + 4);
    	a170 = ((((41 + 5947) + -5953) / 5) - -55);
    	}System.out.println("T");
    } 
    if((((!a300 && (((a75.equals("f")) && ((a173.equals("h")) && (a102 == a127[5] && (input.equals(inputs[2]) && (cf && (a91 == 8)))))) && !a336)) && a225 == a205[1]) && ((a383 == a226[1] && !a260) && (a256.equals("f"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	a94 += (a94 + 20) > a94 ? 1 : 0;
    	cf = false;
    	if(((a286 == 4) && !(a107.equals("f")))) {
    	a206 = 10;
    	a211 = 8;
    	a329 = "h";
    	a386 = ((((a386 % 61) - -202) / 5) - -195);
    	a398 = 15;
    	a201 = ((((((a201 - -17933) % 93) - 125) * 5) % 93) - 68);
    	a218 = "h";
    	a240 = false;
    	a277 = ((((92 * 37) / 10) - -15356) * 1);
    	a336 = false;
    	a75 = "h";
    	a392 = a304;
    	a269 = "f";
    	a134 = false;
    	a313 = "f";
    	a228 = a292;
    	a358 = a335;
    	a224 = "h";
    	a320 = 13;
    	a225 = a205[7];
    	a249 = ((((a249 / 5) * -5) - -21815) + -30910);
    	a171 = "f";
    	a333 = ((((80 - -27644) + -27571) * 10) / 9);
    	a353 = a241;
    	a300 = false;
    	a202 = a217[2];
    	a339 = false;
    	a239 = a299;
    	a357 = ((((((a357 % 65) - 87) * 10) / 9) - 27093) + 27082);
    	a338 = 8;
    	a302 = a346[6];
    	a237 = 7;
    	a243 = (((a243 / 5) / 5) - -27890);
    	a207 = "i";
    	a307 = a227[3];
    	a234 = a372[3];
    	a382 = ((((((a382 % 12) - 39) + -8) * 5) % 12) - 48);
    	a235 = a216[5];
    	a276 = a289;
    	a265 = a376;
    	a270 = 16;
    	a368 = false;
    	a359 = 5;
    	a378 = false;
    	a230 = 8;
    	a295 = a366[(a91 - 6)]; 
    	}else {
    	a57 = (a230 - -13);
    	a75 = "g";
    	a40 = a83;
    	a25 = a68;
    	}System.out.println("P");
    } 
}
private  void calculateOutputm83(String input) {
    if((((a207.equals("f")) && ((a75.equals("f")) && ((a173.equals("h")) && ((a256.equals("f")) && !a240)))) && (((-65 < a382) && (-39 >= a382)) && (((a338 == 4) && ((a91 == 8) && (a102 == a127[6] && (input.equals(inputs[9]) && cf)))) && (a361.equals("f")))))) {
    	a164 += (a164 + 20) > a164 ? 1 : 0;
    	cf = false;
    	a239 = a268;
    	a225 = a205[(a230 + -5)];
    	a368 = true;
    	a256 = "e";
    	a313 = "g";
    	a201 = (((21 + -75) + -6223) + 6255);
    	a218 = "g";
    	a276 = a250;
    	a265 = a303;
    	a338 = (a270 + -6);
    	a302 = a346[((a237 + a338) - 7)];
    	a249 = (((((((a249 * a357) % 14999) % 71) + 428) * 5) % 71) + 359);
    	a57 = (a91 - -4);
    	a310 = ((((((((a357 * a277) % 14999) % 20) + 337) - 14732) * 2) % 20) - -351);
    	a395 = false;
    	a269 = "g";
    	a207 = "f";
    	a320 = ((a206 * a206) + -28);
    	a260 = false;
    	a336 = true;
    	a270 = (a237 - -7);
    	a228 = a292;
    	a392 = a304;
    	a361 = "g";
    	a398 = (a230 - -6);
    	a158 = (a237 - -1);
    	a373 = true;
    	a296 = a362;
    	a240 = true;
    	a382 = ((((((a382 * a277) % 107) - -121) - -3535) - 3420) - 79);
    	a75 = "g";
    	a312 = "g";
    	a81 = a167[(a57 - 10)];
    	a211 = (a320 - 5);
    	a357 = (((((((a357 * a310) % 14999) / 5) - -18427) - -8483) % 38) - 37);
    	a234 = a372[(a237 + -4)];
    	a333 = ((((((a243 * a249) % 14999) + -11579) - -13618) % 14) + 161);
    	a386 = ((((((((a249 * a310) % 14999) + 9424) % 61) + 241) / 5) * 45) / 10);
    	a358 = a335;
    	a329 = "g";
    	a339 = false;
    	a237 = ((a359 - a359) + 3); 
    	System.out.println("S");
    } 
}
private  void calculateOutputm86(String input) {
    if((((a324 == a232[1] && ((a218.equals("f")) && (input.equals(inputs[9]) && ((a173.equals("h")) && (cf && a1 == a87[6]))))) && ((-47 < a333) && (147 >= a333))) && ((a224.equals("f")) && ((a224.equals("f")) && ((!a373 && (a91 == 11)) && (a75.equals("f"))))))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	a161 -= (a161 - 20) < a161 ? 2 : 0;
    	cf = false;
    	a75 = "h";
    	a295 = a366[(a91 + -6)];
    	a134 = false;
    	a53 = (((((29 * 10) / 1) / 5) * 31) / 10); 
    	System.out.println("S");
    } 
    if(((a302 == a346[1] && (!a395 && input.equals(inputs[4]))) && (((((a398 == 11) && ((a173.equals("h")) && ((a75.equals("f")) && ((cf && a1 == a87[6]) && (a91 == 11))))) && (a359 == 4)) && ((-199 < a201) && (-12 >= a201))) && !a368))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a35 -= (a35 - 20) < a35 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a378 = false;
    	a107 = "e";
    	a336 = false;
    	a338 = 5;
    	a207 = "h";
    	a312 = "h";
    	a225 = a205[5];
    	a173 = "f";
    	a256 = "h";
    	a125 = a30[(a230 - -2)]; 
    	System.out.println("S");
    } 
    if(((((160 < a310) && (316 >= a310)) && (!a395 && (a1 == a87[6] && (((a75.equals("f")) && cf) && input.equals(inputs[5]))))) && (a202 == a217[1] && (((a313.equals("f")) && ((11 == a353[4]) && (((160 < a310) && (316 >= a310)) && (a91 == 11)))) && (a173.equals("h")))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a67 -= (a67 - 20) < a67 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	if((!(a141 == a47[6]) || (!a134 && (a206 == 10)))) {
    	a339 = false;
    	a234 = a372[3];
    	a173 = "g";
    	a338 = 6;
    	a211 = 6;
    	a296 = a362;
    	a172 = (a320 - 6);
    	a256 = "i";
    	a269 = "g";
    	a329 = "h";
    	a243 = (((91 + 18269) + 4975) + 4723);
    	a207 = "g";
    	a353 = a263;
    	a225 = a205[6];
    	a129 = a92[((a206 - a91) + 13)]; 
    	}else {
    	a339 = true;
    	a386 = (((((a386 * -5) * 10) / 9) - -7777) * -4);
    	a300 = false;
    	a57 = ((a91 + a91) + -9);
    	a234 = a372[5];
    	a323 = (a211 - -5);
    	a224 = "h";
    	a398 = 16;
    	a277 = (((a277 + 6416) / 5) + 3146);
    	a310 = (((((a310 - -29477) - 22656) / 5) * 2) / 10);
    	a353 = a263;
    	a276 = a289;
    	a312 = "f";
    	a320 = 13;
    	a383 = a226[6];
    	a260 = false;
    	a269 = "f";
    	a357 = (((a357 + -6497) - -36442) - 31541);
    	a137 = a117;
    	a336 = false;
    	a228 = a229;
    	a240 = false;
    	a249 = (((((((a249 * 33) / 10) * 10) / 9) - 9592) * -1) / 10);
    	a392 = a208;
    	a239 = a242;
    	a230 = 9;
    	a296 = a362;
    	a307 = a227[0];
    	a358 = a348;
    	a373 = true;
    	a218 = "f";
    	a237 = 3;
    	a333 = (((a333 + 23570) - -4357) * 1);
    	a225 = a205[3];
    	a313 = "h";
    	a206 = 4;
    	a75 = "g";
    	a378 = false;
    	a201 = ((((a201 % 93) + -88) + 23803) + -23782);
    	a359 = 9;
    	a265 = a303;
    	a270 = 13;
    	a243 = ((((18 - 13823) + 16388) * 10) / 9);
    	a368 = false;
    	a211 = 2;
    	}System.out.println("Z");
    } 
    if(((((a91 == 11) && (((-10 < a277) && (148 >= a277)) && ((input.equals(inputs[0]) && (cf && (a75.equals("f")))) && (40 == a239[3])))) && (53 == a265[5])) && (((-47 < a333) && (147 >= a333)) && ((((-199 < a201) && (-12 >= a201)) && (((-199 < a201) && (-12 >= a201)) && (a173.equals("h")))) && a1 == a87[6])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a63 -= (a63 - 20) < a63 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	a141 = a47[((a237 * a206) + -19)];
    	a75 = "g";
    	a57 = (a398 - -4);
    	a72 = ((((92 * 10) / -8) + -2697) / 5); 
    	System.out.println("R");
    } 
    if(((((((160 < a310) && (316 >= a310)) && !a300) && (a230 == 4)) && a1 == a87[6]) && (((!a336 && ((a75.equals("f")) && (((a91 == 11) && (cf && (a173.equals("h")))) && a234 == a372[1]))) && input.equals(inputs[6])) && a383 == a226[1]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a5 -= (a5 - 20) < a5 ? 3 : 0;
    	cf = false;
    	if(((!(a75.equals("h")) || (76 == a358[5])) || !a240)) {
    	a336 = true;
    	a277 = ((((a277 + 16042) * 1) * 10) / 9);
    	a228 = a264;
    	a237 = 4;
    	a333 = (((a333 + 13224) * 2) - -2005);
    	a243 = (((76 / 5) + 7002) + 9365);
    	a286 = a294[3];
    	a210 = false;
    	a235 = a216[5];
    	a249 = ((((((a249 + 9780) % 71) + 395) * 5) % 71) - -393);
    	a329 = "i";
    	a339 = true;
    	a256 = "f";
    	a338 = 7;
    	a269 = "f";
    	a260 = false;
    	a312 = "e";
    	a225 = a205[3];
    	a207 = "i";
    	a173 = "f";
    	a307 = a227[4];
    	a353 = a399;
    	a358 = a351;
    	a239 = a299;
    	a296 = a212;
    	a373 = true;
    	a211 = 1;
    	a302 = a346[0];
    	a125 = a30[((a398 - a91) - -4)]; 
    	}else {
    	a134 = true;
    	a57 = (a91 + 5);
    	a75 = "g";
    	a278 = a326[((a270 / a57) - -6)];
    	}System.out.println("Y");
    } 
    if((((a75.equals("f")) && ((((43 == a392[3]) && (!a339 && !a336)) && a1 == a87[6]) && (a91 == 11))) && (((((a173.equals("h")) && (input.equals(inputs[3]) && cf)) && (a218.equals("f"))) && (a230 == 4)) && !a260))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a178 += (a178 + 20) > a178 ? 3 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if((((a397 == 8) && (!(21 == a46[0]) || (a125 == a30[4] || (a307 == 7)))) && a240)) {
    	a99 = a26[(a91 + -10)];
    	a75 = "g";
    	a57 = (a270 + 1);
    	a81 = a167[(a91 + -11)]; 
    	}else {
    	a307 = a227[1];
    	a218 = "e";
    	a278 = a326[((a359 - a91) + 9)];
    	a277 = (((a277 + 24884) - -4522) + 406);
    	a383 = a226[3];
    	a224 = "i";
    	a75 = "g";
    	a395 = false;
    	a40 = a65;
    	a302 = a346[3];
    	a357 = ((((a357 * 10) / -9) - 3277) - -23773);
    	a373 = true;
    	a269 = "h";
    	a239 = a242;
    	a336 = false;
    	a320 = 11;
    	a353 = a241;
    	a230 = 4;
    	a240 = false;
    	a57 = ((a206 - a398) - -22);
    	}System.out.println("Y");
    } 
    if(((!a339 && ((a1 == a87[6] && (((cf && (a173.equals("h"))) && input.equals(inputs[1])) && (a91 == 11))) && a234 == a372[1])) && (((-65 < a382) && (-39 >= a382)) && ((((a206 == 5) && !a378) && (a75.equals("f"))) && ((160 < a310) && (316 >= a310)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a51 += (a51 + 20) > a51 ? 4 : 0;
    	cf = false;
    	a357 = (((a357 - -22398) + 7170) - 39258);
    	a398 = 13;
    	a269 = "h";
    	a302 = a346[5];
    	a240 = false;
    	a202 = a217[1];
    	a336 = false;
    	a358 = a351;
    	a277 = ((((a277 % 78) - -68) - -2) - 1);
    	a361 = "h";
    	a158 = (a91 - 5);
    	a228 = a264;
    	a324 = a232[4];
    	a307 = a227[3];
    	a333 = (((((a333 - 13011) * 2) - 1241) % 14) + 172);
    	a237 = 9;
    	a260 = false;
    	a201 = (((a201 + 6798) - -13238) * 1);
    	a218 = "e";
    	a57 = (a359 - -6);
    	a386 = (((((a386 * 5) + 19875) + -47572) % 61) + 314);
    	a239 = a268;
    	a310 = (((a310 + -5355) + -3540) - 11662);
    	a312 = "h";
    	a105 = "g";
    	a313 = "h";
    	a339 = true;
    	a370 = a311;
    	a270 = 16;
    	a320 = 6;
    	a378 = false;
    	a373 = false;
    	a75 = "g";
    	a230 = 5;
    	a243 = (((19 - 178) + 23459) + -23465);
    	a276 = a250;
    	a211 = 6;
    	a395 = false;
    	a382 = ((((a382 * 10) / 6) * 5) - 6303);
    	a235 = a216[3];
    	a368 = false;
    	a383 = a226[1];
    	a353 = a241;
    	a392 = a304;
    	a206 = 5;
    	a359 = 4; 
    	System.out.println("T");
    } 
    if((((input.equals(inputs[7]) && (!a395 && (((a91 == 11) && cf) && (a75.equals("f"))))) && (50 == a228[0])) && ((50 == a228[0]) && ((53 == a265[5]) && ((a173.equals("h")) && (a1 == a87[6] && (a307 == a227[1] && ((152 < a249) && (355 >= a249))))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a122 -= (a122 - 20) < a122 ? 3 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a207 = "g";
    	a202 = a217[2];
    	a249 = (((a249 - -6822) + 18468) - 32006);
    	a300 = true;
    	a329 = "g";
    	a373 = false;
    	a336 = false;
    	a361 = "i";
    	a130 = (((25 / 5) - 1541) - -127);
    	a333 = (((a333 + -26089) / 5) - -30298);
    	a225 = a205[6];
    	a398 = 14;
    	a256 = "i";
    	a353 = a241;
    	a173 = "e";
    	a201 = (((a201 / 5) + 5659) / 5);
    	a276 = a250;
    	a338 = 7;
    	a265 = a376;
    	a296 = a362;
    	a191 = a37[(a211 - -4)]; 
    	System.out.println("Y");
    } 
    if((((a75.equals("f")) && ((77 < a386) && (201 >= a386))) && ((76 == a358[5]) && (((((((((a173.equals("h")) && cf) && (a91 == 11)) && a1 == a87[6]) && input.equals(inputs[8])) && a302 == a346[1]) && (50 == a228[0])) && (11 == a353[4])) && ((77 < a386) && (201 >= a386)))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a98 += (a98 + 20) > a98 ? 3 : 0;
    	a162 -= (a162 - 20) < a162 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	a91 = (a359 - -9);
    	a195 = a58; 
    	System.out.println("S");
    } 
    if((((43 == a392[3]) && ((103 == a276[1]) && !a336)) && ((11 == a353[4]) && (((a75.equals("f")) && (!a378 && (a1 == a87[6] && ((input.equals(inputs[2]) && (cf && (a91 == 11))) && (a173.equals("h")))))) && (a218.equals("f")))))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	if(a129 == a92[0]) {
    	a57 = (a91 + 3);
    	a134 = false;
    	a75 = "g";
    	a71 = a175[((a206 * a320) - 31)]; 
    	}else {
    	a1 = a87[((a206 + a91) - 12)];
    	}System.out.println("T");
    } 
}
private  void calculateOutputm87(String input) {
    if(((((a320 == 7) && ((!a142 && ((a91 == 12) && cf)) && (a75.equals("f")))) && (a338 == 4)) && ((((160 < a310) && (316 >= a310)) && (!a300 && (a383 == a226[1] && (input.equals(inputs[2]) && ((77 < a386) && (201 >= a386)))))) && (a173.equals("h"))))) {
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	cf = false;
    	a361 = "f";
    	a173 = "f";
    	a125 = a30[(a237 - 4)];
    	a234 = a372[(a237 + -2)];
    	a54 = (((((28 + -3250) / 5) / 5) * -32) / 10);
    	a224 = "e";
    	a225 = a205[(a359 + -4)];
    	a383 = a226[((a211 * a91) + -24)]; 
    	System.out.println("V");
    } 
}
private  void calculateOutputm89(String input) {
    if(((((152 < a249) && (355 >= a249)) && (input.equals(inputs[5]) && (a324 == a232[1] && ((77 < a386) && (201 >= a386))))) && (a234 == a372[1] && (((a173.equals("h")) && (!a336 && (((a97 && cf) && (a75.equals("f"))) && (a329.equals("f"))))) && (a91 == 14))))) {
    	a120 -= (a120 - 20) < a120 ? 2 : 0;
    	cf = false;
    	a373 = false;
    	a201 = ((((((a357 * a333) % 93) + -104) / 5) - -25829) + -25864);
    	a286 = a294[(a206 + -2)];
    	a277 = ((((((a277 * a333) * 1) % 78) + 69) - -13849) + -13849);
    	a312 = "f";
    	a324 = a232[((a211 + a359) - 5)];
    	a357 = ((((((a249 * a249) % 14999) - 4301) % 65) - 121) + -2);
    	a265 = a376;
    	a237 = (a270 - 8);
    	a235 = a216[(a398 + -10)];
    	a75 = "h";
    	a269 = "e";
    	a338 = a230;
    	a353 = a263;
    	a211 = ((a398 * a338) + -32);
    	a336 = true;
    	a370 = a285;
    	a361 = "f";
    	a218 = "e";
    	a234 = a372[(a206 - 4)];
    	a256 = "g";
    	a392 = a257;
    	a320 = (a237 + 3);
    	a270 = ((a91 - a211) - 2);
    	a358 = a348;
    	a302 = a346[(a206 + -3)];
    	a383 = a226[(a206 + -2)];
    	a295 = a366[(a91 - 12)];
    	a329 = "g";
    	a134 = false;
    	a395 = true;
    	a382 = (((((((a310 * a249) % 14999) % 12) + -60) / 5) + -18673) - -18636);
    	a307 = a227[(a398 + -10)];
    	a313 = "f";
    	a225 = a205[((a91 * a91) - 195)];
    	a310 = ((((((a310 * a386) % 14999) % 77) + 191) - -27) - 2);
    	a207 = "e";
    	a260 = false;
    	a228 = a229;
    	a333 = (((((((a333 * a243) % 14999) - -5415) % 14976) + -15022) - -20272) - 20274);
    	a240 = false;
    	a368 = false;
    	a398 = (a206 - -7);
    	a300 = true;
    	a171 = "i";
    	a296 = a384;
    	a224 = "e";
    	a378 = true;
    	a386 = (((((((((a386 * a249) % 14999) % 61) - -111) * 10) / 9) * 5) % 61) - -97);
    	a249 = ((((a249 % 101) + 243) - -7549) + -7601); 
    	System.out.println("V");
    } 
    if((((((a302 == a346[1] && a235 == a216[1]) && (a173.equals("h"))) && input.equals(inputs[6])) && ((-188 < a357) && (-57 >= a357))) && (a307 == a227[1] && (((11 == a353[4]) && ((a75.equals("f")) && ((cf && a97) && (a91 == 14)))) && (a398 == 11))))) {
    	cf = false;
    	a286 = a294[(a320 / a320)];
    	a333 = (((((a333 * a277) / 5) % 96) - -51) - 1);
    	a302 = a346[(a320 - 6)];
    	a260 = false;
    	a206 = (a398 - 6);
    	a265 = a293;
    	a202 = a217[((a270 + a270) + -21)];
    	a243 = ((((((a277 * a386) - 670) % 11) - 167) - 27770) + 27770);
    	a398 = ((a270 * a270) + -110);
    	a142 = false;
    	a201 = ((((((a277 * a277) % 93) + -104) / 5) - -19472) - 19507);
    	a359 = ((a320 / a320) - -3);
    	a358 = a351;
    	a91 = (a211 + 10);
    	a207 = "f";
    	a276 = a289;
    	a373 = false;
    	a312 = "f";
    	a382 = (((((((a357 * a249) % 14999) % 12) + -50) - 3) - -4934) - 4931);
    	a239 = a299;
    	a324 = a232[(a211 + -1)];
    	a228 = a229;
    	a336 = false;
    	a353 = a399;
    	a307 = a227[(a320 - 6)];
    	a370 = a285;
    	a230 = a338;
    	a235 = a216[(a237 - 3)];
    	a329 = "f";
    	a339 = false;
    	a277 = (((((((a277 * a249) % 14999) % 95) - -243) * 5) % 95) - -234); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm6(String input) {
    if((((((a329.equals("f")) && ((-199 < a201) && (-12 >= a201))) && (a230 == 4)) && a235 == a216[1]) && ((a338 == 4) && (a302 == a346[1] && (cf && (a91 == 7)))))) {
    	if(((((((cf && (a59.equals("e"))) && (a361.equals("f"))) && a383 == a226[1]) && !a260) && a324 == a232[1]) && (!a339 && ((160 < a310) && (316 >= a310))))) {
    		calculateOutputm81(input);
    	} 
    } 
    if(((((a320 == 7) && ((cf && (a91 == 8)) && (76 == a358[5]))) && !a336) && ((a338 == 4) && (((152 < a249) && (355 >= a249)) && a302 == a346[1])))) {
    	if((((a102 == a127[5] && cf) && (a359 == 4)) && (((((a206 == 5) && !a378) && ((-65 < a382) && (-39 >= a382))) && a235 == a216[1]) && ((-179 < a243) && (-156 >= a243))))) {
    		calculateOutputm82(input);
    	} 
    	if((((103 == a276[1]) && (((a218.equals("f")) && (a102 == a127[6] && cf)) && a302 == a346[1])) && (a234 == a372[1] && ((a211 == 2) && !a240)))) {
    		calculateOutputm83(input);
    	} 
    } 
    if((((((28 == a370[0]) && a307 == a227[1]) && (40 == a239[3])) && (a313.equals("f"))) && (((a361.equals("f")) && (cf && (a91 == 11))) && (11 == a353[4])))) {
    	if((((40 == a239[3]) && (a324 == a232[1] && (cf && a1 == a87[6]))) && (!a378 && ((a398 == 11) && ((a206 == 5) && ((-65 < a382) && (-39 >= a382))))))) {
    		calculateOutputm86(input);
    	} 
    } 
    if(((((a91 == 12) && cf) && !a395) && ((((a256.equals("f")) && (!a378 && !a368)) && ((152 < a249) && (355 >= a249))) && !a240))) {
    	if(((a225 == a205[1] && (a218.equals("f"))) && ((a224.equals("f")) && ((((cf && !a142) && (89 == a296[5])) && a234 == a372[1]) && (a269.equals("f")))))) {
    		calculateOutputm87(input);
    	} 
    } 
    if(((((-47 < a333) && (147 >= a333)) && ((((a91 == 14) && cf) && ((-10 < a277) && (148 >= a277))) && a383 == a226[1])) && (((a312.equals("f")) && a383 == a226[1]) && (50 == a228[0])))) {
    	if((((((((-10 < a277) && (148 >= a277)) && (a97 && cf)) && !a395) && ((-188 < a357) && (-57 >= a357))) && (a207.equals("f"))) && ((a270 == 11) && (76 == a358[5])))) {
    		calculateOutputm89(input);
    	} 
    } 
}
private  void calculateOutputm90(String input) {
    if((((76 == a358[5]) && (((a75.equals("f")) && ((cf && input.equals(inputs[1])) && a141 == a47[0])) && (a173.equals("i")))) && (((!a373 && ((!a123 && a307 == a227[1]) && (43 == a392[3]))) && ((152 < a249) && (355 >= a249))) && (a270 == 11)))) {
    	a94 += (a94 + 20) > a94 ? 4 : 0;
    	cf = false;
    	a172 = a230;
    	a173 = "g";
    	a274 = a290; 
    	System.out.println("R");
    } 
    if(((!a123 && (((a207.equals("f")) && (a329.equals("f"))) && a141 == a47[0])) && (((((((cf && input.equals(inputs[9])) && (a173.equals("i"))) && !a373) && !a368) && a234 == a372[1]) && (a320 == 7)) && (a75.equals("f"))))) {
    	a122 -= (a122 - 20) < a122 ? 2 : 0;
    	cf = false;
    	a353 = a399;
    	a256 = "g";
    	a129 = a92[(a270 - 4)];
    	a383 = a226[a211];
    	a368 = true;
    	a125 = a30[((a206 - a206) + 7)];
    	a269 = "g";
    	a218 = "g";
    	a173 = "f";
    	a358 = a335;
    	a302 = a346[(a320 - 5)];
    	a249 = (((((a249 * a333) % 14999) + -14913) - 15) + -65);
    	a307 = a227[(a237 - 2)];
    	a333 = (((((a333 * a277) % 14) + 162) / 5) + 141); 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm93(String input) {
    if((((((((!a378 && (a75.equals("f"))) && ((-188 < a357) && (-57 >= a357))) && input.equals(inputs[6])) && ((-10 < a277) && (148 >= a277))) && (a42 == 8)) && (103 == a276[1])) && ((a256.equals("f")) && (a324 == a232[1] && ((cf && a141 == a47[1]) && (a173.equals("i"))))))) {
    	cf = false;
    	a310 = ((((((a310 * a386) % 14999) + -7762) / 5) / 5) - 23752);
    	a75 = "i";
    	a324 = a232[(a270 - 11)];
    	a260 = true;
    	a395 = true;
    	a307 = a227[(a211 - 1)];
    	a313 = "e";
    	a146 = true;
    	a151 = true;
    	a277 = ((((((a277 * a310) % 14999) / 5) + -16410) * 10) / 9);
    	a270 = (a338 - -7);
    	a46 = a119; 
    	System.out.println("S");
    } 
}
private  void calculateOutputm96(String input) {
    if(((a234 == a372[1] && ((a218.equals("f")) && (((a184 == a157[6] && ((a75.equals("f")) && (input.equals(inputs[8]) && cf))) && a324 == a232[1]) && a141 == a47[3]))) && ((a302 == a346[1] && (!a373 && (a173.equals("i")))) && (53 == a265[5])))) {
    	a161 -= (a161 - 20) < a161 ? 2 : 0;
    	cf = false;
    	a234 = a372[(a320 - 6)];
    	a224 = "e";
    	a75 = "g";
    	a265 = a376;
    	a218 = "g";
    	a392 = a304;
    	a368 = true;
    	a329 = "e";
    	a270 = (a206 + 6);
    	a57 = 15;
    	a307 = a227[(a206 + -4)];
    	a237 = (a320 + -3);
    	a313 = "g";
    	a201 = ((((((a201 * a249) % 14999) + 23721) + 4035) % 94) + -10);
    	a240 = true;
    	a302 = a346[((a320 + a320) - 14)];
    	a324 = a232[(a398 - 9)];
    	a72 = (((8 + -21335) + -7672) - 67);
    	a398 = (a230 - -5);
    	a373 = false;
    	a336 = true;
    	a357 = (((((((a357 * a243) % 38) + -27) * 5) * 5) % 38) - 17); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm97(String input) {
    if((((((((77 < a386) && (201 >= a386)) && ((a206 == 5) && a324 == a232[1])) && (a75.equals("f"))) && (a313.equals("f"))) && (((input.equals(inputs[1]) && (a141 == a47[3] && ((a173.equals("i")) && (cf && a184 == a157[7])))) && (a237 == 4)) && (40 == a239[3]))) && a165 == 4790)) {
    	a67 -= (a67 - 20) < a67 ? 3 : 0;
    	cf = false;
    	a57 = (a206 + 10);
    	a75 = "g";
    	a72 = (((42 - 7943) * 3) * 1);
    	a141 = a47[(a57 - 13)]; 
    	System.out.println("V");
    } 
    if((((a75.equals("f")) && (((((input.equals(inputs[5]) && (a184 == a157[7] && cf)) && a141 == a47[3]) && a234 == a372[1]) && (a173.equals("i"))) && (89 == a296[5]))) && (((((77 < a386) && (201 >= a386)) && ((77 < a386) && (201 >= a386))) && (a329.equals("f"))) && a225 == a205[1]))) {
    	a131 += (a131 + 20) > a131 ? 2 : 0;
    	a169 += (a169 + 20) > a169 ? 1 : 0;
    	a35 += (a35 + 20) > a35 ? 2 : 0;
    	a90 += (a90 + 20) > a90 ? 6 : 0;
    	a110 += (a110 + 20) > a110 ? 2 : 0;
    	a31 -= (a31 - 20) < a31 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 2 : 0;
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	cf = false;
    	a57 = (a230 - -9);
    	a307 = a227[((a57 / a338) + -1)];
    	a276 = a250;
    	a265 = a376;
    	a277 = ((((((((a386 * a386) % 14999) + -5853) % 95) - -243) * 5) % 95) - -236);
    	a329 = "g";
    	a234 = a372[(a230 + -2)];
    	a312 = "g";
    	a302 = a346[(a57 - 11)];
    	a382 = (((((a382 * a310) / 5) % 107) + 102) + 15);
    	a239 = a268;
    	a202 = a217[((a206 + a338) - 7)];
    	a373 = false;
    	a137 = a189;
    	a249 = ((((((a386 * a201) % 14999) % 71) - -428) / 5) + 344);
    	a211 = ((a57 - a359) - 6);
    	a228 = a292;
    	a361 = "g";
    	a243 = ((((((a249 * a382) % 14999) / 5) % 11) - 167) + 1);
    	a370 = a311;
    	a358 = a335;
    	a333 = ((((((((a201 * a201) % 14999) + -5530) % 14) + 161) * 5) % 14) + 161);
    	a398 = (a320 - -5);
    	a357 = (((((((a357 * a249) % 14999) - -15800) / 5) - -17520) % 38) + -21);
    	a383 = a226[(a57 - 11)];
    	a324 = a232[(a206 + -3)];
    	a336 = true;
    	a237 = (a270 - 6);
    	a296 = a362;
    	a270 = ((a338 / a206) + 12);
    	a320 = (a57 - 5);
    	a75 = "g";
    	a353 = a399;
    	a256 = "f";
    	a269 = "f";
    	a359 = ((a57 - a57) + 5);
    	a224 = "g";
    	a395 = true;
    	a260 = true;
    	a225 = a205[(a57 - 11)];
    	a338 = (a57 + -8);
    	a392 = a304;
    	a339 = false;
    	a313 = "g";
    	a300 = false;
    	a218 = "g";
    	a207 = "g";
    	a230 = (a57 + -8);
    	a235 = a216[(a237 + -4)];
    	a240 = true;
    	a286 = a294[(a398 - 10)];
    	a378 = false;
    	a206 = ((a57 + a57) + -20);
    	a386 = ((((((((a386 * a333) % 14999) % 61) - -262) / 5) / 5) * 229) / 10);
    	a154 = (a57 + 3); 
    	System.out.println("Z");
    } 
    if((((((28 == a370[0]) && ((a312.equals("f")) && (a184 == a157[7] && !a395))) && ((-188 < a357) && (-57 >= a357))) && ((a338 == 4) && ((a206 == 5) && (a141 == a47[3] && (((cf && (a75.equals("f"))) && input.equals(inputs[8])) && (a173.equals("i"))))))) && a174 <= 3)) {
    	a5 -= (a5 - 20) < a5 ? 2 : 0;
    	cf = false;
    	a173 = "f";
    	a129 = a92[(a270 - 10)];
    	a125 = a30[(a211 - -5)]; 
    	System.out.println("T");
    } 
    if(((((((((a75.equals("f")) && cf) && a184 == a157[7]) && !a260) && a286 == a294[1]) && a141 == a47[3]) && ((((77 < a386) && (201 >= a386)) && (((160 < a310) && (316 >= a310)) && (a286 == a294[1] && (input.equals(inputs[4]) && ((-188 < a357) && (-57 >= a357)))))) && (a173.equals("i")))) && a89 == 9776)) {
    	a122 -= (a122 - 20) < a122 ? 1 : 0;
    	cf = false;
    	a172 = ((a359 + a320) + -9);
    	a173 = "g";
    	a8 = (a172 + 4); 
    	System.out.println("X");
    } 
    if(((((((89 == a296[5]) && (((a173.equals("i")) && (cf && input.equals(inputs[2]))) && (a398 == 11))) && a184 == a157[7]) && (11 == a353[4])) && (a75.equals("f"))) && ((a211 == 2) && ((a141 == a47[3] && a235 == a216[1]) && ((-188 < a357) && (-57 >= a357)))))) {
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	cf = false;
    	a234 = a372[((a270 + a270) + -20)];
    	a249 = ((((68 / 5) * 5) + -1842) - -2217);
    	a310 = ((((((a310 * a249) % 14999) - -14369) % 20) + 322) * 1);
    	a230 = (a270 - 6);
    	a373 = false;
    	a312 = "g";
    	a211 = ((a270 / a270) + 2);
    	a378 = true;
    	a359 = (a270 - 6);
    	a302 = a346[(a230 - 3)];
    	a307 = a227[(a359 + -3)];
    	a202 = a217[(a270 + -9)];
    	a276 = a253;
    	a361 = "g";
    	a336 = true;
    	a75 = "g";
    	a382 = (((((a382 * a201) % 107) - 9) - 14) + -3);
    	a357 = (((((((a357 * a277) % 14999) - 1687) % 38) + -1) + 19732) + -19720);
    	a207 = "g";
    	a158 = (a270 - 6);
    	a338 = (a270 + -6);
    	a225 = a205[(a237 + -2)];
    	a265 = a376;
    	a239 = a268;
    	a370 = a318;
    	a235 = a216[((a359 * a206) - 23)];
    	a353 = a399;
    	a224 = "g";
    	a313 = "g";
    	a324 = a232[(a320 - 5)];
    	a320 = (a270 + -3);
    	a57 = (a237 + 6);
    	a269 = "g";
    	a237 = a359;
    	a200 = a115[(a57 + -5)];
    	a333 = ((((79 + -14388) - -14467) - 7035) - -7029);
    	a395 = true;
    	a398 = (a211 - -9);
    	a201 = (((((a243 * a333) % 94) + 84) * 5) / 5);
    	a386 = ((((((((a386 * a249) % 14999) + -16743) % 61) + 314) * 5) % 61) - -239);
    	a329 = "g";
    	a296 = a362;
    	a286 = a294[((a338 + a398) + -15)];
    	a206 = ((a270 + a270) + -16);
    	a260 = true;
    	a339 = false;
    	a218 = "g";
    	a392 = a304;
    	a240 = true;
    	a358 = a335;
    	a270 = (a359 + 7); 
    	System.out.println("V");
    } 
    if((((a184 == a157[7] && (input.equals(inputs[6]) && !a373)) && (a224.equals("f"))) && (((((a173.equals("i")) && (!a395 && (a141 == a47[3] && ((a75.equals("f")) && cf)))) && ((152 < a249) && (355 >= a249))) && !a395) && (a237 == 4)))) {
    	a51 += (a51 + 20) > a51 ? 3 : 0;
    	cf = false;
    	a276 = a253;
    	a310 = ((((((a310 * a386) % 14999) + -16100) + 19318) + 5205) * -1);
    	a296 = a212;
    	a307 = a227[(a320 - 7)];
    	a320 = (a211 + 4);
    	a75 = "h";
    	a224 = "e";
    	a134 = false;
    	a295 = a366[(a398 + -7)];
    	a373 = true;
    	a312 = "e";
    	a358 = a348;
    	a336 = true;
    	a201 = (((((a201 * a333) % 14999) + -19452) / 5) - 12276);
    	a202 = a217[(a206 - 5)];
    	a359 = ((a230 * a270) - 41);
    	a218 = "e";
    	a184 = a157[3]; 
    	System.out.println("V");
    } 
    if((((((152 < a249) && (355 >= a249)) && ((a230 == 4) && (a202 == a217[1] && (a141 == a47[3] && ((77 < a386) && (201 >= a386)))))) && ((76 == a358[5]) && ((a184 == a157[7] && ((a320 == 7) && ((a173.equals("i")) && ((a75.equals("f")) && cf)))) && input.equals(inputs[0])))) && a63 >= 3)) {
    	a178 += (a178 + 20) > a178 ? 2 : 0;
    	cf = false;
    	a336 = true;
    	a206 = (a211 - -4);
    	a239 = a268;
    	a357 = ((((((a386 * a249) % 14999) % 38) - 20) - 10015) - -10000);
    	a228 = a292;
    	a57 = (a270 + 5);
    	a398 = (a230 - -8);
    	a329 = "g";
    	a75 = "g";
    	a183 = "h";
    	a370 = a311;
    	a392 = a304;
    	a218 = "g";
    	a395 = true;
    	a307 = a227[(a206 - a338)];
    	a240 = true;
    	a224 = "g";
    	a312 = "g";
    	a310 = (((((a310 * a357) % 20) + 337) + 1) * 1);
    	a353 = a399;
    	a278 = a326[(a57 - 11)];
    	a333 = ((((((a249 * a249) % 14999) - -8901) % 14) - -151) + -1); 
    	System.out.println("T");
    } 
    if((((a324 == a232[1] && ((a224.equals("f")) && a202 == a217[1])) && (89 == a296[5])) && ((((a237 == 4) && ((a173.equals("i")) && (((cf && a141 == a47[3]) && a184 == a157[7]) && input.equals(inputs[9])))) && (a75.equals("f"))) && ((-65 < a382) && (-39 >= a382))))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 4 : 0;
    	a56 += (a56 + 20) > a56 ? 2 : 0;
    	a5 += (a5 + 20) > a5 ? 2 : 0;
    	a93 += (a93 + 20) > a93 ? 2 : 0;
    	a94 -= (a94 - 20) < a94 ? 4 : 0;
    	cf = false;
    	a302 = a346[(a338 - 2)];
    	a75 = "i";
    	a260 = true;
    	a277 = (((((a333 * a333) % 14999) - 29001) + -946) * 1);
    	a353 = a263;
    	a270 = (a211 + 8);
    	a368 = true;
    	a276 = a250;
    	a256 = "g";
    	a243 = ((((((a277 * a249) % 14999) + -1464) * 10) / 9) - 5172);
    	a237 = ((a211 / a270) - -3);
    	a398 = (a206 - -5);
    	a202 = a217[(a320 - 6)];
    	a230 = ((a206 * a206) - 22);
    	a357 = (((((a357 * a243) % 14999) - 25959) + -3872) - 79);
    	a224 = "g";
    	a383 = a226[(a270 - 10)];
    	a249 = (((((a201 * a382) + -16647) % 71) - -475) + -11);
    	a373 = true;
    	a239 = a242;
    	a235 = a216[(a211 - 2)];
    	a240 = true;
    	a386 = (((((a386 * a310) % 14999) + -24117) * 1) - 1455);
    	a300 = true;
    	a296 = a212;
    	a225 = a205[(a206 - 5)];
    	a269 = "f";
    	a324 = a232[(a398 + -10)];
    	a286 = a294[((a359 - a320) - -5)];
    	a312 = "e";
    	a329 = "e";
    	a234 = a372[(a359 - 2)];
    	a16 = true;
    	a358 = a348;
    	a146 = false;
    	a361 = "e";
    	a73 = (((16 - -164) * 1) + 59);
    	a206 = ((a270 * a338) - 36);
    	a265 = a376;
    	a395 = true;
    	a338 = ((a359 - a320) + 6);
    	a336 = true;
    	a218 = "e";
    	a359 = ((a320 * a320) + -44); 
    	System.out.println("T");
    } 
}
private  void calculateOutputm98(String input) {
    if(((((a75.equals("f")) && (a221 && ((89 == a296[5]) && ((cf && (a173.equals("i"))) && a141 == a47[4])))) && a286 == a294[1]) && ((a361.equals("f")) && ((input.equals(inputs[0]) && ((43 == a392[3]) && a302 == a346[1])) && !a373)))) {
    	a120 -= (a120 - 20) < a120 ? 2 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if(((a151 && (((a302 == 10) && (a295 == 9)) || a368)) && !a64)) {
    	a295 = a366[((a230 * a359) + -11)];
    	a218 = "h";
    	a361 = "f";
    	a373 = false;
    	a53 = (((55 * 5) + -232) - 14);
    	a398 = 17;
    	a75 = "h";
    	a224 = "f";
    	a134 = false;
    	a320 = 9;
    	a358 = a351;
    	a228 = a292;
    	a269 = "g";
    	a392 = a257;
    	a339 = false;
    	a239 = a268;
    	a234 = a372[1];
    	a202 = a217[4];
    	a296 = a362;
    	a276 = a289;
    	a359 = 5; 
    	}else {
    	a173 = "e";
    	a139 = "e";
    	a130 = ((((((63 * 44) / 10) * 10) / 9) * 5) - 1200);
    	}System.out.println("S");
    } 
    if(((43 == a392[3]) && (a221 && (!a378 && ((a173.equals("i")) && ((a75.equals("f")) && (((a286 == a294[1] && ((a141 == a47[4] && (cf && input.equals(inputs[7]))) && ((-179 < a243) && (-156 >= a243)))) && (a359 == 4)) && a286 == a294[1]))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	a57 = (a398 + -1);
    	a75 = "g";
    	a158 = ((a57 + a57) - 16);
    	a81 = a167[((a57 / a57) - -4)]; 
    	System.out.println("T");
    } 
    if(((a202 == a217[1] && (((-199 < a201) && (-12 >= a201)) && (((a173.equals("i")) && !a339) && !a300))) && (a221 && ((40 == a239[3]) && (((input.equals(inputs[4]) && ((a75.equals("f")) && cf)) && (a218.equals("f"))) && a141 == a47[4]))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a172 = ((a230 - a206) - -5);
    	a173 = "g";
    	a274 = a290; 
    	System.out.println("X");
    } 
    if(((((((((cf && input.equals(inputs[5])) && a141 == a47[4]) && (a173.equals("i"))) && (a398 == 11)) && !a373) && a221) && (28 == a370[0])) && ((!a260 && ((a75.equals("f")) && (a313.equals("f")))) && a235 == a216[1]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	 
    	System.out.println("S");
    } 
    if(((((a173.equals("i")) && !a373) && (40 == a239[3])) && ((((a320 == 7) && ((((a75.equals("f")) && ((a221 && cf) && a141 == a47[4])) && (28 == a370[0])) && (a206 == 5))) && input.equals(inputs[6])) && (53 == a265[5])))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a174 += (a174 + 20) > a174 ? 2 : 0;
    	a35 += (a35 + 20) > a35 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a72 = (((57 - 1622) * 5) / 5);
    	a75 = "g";
    	a57 = (a230 - -11);
    	a141 = a47[((a57 - a338) - 10)]; 
    	System.out.println("R");
    } 
    if((((a206 == 5) && ((a141 == a47[4] && ((!a368 && (a338 == 4)) && input.equals(inputs[9]))) && a221)) && (((152 < a249) && (355 >= a249)) && (((89 == a296[5]) && ((a173.equals("i")) && ((a75.equals("f")) && cf))) && a286 == a294[1])))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	a237 = 5;
    	a234 = a372[3];
    	a57 = (a270 - -1);
    	a249 = ((((a249 * 5) * 5) / 5) + -5170);
    	a302 = a346[7];
    	a324 = a232[6];
    	a310 = ((((((a310 * 23) / 10) * 10) / 9) / 5) - -20470);
    	a339 = true;
    	a358 = a351;
    	a81 = a167[((a57 + a270) + -16)];
    	a336 = false;
    	a357 = ((((a357 / 5) - 59) / 5) + -139);
    	a202 = a217[1];
    	a361 = "e";
    	a265 = a376;
    	a313 = "f";
    	a225 = a205[7];
    	a296 = a384;
    	a269 = "h";
    	a373 = false;
    	a338 = 9;
    	a224 = "e";
    	a333 = (((68 + -12098) - 16413) + -1373);
    	a370 = a285;
    	a211 = 5;
    	a323 = ((a230 / a359) + 8);
    	a368 = false;
    	a228 = a264;
    	a307 = a227[0];
    	a277 = (((a277 - 23598) + -2334) - 2035);
    	a206 = 11;
    	a230 = 4;
    	a239 = a299;
    	a240 = false;
    	a320 = 9;
    	a75 = "g";
    	a312 = "f";
    	a300 = false;
    	a382 = (((((29 * -23) / 10) / 5) * 51) / 10);
    	a243 = ((((a243 * 10) / 8) - 19417) / 5);
    	a218 = "i";
    	a276 = a253;
    	a235 = a216[7];
    	a256 = "g";
    	a378 = false;
    	a329 = "e";
    	a359 = 4; 
    	System.out.println("S");
    } 
    if((((a221 && ((((input.equals(inputs[1]) && cf) && a141 == a47[4]) && (a173.equals("i"))) && ((-188 < a357) && (-57 >= a357)))) && a324 == a232[1]) && ((((!a373 && (a75.equals("f"))) && ((152 < a249) && (355 >= a249))) && (43 == a392[3])) && !a260))) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a98 += (a98 + 20) > a98 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a173 = "f";
    	a81 = a167[(a398 + -11)];
    	a125 = a30[(a270 + -6)]; 
    	System.out.println("P");
    } 
    if(((!a260 && (((cf && input.equals(inputs[3])) && (a173.equals("i"))) && !a240)) && (((a75.equals("f")) && (a141 == a47[4] && ((a221 && ((a329.equals("f")) && (a359 == 4))) && a234 == a372[1]))) && !a300))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a67 -= (a67 - 20) < a67 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	a312 = "f";
    	a300 = false;
    	a373 = true;
    	a234 = a372[7];
    	a357 = (((((a357 * 5) + -19107) - 3884) * -1) / 10);
    	a383 = a226[6];
    	a370 = a311;
    	a324 = a232[3];
    	a361 = "f";
    	a395 = false;
    	a269 = "e";
    	a358 = a351;
    	a310 = ((((a310 % 77) + 175) / 5) + 163);
    	a277 = ((((a277 - 4395) * 10) / 9) / 5);
    	a333 = ((((29 - -6288) - 6242) + 28732) - 28785);
    	a296 = a384;
    	a249 = (((((a249 / 5) * 167) / 10) * 10) / 9);
    	a339 = true;
    	a228 = a264;
    	a392 = a257;
    	a239 = a299;
    	a230 = 3;
    	a368 = false;
    	a75 = "g";
    	a286 = a294[7];
    	a243 = (((a243 - 5457) * 5) / -5);
    	a235 = a216[4];
    	a206 = 4;
    	a211 = 6;
    	a240 = false;
    	a218 = "h";
    	a336 = false;
    	a57 = 10;
    	a359 = 7;
    	a260 = false;
    	a382 = ((((58 - 181) / 5) / 5) - 44);
    	a270 = 14;
    	a329 = "h";
    	a201 = (((a201 + 272) - -1369) / 5);
    	a202 = a217[6];
    	a398 = 16;
    	a338 = 4;
    	a302 = a346[6];
    	a224 = "h";
    	a225 = a205[0];
    	a158 = (a320 - 2);
    	a313 = "f";
    	a265 = a293;
    	a307 = a227[5];
    	a276 = a250;
    	a378 = false;
    	a200 = a115[((a158 - a57) - -10)];
    	a320 = 12; 
    	System.out.println("T");
    } 
    if(((((a221 && (a141 == a47[4] && (((103 == a276[1]) && ((input.equals(inputs[2]) && cf) && (a75.equals("f")))) && a234 == a372[1]))) && (a313.equals("f"))) && a383 == a226[1]) && (((103 == a276[1]) && a286 == a294[1]) && (a173.equals("i"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a173 = "f";
    	a129 = a92[(a206 - 4)];
    	a125 = a30[(a338 + 3)]; 
    	System.out.println("R");
    } 
    if(((((cf && a141 == a47[4]) && (a173.equals("i"))) && (a75.equals("f"))) && ((!a373 && ((a398 == 11) && (((a221 && (a307 == a227[1] && input.equals(inputs[8]))) && (50 == a228[0])) && (43 == a392[3])))) && (a320 == 7)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a173 = "f";
    	a81 = a167[(a270 - 11)];
    	a125 = a30[((a230 + a230) + -3)]; 
    	System.out.println("R");
    } 
}
private  void calculateOutputm100(String input) {
    if(((((a225 == a205[1] && ((!a368 && (a128 == 12)) && ((152 < a249) && (355 >= a249)))) && (a75.equals("f"))) && (a173.equals("i"))) && (!a395 && ((((160 < a310) && (316 >= a310)) && (input.equals(inputs[5]) && (cf && a141 == a47[6]))) && !a395)))) {
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	cf = false;
    	if(((16 == a274[1]) || ((!(a307 == a227[3]) || ((47 == a239[4]) || ((-104 < a72) && (100 >= a72)))) || !(a102 == a127[7])))) {
    	a225 = a205[(a338 - 4)];
    	a353 = a399;
    	a312 = "e";
    	a277 = (((((a277 * a201) * 1) * 1) % 14995) + -15004);
    	a230 = ((a206 * a359) - 15);
    	a207 = "g";
    	a141 = a47[(a270 + -8)];
    	a228 = a292;
    	a184 = a157[((a128 + a128) + -18)];
    	a359 = (a270 - 8);
    	a206 = (a398 - 5);
    	a339 = true;
    	a338 = (a270 + -6);
    	a333 = ((((((a333 * a249) % 14999) * 2) * 1) % 14) + 161);
    	a249 = ((((((a249 * a357) % 14999) % 71) - -427) + 2) + -1); 
    	}else {
    	a378 = true;
    	a333 = (((((((a333 * a310) % 14999) % 14976) + -15022) - 2) + 13725) - 13723);
    	a240 = true;
    	a336 = true;
    	a228 = a229;
    	a277 = (((((a277 * a201) - -26078) * 1) % 14995) - 15004);
    	a339 = true;
    	a307 = a227[((a230 - a206) - -3)];
    	a196 = (a398 - -5);
    	a373 = false;
    	a269 = "e";
    	a276 = a253;
    	a295 = a366[(a128 - 12)];
    	a359 = (a237 + -1);
    	a218 = "e";
    	a234 = a372[(a237 + -2)];
    	a353 = a263;
    	a265 = a303;
    	a324 = a232[(a206 - 3)];
    	a211 = ((a230 * a230) + -15);
    	a206 = (a359 + 3);
    	a302 = a346[(a270 - 9)];
    	a338 = (a237 + -1);
    	a313 = "g";
    	a368 = true;
    	a312 = "e";
    	a237 = (a359 + 2);
    	a224 = "e";
    	a270 = (a398 - -1);
    	a230 = (a398 + -6);
    	a357 = ((((((a357 * a249) % 14999) % 38) + -17) / 5) - -12);
    	a207 = "g";
    	a329 = "e";
    	a392 = a208;
    	a75 = "h";
    	a320 = (a338 - -3);
    	a398 = (a359 - -7);
    	a134 = false;
    	a249 = ((((((((a333 * a201) % 14999) + 4384) % 71) - -382) * 5) % 71) + 420);
    	a225 = a205[(a359 + -3)];
    	a201 = (((((a201 * a333) % 14999) / 5) + -16438) + 6427);
    	}System.out.println("Y");
    } 
    if(((a234 == a372[1] && ((input.equals(inputs[6]) && ((a75.equals("f")) && (a141 == a47[6] && (((a128 == 12) && cf) && (a173.equals("i")))))) && ((-47 < a333) && (147 >= a333)))) && (!a339 && ((a256.equals("f")) && ((a207.equals("f")) && (a359 == 4)))))) {
    	a161 -= (a161 - 20) < a161 ? 4 : 0;
    	cf = false;
    	if((a395 && ((a218.equals("h")) || (a171.equals("h"))))) {
    	a57 = (a206 - -6);
    	a270 = ((a128 * a338) - 36);
    	a361 = "g";
    	a378 = false;
    	a312 = "g";
    	a239 = a268;
    	a353 = a399;
    	a228 = a292;
    	a300 = false;
    	a392 = a304;
    	a256 = "g";
    	a333 = (((((((a333 * a310) % 14999) % 14) + 162) / 5) - 21623) + 21749);
    	a359 = (a270 - 7);
    	a240 = true;
    	a206 = (a57 - 5);
    	a211 = (a320 + -6);
    	a218 = "g";
    	a383 = a226[(a270 - 10)];
    	a143 = "f";
    	a202 = a217[(a57 + -9)];
    	a277 = (((((a277 * a357) / 5) * 5) % 95) + 243);
    	a357 = (((((((a382 * a249) % 14999) % 38) + -17) - -8676) + 1180) + -9857);
    	a302 = a346[((a359 + a237) - 7)];
    	a358 = a335;
    	a14 = a79[((a57 * a128) - 130)];
    	a230 = (a338 + 1);
    	a307 = a227[(a230 - 3)];
    	a237 = (a398 + -6);
    	a75 = "g";
    	a224 = "g";
    	a398 = (a320 + 4);
    	a338 = (a320 + -3);
    	a201 = (((((((a201 * a310) % 14999) / 5) % 94) + 83) - 12411) + 12410);
    	a310 = ((((((a310 * a249) % 14999) / 5) % 20) - -332) + 6);
    	a249 = (((((a249 * a243) % 14999) - 14613) - 20) / 5); 
    	}else {
    	a383 = a226[(a230 + -4)];
    	a296 = a212;
    	a256 = "g";
    	a134 = false;
    	a373 = true;
    	a382 = ((((29 - -24623) - 24696) - -23517) - 23517);
    	a320 = (a398 - 5);
    	a228 = a229;
    	a184 = a157[((a338 + a237) + -1)];
    	a359 = (a128 - 9);
    	a357 = ((((((a310 * a310) % 14999) / 5) - 27930) - -41678) + -38437);
    	a295 = a366[((a398 * a270) + -117)];
    	a260 = true;
    	a249 = ((((a249 / 5) - -12080) / 5) * -5);
    	a235 = a216[(a230 + -3)];
    	a370 = a318;
    	a224 = "e";
    	a211 = ((a230 / a230) + 1);
    	a339 = true;
    	a75 = "h";
    	a312 = "e";
    	a237 = ((a206 + a128) - 14);
    	a269 = "e";
    	a333 = (((((((a333 * a357) % 14999) - -11890) / 5) + 11534) * -1) / 10);
    	a239 = a268;
    	a218 = "e";
    	a313 = "e";
    	a307 = a227[(a338 - 2)];
    	a338 = ((a206 - a320) - -4);
    	a300 = false;
    	a276 = a253;
    	a201 = (((((a201 * a310) % 14999) - 1433) - 4126) + -2448);
    	a353 = a399;
    	a240 = true;
    	a378 = false;
    	a336 = true;
    	a206 = ((a320 / a359) - -2);
    	a310 = (((((a310 * a277) % 14999) - 14927) - 27) + -1);
    	a398 = (a359 - -7);
    	}System.out.println("Y");
    } 
    if((((((a173.equals("i")) && (((a128 == 12) && (((cf && input.equals(inputs[3])) && a141 == a47[6]) && (a75.equals("f")))) && ((160 < a310) && (316 >= a310)))) && (a361.equals("f"))) && (11 == a353[4])) && ((a312.equals("f")) && (a234 == a372[1] && (50 == a228[0]))))) {
    	a122 -= (a122 - 20) < a122 ? 1 : 0;
    	cf = false;
    	a206 = (a128 + -6);
    	a207 = "g";
    	a239 = a268;
    	a398 = a128;
    	a386 = (((((((a310 * a310) % 14999) / 5) % 61) - -206) / 5) - -235);
    	a237 = (a398 + -7);
    	a383 = a226[(a206 + -4)];
    	a235 = a216[((a270 / a128) - -1)];
    	a338 = ((a237 - a270) + 11);
    	a234 = a372[((a398 / a398) - -1)];
    	a240 = true;
    	a320 = a206;
    	a336 = true;
    	a373 = false;
    	a201 = (((((((a201 * a310) % 14999) + -6289) % 94) - -143) + 23606) + -23571);
    	a357 = (((((((a357 * a310) % 14999) + 8412) % 38) - 17) + -22492) - -22491);
    	a224 = "g";
    	a265 = a376;
    	a392 = a304;
    	a329 = "g";
    	a313 = "g";
    	a353 = a399;
    	a260 = true;
    	a57 = (a128 - -1);
    	a323 = (a128 + -5);
    	a358 = a335;
    	a228 = a292;
    	a368 = true;
    	a202 = a217[((a270 + a128) + -22)];
    	a230 = (a270 + -6);
    	a307 = a227[(a57 - 11)];
    	a256 = "g";
    	a218 = "g";
    	a312 = "e";
    	a382 = ((((((a310 * a249) % 14999) % 12) + -51) + 13434) - 13433);
    	a339 = false;
    	a75 = "g";
    	a333 = ((((((a333 * a243) + 3711) / 5) / 5) % 14) - -161);
    	a296 = a212;
    	a277 = (((((a277 * a243) % 95) - -243) - 0) * 1);
    	a137 = a117;
    	a249 = (((((((a249 * a310) % 14999) % 71) - -366) + 27) / 5) - -288);
    	a359 = (a211 - -2);
    	a310 = ((((((a310 * a386) % 14999) - 22539) * 1) % 20) - -339);
    	a270 = (a323 - -5); 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm101(String input) {
    if(((((28 == a370[0]) && (a338 == 4)) && input.equals(inputs[2])) && (((-199 < a201) && (-12 >= a201)) && (((a237 == 4) && (((((cf && (a75.equals("f"))) && a141 == a47[7]) && (11 == a353[4])) && (100 == a275[3])) && (a173.equals("i")))) && a235 == a216[1])))) {
    	a67 -= (a67 - 20) < a67 ? 4 : 0;
    	cf = false;
    	a353 = a399;
    	a383 = a226[(a320 - 7)];
    	a173 = "f";
    	a225 = a205[(a230 + -4)];
    	a361 = "f";
    	a224 = "e";
    	a234 = a372[(a270 + -9)];
    	a125 = a30[(a359 - 4)];
    	a54 = ((((77 * 10) / 1) + 22122) * 1); 
    	System.out.println("V");
    } 
}
private  void calculateOutputm7(String input) {
    if((((a312.equals("f")) && (((-199 < a201) && (-12 >= a201)) && (a141 == a47[0] && cf))) && (a234 == a372[1] && ((a383 == a226[1] && (11 == a353[4])) && (a211 == 2))))) {
    	if((((a269.equals("f")) && ((cf && !a123) && ((-47 < a333) && (147 >= a333)))) && ((((a256.equals("f")) && ((152 < a249) && (355 >= a249))) && ((-10 < a277) && (148 >= a277))) && a235 == a216[1]))) {
    		calculateOutputm90(input);
    	} 
    } 
    if((((a269.equals("f")) && (a141 == a47[1] && cf)) && (((-188 < a357) && (-57 >= a357)) && ((((a329.equals("f")) && (a206 == 5)) && (a207.equals("f"))) && !a378)))) {
    	if(((((77 < a386) && (201 >= a386)) && (((a42 == 8) && cf) && (a329.equals("f")))) && (((a286 == a294[1] && (a270 == 11)) && a307 == a227[1]) && (a320 == 7)))) {
    		calculateOutputm93(input);
    	} 
    } 
    if(((a286 == a294[1] && (a324 == a232[1] && (a307 == a227[1] && (!a336 && (a302 == a346[1] && (a141 == a47[3] && cf)))))) && (a237 == 4))) {
    	if(((((a398 == 11) && (!a373 && (a184 == a157[6] && cf))) && a302 == a346[1]) && (a286 == a294[1] && (!a368 && ((160 < a310) && (316 >= a310)))))) {
    		calculateOutputm96(input);
    	} 
    	if(((a235 == a216[1] && a225 == a205[1]) && ((((53 == a265[5]) && ((cf && a184 == a157[7]) && (a218.equals("f")))) && ((-65 < a382) && (-39 >= a382))) && !a240))) {
    		calculateOutputm97(input);
    	} 
    } 
    if(((!a300 && (a307 == a227[1] && (!a300 && (a141 == a47[4] && cf)))) && ((a230 == 4) && ((76 == a358[5]) && a234 == a372[1])))) {
    	if(((((((-10 < a277) && (148 >= a277)) && ((-179 < a243) && (-156 >= a243))) && (76 == a358[5])) && !a260) && (((cf && a221) && (a269.equals("f"))) && (53 == a265[5])))) {
    		calculateOutputm98(input);
    	} 
    } 
    if(((((a256.equals("f")) && !a373) && (a270 == 11)) && ((((-47 < a333) && (147 >= a333)) && ((a141 == a47[6] && cf) && (28 == a370[0]))) && (a230 == 4)))) {
    	if(((a324 == a232[1] && ((a230 == 4) && ((a128 == 12) && cf))) && ((50 == a228[0]) && (((-10 < a277) && (148 >= a277)) && (a307 == a227[1] && (a230 == 4)))))) {
    		calculateOutputm100(input);
    	} 
    } 
    if(((a234 == a372[1] && ((-65 < a382) && (-39 >= a382))) && (a225 == a205[1] && ((a286 == a294[1] && (((160 < a310) && (316 >= a310)) && (a141 == a47[7] && cf))) && !a260)))) {
    	if(((a202 == a217[1] && (((77 < a386) && (201 >= a386)) && ((a230 == 4) && ((100 == a275[3]) && cf)))) && ((103 == a276[1]) && (!a395 && (103 == a276[1]))))) {
    		calculateOutputm101(input);
    	} 
    } 
}
private  void calculateOutputm102(String input) {
    if((((a368 && ((input.equals(inputs[5]) && (cf && a81 == a167[2])) && (56 == a265[2]))) && a368) && (((a158 == 4) && (((148 < a277) && (339 >= a277)) && (((-57 < a357) && (20 >= a357)) && (a336 && (a75.equals("g")))))) && (a57 == 10)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a162 -= (a162 - 20) < a162 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	a333 = (((((a333 + 9187) % 14) + 158) - -16080) - 16083);
    	a269 = "h";
    	a338 = 3;
    	a383 = a226[7];
    	a300 = true;
    	a324 = a232[4];
    	a358 = a351;
    	a206 = 6;
    	a99 = a26[((a57 + a270) + -20)];
    	a312 = "e";
    	a75 = "f";
    	a249 = ((((a249 * 15) / 10) + 3262) + 23354);
    	a395 = true;
    	a173 = "g";
    	a357 = (((a357 / 5) + 1584) - -10852);
    	a320 = 12;
    	a211 = 6;
    	a313 = "h";
    	a202 = a217[4];
    	a336 = false;
    	a361 = "g";
    	a386 = (((a386 - 2329) + -6634) * 3);
    	a234 = a372[6];
    	a240 = true;
    	a307 = a227[3];
    	a207 = "h";
    	a296 = a212;
    	a218 = "h";
    	a230 = 10;
    	a368 = true;
    	a359 = 7;
    	a392 = a304;
    	a277 = ((((a277 + -19310) + 3021) + 43939) - 56531);
    	a339 = true;
    	a270 = 13;
    	a256 = "h";
    	a373 = false;
    	a239 = a268;
    	a243 = (((a243 + -16291) / 5) + -19355);
    	a329 = "g";
    	a398 = 13;
    	a172 = ((a158 + a237) - 2);
    	a276 = a289;
    	a265 = a376;
    	a370 = a318;
    	a225 = a205[4];
    	a382 = ((((a382 / 5) - -111) * 9) / 10);
    	a378 = true;
    	a302 = a346[2];
    	a228 = a264;
    	a237 = 10; 
    	System.out.println("R");
    } 
    if(((((148 < a277) && (339 >= a277)) && (((input.equals(inputs[2]) && (a312.equals("g"))) && (a312.equals("g"))) && !a373)) && ((a158 == 4) && (((((cf && (a75.equals("g"))) && (a57 == 10)) && (77 == a358[0])) && (a313.equals("g"))) && a81 == a167[2])))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a178 -= (a178 - 20) < a178 ? 2 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	if((!(a270 == 10) || (27 == a46[0]))) {
    	a173 = "f";
    	a129 = a92[((a211 + a338) - 2)];
    	a75 = "f";
    	a125 = a30[(a237 + 2)]; 
    	}else {
    	a136 = ((a320 / a359) - -11);
    	a132 = false;
    	a75 = "e";
    	a77 = ((a206 * a398) + -67);
    	}System.out.println("V");
    } 
    if(((((((cf && a81 == a167[2]) && (a158 == 4)) && (a338 == 5)) && (a313.equals("g"))) && !a339) && (a302 == a346[2] && ((((a75.equals("g")) && (a300 && (a57 == 10))) && a234 == a372[2]) && input.equals(inputs[9]))))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	if((339 < a277 || a368)) {
    	a75 = "f";
    	a184 = a157[((a158 / a237) - -2)];
    	a173 = "i";
    	a141 = a47[(a57 - 7)]; 
    	}else {
    	a155 = "g";
    	a132 = false;
    	a75 = "e";
    	a77 = ((a206 + a338) - 4);
    	}System.out.println("R");
    } 
    if(((a324 == a232[2] && (a383 == a226[2] && (a368 && (((((-57 < a357) && (20 >= a357)) && (a158 == 4)) && ((-57 < a357) && (20 >= a357))) && (a57 == 10))))) && ((a230 == 5) && (a81 == a167[2] && ((cf && (a75.equals("g"))) && input.equals(inputs[6])))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a169 -= (a169 - 20) < a169 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	cf = false;
    	a57 = ((a237 / a237) - -14);
    	a72 = (((((50 * 9) / 10) * 9) / 10) / 5);
    	a36 = ((((((a72 * a72) % 14) - -293) * 5) % 14) - -279); 
    	System.out.println("Y");
    } 
    if(((a225 == a205[2] && (((-156 < a243) && (-68 >= a243)) && ((a307 == a227[2] && input.equals(inputs[3])) && (a320 == 8)))) && ((((a75.equals("g")) && ((a158 == 4) && ((a57 == 10) && (cf && a81 == a167[2])))) && ((201 < a386) && (325 >= a386))) && (38 == a370[4])))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a51 += (a51 + 20) > a51 ? 4 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	 
    	System.out.println("V");
    } 
    if((((a57 == 10) && ((((a158 == 4) && ((a75.equals("g")) && cf)) && ((355 < a249) && (499 >= a249))) && a395)) && (a81 == a167[2] && (((-57 < a357) && (20 >= a357)) && (input.equals(inputs[7]) && (((-39 < a382) && (176 >= a382)) && ((59 == a228[3]) && (92 == a296[2])))))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a63 -= (a63 - 20) < a63 ? 4 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a211 = 1;
    	a237 = 5;
    	a358 = a335;
    	a91 = (a158 + 4);
    	a270 = 15;
    	a269 = "h";
    	a313 = "e";
    	a302 = a346[7];
    	a392 = a304;
    	a234 = a372[4];
    	a336 = true;
    	a320 = 9;
    	a240 = true;
    	a382 = (((a382 - -24725) + 3547) - -16);
    	a357 = (((a357 + -14690) * 2) + -282);
    	a218 = "h";
    	a75 = "f";
    	a173 = "h";
    	a224 = "i";
    	a235 = a216[3];
    	a338 = 3;
    	a361 = "i";
    	a249 = ((((a249 * 5) + 21089) * 1) * -1);
    	a207 = "e";
    	a256 = "i";
    	a102 = a127[(a91 - 2)]; 
    	System.out.println("R");
    } 
    if((((((a324 == a232[2] && ((a57 == 10) && (cf && (a75.equals("g"))))) && (92 == a296[2])) && a81 == a167[2]) && (a230 == 5)) && ((((a158 == 4) && ((50 == a392[4]) && (47 == a239[4]))) && input.equals(inputs[1])) && ((-57 < a357) && (20 >= a357))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	if(((!cf && (cf && (a139.equals("e")))) || 493 < a130)) {
    	a244 = a363[(a230 + -5)];
    	a173 = "i";
    	a75 = "f";
    	a141 = a47[(a57 + -8)]; 
    	}else {
    	a398 = 14;
    	a202 = a217[5];
    	a75 = "f";
    	a296 = a212;
    	a230 = 3;
    	a206 = 9;
    	a320 = 7;
    	a237 = 10;
    	a211 = 8;
    	a207 = "i";
    	a269 = "e";
    	a240 = true;
    	a386 = ((((a386 * 5) * 5) % 61) - -210);
    	a357 = (((a357 + -8177) * 3) + -4389);
    	a373 = true;
    	a249 = ((((a249 - 27085) - 1228) - -40965) * -2);
    	a270 = 12;
    	a239 = a242;
    	a307 = a227[2];
    	a313 = "h";
    	a265 = a303;
    	a234 = a372[6];
    	a312 = "i";
    	a382 = (((((a382 % 107) - -69) + 14638) - -6632) + -21270);
    	a358 = a348;
    	a353 = a263;
    	a361 = "i";
    	a228 = a229;
    	a329 = "g";
    	a336 = false;
    	a378 = true;
    	a339 = true;
    	a302 = a346[6];
    	a130 = (((28 / 5) + 25932) - -935);
    	a338 = 6;
    	a368 = true;
    	a370 = a311;
    	a392 = a304;
    	a324 = a232[5];
    	a173 = "e";
    	a277 = (((a277 / 5) + 11300) - -17080);
    	a333 = ((((a333 - 189) / 5) - -16539) + -16417);
    	a243 = (((a243 - -28749) + -5533) - -2979);
    	a359 = 7;
    	a300 = true;
    	a218 = "h";
    	a13 = (((((((a130 * a130) % 14999) / 5) + 20202) - -2683) % 60) - -241);
    	}System.out.println("Y");
    } 
    if(((((a395 && ((cf && a81 == a167[2]) && (a75.equals("g")))) && ((-57 < a357) && (20 >= a357))) && input.equals(inputs[8])) && (((((a312.equals("g")) && ((a158 == 4) && (a338 == 5))) && a225 == a205[2]) && (a320 == 8)) && (a57 == 10)))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a67 -= (a67 - 20) < a67 ? 4 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a320 = 6;
    	a270 = 15;
    	a378 = true;
    	a276 = a289;
    	a307 = a227[4];
    	a383 = a226[2];
    	a218 = "h";
    	a202 = a217[5];
    	a302 = a346[4];
    	a333 = ((((a333 + -17815) * 10) / 9) * 1);
    	a240 = true;
    	a225 = a205[3];
    	a300 = true;
    	a256 = "i";
    	a395 = true;
    	a277 = (((((((a277 * 10) / 4) * 10) / 9) / 5) * 42) / 10);
    	a312 = "h";
    	a368 = true;
    	a339 = false;
    	a206 = 11;
    	a329 = "g";
    	a392 = a208;
    	a269 = "g";
    	a386 = ((((a386 - 16272) * 10) / 9) + 15921);
    	a243 = ((((a243 % 11) - 159) + 2) - -1);
    	a211 = 3;
    	a230 = 9;
    	a398 = 12;
    	a237 = 5;
    	a359 = 4;
    	a228 = a229;
    	a234 = a372[6];
    	a75 = "f";
    	a353 = a399;
    	a97 = true;
    	a336 = true;
    	a173 = "h";
    	a239 = a299;
    	a249 = (((a249 - -25842) + -41065) * 2);
    	a358 = a335;
    	a207 = "g";
    	a338 = 9;
    	a324 = a232[7];
    	a296 = a212;
    	a357 = (((a357 - 13053) + -4007) * 1);
    	a313 = "i";
    	a91 = (a57 + 4); 
    	System.out.println("V");
    } 
    if(((((a75.equals("g")) && ((a338 == 5) && ((a158 == 4) && (input.equals(inputs[0]) && ((a57 == 10) && cf))))) && (a206 == 6)) && (((148 < a277) && (339 >= a277)) && (((a81 == a167[2] && ((-39 < a382) && (176 >= a382))) && ((-156 < a243) && (-68 >= a243))) && (15 == a353[2]))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a35 -= (a35 - 20) < a35 ? 1 : 0;
    	a98 -= (a98 - 20) < a98 ? 2 : 0;
    	a162 += (a162 + 20) > a162 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	if((((a151 && !(76 == a358[5])) || a378) || !(a183.equals("h")))) {
    	a243 = (((a243 * 5) / -5) / 5);
    	a239 = a242;
    	a277 = ((((a277 % 95) + 174) + 9976) + -9924);
    	a324 = a232[4];
    	a173 = "h";
    	a368 = true;
    	a313 = "h";
    	a307 = a227[3];
    	a211 = 1;
    	a339 = true;
    	a373 = true;
    	a1 = a87[((a57 / a158) - -4)];
    	a386 = ((((((a386 + 29169) % 61) + 233) * 5) % 61) - -229);
    	a359 = 5;
    	a234 = a372[7];
    	a353 = a399;
    	a269 = "h";
    	a286 = a294[6];
    	a336 = true;
    	a378 = true;
    	a300 = true;
    	a370 = a318;
    	a383 = a226[4];
    	a320 = 11;
    	a228 = a292;
    	a392 = a208;
    	a358 = a348;
    	a333 = (((a333 * 5) + -21738) - 5884);
    	a218 = "h";
    	a75 = "f";
    	a230 = 9;
    	a91 = (a398 + -1);
    	a240 = true;
    	a382 = (((a382 / 5) - 8405) - 19449);
    	a361 = "e";
    	a270 = 16;
    	a265 = a303;
    	a302 = a346[7];
    	a235 = a216[5];
    	a202 = a217[7];
    	a249 = (((a249 * -5) - 16509) * 1);
    	a395 = true;
    	a357 = (((((a357 * 5) % 38) - 18) - 7247) - -7247);
    	a237 = 10;
    	a206 = 10;
    	a312 = "i";
    	a398 = 12; 
    	}else {
    	a173 = "f";
    	a81 = a167[(a57 - 7)];
    	a75 = "f";
    	a125 = a30[(a206 + -1)];
    	}System.out.println("R");
    } 
    if(((((((147 < a333) && (177 >= a333)) && input.equals(inputs[4])) && ((147 < a333) && (177 >= a333))) && (a57 == 10)) && (a368 && (a81 == a167[2] && ((((a313.equals("g")) && (((a158 == 4) && cf) && (a75.equals("g")))) && (a237 == 5)) && ((355 < a249) && (499 >= a249))))))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if((a200 == a115[7] && (a99 == 7))) {
    	a265 = a293;
    	a320 = 9;
    	a373 = true;
    	a260 = false;
    	a206 = 9;
    	a370 = a311;
    	a137 = a189;
    	a296 = a384;
    	a225 = a205[7];
    	a154 = a270;
    	a300 = false;
    	a286 = a294[0];
    	a201 = (((95 * 5) - -27422) * 1);
    	a256 = "h";
    	a224 = "i";
    	a235 = a216[6];
    	a230 = 8;
    	a276 = a289;
    	a361 = "e";
    	a392 = a304;
    	a249 = (((((a249 - 25440) - -21628) - 13821) * -1) / 10);
    	a57 = (a158 + 9);
    	a310 = (((79 + 248) * 5) - 1306);
    	a237 = 7;
    	a211 = 3;
    	a270 = 10; 
    	}else {
    	a77 = (a158 + 7);
    	a132 = false;
    	a207 = "e";
    	a256 = "g";
    	a218 = "f";
    	a270 = 16;
    	a240 = false;
    	a235 = a216[2];
    	a211 = 6;
    	a276 = a250;
    	a300 = false;
    	a370 = a311;
    	a75 = "e";
    	a361 = "g";
    	a277 = (((((a277 / 5) - -24107) * 1) * -1) / 10);
    	a368 = false;
    	a243 = ((((((a243 * 5) % 43) + -70) * 5) % 43) + -112);
    	a269 = "h";
    	a265 = a293;
    	a378 = false;
    	a202 = a217[5];
    	a260 = false;
    	a225 = a205[2];
    	a338 = 5;
    	a320 = 12;
    	a206 = 10;
    	a359 = 7;
    	a237 = 4;
    	a398 = 12;
    	a286 = a294[4];
    	a373 = false;
    	a392 = a304;
    	a395 = false;
    	a310 = ((((76 * 5) / 5) - 14021) - -14192);
    	a239 = a268;
    	a224 = "i";
    	a358 = a335;
    	a313 = "f";
    	a249 = (((((a249 % 71) + 418) - 9) + 10826) - 10847);
    	a228 = a264;
    	a353 = a241;
    	a230 = 10;
    	a312 = "h";
    	a234 = a372[2];
    	a307 = a227[3];
    	a99 = a26[(a57 - 3)];
    	}System.out.println("R");
    } 
}
private  void calculateOutputm104(String input) {
    if(((((59 == a228[3]) && ((input.equals(inputs[8]) && ((a57 == 10) && (cf && (a75.equals("g"))))) && a225 == a205[2])) && (a206 == 6)) && ((((a383 == a226[2] && a200 == a115[2]) && (a158 == 5)) && (a313.equals("g"))) && (a256.equals("g"))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a174 += (a174 + 20) > a174 ? 4 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	if((a243 <=  -179 && ((!a260 || (a398 == 13)) || (a320 == 9)))) {
    	a398 = 16;
    	a202 = a217[3];
    	a359 = 9;
    	a300 = true;
    	a239 = a242;
    	a286 = a294[6];
    	a392 = a304;
    	a75 = "f";
    	a224 = "g";
    	a312 = "i";
    	a333 = ((((a333 % 14) + 158) + 27214) - 27223);
    	a370 = a311;
    	a234 = a372[3];
    	a130 = (((29 - -17158) * 1) * 1);
    	a373 = true;
    	a218 = "g";
    	a296 = a362;
    	a270 = 10;
    	a338 = 3;
    	a320 = 7;
    	a235 = a216[7];
    	a237 = 8;
    	a243 = ((((a243 % 43) + -90) * 5) / 5);
    	a357 = (((a357 + 7074) - -12920) + 5493);
    	a378 = true;
    	a230 = 3;
    	a368 = true;
    	a310 = (((a310 + -23277) - 5279) + -1713);
    	a228 = a229;
    	a302 = a346[7];
    	a382 = (((1 - 8228) - 15208) - 6289);
    	a339 = true;
    	a324 = a232[7];
    	a211 = 5;
    	a207 = "e";
    	a336 = false;
    	a329 = "i";
    	a361 = "g";
    	a269 = "h";
    	a276 = a250;
    	a240 = true;
    	a201 = (((a201 - 6972) + -12610) * 1);
    	a249 = ((((a249 + 5738) * 4) * 10) / 9);
    	a307 = a227[0];
    	a206 = 9;
    	a313 = "e";
    	a353 = a399;
    	a358 = a335;
    	a173 = "e";
    	a277 = ((((a277 - 9185) * 3) + -149) + 43597);
    	a265 = a376;
    	a13 = (((((((a130 * a130) % 14999) - -6013) % 60) + 251) - 28815) - -28801); 
    	}else {
    	a320 = 8;
    	a211 = 5;
    	a312 = "e";
    	a307 = a227[4];
    	a249 = (((((a249 * 15) / 10) / 5) * 10) / 2);
    	a338 = 7;
    	a300 = false;
    	a224 = "e";
    	a358 = a351;
    	a333 = (((((a333 * -1) / 10) + 4) * 10) / 9);
    	a256 = "g";
    	a235 = a216[2];
    	a135 = "e";
    	a270 = 14;
    	a201 = (((a201 + 7728) * 3) * 1);
    	a368 = false;
    	a228 = a264;
    	a357 = (((a357 * 5) + -18686) * 1);
    	a202 = a217[4];
    	a310 = ((((((a310 * 5) % 20) + 328) * 5) % 20) + 323);
    	a269 = "f";
    	a207 = "g";
    	a395 = true;
    	a386 = (((43 * 5) * -5) * 5);
    	a239 = a268;
    	a378 = false;
    	a260 = true;
    	a336 = false;
    	a296 = a384;
    	a75 = "h";
    	a324 = a232[4];
    	a230 = 10;
    	a339 = false;
    	a206 = 10;
    	a134 = true;
    	a398 = 15;
    	a237 = 6;
    	a240 = false;
    	a329 = "i";
    	a277 = (((a277 / 5) + -15240) - 6797);
    	a218 = "i";
    	a373 = false;
    	a359 = 8;
    	a313 = "i";
    	a392 = a304;
    	a361 = "i";
    	a370 = a311;
    	a225 = a205[5];
    	a353 = a263;
    	a243 = (((((a243 % 11) + -165) / 5) * 10) / 2);
    	a196 = (a158 + 10);
    	}System.out.println("R");
    } 
    if(((((a75.equals("g")) && ((47 == a239[4]) && (15 == a353[2]))) && a368) && ((((input.equals(inputs[2]) && ((a57 == 10) && ((a158 == 5) && (a200 == a115[2] && cf)))) && (47 == a239[4])) && (a237 == 5)) && (77 == a358[0])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a178 -= (a178 - 20) < a178 ? 2 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a75 = "h";
    	a196 = (a320 + 2);
    	a134 = true;
    	a129 = a92[(a196 - 3)]; 
    	System.out.println("R");
    } 
    if(((a307 == a227[2] && (((a359 == 5) && (a359 == 5)) && (a359 == 5))) && ((((((a57 == 10) && (((a75.equals("g")) && cf) && (a158 == 5))) && (a270 == 12)) && a200 == a115[2]) && (77 == a358[0])) && input.equals(inputs[6])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	a168 += (a168 + 20) > a168 ? 2 : 0;
    	cf = false;
    	a57 = ((a338 / a398) - -15);
    	a72 = (((((90 + -10021) / 5) / 5) * -7) / 10);
    	a17 = ((((((a72 * a72) % 14999) / 5) + 26465) + -12364) - 22568); 
    	System.out.println("X");
    } 
    if(((a225 == a205[2] && ((input.equals(inputs[0]) && ((cf && a200 == a115[2]) && (a57 == 10))) && a378)) && (a324 == a232[2] && (((a75.equals("g")) && ((a361.equals("g")) && (((147 < a333) && (177 >= a333)) && (a158 == 5)))) && a202 == a217[2])))) {
    	a89 += (a89 + 20) > a89 ? 1 : 0;
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	cf = false;
    	a75 = "i";
    	a46 = a148;
    	a146 = true;
    	a72 = ((((64 + 5194) - -3211) * 10) / 9); 
    	System.out.println("V");
    } 
    if(((a324 == a232[2] && ((a211 == 3) && ((a329.equals("g")) && a324 == a232[2]))) && (input.equals(inputs[5]) && ((a158 == 5) && (((a336 && ((cf && (a57 == 10)) && (a75.equals("g")))) && a200 == a115[2]) && ((-156 < a243) && (-68 >= a243))))))) {
    	a120 -= (a120 - 20) < a120 ? 4 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a57 = (a158 + 10);
    	a141 = a47[a158];
    	a72 = (((74 - 28981) + -796) - 126); 
    	System.out.println("T");
    } 
    if((((((((-156 < a243) && (-68 >= a243)) && (a218.equals("g"))) && (a57 == 10)) && (a320 == 8)) && a240) && (((((148 < a277) && (339 >= a277)) && (((a75.equals("g")) && (input.equals(inputs[7]) && cf)) && (a158 == 5))) && a200 == a115[2]) && (a211 == 3)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a35 += (a35 + 20) > a35 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	if(((a295 == 11) || (!(47 == a265[5]) || (!(a184 == 9) || ((77 == a195[1]) || !(a171.equals("i"))))))) {
    	a357 = (((a357 / 5) - 17877) / 5);
    	a211 = 3;
    	a75 = "f";
    	a320 = 9;
    	a91 = (a158 + 3);
    	a270 = 15;
    	a358 = a335;
    	a218 = "g";
    	a256 = "i";
    	a338 = 5;
    	a382 = (((43 - -18490) - -5499) * 1);
    	a336 = true;
    	a240 = true;
    	a392 = a304;
    	a269 = "h";
    	a173 = "h";
    	a234 = a372[7];
    	a276 = a250;
    	a313 = "h";
    	a302 = a346[7];
    	a249 = ((((a249 + -25340) * 1) - 3643) + 47528);
    	a207 = "e";
    	a237 = 7;
    	a310 = (((a310 / 5) / -5) + -14747);
    	a361 = "g";
    	a102 = a127[(a57 + -4)]; 
    	}else {
    	a373 = true;
    	a358 = a351;
    	a202 = a217[5];
    	a368 = true;
    	a75 = "h";
    	a134 = false;
    	a320 = 10;
    	a313 = "i";
    	a386 = ((((94 * 5) * 5) * 1) / 10);
    	a240 = true;
    	a357 = (((a357 + -16834) / 5) * 5);
    	a382 = ((((16 * -41) / 10) * 5) - 18182);
    	a359 = 9;
    	a324 = a232[1];
    	a286 = a294[1];
    	a392 = a304;
    	a237 = 10;
    	a307 = a227[2];
    	a206 = 8;
    	a383 = a226[4];
    	a201 = ((((a201 + 8044) * 3) * 10) / 9);
    	a370 = a318;
    	a228 = a264;
    	a329 = "i";
    	a270 = 16;
    	a171 = "i";
    	a265 = a293;
    	a256 = "h";
    	a395 = false;
    	a260 = true;
    	a239 = a268;
    	a312 = "g";
    	a398 = 15;
    	a249 = ((((a249 * 15) / 10) / 5) * 5);
    	a224 = "h";
    	a339 = false;
    	a243 = (((a243 - 23668) + 34849) - -8712);
    	a300 = false;
    	a235 = a216[6];
    	a295 = a366[(a338 + -3)];
    	a207 = "f";
    	a336 = false;
    	a218 = "f";
    	a277 = ((((((a277 * 23) / 10) * 10) / 9) - 16100) + 18285);
    	a378 = false;
    	a310 = (((a310 / 5) - -255) - -14);
    	a296 = a212;
    	a230 = 7;
    	a353 = a399;
    	a234 = a372[4];
    	a225 = a205[2];
    	a211 = 8;
    	a361 = "g";
    	a333 = (((a333 + -11339) + -14962) - -26268);
    	a302 = a346[5];
    	a338 = 6;
    	}System.out.println("S");
    } 
    if(((((316 < a310) && (357 >= a310)) && ((a338 == 5) && ((a313.equals("g")) && ((cf && (a57 == 10)) && (a158 == 5))))) && ((((a75.equals("g")) && (((a256.equals("g")) && (a359 == 5)) && input.equals(inputs[9]))) && ((355 < a249) && (499 >= a249))) && a200 == a115[2]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a57 = (a158 - -7);
    	a81 = a167[((a57 + a57) - 22)];
    	a158 = (a57 - 4); 
    	System.out.println("S");
    } 
    if((((a158 == 5) && (((input.equals(inputs[3]) && ((a320 == 8) && ((355 < a249) && (499 >= a249)))) && (a75.equals("g"))) && (a224.equals("g")))) && ((((((a57 == 10) && cf) && a200 == a115[2]) && (a230 == 5)) && (a359 == 5)) && ((-12 < a201) && (178 >= a201))))) {
    	a120 += (a120 + 20) > a120 ? 2 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	a206 = 10;
    	a240 = true;
    	a228 = a292;
    	a235 = a216[0];
    	a201 = ((((a201 / 5) - 21542) * 10) / 9);
    	a336 = true;
    	a256 = "h";
    	a329 = "h";
    	a234 = a372[5];
    	a382 = (((((49 - -12214) / 5) + -3511) * -2) / 10);
    	a339 = true;
    	a398 = 12;
    	a307 = a227[0];
    	a378 = true;
    	a368 = true;
    	a91 = (a237 + 3);
    	a211 = 8;
    	a392 = a208;
    	a243 = (((a243 + 18038) + 11571) * 1);
    	a302 = a346[0];
    	a313 = "g";
    	a353 = a263;
    	a276 = a253;
    	a358 = a335;
    	a207 = "h";
    	a359 = 3;
    	a312 = "g";
    	a173 = "h";
    	a357 = ((((a357 % 38) - 17) - 2) + 2);
    	a224 = "e";
    	a225 = a205[4];
    	a286 = a294[4];
    	a102 = a127[((a230 + a57) - 10)];
    	a310 = (((a310 * 5) * 5) * -3);
    	a218 = "g";
    	a270 = 17;
    	a395 = true;
    	a370 = a318;
    	a269 = "e";
    	a320 = 13;
    	a300 = true;
    	a230 = 8;
    	a265 = a303;
    	a75 = "f";
    	a338 = 6;
    	a383 = a226[6];
    	a324 = a232[5];
    	a249 = (((a249 - -19584) + 5915) + 1033);
    	a237 = 6; 
    	System.out.println("T");
    } 
    if((((a158 == 5) && ((a302 == a346[2] && ((-156 < a243) && (-68 >= a243))) && a300)) && (((((15 == a353[2]) && ((((a75.equals("g")) && cf) && (a57 == 10)) && (a270 == 12))) && a324 == a232[2]) && a200 == a115[2]) && input.equals(inputs[1])))) {
    	a122 -= (a122 - 20) < a122 ? 3 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a183 = "h";
    	a278 = a326[a158];
    	a57 = (a230 - -11); 
    	System.out.println("X");
    } 
    if(((a302 == a346[2] && a202 == a217[2]) && ((a398 == 12) && ((a312.equals("g")) && (a302 == a346[2] && (((a158 == 5) && ((a57 == 10) && (a307 == a227[2] && ((cf && (a75.equals("g"))) && a200 == a115[2])))) && input.equals(inputs[4]))))))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a51 += (a51 + 20) > a51 ? 1 : 0;
    	cf = false;
    	if(((23 == a274[2]) && a339)) {
    	a260 = false;
    	a57 = (a270 + 1);
    	a296 = a384;
    	a312 = "g";
    	a370 = a311;
    	a361 = "g";
    	a395 = true;
    	a225 = a205[0];
    	a324 = a232[6];
    	a265 = a303;
    	a320 = 13;
    	a234 = a372[4];
    	a202 = a217[2];
    	a276 = a253;
    	a286 = a294[0];
    	a386 = ((((((46 * 10) / 1) + -14206) - -4412) * -1) / 10);
    	a269 = "i";
    	a302 = a346[5];
    	a235 = a216[6];
    	a382 = (((52 - -3736) / 5) * 5);
    	a323 = (a211 - -4);
    	a137 = a117; 
    	}else {
    	a228 = a229;
    	a277 = ((((((a277 * 10) / 4) + 21337) - 24879) * -2) / 10);
    	a201 = ((((a201 + 8056) * 3) + 4261) + -30177);
    	a206 = 9;
    	a307 = a227[0];
    	a382 = ((((2 * 5) + -38) * 5) - -212);
    	a312 = "e";
    	a237 = 5;
    	a172 = (a57 - 9);
    	a265 = a376;
    	a358 = a348;
    	a302 = a346[5];
    	a370 = a311;
    	a296 = a362;
    	a339 = false;
    	a234 = a372[5];
    	a361 = "h";
    	a207 = "e";
    	a75 = "f";
    	a270 = 12;
    	a225 = a205[0];
    	a357 = ((((a357 * 5) + 7045) * 10) / 9);
    	a249 = ((((a249 * -5) - 18955) + 43035) + -30265);
    	a129 = a92[((a359 * a172) - -2)];
    	a338 = 5;
    	a373 = true;
    	a224 = "h";
    	a398 = 17;
    	a336 = true;
    	a202 = a217[5];
    	a310 = (((((a310 * 12) / 10) + -23759) * -1) / 10);
    	a230 = 7;
    	a333 = (((a333 + 457) - -21469) * 1);
    	a300 = true;
    	a313 = "g";
    	a320 = 10;
    	a218 = "h";
    	a239 = a242;
    	a240 = true;
    	a395 = true;
    	a383 = a226[4];
    	a235 = a216[4];
    	a286 = a294[5];
    	a392 = a304;
    	a276 = a250;
    	a368 = true;
    	a378 = true;
    	a329 = "g";
    	a173 = "g";
    	a256 = "i";
    	a324 = a232[0];
    	a359 = 3;
    	}System.out.println("T");
    } 
}
private  void calculateOutputm105(String input) {
    if((((((-156 < a243) && (-68 >= a243)) && (((a312.equals("g")) && (a57 == 10)) && (a256.equals("g")))) && (a302 == a346[2] && ((((a336 && (input.equals(inputs[4]) && (cf && (a158 == 5)))) && a200 == a115[5]) && (a75.equals("g"))) && !a373))) && (a88 % 2==0))) {
    	a178 += (a178 + 20) > a178 ? 4 : 0;
    	cf = false;
    	a146 = false;
    	a73 = (((((a277 * a386) % 14999) - 15840) * 1) - 2194);
    	a75 = "i";
    	a39 = a144; 
    	System.out.println("R");
    } 
    if(((((a361.equals("g")) && (a200 == a115[5] && ((92 == a296[2]) && ((input.equals(inputs[3]) && cf) && (a158 == 5))))) && ((92 == a296[2]) && (((((a312.equals("g")) && (a313.equals("g"))) && (a75.equals("g"))) && ((201 < a386) && (325 >= a386))) && (a57 == 10)))) && a161 >= 5)) {
    	cf = false;
    	a105 = "i";
    	a158 = (a57 - 4); 
    	System.out.println("R");
    } 
    if(((((((a57 == 10) && (a234 == a372[2] && (((cf && (a158 == 5)) && input.equals(inputs[2])) && (92 == a296[2])))) && (a361.equals("g"))) && a383 == a226[2]) && (((a75.equals("g")) && (a200 == a115[5] && ((147 < a333) && (177 >= a333)))) && (a218.equals("g")))) && a181 == 1220)) {
    	a31 += (a31 + 20) > a31 ? 1 : 0;
    	cf = false;
    	a143 = "h";
    	a210 = true;
    	a57 = (a206 + 5); 
    	System.out.println("T");
    } 
    if((((a270 == 12) && ((a158 == 5) && (((a200 == a115[5] && ((input.equals(inputs[6]) && (cf && (a75.equals("g")))) && ((355 < a249) && (499 >= a249)))) && !a373) && (47 == a239[4])))) && ((a300 && (a237 == 5)) && (a57 == 10)))) {
    	a162 += (a162 + 20) > a162 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 2 : 0;
    	a122 -= (a122 - 20) < a122 ? 1 : 0;
    	cf = false;
    	a265 = a293;
    	a386 = ((((((a386 * a243) % 14999) % 61) + 139) / 5) + 95);
    	a361 = "f";
    	a249 = (((((((a249 * a277) % 14999) / 5) / 5) - -28808) % 101) - -196);
    	a395 = false;
    	a234 = a372[(a338 - 5)];
    	a392 = a257;
    	a225 = a205[((a338 - a338) - -1)];
    	a237 = ((a398 - a206) - 2);
    	a358 = a348;
    	a382 = ((((((((a382 * a201) % 14999) % 12) + -52) - -1) * 5) % 12) - 43);
    	a353 = a241;
    	a296 = a384;
    	a173 = "g";
    	a256 = "f";
    	a240 = false;
    	a302 = a346[(a338 - 4)];
    	a207 = "f";
    	a339 = false;
    	a270 = (a158 - -6);
    	a276 = a289;
    	a206 = a338;
    	a336 = false;
    	a357 = (((((a357 * a333) / 5) % 65) + -121) + -1);
    	a235 = a216[((a338 + a338) - 9)];
    	a307 = a227[(a237 - 3)];
    	a286 = a294[(a270 - 10)];
    	a218 = "f";
    	a129 = a92[(a320 - 7)];
    	a211 = (a338 + -4);
    	a378 = false;
    	a368 = false;
    	a269 = "f";
    	a172 = (a57 - 9);
    	a320 = (a158 + 2);
    	a224 = "f";
    	a359 = (a338 - 1);
    	a260 = false;
    	a333 = ((((((((a201 * a201) % 14999) / 5) % 96) + 51) * 5) % 96) + 51);
    	a202 = a217[((a338 * a398) - 60)];
    	a313 = "f";
    	a329 = "f";
    	a312 = "f";
    	a300 = false;
    	a230 = (a338 - 1);
    	a310 = (((((((a243 * a386) % 14999) % 77) + 238) / 5) / 5) + 165);
    	a373 = true;
    	a75 = "f";
    	a398 = (a338 - -6);
    	a201 = (((((((a201 * a243) % 93) - 105) - 1) * 5) % 93) - 104);
    	a243 = ((((((a243 * a310) % 14999) + 3397) + -1852) % 11) + -167);
    	a370 = a285;
    	a338 = a230; 
    	System.out.println("P");
    } 
    if((((((a269.equals("g")) && ((((cf && (a158 == 5)) && (a57 == 10)) && (56 == a265[2])) && a368)) && a234 == a372[2]) && ((input.equals(inputs[5]) && (a200 == a115[5] && (((-57 < a357) && (20 >= a357)) && (a75.equals("g"))))) && a368)) && a94 <= 3)) {
    	cf = false;
    	a224 = "e";
    	a353 = a263;
    	a230 = (a206 - 3);
    	a99 = a26[((a398 / a237) - -1)];
    	a75 = "e";
    	a202 = a217[(a57 + -10)];
    	a201 = ((((((a201 * a277) % 14999) + -14160) % 14900) + -15098) + -3);
    	a320 = (a230 + 3);
    	a260 = true;
    	a132 = false;
    	a368 = true;
    	a239 = a242;
    	a77 = ((a211 - a158) - -13);
    	a269 = "e";
    	a218 = "e";
    	a243 = ((((((a243 * a386) % 14999) + -1561) * 10) / 9) * 1);
    	a359 = (a338 - 2);
    	a211 = (a57 - 9);
    	a358 = a348;
    	a310 = (((((a333 * a333) % 14999) + -29473) - 469) + -55);
    	a324 = a232[(a320 + -6)]; 
    	System.out.println("T");
    } 
    if((((((a329.equals("g")) && (((92 == a296[2]) && (((a158 == 5) && cf) && (a75.equals("g")))) && (77 == a358[0]))) && input.equals(inputs[8])) && ((((a200 == a115[5] && a202 == a217[2]) && (a57 == 10)) && (a237 == 5)) && ((-12 < a201) && (178 >= a201)))) && a31 <= -3)) {
    	a67 -= (a67 - 20) < a67 ? 2 : 0;
    	cf = false;
    	a158 = (a57 + -2);
    	a81 = a167[((a320 + a237) + -11)];
    	a57 = (a230 + 7); 
    	System.out.println("R");
    } 
    if(((((77 == a358[0]) && ((92 == a296[2]) && ((a75.equals("g")) && ((a57 == 10) && ((a398 == 12) && ((a158 == 5) && (cf && a200 == a115[5]))))))) && ((a206 == 6) && (((50 == a392[4]) && input.equals(inputs[9])) && !a339))) && a51 <= 3)) {
    	a169 -= (a169 - 20) < a169 ? 3 : 0;
    	cf = false;
    	a278 = a326[((a206 + a359) + -9)];
    	a40 = a65;
    	a57 = ((a338 * a320) - 24); 
    	System.out.println("X");
    } 
    if(((((((((-39 < a382) && (176 >= a382)) && ((a312.equals("g")) && ((cf && (a75.equals("g"))) && a200 == a115[5]))) && (a158 == 5)) && a395) && (a57 == 10)) && (((a336 && a240) && a260) && input.equals(inputs[1]))) && (a93 % 2==0))) {
    	cf = false;
    	a132 = false;
    	a129 = a92[a338];
    	a75 = "e";
    	a77 = (a57 + 2); 
    	System.out.println("X");
    } 
    if(((((92 == a296[2]) && (a286 == a294[2] && ((a75.equals("g")) && (a200 == a115[5] && (((a207.equals("g")) && (a361.equals("g"))) && (a57 == 10)))))) && (((a398 == 12) && (input.equals(inputs[7]) && (cf && (a158 == 5)))) && a202 == a217[2])) && (a156 % 2==0))) {
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	cf = false;
    	a132 = false;
    	a75 = "e";
    	a77 = ((a230 - a359) + 10);
    	a1 = a87[((a206 + a359) + -8)]; 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm107(String input) {
    if((((77 == a358[0]) && (a200 == a115[7] && ((a75.equals("g")) && ((a158 == 5) && (cf && input.equals(inputs[6])))))) && (a202 == a217[2] && ((a338 == 5) && ((((201 < a386) && (325 >= a386)) && (((148 < a277) && (339 >= a277)) && a234 == a372[2])) && (a57 == 10)))))) {
    	a169 -= (a169 - 20) < a169 ? 4 : 0;
    	cf = false;
    	a75 = "f";
    	a237 = ((a320 + a320) - 11);
    	a277 = ((((((((a386 * a310) % 14999) / 5) % 78) - -61) * 5) % 78) + 11);
    	a234 = a372[((a398 * a57) + -120)];
    	a260 = false;
    	a357 = (((((((a277 * a310) % 14999) * 2) - -1) + 0) % 65) - 121);
    	a239 = a242;
    	a333 = ((((((a310 * a277) % 14999) + -2122) % 14976) - 15022) * 1);
    	a228 = a229;
    	a240 = false;
    	a201 = (((((((a201 * a357) % 14999) * 2) * 1) * 1) % 93) + -104);
    	a218 = "e";
    	a395 = false;
    	a207 = "f";
    	a42 = (a57 + -2);
    	a256 = "f";
    	a359 = ((a338 - a206) - -4);
    	a383 = a226[(a398 - 12)];
    	a353 = a263;
    	a386 = ((((((a386 * a277) % 14999) % 61) - -138) - -2745) - 2742);
    	a324 = a232[(a206 - 5)];
    	a173 = "i";
    	a373 = true;
    	a398 = a57;
    	a141 = a47[(a206 - 5)];
    	a225 = a205[(a338 + -5)];
    	a307 = a227[(a270 - 10)];
    	a211 = (a158 + -4);
    	a230 = ((a158 - a158) - -3);
    	a300 = true;
    	a312 = "e";
    	a206 = a338;
    	a338 = ((a320 + a320) + -11); 
    	System.out.println("S");
    } 
}
private  void calculateOutputm108(String input) {
    if((((((((a313.equals("g")) && ((316 < a310) && (357 >= a310))) && a368) && a225 == a205[2]) && input.equals(inputs[4])) && (a57 == 10)) && (((((a105.equals("g")) && ((a75.equals("g")) && cf)) && ((-57 < a357) && (20 >= a357))) && a336) && (a158 == 6)))) {
    	a178 -= (a178 - 20) < a178 ? 1 : 0;
    	cf = false;
    	a307 = a227[(a158 + -5)];
    	a146 = false;
    	a207 = "e";
    	a237 = ((a57 + a57) - 17);
    	a286 = a294[(a158 + -4)];
    	a336 = true;
    	a249 = ((((((a386 * a386) % 14999) - 2334) * 1) % 71) + 427);
    	a357 = (((((a357 * a386) % 14999) / 5) - 19419) * 1);
    	a329 = "e";
    	a224 = "g";
    	a73 = ((((16 + 23698) + -23503) - -13235) - 13255);
    	a353 = a263;
    	a395 = true;
    	a382 = ((((((((a382 * a333) % 14999) % 12) - 50) * 1) * 5) % 12) + -43);
    	a234 = a372[(a158 + -4)];
    	a358 = a348;
    	a16 = true;
    	a202 = a217[(a359 + -4)];
    	a310 = ((((((a310 * a277) % 14999) - -19206) + 9876) % 77) - -212);
    	a338 = ((a158 + a206) - 9);
    	a368 = true;
    	a75 = "i";
    	a300 = true;
    	a296 = a212;
    	a320 = (a359 - -2);
    	a383 = a226[((a398 * a398) + -100)];
    	a276 = a250;
    	a265 = a376;
    	a240 = true;
    	a225 = a205[((a206 + a230) - 9)];
    	a256 = "g";
    	a218 = "e";
    	a313 = "f";
    	a302 = a346[(a158 + -4)];
    	a339 = true;
    	a206 = (a359 + -1);
    	a359 = (a230 + 2); 
    	System.out.println("V");
    } 
    if((((a359 == 5) && (a307 == a227[2] && (((a158 == 6) && ((a57 == 10) && cf)) && (a75.equals("g"))))) && ((((a105.equals("g")) && (((77 == a358[0]) && input.equals(inputs[0])) && (a329.equals("g")))) && (92 == a296[2])) && !a339))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	cf = false;
    	a302 = a346[((a230 + a230) + -6)];
    	a357 = ((((a357 * a310) - 8623) * 1) + -1273);
    	a339 = true;
    	a75 = "i";
    	a240 = true;
    	a64 = false;
    	a146 = false;
    	a296 = a212;
    	a338 = (a398 / a230);
    	a218 = "e";
    	a237 = a338;
    	a395 = true;
    	a73 = (((46 + 10432) / 5) / 5); 
    	System.out.println("V");
    } 
}
private  void calculateOutputm113(String input) {
    if((((a237 == 5) && (a383 == a226[2] && ((a75.equals("g")) && ((cf && (a57 == 10)) && a81 == a167[5])))) && (input.equals(inputs[6]) && (((a158 == 8) && ((a378 && a336) && a307 == a227[2])) && ((-156 < a243) && (-68 >= a243)))))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	cf = false;
    	 
    	System.out.println("S");
    } 
}
private  void calculateOutputm115(String input) {
    if(((((-39 < a382) && (176 >= a382)) && ((a57 == 10) && (a240 && ((a383 == a226[2] && a302 == a346[2]) && (56 == a265[2]))))) && ((59 == a228[3]) && ((((a158 == 11) && ((a75.equals("g")) && cf)) && input.equals(inputs[4])) && (a155.equals("i")))))) {
    	a165 += (a165 + 20) > a165 ? 4 : 0;
    	a98 += (a98 + 20) > a98 ? 6 : 0;
    	a174 += (a174 + 20) > a174 ? 4 : 0;
    	cf = false;
    	a302 = a346[(a57 + -9)];
    	a8 = ((a359 - a270) - -17);
    	a277 = (((((((a310 * a310) % 14999) + 14212) + -29297) * 1) % 78) + 113);
    	a333 = (((((a249 * a249) % 14999) + -28199) / 5) - 9931);
    	a358 = a351;
    	a286 = a294[(a237 - 2)];
    	a225 = a205[(a398 - a57)];
    	a75 = "f";
    	a243 = (((((a243 * a277) % 11) + -166) * 1) - 1);
    	a370 = a285;
    	a320 = ((a57 * a398) + -103);
    	a125 = a30[(a158 - 10)];
    	a373 = false;
    	a211 = (a398 - 10);
    	a307 = a227[(a206 + -4)];
    	a382 = ((((((a382 * a201) % 14999) % 12) - 51) + -18636) + 18636);
    	a260 = false;
    	a224 = "f";
    	a338 = (a8 + -6);
    	a240 = false;
    	a353 = a241;
    	a234 = a372[(a398 - 11)];
    	a386 = ((((((a386 * a357) % 61) - -138) * 1) + 4508) - 4507);
    	a228 = a264;
    	a357 = (((((a357 * a310) * 1) % 65) - 121) - 2);
    	a202 = a217[((a398 + a237) + -15)];
    	a173 = "f";
    	a336 = true;
    	a329 = "g";
    	a270 = ((a237 + a230) + 3);
    	a359 = (a398 + -7); 
    	System.out.println("R");
    } 
    if(((((a75.equals("g")) && (((77 == a358[0]) && !a373) && ((201 < a386) && (325 >= a386)))) && (a218.equals("g"))) && (((a158 == 11) && ((a155.equals("i")) && ((a359 == 5) && ((a57 == 10) && (input.equals(inputs[3]) && cf))))) && ((-156 < a243) && (-68 >= a243))))) {
    	cf = false;
    	a218 = "f";
    	a211 = (a158 + -9);
    	a240 = false;
    	a225 = a205[(a359 + -5)];
    	a256 = "f";
    	a270 = a398;
    	a302 = a346[(a237 - 3)];
    	a357 = (((((a357 * a310) * 1) % 65) - 121) + -1);
    	a228 = a229;
    	a370 = a285;
    	a173 = "f";
    	a383 = a226[(a320 - a320)];
    	a207 = "f";
    	a373 = false;
    	a382 = ((((((a382 * a357) % 14999) % 12) + -50) + 18515) + -18516);
    	a307 = a227[(a158 - 10)];
    	a361 = "f";
    	a395 = false;
    	a243 = (((((((a249 * a386) % 14999) - -604) - -6258) / 5) % 11) + -175);
    	a239 = a299;
    	a338 = ((a230 / a230) + 3);
    	a359 = (a211 + 2);
    	a358 = a351;
    	a320 = ((a206 - a359) + 6);
    	a75 = "f";
    	a260 = false;
    	a386 = ((((((a386 * a243) % 14999) - 11300) - 1625) % 61) - -186);
    	a125 = a30[(a57 + -10)];
    	a300 = false;
    	a265 = a293;
    	a54 = ((((87 * 5) * 5) * 10) / 9); 
    	System.out.println("V");
    } 
}
private  void calculateOutputm8(String input) {
    if(((((147 < a333) && (177 >= a333)) && (a378 && ((50 == a392[4]) && (38 == a370[4])))) && (((cf && (a158 == 4)) && (59 == a228[3])) && ((-156 < a243) && (-68 >= a243))))) {
    	if((((92 == a296[2]) && ((((a361.equals("g")) && (a269.equals("g"))) && (a361.equals("g"))) && (92 == a296[2]))) && ((cf && a81 == a167[2]) && a202 == a217[2]))) {
    		calculateOutputm102(input);
    	} 
    } 
    if((((((a158 == 5) && cf) && (a256.equals("g"))) && a324 == a232[2]) && (((((-57 < a357) && (20 >= a357)) && !a339) && (a207.equals("g"))) && (47 == a239[4])))) {
    	if(((a235 == a216[2] && ((47 == a239[4]) && (a200 == a115[2] && cf))) && (((!a373 && (a359 == 5)) && a240) && (47 == a239[4])))) {
    		calculateOutputm104(input);
    	} 
    	if(((((a211 == 3) && (!a339 && ((a359 == 5) && (a312.equals("g"))))) && a286 == a294[2]) && ((77 == a358[0]) && (a200 == a115[5] && cf)))) {
    		calculateOutputm105(input);
    	} 
    	if((((a395 && (cf && a200 == a115[7])) && ((-12 < a201) && (178 >= a201))) && ((a260 && ((a398 == 12) && a307 == a227[2])) && a235 == a216[2]))) {
    		calculateOutputm107(input);
    	} 
    } 
    if(((((a206 == 6) && (a320 == 8)) && (15 == a353[2])) && (((((a158 == 6) && cf) && (a313.equals("g"))) && (a256.equals("g"))) && (a313.equals("g"))))) {
    	if(((((-39 < a382) && (176 >= a382)) && (a302 == a346[2] && ((a105.equals("g")) && cf))) && ((((a320 == 8) && (a237 == 5)) && a395) && (a313.equals("g"))))) {
    		calculateOutputm108(input);
    	} 
    } 
    if(((((((-12 < a201) && (178 >= a201)) && (56 == a265[2])) && a235 == a216[2]) && (a230 == 5)) && (((cf && (a158 == 8)) && a202 == a217[2]) && (56 == a265[2])))) {
    	if(((!a373 && ((cf && a81 == a167[5]) && (47 == a239[4]))) && (a378 && ((((-39 < a382) && (176 >= a382)) && a260) && a324 == a232[2])))) {
    		calculateOutputm113(input);
    	} 
    } 
    if(((((38 == a370[4]) && a300) && (47 == a239[4])) && (((((a158 == 11) && cf) && !a373) && (47 == a239[4])) && (59 == a228[3])))) {
    	if(((((201 < a386) && (325 >= a386)) && a260) && (((a256.equals("g")) && ((56 == a265[2]) && ((a338 == 5) && (cf && (a155.equals("i")))))) && (a320 == 8)))) {
    		calculateOutputm115(input);
    	} 
    } 
}
private  void calculateOutputm116(String input) {
    if(((((148 < a277) && (339 >= a277)) && (((a75.equals("g")) && (input.equals(inputs[1]) && cf)) && (a57 == 11))) && (((316 < a310) && (357 >= a310)) && ((a218.equals("g")) && ((a256.equals("g")) && (a302 == a346[2] && ((a143.equals("f")) && (((-12 < a201) && (178 >= a201)) && a14 == a79[2])))))))) {
    	cf = false;
    	a270 = ((a359 + a230) + 1);
    	a218 = "f";
    	a240 = false;
    	a338 = (a57 + -7);
    	a172 = (a237 + -1);
    	a357 = (((((a357 * a243) % 65) + -122) * 1) * 1);
    	a302 = a346[(a359 - 4)];
    	a224 = "f";
    	a75 = "f";
    	a310 = (((((((a310 * a243) % 14999) % 77) + 238) + 2) + 29314) + -29316);
    	a312 = "f";
    	a392 = a257;
    	a320 = ((a206 / a270) + 7);
    	a173 = "g";
    	a274 = a290; 
    	System.out.println("R");
    } 
    if((((((a359 == 5) && ((-156 < a243) && (-68 >= a243))) && a235 == a216[2]) && (77 == a358[0])) && (((a361.equals("g")) && ((a143.equals("f")) && (((((a75.equals("g")) && cf) && (a57 == 11)) && input.equals(inputs[9])) && (50 == a392[4])))) && a14 == a79[2]))) {
    	a67 += (a67 + 20) > a67 ? 1 : 0;
    	cf = false;
    	a235 = a216[((a230 + a211) + -6)];
    	a125 = a30[(a206 - -1)];
    	a382 = (((((a382 * a357) % 12) - 51) - -1) + -3);
    	a224 = "f";
    	a276 = a289;
    	a357 = (((((a357 * a243) * 3) - 508) % 65) - 121);
    	a320 = ((a338 / a338) + 6);
    	a240 = false;
    	a173 = "f";
    	a361 = "f";
    	a75 = "f";
    	a392 = a257;
    	a129 = a92[(a270 - 5)];
    	a270 = (a230 - -6);
    	a338 = (a206 + -2);
    	a201 = (((((((a201 * a277) % 14999) - -2340) * 1) + 9250) % 93) - 104);
    	a312 = "f";
    	a359 = (a57 - 7);
    	a368 = true;
    	a310 = (((((((a310 * a277) % 14999) + 1478) - 10320) / 5) % 77) - -237);
    	a277 = (((((((a277 * a310) % 14999) % 78) - 2) - 3) - 21339) - -21353);
    	a230 = (a237 - 1);
    	a206 = a237;
    	a237 = (a211 * a211); 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm118(String input) {
    if(((((a320 == 8) && ((a57 == 11) && (cf && (a75.equals("g"))))) && input.equals(inputs[6])) && ((((a398 == 12) && ((a173.equals("e")) && ((a378 && (a361.equals("g"))) && (a143.equals("g"))))) && (a361.equals("g"))) && (56 == a265[2])))) {
    	a164 -= (a164 - 20) < a164 ? 4 : 0;
    	a168 += (a168 + 20) > a168 ? 2 : 0;
    	cf = false;
    	a333 = ((((((a333 * a382) % 14999) * 2) % 14976) - 15022) * 1);
    	a260 = false;
    	a75 = "h";
    	a320 = (a338 + a359);
    	a353 = a241;
    	a265 = a303;
    	a234 = a372[((a57 / a320) + -1)];
    	a201 = (((((a201 * a357) - 5843) / 5) % 93) - 66);
    	a373 = true;
    	a361 = "e";
    	a382 = (((((((a357 * a249) % 14999) - -9254) % 14967) - 15031) - -23443) - 23443);
    	a240 = true;
    	a196 = (a270 - -3);
    	a307 = a227[(a237 - 2)];
    	a218 = "e";
    	a398 = (a237 - -8);
    	a135 = "g";
    	a324 = a232[((a57 - a320) + -4)];
    	a134 = true;
    	a230 = (a57 - 7);
    	a270 = (a338 - -8);
    	a357 = ((((((a357 * a249) % 14999) - -3000) % 65) + -121) + -2); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm9(String input) {
    if(((((a230 == 5) && ((a240 && (cf && (a143.equals("f")))) && (a312.equals("g")))) && (a359 == 5)) && (a202 == a217[2] && (a338 == 5)))) {
    	if((((a224.equals("g")) && (((cf && a14 == a79[2]) && (a320 == 8)) && a240)) && (((a206 == 6) && (47 == a239[4])) && (110 == a276[2])))) {
    		calculateOutputm116(input);
    	} 
    } 
    if((((cf && (a143.equals("g"))) && a260) && ((77 == a358[0]) && (!a373 && (a300 && ((15 == a353[2]) && a307 == a227[2])))))) {
    	if(((((a173.equals("e")) && cf) && (56 == a265[2])) && (((147 < a333) && (177 >= a333)) && (((a202 == a217[2] && a324 == a232[2]) && a234 == a372[2]) && (56 == a265[2]))))) {
    		calculateOutputm118(input);
    	} 
    } 
}
private  void calculateOutputm128(String input) {
    if(((((input.equals(inputs[0]) && (((((a57 == 12) && cf) && a81 == a167[2]) && (a75.equals("g"))) && (a361.equals("g")))) && (a158 == 4)) && a234 == a372[2]) && ((a336 && ((56 == a265[2]) && ((147 < a333) && (177 >= a333)))) && (a338 == 5)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a162 -= (a162 - 20) < a162 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	a44 = "f";
    	a132 = false;
    	a75 = "e";
    	a77 = (a57 + -4); 
    	System.out.println("T");
    } 
    if((((((38 == a370[4]) && ((a158 == 4) && ((59 == a228[3]) && input.equals(inputs[3])))) && (a207.equals("g"))) && ((147 < a333) && (177 >= a333))) && ((15 == a353[2]) && (a202 == a217[2] && ((a81 == a167[2] && ((a57 == 12) && cf)) && (a75.equals("g"))))))) {
    	a122 -= (a122 - 20) < a122 ? 4 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	cf = false;
    	if((((a125 == 5) && (((82 < a170) && (121 >= a170)) && !(a191 == 8))) || (26 == a137[1]))) {
    	a72 = (((((a382 * a243) % 14948) + -15051) + 12910) + -12910);
    	a57 = (a158 + 11);
    	a141 = a47[(a270 + -5)]; 
    	}else {
    	a225 = a205[6];
    	a368 = true;
    	a277 = (((a277 + -28407) + -1250) - 120);
    	a357 = (((a357 - 16778) + -2204) - 544);
    	a358 = a335;
    	a243 = (((a243 - 14915) * 1) - 2051);
    	a206 = 4;
    	a386 = ((((a386 * 5) % 61) - -236) + -29);
    	a378 = true;
    	a75 = "f";
    	a353 = a399;
    	a269 = "i";
    	a235 = a216[3];
    	a270 = 15;
    	a240 = true;
    	a239 = a268;
    	a313 = "g";
    	a296 = a362;
    	a359 = 7;
    	a228 = a292;
    	a398 = 17;
    	a130 = (((74 - -7467) + -14840) * 4);
    	a173 = "e";
    	a276 = a289;
    	a265 = a376;
    	a207 = "i";
    	a361 = "e";
    	a333 = ((((a333 * 10) / -9) - 26275) * 1);
    	a302 = a346[3];
    	a329 = "i";
    	a382 = (((a382 + -4013) + -1440) / 5);
    	a310 = ((((a310 + 23163) * 10) / 9) - -2154);
    	a191 = a37[((a338 - a230) + 6)];
    	a373 = false;
    	a234 = a372[7];
    	a202 = a217[3];
    	a392 = a208;
    	a300 = true;
    	a312 = "e";
    	a201 = (((((a201 - -6305) % 94) - -44) - -3312) + -3315);
    	a237 = 7;
    	a211 = 1;
    	a383 = a226[6];
    	a370 = a318;
    	a224 = "g";
    	a218 = "h";
    	a336 = false;
    	a230 = 8;
    	a339 = true;
    	a256 = "i";
    	a307 = a227[0];
    	a249 = (((((a249 % 71) + 384) + 17360) - 14917) - 2415);
    	a320 = 9;
    	a324 = a232[2];
    	a338 = 8;
    	}System.out.println("S");
    } 
    if(((((a313.equals("g")) && ((((cf && a81 == a167[2]) && (a158 == 4)) && (a75.equals("g"))) && ((-156 < a243) && (-68 >= a243)))) && (a312.equals("g"))) && (a234 == a372[2] && ((input.equals(inputs[6]) && ((a398 == 12) && (92 == a296[2]))) && (a57 == 12))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if((45 == a228[1])) {
    	a179 = ((((((((a243 * a386) % 14999) % 40) - 16) + -2) * 5) % 40) + -17);
    	a81 = a167[((a270 / a57) - -3)]; 
    	}else {
    	a235 = a216[1];
    	a296 = a212;
    	a265 = a376;
    	a237 = 9;
    	a277 = ((((a277 * 23) / 10) * 5) - -12882);
    	a398 = 11;
    	a249 = (((a249 / 5) - 4132) + 18934);
    	a338 = 10;
    	a320 = 10;
    	a46 = a18;
    	a260 = false;
    	a269 = "h";
    	a270 = 12;
    	a146 = true;
    	a329 = "h";
    	a234 = a372[4];
    	a373 = false;
    	a243 = (((a243 + -6565) + -20100) - -42616);
    	a383 = a226[3];
    	a312 = "g";
    	a386 = (((a386 - 3009) + 26681) * 1);
    	a286 = a294[2];
    	a324 = a232[3];
    	a313 = "h";
    	a395 = false;
    	a336 = false;
    	a218 = "h";
    	a240 = false;
    	a202 = a217[7];
    	a392 = a304;
    	a300 = false;
    	a211 = 2;
    	a358 = a348;
    	a276 = a289;
    	a228 = a292;
    	a207 = "g";
    	a224 = "i";
    	a239 = a299;
    	a310 = ((((a310 - 145) * 1) - -5039) - 4966);
    	a302 = a346[3];
    	a370 = a311;
    	a382 = (((((a382 * 5) - -11924) / 5) % 107) - -46);
    	a225 = a205[0];
    	a206 = 10;
    	a359 = 6;
    	a201 = ((((a201 % 94) + 83) - -2021) + -2019);
    	a230 = 8;
    	a307 = a227[6];
    	a256 = "i";
    	a361 = "h";
    	a75 = "i";
    	a357 = ((((a357 + 12871) * 2) - 42771) + 25608);
    	a378 = true;
    	a353 = a399;
    	a13 = (((19 - 15553) * 1) + 15620);
    	}System.out.println("R");
    } 
    if(((((a237 == 5) && ((a158 == 4) && (((a81 == a167[2] && cf) && (a57 == 12)) && (a75.equals("g"))))) && (a313.equals("g"))) && ((((a338 == 5) && (((147 < a333) && (177 >= a333)) && a302 == a346[2])) && input.equals(inputs[8])) && (a269.equals("g"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a98 -= (a98 - 20) < a98 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a31 += (a31 + 20) > a31 ? 4 : 0;
    	cf = false;
    	a395 = false;
    	a278 = a326[((a57 - a57) + 2)];
    	a57 = (a320 + 8);
    	a40 = a65; 
    	System.out.println("Y");
    } 
    if((((input.equals(inputs[2]) && (a81 == a167[2] && ((110 == a276[2]) && ((148 < a277) && (339 >= a277))))) && (a313.equals("g"))) && ((a398 == 12) && ((a158 == 4) && ((((-156 < a243) && (-68 >= a243)) && ((cf && (a57 == 12)) && (a75.equals("g")))) && (a218.equals("g"))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if(((((a307 == 5) || a132) || ((-188 < a357) && (-57 >= a357))) && !(a206 == 10))) {
    	a72 = (((5 + -14540) - 7389) - -2361);
    	a57 = (a206 - -9);
    	a141 = a47[(a270 + -7)]; 
    	}else {
    	a239 = a268;
    	a358 = a351;
    	a329 = "h";
    	a224 = "i";
    	a277 = (((a277 + -26840) + -243) - -43536);
    	a310 = (((((a310 * 12) / 10) * 10) / 9) - -7738);
    	a370 = a311;
    	a141 = a47[(a158 + -1)];
    	a57 = (a270 + 3);
    	a383 = a226[2];
    	a386 = ((((a386 - 531) + -22269) * -1) / 10);
    	a312 = "h";
    	a225 = a205[3];
    	a398 = 13;
    	a202 = a217[4];
    	a359 = 10;
    	a72 = (((82 - 7497) * 4) - 246);
    	}System.out.println("R");
    } 
    if(((((a81 == a167[2] && (((a57 == 12) && ((147 < a333) && (177 >= a333))) && (a158 == 4))) && (77 == a358[0])) && ((148 < a277) && (339 >= a277))) && ((a269.equals("g")) && (((-12 < a201) && (178 >= a201)) && (a300 && ((cf && (a75.equals("g"))) && input.equals(inputs[4]))))))) {
    	a122 -= (a122 - 20) < a122 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	if(((a256.equals("h")) || !a132)) {
    	a57 = ((a320 + a230) + 2);
    	a141 = a47[(a338 - -2)];
    	a72 = ((((89 - 10928) * 2) * 10) / 9); 
    	}else {
    	a312 = "h";
    	a310 = ((((a310 * 12) / 10) + 3095) * 5);
    	a256 = "g";
    	a307 = a227[6];
    	a358 = a351;
    	a237 = 7;
    	a398 = 14;
    	a225 = a205[6];
    	a235 = a216[3];
    	a370 = a311;
    	a329 = "h";
    	a201 = ((((a201 / 5) - 3) * 5) - -16);
    	a302 = a346[3];
    	a353 = a263;
    	a368 = true;
    	a269 = "e";
    	a276 = a253;
    	a218 = "g";
    	a75 = "f";
    	a339 = true;
    	a338 = 10;
    	a129 = a92[(a359 - 4)];
    	a296 = a212;
    	a336 = true;
    	a224 = "i";
    	a172 = (a158 - 3);
    	a357 = (((a357 - -13824) * 2) + 139);
    	a313 = "h";
    	a270 = 13;
    	a240 = true;
    	a173 = "g";
    	a378 = true;
    	a265 = a303;
    	a230 = 3;
    	a207 = "h";
    	a382 = (((a382 + 3719) / 5) * 5);
    	a300 = true;
    	a243 = (((((a243 % 43) - 72) * 10) / 9) - 8);
    	a206 = 8;
    	a211 = 5;
    	a249 = ((((a249 * 5) + 14753) * 10) / 9);
    	a361 = "h";
    	a320 = 11;
    	a373 = false;
    	a202 = a217[1];
    	a234 = a372[6];
    	a333 = (((a333 - -2462) - -12820) * 1);
    	a386 = (((((a386 % 61) - -262) * 5) % 61) - -245);
    	a228 = a264;
    	a392 = a304;
    	a359 = 10;
    	}System.out.println("P");
    } 
    if((((a211 == 3) && (((-12 < a201) && (178 >= a201)) && ((a359 == 5) && (a57 == 12)))) && ((((a230 == 5) && (a302 == a346[2] && (((input.equals(inputs[1]) && cf) && (a158 == 4)) && a81 == a167[2]))) && (a75.equals("g"))) && (a338 == 5)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if(((a125 == a30[0] && !(a177.equals("e"))) && a383 == a226[3])) {
    	a69 = "h";
    	a143 = "i";
    	a57 = ((a320 / a206) - -10); 
    	}else {
    	a370 = a318;
    	a270 = 16;
    	a225 = a205[6];
    	a338 = 10;
    	a361 = "e";
    	a313 = "h";
    	a307 = a227[4];
    	a235 = a216[3];
    	a230 = 10;
    	a201 = (((a201 * 5) - -21691) - -3046);
    	a329 = "i";
    	a276 = a253;
    	a357 = (((((a357 * 5) % 38) + -17) - 15500) + 15499);
    	a382 = ((((a382 / 5) - -16611) * 10) / 9);
    	a75 = "f";
    	a392 = a304;
    	a310 = (((a310 + 10925) + 16347) / 5);
    	a202 = a217[0];
    	a302 = a346[6];
    	a129 = a92[(a398 - 11)];
    	a324 = a232[6];
    	a240 = true;
    	a173 = "f";
    	a125 = a30[(a211 + 4)];
    	}System.out.println("R");
    } 
    if(((a324 == a232[2] && ((((a75.equals("g")) && (a81 == a167[2] && cf)) && input.equals(inputs[5])) && (a57 == 12))) && ((((a302 == a346[2] && ((47 == a239[4]) && (a158 == 4))) && a336) && (a211 == 3)) && (a224.equals("g"))))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a35 -= (a35 - 20) < a35 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	if((!(a81 == 4) && (!a97 && (!a221 || ((a141 == 14) && !(27 == a46[0])))))) {
    	a57 = (a158 - -10);
    	a134 = false;
    	a71 = a175[(a57 - 13)]; 
    	}else {
    	a57 = (a398 + 5);
    	a40 = a83;
    	a25 = a68;
    	}System.out.println("T");
    } 
    if((((a158 == 4) && ((((a225 == a205[2] && ((input.equals(inputs[7]) && cf) && a81 == a167[2])) && (a329.equals("g"))) && (a312.equals("g"))) && ((-156 < a243) && (-68 >= a243)))) && (((a75.equals("g")) && ((a237 == 5) && (a237 == 5))) && (a57 == 12)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a178 += (a178 + 20) > a178 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a136 = ((a158 / a57) + 5);
    	a132 = false;
    	a75 = "e";
    	a77 = a359; 
    	System.out.println("Y");
    } 
    if(((a81 == a167[2] && ((a338 == 5) && (((-12 < a201) && (178 >= a201)) && (((input.equals(inputs[9]) && cf) && (a75.equals("g"))) && (a158 == 4))))) && (((a57 == 12) && (((-156 < a243) && (-68 >= a243)) && (a383 == a226[2] && a202 == a217[2]))) && (a207.equals("g"))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a35 -= (a35 - 20) < a35 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a75 = "f";
    	a172 = (a338 - -2);
    	a173 = "g";
    	a99 = a26[((a172 / a158) - 1)]; 
    	System.out.println("S");
    } 
}
private  void calculateOutputm129(String input) {
    if((((38 == a370[4]) && ((a313.equals("g")) && (((77 == a358[0]) && ((((a57 == 12) && cf) && (a75.equals("g"))) && (a320 == 8))) && (a211 == 3)))) && ((a158 == 5) && (a81 == a167[2] && (a300 && input.equals(inputs[4])))))) {
    	a174 += (a174 + 20) > a174 ? 4 : 0;
    	cf = false;
    	a154 = ((a158 * a206) + -15);
    	a224 = "f";
    	a249 = ((((((a249 * a277) % 14999) + 6368) - 26667) * 10) / 9);
    	a57 = (a359 - -8);
    	a230 = (a338 + -1);
    	a313 = "e";
    	a310 = (((((((a310 * a386) % 14999) - 21422) % 77) + 258) + 19761) + -19706);
    	a137 = a189;
    	a361 = "e";
    	a235 = a216[(a57 - 13)];
    	a370 = a285;
    	a206 = (a320 - 4);
    	a277 = ((((((a277 * a333) % 14999) / 5) % 78) + -7) / 5);
    	a392 = a257;
    	a320 = ((a154 * a211) - 39); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm131(String input) {
    if(((input.equals(inputs[5]) && (((a307 == a227[2] && a378) && (a158 == 9)) && a81 == a167[2])) && ((a269.equals("g")) && ((a240 && ((a218.equals("g")) && ((cf && (a57 == 12)) && (a75.equals("g"))))) && (77 == a358[0]))))) {
    	cf = false;
    	a134 = false;
    	a270 = ((a57 * a57) + -133);
    	a206 = ((a338 / a237) + 4);
    	a201 = ((((((a249 * a277) % 14999) / 5) + 15725) % 93) - 140);
    	a230 = ((a338 - a237) + 5);
    	a329 = "g";
    	a202 = a217[(a57 - 12)];
    	a235 = a216[((a211 + a57) - 12)];
    	a295 = a366[((a158 * a158) + -79)];
    	a171 = "i";
    	a383 = a226[((a57 / a57) + 1)];
    	a277 = (((((((a277 * a249) % 14999) + -23040) - -33261) / 5) % 78) + -7);
    	a392 = a257;
    	a265 = a376;
    	a382 = ((((((a249 * a310) % 14999) % 12) - 54) + -25921) - -25916);
    	a307 = a227[(a398 - 11)];
    	a313 = "f";
    	a240 = false;
    	a333 = ((((a333 * a357) - 6051) - -1656) - 2595);
    	a296 = a384;
    	a75 = "h";
    	a312 = "f";
    	a310 = ((((((a249 * a249) % 14999) / 5) + 6868) % 77) - -195);
    	a218 = "e";
    	a378 = true;
    	a361 = "f";
    	a260 = false;
    	a353 = a263;
    	a368 = false;
    	a225 = a205[(a57 + -11)];
    	a237 = a359;
    	a286 = a294[(a57 + -10)];
    	a269 = "e";
    	a398 = (a211 + 10);
    	a370 = a285;
    	a358 = a348;
    	a386 = ((((70 * 15) / 10) - 27) * 1);
    	a243 = ((((a243 * a357) - 18099) / 5) / 5);
    	a234 = a372[(a57 - 12)];
    	a395 = true;
    	a249 = (((53 - 3397) / 5) - -924);
    	a256 = "g";
    	a300 = true;
    	a224 = "e";
    	a357 = ((((83 + -891) - -739) * 9) / 10);
    	a373 = false;
    	a302 = a346[((a206 / a320) + 1)]; 
    	System.out.println("V");
    } 
    if((((a398 == 12) && (a302 == a346[2] && ((a398 == 12) && ((a75.equals("g")) && ((148 < a277) && (339 >= a277)))))) && ((a158 == 9) && (input.equals(inputs[6]) && (a202 == a217[2] && (((a57 == 12) && (a81 == a167[2] && cf)) && ((-57 < a357) && (20 >= a357)))))))) {
    	cf = false;
    	if(((a125 == 5) && ((-10 < a13) && (203 >= a13)))) {
    	a378 = false;
    	a307 = a227[(a57 - 11)];
    	a125 = a30[(a237 + 2)];
    	a240 = false;
    	a206 = (a57 - 7);
    	a302 = a346[(a230 - 4)];
    	a357 = (((((a357 * a201) - 3100) % 65) - 122) - 1);
    	a310 = (((((((a310 * a277) % 14999) / 5) / 5) - -2120) % 77) - -161);
    	a207 = "f";
    	a336 = false;
    	a370 = a285;
    	a224 = "e";
    	a359 = (a237 - 1);
    	a211 = ((a270 + a237) + -14);
    	a243 = ((((((a243 * a201) % 14999) - 26940) % 11) + -166) + -2);
    	a398 = a270;
    	a338 = (a57 - 8);
    	a269 = "f";
    	a249 = (((((((a249 * a382) % 14999) % 101) + 254) - -1) - -23191) + -23192);
    	a324 = a232[((a158 + a57) + -20)];
    	a235 = a216[(a237 + -4)];
    	a333 = (((((((a333 * a201) % 14999) + -2497) % 96) + 122) - 23003) - -22971);
    	a239 = a299;
    	a392 = a257;
    	a276 = a289;
    	a320 = (a237 + 2);
    	a230 = ((a206 - a398) + 10);
    	a75 = "f";
    	a368 = false;
    	a173 = "f";
    	a202 = a217[(a158 + -8)];
    	a339 = false;
    	a129 = a92[(a57 - 10)];
    	a358 = a351;
    	a218 = "f";
    	a237 = ((a320 * a320) - 45); 
    	}else {
    	a211 = ((a230 + a230) - 8);
    	a207 = "f";
    	a368 = false;
    	a357 = (((((((a386 * a382) % 65) - 63) * 10) / 9) - 10964) - -10975);
    	a75 = "f";
    	a353 = a241;
    	a339 = false;
    	a310 = (((((((a201 * a277) % 14999) % 77) + 237) - -1) / 5) - -206);
    	a338 = ((a230 + a230) - 6);
    	a392 = a257;
    	a173 = "i";
    	a202 = a217[(a230 + -4)];
    	a237 = (a230 - 1);
    	a307 = a227[(a230 - 4)];
    	a218 = "f";
    	a370 = a285;
    	a141 = a47[(a57 + -5)];
    	a234 = a372[(a270 + -10)];
    	a275 = a223;
    	a239 = a299;
    	a235 = a216[((a158 / a338) + -1)];
    	a324 = a232[(a230 + -4)];
    	a249 = ((((((a249 * a357) % 14999) % 101) - -253) - -1) * 1);
    	a243 = ((((((a243 * a201) % 14999) % 11) + -166) - 2) - -1);
    	a302 = a346[(a230 - 4)];
    	a276 = a289;
    	a333 = ((((((((a333 * a310) % 14999) % 96) - 5) - -51) * 5) % 96) + 19);
    	a359 = (a230 + -1);
    	a224 = "f";
    	a269 = "f";
    	a206 = (a398 - 7);
    	a336 = false;
    	a358 = a351;
    	a378 = false;
    	a320 = (a230 - -2);
    	a240 = false;
    	a398 = (a230 + 6);
    	a230 = ((a359 + a359) + -4);
    	}System.out.println("R");
    } 
}
private  void calculateOutputm132(String input) {
    if((((a307 == a227[2] && (((47 == a239[4]) && input.equals(inputs[4])) && (a206 == 6))) && (a398 == 12)) && ((((a158 == 11) && ((a312.equals("g")) && ((cf && (a75.equals("g"))) && a81 == a167[2]))) && (a57 == 12)) && (110 == a276[2])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	if(((9 == a274[0]) || ((241 < a54) && (402 >= a54)))) {
    	a42 = ((a158 * a57) + -125);
    	a173 = "i";
    	a75 = "f";
    	a141 = a47[(a158 - 10)]; 
    	}else {
    	a278 = a326[(a158 - 9)];
    	a57 = (a158 + 5);
    	a40 = a65;
    	}System.out.println("T");
    } 
    if(((a81 == a167[2] && (input.equals(inputs[6]) && (((50 == a392[4]) && ((201 < a386) && (325 >= a386))) && (a218.equals("g"))))) && ((59 == a228[3]) && (!a373 && ((a75.equals("g")) && (((a57 == 12) && (cf && (a158 == 11))) && !a339)))))) {
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	if(((!(a237 == 7) && ((((164 < a54) && (241 >= a54)) || ((276 < a130) && (493 >= a130))) || (a329.equals("h")))) && (a14 == 11))) {
    	a329 = "h";
    	a201 = (((a201 - 12641) - -4752) + -16323);
    	a228 = a229;
    	a276 = a253;
    	a75 = "f";
    	a310 = ((((a310 - -24852) - 34967) / 5) - -2290);
    	a392 = a304;
    	a173 = "f";
    	a81 = a167[((a359 - a57) + 7)];
    	a370 = a318;
    	a230 = 9;
    	a240 = true;
    	a382 = ((((a382 - -17139) * 10) / 9) - -566);
    	a302 = a346[7];
    	a218 = "e";
    	a324 = a232[2];
    	a270 = 14;
    	a357 = (((a357 - 7581) * 3) * 1);
    	a286 = a294[6];
    	a249 = (((a249 * 5) * 5) - -3065);
    	a125 = a30[((a158 - a57) - -6)]; 
    	}else {
    	a339 = false;
    	a358 = a351;
    	a361 = "e";
    	a313 = "e";
    	a368 = false;
    	a286 = a294[4];
    	a395 = true;
    	a225 = a205[6];
    	a158 = (a359 - 1);
    	}System.out.println("S");
    } 
    if(((((((a329.equals("g")) && ((a158 == 11) && (a218.equals("g")))) && (a75.equals("g"))) && (a329.equals("g"))) && (15 == a353[2])) && ((a359 == 5) && ((a81 == a167[2] && (((a57 == 12) && cf) && input.equals(inputs[2]))) && a234 == a372[2])))) {
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a75 = "e";
    	a132 = true;
    	a123 = true;
    	a137 = a117; 
    	System.out.println("T");
    } 
    if(((a202 == a217[2] && (((-39 < a382) && (176 >= a382)) && ((a224.equals("g")) && a286 == a294[2]))) && ((a75.equals("g")) && ((a57 == 12) && (((((input.equals(inputs[8]) && cf) && (a158 == 11)) && (a256.equals("g"))) && a81 == a167[2]) && a300))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a122 -= (a122 - 20) < a122 ? 3 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a370 = a318;
    	a373 = false;
    	a202 = a217[3];
    	a201 = (((a201 - -24445) * 1) * 1);
    	a277 = ((((a277 % 95) + 156) + 34) + -1);
    	a269 = "h";
    	a276 = a250;
    	a228 = a264;
    	a386 = (((((a386 * 5) % 61) + 229) + -13417) + 13408);
    	a235 = a216[1];
    	a324 = a232[3];
    	a270 = 11;
    	a392 = a208;
    	a312 = "g";
    	a361 = "h";
    	a358 = a348;
    	a57 = (a230 - -5);
    	a239 = a299;
    	a225 = a205[6];
    	a105 = "g";
    	a313 = "i";
    	a260 = false;
    	a378 = false;
    	a158 = ((a211 * a237) + -9);
    	a230 = 4;
    	a243 = ((((((a243 % 11) - 164) + 4) / 5) * 49) / 10);
    	a333 = (((a333 + 746) + 9989) * 2);
    	a398 = 12;
    	a211 = 5; 
    	System.out.println("R");
    } 
    if((((a395 && ((a75.equals("g")) && ((a81 == a167[2] && cf) && (a57 == 12)))) && (92 == a296[2])) && ((input.equals(inputs[5]) && ((((147 < a333) && (177 >= a333)) && ((15 == a353[2]) && ((355 < a249) && (499 >= a249)))) && (a158 == 11))) && a286 == a294[2]))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a46 = a148;
    	a146 = true;
    	a75 = "i";
    	a72 = ((((((2 * 5) * 259) / 10) / 5) * 51) / 10); 
    	System.out.println("Y");
    } 
    if(((a286 == a294[2] && ((a57 == 12) && a302 == a346[2])) && (((-12 < a201) && (178 >= a201)) && (((38 == a370[4]) && ((50 == a392[4]) && (input.equals(inputs[1]) && ((a158 == 11) && ((a75.equals("g")) && (a81 == a167[2] && cf)))))) && ((-57 < a357) && (20 >= a357)))))) {
    	a164 += (a164 + 20) > a164 ? 2 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a158 = (a230 - -4);
    	a3 = a114;
    	a57 = (a158 - -1); 
    	System.out.println("X");
    } 
    if(((((50 == a392[4]) && (((cf && (a75.equals("g"))) && (a57 == 12)) && a81 == a167[2])) && (110 == a276[2])) && (a324 == a232[2] && ((a329.equals("g")) && (((((-156 < a243) && (-68 >= a243)) && input.equals(inputs[0])) && (92 == a296[2])) && (a158 == 11)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if((((cf || (a286 == 6)) || (a329.equals("g"))) && !a368)) {
    	a256 = "h";
    	a382 = (((a382 / 5) + 28207) + 1321);
    	a383 = a226[5];
    	a395 = true;
    	a277 = (((a277 + 9710) / 5) * 5);
    	a358 = a351;
    	a336 = true;
    	a125 = a30[(a270 - 8)];
    	a211 = 5;
    	a249 = ((((a249 / 5) * 5) - 19966) + 19808);
    	a201 = (((a201 * 5) + -7971) * 3);
    	a265 = a376;
    	a218 = "h";
    	a240 = true;
    	a206 = 9;
    	a359 = 6;
    	a307 = a227[4];
    	a296 = a384;
    	a398 = 12;
    	a357 = ((((a357 / 5) + -4) + -21642) + 21609);
    	a269 = "g";
    	a302 = a346[0];
    	a225 = a205[0];
    	a210 = false;
    	a237 = 8;
    	a243 = ((((a243 * 10) / 3) + -24615) * 1);
    	a338 = 10;
    	a230 = 3;
    	a324 = a232[7];
    	a333 = (((a333 + 10630) - -9913) / 5);
    	a286 = a294[6];
    	a234 = a372[0];
    	a373 = true;
    	a310 = ((((((a310 % 20) + 329) * 5) + 14711) % 20) - -320);
    	a353 = a241;
    	a300 = true;
    	a235 = a216[0];
    	a370 = a311;
    	a312 = "e";
    	a173 = "f";
    	a329 = "e";
    	a339 = true;
    	a239 = a299;
    	a378 = true;
    	a320 = 6;
    	a260 = false;
    	a368 = true;
    	a202 = a217[3];
    	a276 = a253;
    	a228 = a264;
    	a75 = "f";
    	a392 = a304;
    	a224 = "e";
    	a207 = "e";
    	a270 = 13; 
    	}else {
    	a278 = a326[((a57 - a57) + 2)];
    	a57 = ((a320 + a211) + 5);
    	a40 = a65;
    	}System.out.println("Y");
    } 
    if(((((316 < a310) && (357 >= a310)) && (((a378 && !a373) && a300) && a81 == a167[2])) && ((((a158 == 11) && ((a57 == 12) && (input.equals(inputs[9]) && ((a75.equals("g")) && cf)))) && a378) && a378))) {
    	a120 -= (a120 - 20) < a120 ? 4 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	if((!(50 == a228[0]) || (!(a256.equals("g")) || (((-57 < a357) && (20 >= a357)) && (a307 == 2))))) {
    	a75 = "i";
    	a73 = (((93 * 5) / 5) + -23335);
    	a146 = false;
    	a39 = a144; 
    	}else {
    	a75 = "e";
    	a44 = "f";
    	a132 = false;
    	a77 = ((a270 - a158) + 7);
    	}System.out.println("T");
    } 
    if(((input.equals(inputs[3]) && (a234 == a372[2] && (a75.equals("g")))) && ((a230 == 5) && ((a256.equals("g")) && (a286 == a294[2] && ((((a81 == a167[2] && ((a57 == 12) && cf)) && ((355 < a249) && (499 >= a249))) && (a158 == 11)) && a368)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	 
    	System.out.println("V");
    } 
    if(((((((a75.equals("g")) && (input.equals(inputs[7]) && cf)) && (47 == a239[4])) && (a158 == 11)) && (a57 == 12)) && (((a383 == a226[2] && ((a359 == 5) && (a286 == a294[2] && (a230 == 5)))) && a81 == a167[2]) && (92 == a296[2])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a98 += (a98 + 20) > a98 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	a31 += (a31 + 20) > a31 ? 4 : 0;
    	cf = false;
    	a277 = (((((a277 / 5) * 5) * 5) * -1) / 10);
    	a207 = "h";
    	a357 = ((((((a357 % 38) + -18) * 5) * 5) % 38) + -17);
    	a302 = a346[6];
    	a382 = ((((a382 % 107) - -69) + 11469) + -11469);
    	a320 = 7;
    	a240 = true;
    	a211 = 5;
    	a359 = 8;
    	a75 = "f";
    	a230 = 8;
    	a239 = a268;
    	a392 = a304;
    	a310 = ((((a310 + -4465) + -4432) % 20) + 337);
    	a201 = (((a201 + 27721) + -3107) / 5);
    	a336 = false;
    	a338 = 3;
    	a235 = a216[5];
    	a270 = 14;
    	a224 = "i";
    	a368 = false;
    	a265 = a293;
    	a361 = "f";
    	a243 = (((a243 * -5) + -21395) - -32781);
    	a260 = true;
    	a386 = (((a386 - -28946) * 1) * -1);
    	a237 = 7;
    	a353 = a399;
    	a206 = 8;
    	a202 = a217[0];
    	a256 = "h";
    	a228 = a229;
    	a296 = a362;
    	a373 = false;
    	a324 = a232[4];
    	a329 = "e";
    	a234 = a372[3];
    	a125 = a30[((a158 + a57) + -22)];
    	a312 = "f";
    	a333 = (((a333 + 3436) - -26326) + 24);
    	a300 = false;
    	a249 = ((((a249 - -5603) + -18777) * 2) + 50337);
    	a383 = a226[3];
    	a378 = false;
    	a398 = 10;
    	a286 = a294[7];
    	a395 = true;
    	a370 = a311;
    	a339 = true;
    	a269 = "e";
    	a276 = a250;
    	a173 = "f";
    	a218 = "i";
    	a307 = a227[5];
    	a8 = a158; 
    	System.out.println("S");
    } 
}
private  void calculateOutputm135(String input) {
    if((((38 == a370[4]) && ((a57 == 12) && (a336 && a81 == a167[5]))) && (((-12 < a201) && (178 >= a201)) && ((a170 <=  26 && (((-39 < a382) && (176 >= a382)) && ((a218.equals("g")) && ((input.equals(inputs[1]) && cf) && (a75.equals("g")))))) && a368)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a158 = ((a57 / a57) + 6);
    	a13 = ((((((a170 * a170) % 14999) * 2) % 14995) - 15004) - 2);
    	a57 = (a158 + 3); 
    	System.out.println("Z");
    } 
    if((((((((56 == a265[2]) && (a361.equals("g"))) && a324 == a232[2]) && a378) && input.equals(inputs[0])) && (a57 == 12)) && ((a75.equals("g")) && ((((a81 == a167[5] && cf) && a170 <=  26) && !a339) && ((148 < a277) && (339 >= a277)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a40 = a83;
    	a57 = (a338 - -12);
    	a25 = a68; 
    	System.out.println("S");
    } 
    if((((47 == a239[4]) && (a324 == a232[2] && (a312.equals("g")))) && (((a338 == 5) && ((15 == a353[2]) && ((a57 == 12) && (((355 < a249) && (499 >= a249)) && (a170 <=  26 && ((cf && a81 == a167[5]) && (a75.equals("g")))))))) && input.equals(inputs[7])))) {
    	a122 -= (a122 - 20) < a122 ? 3 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a129 = a92[(a320 - 3)];
    	a75 = "e";
    	a132 = false;
    	a77 = a57; 
    	System.out.println("T");
    } 
    if(((a260 && (((a57 == 12) && (((a170 <=  26 && ((input.equals(inputs[8]) && cf) && a81 == a167[5])) && ((-156 < a243) && (-68 >= a243))) && a307 == a227[2])) && (47 == a239[4]))) && ((a75.equals("g")) && (a336 && (92 == a296[2]))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	a94 -= (a94 - 20) < a94 ? 2 : 0;
    	cf = false;
    	a75 = "f";
    	a125 = a30[(a206 - 1)];
    	a173 = "f";
    	a81 = a167[(a270 - 9)]; 
    	System.out.println("Y");
    } 
    if(((((a320 == 8) && ((a81 == a167[5] && (77 == a358[0])) && (a57 == 12))) && (15 == a353[2])) && ((a211 == 3) && ((((a75.equals("g")) && ((a170 <=  26 && cf) && input.equals(inputs[3]))) && a307 == a227[2]) && a307 == a227[2])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 += (a164 + 20) > a164 ? 3 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	a57 = 16;
    	a134 = true;
    	a278 = a326[(a57 + -10)]; 
    	System.out.println("V");
    } 
    if(((((a240 && (((-12 < a201) && (178 >= a201)) && ((cf && (a57 == 12)) && a81 == a167[5]))) && (92 == a296[2])) && (a75.equals("g"))) && ((((a170 <=  26 && a286 == a294[2]) && input.equals(inputs[9])) && a302 == a346[2]) && ((-39 < a382) && (176 >= a382))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a67 += (a67 + 20) > a67 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	if((!(a244 == 12) || (a240 || ((!(a129 == 10) || (a235 == 11)) || !(a81 == 5))))) {
    	a249 = (((((a249 * 10) / 15) + -19) - -14044) - 14084);
    	a370 = a311;
    	a234 = a372[7];
    	a324 = a232[4];
    	a260 = true;
    	a8 = (a237 - -6);
    	a207 = "e";
    	a358 = a335;
    	a357 = (((((a357 - 24057) % 38) - 15) * 9) / 10);
    	a353 = a399;
    	a320 = 9;
    	a383 = a226[0];
    	a386 = ((((a386 - 910) % 61) + 321) + 1);
    	a373 = false;
    	a239 = a268;
    	a173 = "f";
    	a338 = 6;
    	a201 = ((((a201 + 15164) * 10) / 9) + 46);
    	a339 = true;
    	a211 = 7;
    	a378 = false;
    	a206 = 8;
    	a395 = true;
    	a218 = "i";
    	a368 = false;
    	a230 = 9;
    	a398 = 12;
    	a286 = a294[2];
    	a243 = ((((a243 * 5) + 18318) % 43) + -152);
    	a235 = a216[7];
    	a224 = "g";
    	a265 = a293;
    	a313 = "e";
    	a202 = a217[0];
    	a125 = a30[(a270 + -11)];
    	a333 = ((((88 * -6) / 10) + -27058) * 1);
    	a336 = false;
    	a382 = (((a382 - 14975) * 1) - 11238);
    	a312 = "h";
    	a277 = (((((a277 + 11301) % 95) + 237) / 5) - -171);
    	a240 = true;
    	a75 = "f";
    	a237 = 3;
    	a329 = "e";
    	a276 = a250;
    	a392 = a208;
    	a225 = a205[7];
    	a359 = 3;
    	a256 = "i";
    	a296 = a362;
    	a269 = "f";
    	a300 = false;
    	a307 = a227[0];
    	a302 = a346[0];
    	a361 = "i";
    	a270 = 13; 
    	}else {
    	a69 = "g";
    	a143 = "i";
    	a57 = (a206 + 5);
    	}System.out.println("V");
    } 
    if(((a240 && ((a307 == a227[2] && ((a75.equals("g")) && (a329.equals("g")))) && (110 == a276[2]))) && (((((a57 == 12) && (a81 == a167[5] && (input.equals(inputs[5]) && cf))) && ((201 < a386) && (325 >= a386))) && a170 <=  26) && ((-39 < a382) && (176 >= a382))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	a31 += (a31 + 20) > a31 ? 1 : 0;
    	cf = false;
    	a143 = "i";
    	a69 = "f";
    	a57 = (a398 - 1); 
    	System.out.println("T");
    } 
    if(((a225 == a205[2] && ((a300 && (((a81 == a167[5] && ((a75.equals("g")) && cf)) && (a398 == 12)) && (a57 == 12))) && a170 <=  26)) && (a235 == a216[2] && ((input.equals(inputs[2]) && ((-39 < a382) && (176 >= a382))) && a368)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a75 = "f";
    	a91 = (a57 - 1);
    	a173 = "h";
    	a1 = a87[(a91 - 7)]; 
    	System.out.println("X");
    } 
    if(((a81 == a167[5] && ((a359 == 5) && (((355 < a249) && (499 >= a249)) && (!a339 && ((a57 == 12) && (((cf && a170 <=  26) && (a75.equals("g"))) && input.equals(inputs[4]))))))) && (((a398 == 12) && (a338 == 5)) && (a313.equals("g"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a51 += (a51 + 20) > a51 ? 3 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	a179 = (((((((a386 * a386) % 14999) % 40) - 27) + -23713) / 5) - -4701);
    	a81 = a167[((a57 + a320) - 16)]; 
    	System.out.println("R");
    } 
    if((((a170 <=  26 && (a81 == a167[5] && ((110 == a276[2]) && (input.equals(inputs[6]) && ((a57 == 12) && cf))))) && a378) && (!a373 && ((a211 == 3) && (((a218.equals("g")) && (a75.equals("g"))) && ((-156 < a243) && (-68 >= a243))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a158 = ((a211 - a338) - -7);
    	a200 = a115[((a270 - a158) - 1)];
    	a57 = (a359 + 5); 
    	System.out.println("T");
    } 
}
private  void calculateOutputm137(String input) {
    if(((((a207.equals("g")) && ((((a224.equals("g")) && ((a57 == 12) && (cf && (a75.equals("g"))))) && input.equals(inputs[4])) && a81 == a167[7])) && (a323 == 9)) && ((((15 == a353[2]) && (a218.equals("g"))) && ((147 < a333) && (177 >= a333))) && (38 == a370[4])))) {
    	a178 += (a178 + 20) > a178 ? 4 : 0;
    	cf = false;
    	if((((a278 == a326[5] && cf) && !(a202 == a217[5])) || !(a14 == a79[6]))) {
    	a277 = (((((((a277 * a333) % 14999) + -23601) * 1) / 5) % 78) - -81);
    	a137 = a189;
    	a57 = (a230 + 8);
    	a224 = "f";
    	a154 = ((a57 - a211) - -5);
    	a230 = (a57 + -9);
    	a249 = (((((a249 * a357) + 18944) % 15076) - 14923) + 0);
    	a361 = "e";
    	a206 = (a154 + -11);
    	a370 = a285;
    	a207 = "f";
    	a235 = a216[(a154 - 15)];
    	a310 = ((((((((a310 * a386) % 14999) % 77) + 176) * 10) / 9) * 10) / 9);
    	a225 = a205[(a154 + -15)]; 
    	}else {
    	a57 = (a359 - -6);
    	a265 = a376;
    	a339 = true;
    	a312 = "e";
    	a260 = true;
    	a270 = (a57 - -1);
    	a224 = "f";
    	a225 = a205[(a211 + -3)];
    	a173 = "e";
    	a249 = (((((a249 * a277) % 14999) + -1147) / 5) + -7398);
    	a368 = true;
    	a228 = a229;
    	a296 = a212;
    	a373 = false;
    	a398 = (a57 + 1);
    	a143 = "g";
    	a338 = (a323 - 6);
    	a277 = ((((((a243 * a333) % 78) - -103) + -25) * 9) / 10);
    	a201 = ((((((a382 * a243) % 94) + 84) - 2) - -5301) - 5299);
    	a320 = (a270 + -4);
    	a370 = a285;
    	a206 = (a270 - 8);
    	a234 = a372[((a57 * a323) - 97)];
    	a211 = (a230 + -4);
    	a276 = a253;
    	a207 = "f";
    	a235 = a216[((a323 * a320) + -72)];
    	a329 = "f";
    	a359 = (a398 - 9);
    	a310 = ((((((a310 * a243) % 14999) % 77) - -239) + -28219) + 28219);
    	}System.out.println("X");
    } 
}
private  void calculateOutputm10(String input) {
    if((((((50 == a392[4]) && ((((316 < a310) && (357 >= a310)) && (a224.equals("g"))) && a235 == a216[2])) && (a206 == 6)) && (15 == a353[2])) && (cf && a81 == a167[2]))) {
    	if(((((a361.equals("g")) && ((a230 == 5) && ((a158 == 4) && cf))) && a235 == a216[2]) && (a202 == a217[2] && ((a312.equals("g")) && (a218.equals("g")))))) {
    		calculateOutputm128(input);
    	} 
    	if((((a211 == 3) && (((cf && (a158 == 5)) && a324 == a232[2]) && (a361.equals("g")))) && (((110 == a276[2]) && a383 == a226[2]) && (a338 == 5)))) {
    		calculateOutputm129(input);
    	} 
    	if((((77 == a358[0]) && ((38 == a370[4]) && (((147 < a333) && (177 >= a333)) && (cf && (a158 == 9))))) && ((a368 && a378) && (a237 == 5)))) {
    		calculateOutputm131(input);
    	} 
    	if((((((a211 == 3) && (a211 == 3)) && a368) && (59 == a228[3])) && ((110 == a276[2]) && ((cf && (a158 == 11)) && (56 == a265[2]))))) {
    		calculateOutputm132(input);
    	} 
    } 
    if((((a359 == 5) && (((a206 == 6) && a286 == a294[2]) && (a237 == 5))) && ((a338 == 5) && ((cf && a81 == a167[5]) && a240)))) {
    	if((((((a211 == 3) && (a207.equals("g"))) && a202 == a217[2]) && a378) && ((50 == a392[4]) && (a302 == a346[2] && (a170 <=  26 && cf))))) {
    		calculateOutputm135(input);
    	} 
    } 
    if(((a235 == a216[2] && (((59 == a228[3]) && a302 == a346[2]) && ((316 < a310) && (357 >= a310)))) && (((a329.equals("g")) && (a81 == a167[7] && cf)) && a225 == a205[2]))) {
    	if((((cf && (a323 == 9)) && (a218.equals("g"))) && (((a336 && ((a361.equals("g")) && a324 == a232[2])) && (a206 == 6)) && ((355 < a249) && (499 >= a249))))) {
    		calculateOutputm137(input);
    	} 
    } 
}
private  void calculateOutputm138(String input) {
    if((((a240 && ((((cf && (a75.equals("g"))) && (a57 == 13)) && a302 == a346[2]) && (a154 == 10))) && (a338 == 5)) && ((20 == a137[1]) && (((a398 == 12) && ((47 == a239[4]) && (a207.equals("g")))) && input.equals(inputs[0]))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	if((a75.equals("f"))) {
    	a312 = "f";
    	a370 = a311;
    	a57 = (a320 + 8);
    	a313 = "i";
    	a378 = false;
    	a249 = ((((((75 + -20420) * -1) / 10) / 5) * 13) / 10);
    	a269 = "g";
    	a383 = a226[6];
    	a276 = a253;
    	a81 = a167[(a270 + -12)];
    	a392 = a208;
    	a228 = a264;
    	a382 = (((((a382 + -22880) % 107) - -139) - 13770) + 13777);
    	a339 = false;
    	a358 = a335;
    	a278 = a326[(a57 - 9)]; 
    	}else {
    	a129 = a92[(a57 - 13)];
    	a75 = "h";
    	a134 = true;
    	a196 = ((a206 * a154) + -50);
    	}System.out.println("R");
    } 
    if((((((20 == a137[1]) && (((a57 == 13) && (((-156 < a243) && (-68 >= a243)) && input.equals(inputs[3]))) && (a230 == 5))) && ((201 < a386) && (325 >= a386))) && a336) && (a307 == a227[2] && (!a373 && ((a75.equals("g")) && ((a154 == 10) && cf)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a169 -= (a169 - 20) < a169 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a336 = false;
    	a339 = false;
    	a358 = a351;
    	a276 = a250;
    	a234 = a372[4];
    	a359 = 4;
    	a211 = 4;
    	a134 = false;
    	a218 = "i";
    	a239 = a268;
    	a398 = 13;
    	a75 = "h";
    	a333 = ((((a333 / 5) - -5456) * 10) / 9);
    	a320 = 9;
    	a296 = a362;
    	a269 = "f";
    	a392 = a257;
    	a224 = "f";
    	a202 = a217[4];
    	a295 = a366[((a57 + a57) + -21)];
    	a228 = a264;
    	a361 = "i";
    	a373 = false;
    	a53 = (((((((a386 * a310) % 14999) % 59) + -15) * 5) % 59) + 14); 
    	System.out.println("T");
    } 
    if((((a256.equals("g")) && (((cf && input.equals(inputs[4])) && (a75.equals("g"))) && (77 == a358[0]))) && (((((((77 == a358[0]) && (a211 == 3)) && (59 == a228[3])) && (20 == a137[1])) && (a154 == 10)) && (a57 == 13)) && a235 == a216[2]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a174 += (a174 + 20) > a174 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a239 = a268;
    	a243 = (((((((a243 % 43) - 90) * 9) / 10) * 5) % 43) + -106);
    	a173 = "e";
    	a240 = true;
    	a310 = (((a310 * 5) + 9364) * 2);
    	a75 = "f";
    	a270 = 17;
    	a269 = "i";
    	a235 = a216[2];
    	a324 = a232[7];
    	a276 = a253;
    	a277 = ((((a277 / 5) + 162) + 21921) + -21856);
    	a357 = (((a357 - 16994) - 12672) + -276);
    	a206 = 7;
    	a191 = a37[((a237 + a211) + -3)];
    	a225 = a205[5];
    	a339 = true;
    	a359 = 7;
    	a224 = "i";
    	a130 = (((93 / 5) + -17133) * 1); 
    	System.out.println("Z");
    } 
    if(((((a211 == 3) && ((a206 == 6) && ((a256.equals("g")) && (a57 == 13)))) && a286 == a294[2]) && (((a154 == 10) && ((((cf && (a75.equals("g"))) && input.equals(inputs[6])) && a225 == a205[2]) && (20 == a137[1]))) && (a207.equals("g"))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a169 -= (a169 - 20) < a169 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	if((((147 < a333) && (177 >= a333)) || a260)) {
    	a302 = a346[0];
    	a172 = ((a57 / a230) - -5);
    	a218 = "e";
    	a75 = "f";
    	a329 = "h";
    	a269 = "f";
    	a206 = 4;
    	a270 = 13;
    	a300 = true;
    	a224 = "i";
    	a296 = a384;
    	a234 = a372[4];
    	a386 = ((((((a386 % 61) + 252) - 49) / 5) * 55) / 10);
    	a243 = ((((a243 - 2775) + 5473) + -7681) * -5);
    	a260 = true;
    	a336 = true;
    	a339 = true;
    	a307 = a227[1];
    	a237 = 9;
    	a256 = "e";
    	a277 = (((((a277 * 10) / 4) / 5) + -27063) - -37928);
    	a373 = true;
    	a310 = (((a310 + -13607) + 29461) / 5);
    	a359 = 9;
    	a338 = 7;
    	a286 = a294[3];
    	a235 = a216[7];
    	a225 = a205[7];
    	a358 = a348;
    	a201 = (((((a201 % 94) + 83) + -1) + 19883) - 19881);
    	a239 = a242;
    	a320 = 12;
    	a173 = "g";
    	a368 = true;
    	a265 = a303;
    	a398 = 10;
    	a357 = (((a357 + -14792) + -76) + -12963);
    	a202 = a217[7];
    	a333 = ((((((a333 % 14) - -156) + 1) / 5) * 48) / 10);
    	a249 = ((((59 - 29662) / 5) * 5) + 29920);
    	a228 = a229;
    	a382 = (((a382 + 2568) - -13881) * 1);
    	a207 = "h";
    	a240 = true;
    	a370 = a318;
    	a276 = a253;
    	a395 = true;
    	a211 = 8;
    	a312 = "g";
    	a324 = a232[0];
    	a230 = 8;
    	a361 = "h";
    	a353 = a399;
    	a99 = a26[((a57 - a172) + -1)]; 
    	}else {
    	a320 = 6;
    	a339 = true;
    	a237 = 9;
    	a225 = a205[0];
    	a382 = (((a382 - 8905) - 18713) - 1992);
    	a75 = "f";
    	a307 = a227[7];
    	a230 = 8;
    	a338 = 3;
    	a123 = false;
    	a206 = 9;
    	a207 = "i";
    	a300 = true;
    	a333 = (((a333 * 5) * 5) + -29731);
    	a235 = a216[7];
    	a256 = "i";
    	a234 = a372[4];
    	a276 = a253;
    	a277 = ((((((a277 % 95) + 164) * 10) / 9) - -28310) + -28262);
    	a211 = 8;
    	a395 = true;
    	a218 = "h";
    	a173 = "i";
    	a373 = true;
    	a361 = "h";
    	a286 = a294[0];
    	a260 = true;
    	a357 = (((((a357 % 38) + -18) * 5) % 38) + -18);
    	a269 = "i";
    	a336 = true;
    	a201 = (((((a201 % 94) + 83) + -19156) + 11678) - -7479);
    	a224 = "i";
    	a353 = a263;
    	a370 = a318;
    	a310 = (((a310 - -22744) - -428) + -17332);
    	a265 = a376;
    	a359 = 5;
    	a324 = a232[7];
    	a312 = "h";
    	a358 = a348;
    	a302 = a346[6];
    	a240 = true;
    	a329 = "e";
    	a270 = 17;
    	a368 = true;
    	a141 = a47[(a57 - 13)];
    	}System.out.println("S");
    } 
    if((((a154 == 10) && ((((a57 == 13) && (cf && (a75.equals("g")))) && (77 == a358[0])) && (a361.equals("g")))) && ((20 == a137[1]) && (input.equals(inputs[9]) && (a286 == a294[2] && ((a234 == a372[2] && a240) && a234 == a372[2])))))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a51 += (a51 + 20) > a51 ? 4 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a75 = "f";
    	a172 = (a230 + -4);
    	a173 = "g";
    	a129 = a92[(a172 - -2)]; 
    	System.out.println("T");
    } 
    if((((56 == a265[2]) && (((a230 == 5) && ((((47 == a239[4]) && (20 == a137[1])) && input.equals(inputs[1])) && ((316 < a310) && (357 >= a310)))) && (77 == a358[0]))) && (a395 && ((a57 == 13) && ((a154 == 10) && ((a75.equals("g")) && cf)))))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a162 += (a162 + 20) > a162 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	if(((258 < a72 && a295 == a366[3]) && a225 == a205[4])) {
    	a225 = a205[4];
    	a358 = a351;
    	a324 = a232[5];
    	a243 = (((a243 / 5) - 444) + 298);
    	a300 = false;
    	a296 = a384;
    	a201 = ((((a201 + 3716) % 94) - 8) - 3);
    	a237 = 8;
    	a373 = false;
    	a286 = a294[3];
    	a235 = a216[7];
    	a339 = false;
    	a249 = ((((90 - -23590) * 10) / 9) * 1);
    	a398 = 17;
    	a207 = "h";
    	a270 = 11;
    	a134 = true;
    	a269 = "g";
    	a260 = false;
    	a228 = a264;
    	a230 = 10;
    	a240 = false;
    	a382 = ((((a382 - -20967) * 1) % 107) - 11);
    	a336 = false;
    	a265 = a293;
    	a224 = "i";
    	a75 = "h";
    	a353 = a241;
    	a234 = a372[3];
    	a239 = a268;
    	a320 = 11;
    	a277 = (((((((a277 % 95) + 154) * 10) / 9) * 5) % 95) - -192);
    	a338 = 5;
    	a392 = a304;
    	a211 = 7;
    	a91 = (a154 + 3);
    	a329 = "f";
    	a312 = "g";
    	a333 = ((((a333 - 25031) + 24900) * 9) / 10);
    	a196 = (a359 - -12);
    	a307 = a227[5];
    	a386 = ((((a386 / 5) + 164) + 11837) - 11758);
    	a378 = false;
    	a383 = a226[6];
    	a361 = "f";
    	a276 = a289;
    	a256 = "g";
    	a370 = a311;
    	a395 = false;
    	a368 = false;
    	a206 = 7;
    	a310 = ((((a310 % 20) + 326) * 5) / 5);
    	a359 = 7; 
    	}else {
    	a239 = a242;
    	a207 = "h";
    	a370 = a311;
    	a312 = "f";
    	a243 = ((((a243 % 43) + -70) - -1) - -1);
    	a336 = true;
    	a276 = a253;
    	a211 = 8;
    	a339 = false;
    	a368 = true;
    	a81 = a167[(a270 - 10)];
    	a333 = ((((a333 + -4287) - 6746) * 10) / 9);
    	a277 = ((((a277 - -17228) - 10489) * 10) / 9);
    	a320 = 12;
    	a324 = a232[2];
    	a310 = (((a310 + 18437) - 18507) - 23880);
    	a357 = (((a357 + 27275) - -1928) - -235);
    	a338 = 7;
    	a395 = true;
    	a218 = "h";
    	a386 = (((a386 - 25429) * 1) / 5);
    	a225 = a205[3];
    	a256 = "e";
    	a228 = a229;
    	a173 = "f";
    	a361 = "g";
    	a240 = true;
    	a235 = a216[5];
    	a302 = a346[2];
    	a300 = true;
    	a125 = a30[(a154 + -5)];
    	a296 = a362;
    	a358 = a335;
    	a329 = "g";
    	a230 = 7;
    	a237 = 9;
    	a373 = true;
    	a202 = a217[4];
    	a398 = 17;
    	a307 = a227[4];
    	a359 = 7;
    	a201 = (((a201 - 26842) - 2086) + -554);
    	a75 = "f";
    	a206 = 11;
    	a382 = (((a382 - -29804) - 44817) + 39530);
    	a286 = a294[6];
    	a260 = true;
    	a270 = 15;
    	}System.out.println("V");
    } 
    if(((input.equals(inputs[2]) && (((((a154 == 10) && (cf && (20 == a137[1]))) && a260) && (a230 == 5)) && (a269.equals("g")))) && (((a75.equals("g")) && ((((-39 < a382) && (176 >= a382)) && (38 == a370[4])) && a225 == a205[2])) && (a57 == 13)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a164 -= (a164 - 20) < a164 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	if(a373) {
    	a296 = a212;
    	a237 = 7;
    	a383 = a226[6];
    	a155 = "i";
    	a277 = (((((a277 * 23) / 10) / 5) * 10) / 2);
    	a249 = (((((8 + 362) * 10) / 9) * 9) / 10);
    	a361 = "e";
    	a201 = (((a201 + 8332) + 6842) * 1);
    	a339 = true;
    	a235 = a216[0];
    	a324 = a232[5];
    	a310 = ((((a310 + 19898) * -1) * 10) / 9);
    	a206 = 6;
    	a378 = true;
    	a286 = a294[6];
    	a312 = "e";
    	a224 = "h";
    	a269 = "g";
    	a329 = "i";
    	a276 = a250;
    	a158 = ((a211 * a359) + -4);
    	a230 = 5;
    	a234 = a372[1];
    	a333 = (((a333 - 4547) - 9147) - 10224);
    	a398 = 10;
    	a392 = a208;
    	a313 = "i";
    	a202 = a217[2];
    	a368 = true;
    	a336 = true;
    	a57 = (a211 - -7); 
    	}else {
    	a296 = a212;
    	a224 = "i";
    	a235 = a216[2];
    	a172 = ((a57 * a154) + -129);
    	a310 = (((a310 - 6817) - 8156) + -6750);
    	a228 = a264;
    	a383 = a226[1];
    	a207 = "h";
    	a338 = 10;
    	a361 = "h";
    	a239 = a268;
    	a329 = "e";
    	a382 = ((((a382 * 5) % 107) + 68) + 0);
    	a320 = 12;
    	a260 = true;
    	a206 = 9;
    	a386 = (((a386 + -24132) + -6035) + -29);
    	a240 = true;
    	a357 = ((((a357 - 24434) % 38) - -20) - 37);
    	a312 = "g";
    	a201 = (((a201 + -8632) - 11284) - 2860);
    	a307 = a227[0];
    	a75 = "f";
    	a359 = 10;
    	a373 = true;
    	a276 = a253;
    	a368 = true;
    	a256 = "h";
    	a302 = a346[6];
    	a218 = "g";
    	a370 = a318;
    	a265 = a303;
    	a230 = 9;
    	a202 = a217[2];
    	a398 = 17;
    	a225 = a205[4];
    	a237 = 5;
    	a395 = true;
    	a300 = true;
    	a173 = "g";
    	a333 = ((((((a333 % 14) + 161) * 5) - -16297) % 14) + 150);
    	a243 = (((a243 + -18629) + -10440) - 643);
    	a336 = true;
    	a339 = true;
    	a211 = 4;
    	a269 = "e";
    	a270 = 12;
    	a286 = a294[7];
    	a358 = a335;
    	a324 = a232[4];
    	a129 = a92[(a172 + a172)];
    	}System.out.println("T");
    } 
    if(((a235 == a216[2] && (((20 == a137[1]) && a202 == a217[2]) && (a256.equals("g")))) && ((a237 == 5) && ((((((a154 == 10) && (cf && (a57 == 13))) && (a211 == 3)) && input.equals(inputs[7])) && a307 == a227[2]) && (a75.equals("g")))))) {
    	a164 += (a164 + 20) > a164 ? 4 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	a132 = false;
    	a155 = "f";
    	a75 = "e";
    	a77 = (a154 + -3); 
    	System.out.println("X");
    } 
    if(((((-12 < a201) && (178 >= a201)) && ((cf && (a57 == 13)) && (a154 == 10))) && (a235 == a216[2] && ((a359 == 5) && (input.equals(inputs[8]) && (((a75.equals("g")) && ((15 == a353[2]) && (((-57 < a357) && (20 >= a357)) && (20 == a137[1])))) && a336)))))) {
    	a169 -= (a169 - 20) < a169 ? 4 : 0;
    	a98 += (a98 + 20) > a98 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if((a99 == 6)) {
    	a134 = true;
    	a129 = a92[(a359 + 2)];
    	a75 = "h";
    	a196 = (a211 - -7); 
    	}else {
    	a40 = a65;
    	a141 = a47[(a57 - 7)];
    	a57 = ((a270 + a270) + -7);
    	}System.out.println("S");
    } 
    if((((a75.equals("g")) && (((cf && (20 == a137[1])) && input.equals(inputs[5])) && (a154 == 10))) && ((((((a57 == 13) && ((a224.equals("g")) && (56 == a265[2]))) && (56 == a265[2])) && a336) && a234 == a372[2]) && ((-39 < a382) && (176 >= a382))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	cf = false;
    	a75 = "h";
    	a295 = a366[((a57 + a154) - 18)];
    	a134 = false;
    	a53 = (((((a386 * a243) % 14999) - -27153) - -472) - -381); 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm139(String input) {
    if((((((a269.equals("g")) && ((147 < a333) && (177 >= a333))) && a202 == a217[2]) && (a154 == 11)) && ((((a256.equals("g")) && ((((a57 == 13) && (input.equals(inputs[3]) && cf)) && (20 == a137[1])) && (a75.equals("g")))) && a300) && (110 == a276[2])))) {
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	cf = false;
    	a269 = "f";
    	a392 = a257;
    	a75 = "f";
    	a329 = "f";
    	a339 = false;
    	a353 = a241;
    	a173 = "i";
    	a357 = (((((((a357 * a277) % 65) - 121) / 5) * 5) % 65) + -80);
    	a42 = ((a398 - a57) + 2);
    	a141 = a47[((a237 * a211) - 14)];
    	a260 = false;
    	a237 = (a359 - 1);
    	a240 = false;
    	a310 = (((((((a310 * a357) % 14999) + -13738) + 34656) * 1) % 77) - -177); 
    	System.out.println("Y");
    } 
    if(((((148 < a277) && (339 >= a277)) && ((a75.equals("g")) && (((a398 == 12) && ((a256.equals("g")) && ((a154 == 11) && ((20 == a137[1]) && cf)))) && input.equals(inputs[5])))) && ((a57 == 13) && (a307 == a227[2] && (a260 && (15 == a353[2])))))) {
    	a67 += (a67 + 20) > a67 ? 1 : 0;
    	cf = false;
    	a313 = "g";
    	a270 = (a338 + 5);
    	a395 = true;
    	a158 = (a398 - 6);
    	a206 = ((a237 + a211) - 2);
    	a276 = a253;
    	a57 = (a359 - -5);
    	a361 = "e";
    	a324 = a232[(a154 + -11)];
    	a201 = (((((((a310 * a310) % 14999) / 5) % 93) + -145) / 5) + -158);
    	a243 = ((((((a333 * a277) % 14999) - 27289) + -101) - -17015) + -13158);
    	a378 = true;
    	a386 = (((((a277 * a333) % 14999) - 16413) / 5) * 5);
    	a277 = ((((a277 * a357) + -1583) * 1) - 7962);
    	a105 = "g";
    	a373 = true;
    	a235 = a216[(a211 - 3)];
    	a260 = true;
    	a207 = "g";
    	a211 = ((a398 + a398) + -22);
    	a392 = a257;
    	a398 = a57;
    	a312 = "e";
    	a202 = a217[(a320 + -8)];
    	a228 = a229;
    	a239 = a242;
    	a370 = a285;
    	a269 = "f";
    	a333 = ((((((a333 * a382) % 14999) + 13800) % 14976) - 15022) * 1); 
    	System.out.println("T");
    } 
}
private  void calculateOutputm140(String input) {
    if((((a75.equals("g")) && (((a57 == 13) && ((201 < a386) && (325 >= a386))) && (a207.equals("g")))) && (((a398 == 12) && (((input.equals(inputs[4]) && ((cf && (a154 == 12)) && (20 == a137[1]))) && ((147 < a333) && (177 >= a333))) && (a329.equals("g")))) && a286 == a294[2]))) {
    	a164 += (a164 + 20) > a164 ? 1 : 0;
    	cf = false;
    	a339 = true;
    	a338 = (a398 + -9);
    	a276 = a253;
    	a395 = false;
    	a228 = a229;
    	a329 = "f";
    	a359 = (a398 + -9);
    	a173 = "e";
    	a143 = "g";
    	a57 = ((a154 * a154) + -133);
    	a207 = "f";
    	a313 = "e";
    	a368 = true;
    	a312 = "e";
    	a277 = (((((a277 * a357) % 78) + 69) + 2) + -2); 
    	System.out.println("X");
    } 
    if(((a378 && ((a234 == a372[2] && (input.equals(inputs[8]) && (cf && (a154 == 12)))) && (a75.equals("g")))) && ((((((-39 < a382) && (176 >= a382)) && (((-156 < a243) && (-68 >= a243)) && a202 == a217[2])) && (20 == a137[1])) && ((-12 < a201) && (178 >= a201))) && (a57 == 13)))) {
    	a51 += (a51 + 20) > a51 ? 3 : 0;
    	cf = false;
    	a277 = (((((((a277 * a382) % 14999) / 5) + 4813) + 6948) % 78) - -20);
    	a312 = "e";
    	a329 = "f";
    	a57 = (a338 - -6);
    	a339 = true;
    	a359 = (a338 - 2);
    	a173 = "e";
    	a368 = true;
    	a395 = false;
    	a313 = "e";
    	a143 = "g";
    	a207 = "f";
    	a276 = a253;
    	a228 = a229;
    	a338 = (a398 - 9); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm141(String input) {
    if((((a324 == a232[2] && ((a211 == 3) && ((a57 == 13) && ((cf && (a154 == 15)) && input.equals(inputs[6]))))) && a300) && ((a75.equals("g")) && ((20 == a137[1]) && (((a269.equals("g")) && (a218.equals("g"))) && (59 == a228[3])))))) {
    	a164 -= (a164 - 20) < a164 ? 4 : 0;
    	cf = false;
    	a276 = a253;
    	a382 = (((((((a382 * a249) % 14999) / 5) - 21925) - -30412) * -1) / 10);
    	a134 = true;
    	a135 = "g";
    	a228 = a229;
    	a339 = true;
    	a307 = a227[(a206 - 3)];
    	a211 = (a206 + -3);
    	a368 = true;
    	a196 = (a57 - -2);
    	a296 = a212;
    	a218 = "e";
    	a333 = ((((((a333 * a357) - 15311) + 19877) * 3) % 14976) + -15022);
    	a353 = a241;
    	a240 = true;
    	a75 = "h";
    	a329 = "f";
    	a338 = ((a154 / a237) - 2);
    	a359 = (a320 - 3);
    	a312 = "e";
    	a324 = a232[(a320 / a320)];
    	a357 = ((((((a357 * a249) % 14999) + 11006) * 1) % 65) + -122); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm142(String input) {
    if((((a202 == a217[2] && (a260 && ((20 == a137[1]) && (((a57 == 13) && ((a75.equals("g")) && (input.equals(inputs[5]) && cf))) && (a154 == 16))))) && ((((355 < a249) && (499 >= a249)) && ((a338 == 5) && (38 == a370[4]))) && ((355 < a249) && (499 >= a249)))) && (a56 % 2==0))) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	cf = false;
    	a155 = "f";
    	a75 = "e";
    	a132 = false;
    	a77 = (a154 - 9); 
    	System.out.println("T");
    } 
    if(((((15 == a353[2]) && ((38 == a370[4]) && (((a313.equals("g")) && (38 == a370[4])) && (20 == a137[1])))) && (a224.equals("g"))) && ((a230 == 5) && (((a75.equals("g")) && ((input.equals(inputs[0]) && cf) && (a154 == 16))) && (a57 == 13))))) {
    	a50 += (a50 + 20) > a50 ? 4 : 0;
    	cf = false;
    	a224 = "f";
    	a228 = a229;
    	a249 = ((((((a249 * a382) % 14999) * 2) % 15076) - 14923) * 1);
    	a201 = (((((((a333 * a333) % 14999) % 94) - -30) / 5) * 10) / 9);
    	a256 = "e";
    	a300 = true;
    	a395 = false;
    	a235 = a216[(a359 - 5)];
    	a329 = "f";
    	a237 = a211;
    	a378 = true;
    	a143 = "g";
    	a277 = ((((((a386 * a201) % 14999) % 78) - -69) / 5) * 5);
    	a276 = a253;
    	a392 = a257;
    	a207 = "f";
    	a370 = a285;
    	a57 = (a154 - 5);
    	a225 = a205[(a320 - a320)];
    	a313 = "e";
    	a339 = true;
    	a206 = (a359 + -1);
    	a368 = true;
    	a312 = "e";
    	a359 = (a154 + -13);
    	a338 = ((a154 * a211) + -45);
    	a296 = a212;
    	a173 = "e";
    	a211 = ((a398 - a270) + 1); 
    	System.out.println("X");
    } 
    if(((((a329.equals("g")) && ((92 == a296[2]) && (((20 == a137[1]) && a336) && input.equals(inputs[1])))) && ((a218.equals("g")) && (((a57 == 13) && (a234 == a372[2] && ((cf && (a154 == 16)) && (a75.equals("g"))))) && (a320 == 8)))) && a5 >= 7)) {
    	cf = false;
    	a338 = (a237 - 1);
    	a218 = "f";
    	a172 = ((a320 - a57) + 9);
    	a240 = false;
    	a313 = "f";
    	a302 = a346[(a206 - 5)];
    	a270 = ((a206 + a211) + 2);
    	a329 = "f";
    	a373 = false;
    	a265 = a293;
    	a370 = a285;
    	a357 = ((((((a357 * a382) % 65) - 121) * 1) + -28874) + 28872);
    	a224 = "f";
    	a392 = a257;
    	a368 = false;
    	a207 = "f";
    	a274 = a245;
    	a173 = "g";
    	a75 = "f";
    	a320 = ((a206 - a359) + 6); 
    	System.out.println("S");
    } 
    if((((((a75.equals("g")) && ((a206 == 6) && (((((a329.equals("g")) && (((a57 == 13) && cf) && (20 == a137[1]))) && input.equals(inputs[3])) && (a361.equals("g"))) && (56 == a265[2])))) && (a207.equals("g"))) && ((15 == a353[2]) && (a154 == 16))) && a90 == 9680)) {
    	a168 += (a168 + 20) > a168 ? 4 : 0;
    	cf = false;
    	a75 = "e";
    	a155 = "g";
    	a132 = false;
    	a77 = ((a359 * a359) + -18); 
    	System.out.println("T");
    } 
    if((((((50 == a392[4]) && ((20 == a137[1]) && (input.equals(inputs[8]) && (a202 == a217[2] && a202 == a217[2])))) && (a206 == 6)) && ((59 == a228[3]) && ((((355 < a249) && (499 >= a249)) && ((cf && (a57 == 13)) && (a154 == 16))) && (a75.equals("g"))))) && a178 <= 3)) {
    	a178 += (a178 + 20) > a178 ? 1 : 0;
    	cf = false;
    	a276 = a289;
    	a173 = "f";
    	a270 = (a359 - -6);
    	a392 = a257;
    	a324 = a232[((a320 * a270) - 87)];
    	a218 = "f";
    	a382 = ((((((a382 * a310) % 14999) / 5) + -21218) % 12) - 49);
    	a81 = a167[(a57 - 13)];
    	a260 = false;
    	a228 = a264;
    	a125 = a30[((a211 - a320) - -10)];
    	a75 = "f";
    	a230 = ((a398 * a398) - 140);
    	a286 = a294[(a211 + -2)];
    	a370 = a285;
    	a240 = false;
    	a313 = "f";
    	a357 = (((((a357 * a386) % 65) + -122) - -12303) - 12302);
    	a329 = "f";
    	a249 = ((((((((a249 * a310) % 14999) % 101) - -209) * 9) / 10) * 10) / 9);
    	a302 = a346[((a206 + a338) - 10)]; 
    	System.out.println("R");
    } 
    if(((((((a57 == 13) && ((a320 == 8) && ((((a154 == 16) && cf) && (20 == a137[1])) && (a75.equals("g"))))) && a225 == a205[2]) && !a373) && ((a312.equals("g")) && ((input.equals(inputs[7]) && ((147 < a333) && (177 >= a333))) && (a313.equals("g"))))) && a162 >= 2)) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	cf = false;
    	a69 = "h";
    	a143 = "i";
    	a57 = (a398 + -1);
    	a201 = (((((a357 * a382) % 94) + 83) / 5) / 5); 
    	System.out.println("V");
    } 
    if((((a302 == a346[2] && ((((a218.equals("g")) && a307 == a227[2]) && (a75.equals("g"))) && !a339)) && (a361.equals("g"))) && (((a57 == 13) && ((a154 == 16) && (input.equals(inputs[9]) && (cf && (20 == a137[1]))))) && (a313.equals("g"))))) {
    	a178 -= (a178 - 20) < a178 ? 4 : 0;
    	a181 += (a181 + 20) > a181 ? 4 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	cf = false;
    	a353 = a241;
    	a336 = false;
    	a338 = 4;
    	a313 = "f";
    	a230 = a338;
    	a358 = a348;
    	a296 = a384;
    	a392 = a257;
    	a386 = ((((((a310 * a357) % 61) + 138) / 5) + 28757) + -28687);
    	a395 = false;
    	a302 = a346[((a270 + a57) + -24)];
    	a228 = a229;
    	a370 = a285;
    	a206 = ((a237 + a230) + -4);
    	a312 = "f";
    	a129 = a92[(a154 + -15)];
    	a361 = "f";
    	a172 = ((a320 - a398) - -5);
    	a265 = a293;
    	a173 = "g";
    	a398 = (a57 - 2);
    	a224 = "f";
    	a307 = a227[(a398 - 10)];
    	a286 = a294[(a211 + -2)];
    	a270 = (a338 - -7);
    	a373 = true;
    	a240 = false;
    	a382 = (((((((a386 * a243) % 14999) % 12) + -52) - -2) + 5594) + -5596);
    	a320 = ((a57 * a57) + -162);
    	a329 = "f";
    	a75 = "f";
    	a207 = "f";
    	a234 = a372[(a338 - 4)];
    	a237 = ((a230 * a338) + -12);
    	a276 = a289;
    	a339 = false;
    	a333 = (((((a333 * a357) % 96) + 49) - 0) - -1);
    	a249 = ((((((a249 * a357) * 1) % 101) + 254) / 5) - -245);
    	a211 = (a338 / a338);
    	a260 = false;
    	a218 = "f";
    	a359 = (a398 + -7);
    	a202 = a217[(a338 - 4)];
    	a368 = false;
    	a225 = a205[((a230 + a338) + -7)];
    	a357 = (((((a357 * a382) / 5) % 65) + -121) * 1); 
    	System.out.println("T");
    } 
    if((((a206 == 6) && ((input.equals(inputs[2]) && ((a75.equals("g")) && ((a154 == 16) && cf))) && (38 == a370[4]))) && ((((92 == a296[2]) && (((a57 == 13) && (a398 == 12)) && (20 == a137[1]))) && (77 == a358[0])) && (a359 == 5)))) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	cf = false;
    	a201 = (((((((a357 * a382) % 94) + 82) * 5) - -27427) % 94) + -7);
    	a310 = (((((a201 * a357) - -6581) + 15570) % 20) - -318);
    	a235 = a216[((a211 - a230) - -4)];
    	a339 = true;
    	a278 = a326[((a320 / a237) - -6)];
    	a378 = true;
    	a228 = a229;
    	a256 = "g";
    	a370 = a285;
    	a382 = ((((((a382 * a333) % 14999) * 2) + 3) % 12) + -52);
    	a57 = (a230 - -11);
    	a358 = a348;
    	a269 = "e";
    	a300 = true;
    	a276 = a289;
    	a312 = "e";
    	a81 = a167[(a230 + -5)];
    	a243 = ((((((((a333 * a249) % 14999) % 43) - 150) * 9) / 10) - 28985) + 28997); 
    	System.out.println("T");
    } 
    if(((((92 == a296[2]) && ((20 == a137[1]) && ((((a57 == 13) && !a373) && ((147 < a333) && (177 >= a333))) && a395))) && ((((a361.equals("g")) && ((cf && input.equals(inputs[4])) && (a154 == 16))) && (a75.equals("g"))) && (92 == a296[2]))) && a98 == 9886)) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	cf = false;
    	a132 = true;
    	a177 = "g";
    	a75 = "e";
    	a137 = a116; 
    	System.out.println("R");
    } 
    if((((((77 == a358[0]) && ((56 == a265[2]) && ((201 < a386) && (325 >= a386)))) && !a339) && (input.equals(inputs[6]) && ((a75.equals("g")) && ((20 == a137[1]) && (((56 == a265[2]) && ((a57 == 13) && ((a154 == 16) && cf))) && ((-39 < a382) && (176 >= a382))))))) && (a110 % 2==0))) {
    	cf = false;
    	a14 = a79[((a57 * a270) - 152)];
    	a143 = "f";
    	a57 = (a237 + 6); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm143(String input) {
    if((((((-39 < a382) && (176 >= a382)) && ((59 == a228[3]) && (((-156 < a243) && (-68 >= a243)) && (a383 == a226[2] && ((a75.equals("g")) && (92 == a296[2])))))) && (26 == a137[1])) && (input.equals(inputs[9]) && ((a218.equals("g")) && (((a57 == 13) && cf) && a295 == a366[2]))))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a67 -= (a67 - 20) < a67 ? 4 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	cf = false;
    	if((!(a200 == a115[0]) || a244 == a363[7])) {
    	a81 = a167[(a206 + -6)];
    	a99 = a26[(a270 + -10)];
    	a57 = a270; 
    	}else {
    	a141 = a47[(a320 + -1)];
    	a72 = ((((1 * 5) - -21586) * 10) / -9);
    	a57 = (a211 - -12);
    	}System.out.println("Y");
    } 
    if(((((201 < a386) && (325 >= a386)) && (a202 == a217[2] && (input.equals(inputs[5]) && ((((a75.equals("g")) && cf) && (a57 == 13)) && (a207.equals("g")))))) && (((a218.equals("g")) && ((26 == a137[1]) && (a302 == a346[2] && a324 == a232[2]))) && a295 == a366[2]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a169 -= (a169 - 20) < a169 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a237 = 5;
    	a313 = "h";
    	a307 = a227[4];
    	a357 = (((a357 + -3937) - 2081) - 22055);
    	a211 = 7;
    	a206 = 5;
    	a256 = "f";
    	a312 = "f";
    	a135 = "g";
    	a353 = a263;
    	a368 = false;
    	a225 = a205[3];
    	a260 = true;
    	a333 = ((((a333 + 11400) - 34175) + -3816) - -36148);
    	a240 = false;
    	a338 = 9;
    	a329 = "g";
    	a361 = "h";
    	a228 = a292;
    	a75 = "h";
    	a382 = ((((a382 / 5) - 731) * 5) + 3652);
    	a218 = "g";
    	a249 = ((((a249 - 16383) % 71) - -462) + 14);
    	a339 = false;
    	a230 = 9;
    	a324 = a232[2];
    	a196 = (a270 - -3);
    	a398 = 14;
    	a234 = a372[6];
    	a134 = true;
    	a310 = (((a310 + 20340) - -1410) - -4939);
    	a276 = a250;
    	a320 = 7;
    	a224 = "e";
    	a207 = "i";
    	a235 = a216[6];
    	a359 = 10;
    	a296 = a384;
    	a270 = 17; 
    	System.out.println("Y");
    } 
    if((((a286 == a294[2] && (((-39 < a382) && (176 >= a382)) && ((a75.equals("g")) && ((a57 == 13) && ((26 == a137[1]) && (input.equals(inputs[0]) && cf)))))) && (a329.equals("g"))) && (((a295 == a366[2] && ((355 < a249) && (499 >= a249))) && ((147 < a333) && (177 >= a333))) && a260))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	a339 = false;
    	a398 = 11;
    	a359 = 4;
    	a218 = "h";
    	a256 = "h";
    	a361 = "i";
    	a313 = "g";
    	a54 = ((((((((a333 * a386) % 14999) % 80) - -319) * 9) / 10) / 5) - -225);
    	a296 = a362;
    	a333 = ((((((a333 % 14) + 163) - 10) * 5) % 14) + 150);
    	a237 = 4;
    	a206 = 6;
    	a269 = "i";
    	a75 = "h";
    	a302 = a346[1];
    	a383 = a226[7];
    	a239 = a299;
    	a310 = ((((a310 + -11347) * 10) / -9) * 2);
    	a395 = false;
    	a270 = 15;
    	a324 = a232[3];
    	a249 = (((((a249 + 16397) % 71) - -408) + -14166) + 14151);
    	a196 = (a57 - 1);
    	a353 = a241;
    	a201 = ((((76 - -27) * 5) + -3546) - -3042);
    	a312 = "g";
    	a368 = false;
    	a370 = a311;
    	a307 = a227[7];
    	a134 = true;
    	a224 = "h";
    	a382 = ((((a382 - 26071) * 1) % 12) + -46);
    	a286 = a294[6];
    	a277 = ((((((92 * 10) / 2) * 10) / 9) * 10) / 9);
    	a358 = a335;
    	a240 = false;
    	a336 = false;
    	a357 = (((a357 + 23784) + -2651) * 1);
    	a300 = false;
    	a211 = 7;
    	a276 = a289;
    	a234 = a372[6];
    	a230 = 4;
    	a338 = 8;
    	a207 = "h";
    	a320 = 11;
    	a329 = "g";
    	a202 = a217[5];
    	a228 = a264;
    	a243 = ((((a243 + -17354) % 11) + -164) * 1);
    	a260 = false;
    	a235 = a216[6];
    	a225 = a205[7];
    	a392 = a257;
    	a378 = false;
    	a386 = (((((a386 * 10) / 17) - 1064) + -8526) + 9596); 
    	System.out.println("V");
    } 
    if((((a269.equals("g")) && ((a398 == 12) && (((((a57 == 13) && ((26 == a137[1]) && cf)) && (a361.equals("g"))) && (a75.equals("g"))) && ((-156 < a243) && (-68 >= a243))))) && ((a313.equals("g")) && (input.equals(inputs[8]) && (a378 && a295 == a366[2]))))) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	a173 = "h";
    	a75 = "f";
    	a1 = a87[(a237 - 1)];
    	a91 = ((a320 / a320) - -10); 
    	System.out.println("R");
    } 
    if((((a240 && ((((201 < a386) && (325 >= a386)) && a295 == a366[2]) && (a57 == 13))) && a234 == a372[2]) && ((a312.equals("g")) && ((a256.equals("g")) && (a336 && (((26 == a137[1]) && (cf && input.equals(inputs[2]))) && (a75.equals("g")))))))) {
    	a164 += (a164 + 20) > a164 ? 2 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	a211 = 7;
    	a132 = false;
    	a75 = "e";
    	a270 = 13;
    	a228 = a292;
    	a300 = true;
    	a218 = "g";
    	a265 = a303;
    	a249 = ((((a249 * 15) / 10) - -16471) - -4334);
    	a358 = a335;
    	a353 = a399;
    	a224 = "h";
    	a338 = 7;
    	a361 = "g";
    	a239 = a268;
    	a324 = a232[7];
    	a230 = 7;
    	a16 = true;
    	a320 = 11;
    	a243 = ((((a243 * 5) * 5) % 43) + -93);
    	a256 = "g";
    	a235 = a216[5];
    	a313 = "h";
    	a237 = 7;
    	a398 = 14;
    	a202 = a217[5];
    	a276 = a289;
    	a207 = "f";
    	a234 = a372[3];
    	a260 = false;
    	a368 = false;
    	a339 = false;
    	a201 = ((((52 + -27630) + 27693) * 10) / 9);
    	a336 = false;
    	a370 = a285;
    	a329 = "i";
    	a277 = ((((38 - -11590) + -27841) / 5) + 3307);
    	a382 = ((((a382 - 18318) + -5243) * 10) / 9);
    	a378 = false;
    	a359 = 5;
    	a286 = a294[1];
    	a206 = 7;
    	a269 = "g";
    	a395 = false;
    	a383 = a226[1];
    	a386 = (((((a386 % 61) + 215) - -16) * 10) / 9);
    	a307 = a227[4];
    	a357 = (((((a357 % 38) - 18) + -24637) / 5) + 4920);
    	a333 = (((a333 + 5447) * 5) * 1);
    	a225 = a205[4];
    	a312 = "f";
    	a392 = a257;
    	a296 = a384;
    	a310 = (((a310 - -11509) + -11651) + -9);
    	a77 = (a57 - 4); 
    	System.out.println("S");
    } 
    if(((((a300 && (((201 < a386) && (325 >= a386)) && (((26 == a137[1]) && (cf && input.equals(inputs[4]))) && a295 == a366[2]))) && a368) && !a339) && (((316 < a310) && (357 >= a310)) && ((a57 == 13) && ((a75.equals("g")) && a383 == a226[2]))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a174 += (a174 + 20) > a174 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a237 = 8;
    	a307 = a227[2];
    	a1 = a87[(a270 - 6)];
    	a324 = a232[3];
    	a239 = a268;
    	a383 = a226[7];
    	a235 = a216[4];
    	a269 = "i";
    	a211 = 7;
    	a202 = a217[5];
    	a260 = true;
    	a310 = (((a310 / -5) - 14560) / 5);
    	a265 = a303;
    	a357 = ((((a357 - 15304) * 10) / -9) + 9275);
    	a75 = "f";
    	a206 = 7;
    	a353 = a399;
    	a240 = true;
    	a361 = "g";
    	a382 = ((((a382 * 5) % 107) - -68) / 5);
    	a243 = ((((a243 - 10702) * 10) / 9) * 2);
    	a358 = a335;
    	a286 = a294[3];
    	a336 = true;
    	a320 = 13;
    	a378 = true;
    	a218 = "g";
    	a91 = (a398 + -1);
    	a313 = "i";
    	a249 = (((a249 / -5) - 3450) * 5);
    	a368 = true;
    	a339 = true;
    	a228 = a292;
    	a312 = "e";
    	a234 = a372[2];
    	a173 = "h";
    	a300 = true;
    	a302 = a346[6];
    	a230 = 6;
    	a373 = true;
    	a386 = (((a386 - 20092) / 5) * 5);
    	a333 = ((((a333 / 5) - -125) / 5) + 136);
    	a359 = 8;
    	a270 = 17;
    	a225 = a205[6];
    	a224 = "e";
    	a398 = 16; 
    	System.out.println("S");
    } 
    if(((((a359 == 5) && ((((355 < a249) && (499 >= a249)) && (((-39 < a382) && (176 >= a382)) && ((15 == a353[2]) && ((a57 == 13) && ((26 == a137[1]) && (a361.equals("g"))))))) && (a75.equals("g")))) && a240) && (a295 == a366[2] && (cf && input.equals(inputs[7]))))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if((((a221 && a368) || a240) && (a270 == 17))) {
    	a392 = a257;
    	a57 = (a206 + 6);
    	a313 = "h";
    	a276 = a253;
    	a265 = a303;
    	a81 = a167[(a57 - 10)];
    	a358 = a348;
    	a235 = a216[6];
    	a361 = "e";
    	a260 = true;
    	a277 = (((70 / 5) - -15016) + 3057);
    	a370 = a285;
    	a395 = false;
    	a201 = ((((46 * 10) / -2) * 5) * 5);
    	a373 = true;
    	a158 = ((a237 / a237) - -10); 
    	}else {
    	a373 = true;
    	a300 = true;
    	a370 = a318;
    	a256 = "g";
    	a378 = true;
    	a383 = a226[7];
    	a395 = false;
    	a235 = a216[6];
    	a276 = a253;
    	a392 = a257;
    	a310 = (((a310 - -1875) / 5) - -13717);
    	a265 = a303;
    	a154 = ((a230 + a359) + 6);
    	a277 = ((((35 * -3) / 10) - 15215) - 1964);
    	a243 = (((a243 - -28752) + 825) * 1);
    	a269 = "h";
    	a225 = a205[3];
    	a137 = a189;
    	}System.out.println("P");
    } 
    if((((((47 == a239[4]) && ((a269.equals("g")) && (a218.equals("g")))) && (a75.equals("g"))) && (a206 == 6)) && ((59 == a228[3]) && (!a339 && (((26 == a137[1]) && ((a295 == a366[2] && cf) && (a57 == 13))) && input.equals(inputs[6])))))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a243 = ((((a243 % 43) - 78) / 5) + -94);
    	a392 = a257;
    	a378 = true;
    	a383 = a226[5];
    	a154 = ((a57 + a320) + -5);
    	a235 = a216[3];
    	a373 = true;
    	a310 = (((a310 / 5) + 15083) - 14806);
    	a300 = true;
    	a265 = a293;
    	a276 = a253;
    	a225 = a205[4];
    	a395 = false;
    	a370 = a285;
    	a269 = "e";
    	a277 = ((((92 * 37) / 10) + 27118) / 5);
    	a256 = "e";
    	a137 = a189; 
    	System.out.println("T");
    } 
    if((((((a359 == 5) && a307 == a227[2]) && (47 == a239[4])) && input.equals(inputs[1])) && (a286 == a294[2] && (a295 == a366[2] && (a336 && ((((cf && (26 == a137[1])) && (a75.equals("g"))) && (a57 == 13)) && (a218.equals("g")))))))) {
    	a120 -= (a120 - 20) < a120 ? 4 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	if(405 < a19) {
    	a357 = (((a357 + -12113) * 2) * 1);
    	a339 = true;
    	a75 = "f";
    	a191 = a37[(a398 + -7)];
    	a269 = "h";
    	a243 = (((((a243 % 43) + -104) - 1) + -15305) + 15308);
    	a270 = 10;
    	a359 = 8;
    	a239 = a268;
    	a313 = "e";
    	a240 = true;
    	a206 = 10;
    	a378 = true;
    	a224 = "h";
    	a324 = a232[6];
    	a310 = ((((a310 + 8948) % 20) + 332) + -11);
    	a173 = "e";
    	a130 = (((38 + -8630) - -38166) / -5); 
    	}else {
    	a75 = "f";
    	a81 = a167[(a338 + -2)];
    	a173 = "f";
    	a125 = a30[(a270 + -7)];
    	}System.out.println("T");
    } 
    if((((((a75.equals("g")) && (a207.equals("g"))) && (a57 == 13)) && (26 == a137[1])) && ((47 == a239[4]) && ((a234 == a372[2] && ((a224.equals("g")) && ((a295 == a366[2] && (input.equals(inputs[3]) && cf)) && (a230 == 5)))) && ((355 < a249) && (499 >= a249)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	a359 = 9;
    	a228 = a264;
    	a239 = a299;
    	a313 = "h";
    	a370 = a311;
    	a382 = (((a382 + 12549) * 2) + 3495);
    	a218 = "i";
    	a256 = "g";
    	a324 = a232[6];
    	a307 = a227[1];
    	a395 = false;
    	a234 = a372[3];
    	a265 = a376;
    	a300 = true;
    	a286 = a294[7];
    	a276 = a250;
    	a77 = (a206 + 3);
    	a260 = false;
    	a398 = 14;
    	a336 = false;
    	a224 = "f";
    	a383 = a226[6];
    	a392 = a304;
    	a16 = true;
    	a235 = a216[7];
    	a368 = false;
    	a202 = a217[7];
    	a237 = 10;
    	a312 = "h";
    	a277 = (((9 - 28565) + -986) * 1);
    	a201 = (((18 + 29844) - -3) / 5);
    	a310 = (((((a310 * 12) / 10) / 5) - 10873) + 14176);
    	a320 = 10;
    	a230 = 4;
    	a296 = a384;
    	a211 = 6;
    	a358 = a335;
    	a357 = ((((a357 / 5) / 5) + 10753) + -10736);
    	a207 = "h";
    	a338 = 6;
    	a249 = ((((a249 % 71) - -369) - 7123) + 7148);
    	a339 = false;
    	a333 = (((a333 - -21453) / 5) / 5);
    	a225 = a205[4];
    	a132 = false;
    	a75 = "e";
    	a270 = 17;
    	a353 = a241;
    	a378 = false;
    	a269 = "g";
    	a361 = "f";
    	a386 = ((((a386 * 10) / 17) + 18179) - 18171);
    	a243 = (((((a243 * 5) / 5) + 20494) % 43) - 136);
    	a329 = "g";
    	a206 = 5; 
    	System.out.println("V");
    } 
}
private  void calculateOutputm144(String input) {
    if((((77 == a358[0]) && (a300 && (!a373 && ((a313.equals("g")) && (a57 == 13))))) && ((15 == a353[2]) && ((a75.equals("g")) && (((26 == a137[1]) && (input.equals(inputs[0]) && (cf && a295 == a366[4]))) && a286 == a294[2]))))) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a75 = "f";
    	a173 = "f";
    	a125 = a30[((a398 * a230) + -59)];
    	a8 = (a57 - 1); 
    	System.out.println("T");
    } 
    if(((((a324 == a232[2] && !a339) && (56 == a265[2])) && a307 == a227[2]) && (((((((a57 == 13) && (cf && (26 == a137[1]))) && input.equals(inputs[3])) && a295 == a366[4]) && (a75.equals("g"))) && (a359 == 5)) && a368))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a67 -= (a67 - 20) < a67 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 2 : 0;
    	a35 += (a35 + 20) > a35 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a269 = "e";
    	a302 = a346[0];
    	a91 = ((a206 * a270) - 60);
    	a240 = true;
    	a260 = true;
    	a392 = a304;
    	a307 = a227[4];
    	a368 = true;
    	a218 = "e";
    	a310 = ((((a310 * 5) * 5) % 20) - -323);
    	a373 = true;
    	a370 = a311;
    	a276 = a253;
    	a202 = a217[3];
    	a256 = "g";
    	a265 = a303;
    	a395 = true;
    	a312 = "g";
    	a313 = "i";
    	a235 = a216[0];
    	a237 = 5;
    	a336 = true;
    	a206 = 11;
    	a243 = ((((a243 * 27) / 10) * 5) * 5);
    	a201 = ((((35 - 22531) * 1) * 10) / 9);
    	a398 = 14;
    	a249 = (((((a249 % 71) + 390) / 5) / 5) - -478);
    	a338 = 3;
    	a228 = a264;
    	a207 = "g";
    	a277 = (((a277 - 27781) - 1577) + -298);
    	a339 = true;
    	a211 = 5;
    	a333 = (((a333 * 5) - -26986) * 1);
    	a142 = false;
    	a300 = true;
    	a358 = a335;
    	a320 = 10;
    	a286 = a294[2];
    	a329 = "h";
    	a386 = (((a386 + -26562) + -684) - 2878);
    	a324 = a232[6];
    	a230 = 3;
    	a353 = a263;
    	a383 = a226[7];
    	a239 = a268;
    	a382 = ((((a382 + 10002) + -36187) * 10) / -9);
    	a234 = a372[7];
    	a75 = "f";
    	a225 = a205[7];
    	a357 = (((a357 + 23598) - -5516) + 204);
    	a359 = 6;
    	a173 = "h";
    	a270 = 15; 
    	System.out.println("V");
    } 
    if(((((355 < a249) && (499 >= a249)) && ((((316 < a310) && (357 >= a310)) && (50 == a392[4])) && !a339)) && ((a57 == 13) && (a234 == a372[2] && (((((a75.equals("g")) && ((26 == a137[1]) && cf)) && (a361.equals("g"))) && a295 == a366[4]) && input.equals(inputs[4])))))) {
    	a120 -= (a120 - 20) < a120 ? 4 : 0;
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a57 = (a211 + 12);
    	a141 = a47[((a237 * a359) - 25)];
    	a72 = (((((a386 * a386) % 14999) / 5) - -2963) + -6919); 
    	System.out.println("P");
    } 
    if(((a225 == a205[2] && (((a313.equals("g")) && (a57 == 13)) && (38 == a370[4]))) && (!a339 && (a307 == a227[2] && ((a269.equals("g")) && (a295 == a366[4] && (((a75.equals("g")) && ((26 == a137[1]) && cf)) && input.equals(inputs[7])))))))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	cf = false;
    	a173 = "f";
    	a75 = "f";
    	a125 = a30[(a57 + -12)];
    	a8 = (a57 - 1); 
    	System.out.println("R");
    } 
    if((((a225 == a205[2] && (a395 && (((a57 == 13) && (a295 == a366[4] && ((15 == a353[2]) && (a398 == 12)))) && (47 == a239[4])))) && a260) && (input.equals(inputs[9]) && ((a75.equals("g")) && ((26 == a137[1]) && cf))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a5 -= (a5 - 20) < a5 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a224 = "h";
    	a276 = a289;
    	a202 = a217[3];
    	a333 = (((a333 * 5) + -770) / 5);
    	a260 = false;
    	a196 = (a57 + 4);
    	a243 = ((((a243 % 43) + -82) + 12) + -21);
    	a134 = true;
    	a361 = "h";
    	a234 = a372[7];
    	a307 = a227[4];
    	a353 = a399;
    	a392 = a257;
    	a207 = "g";
    	a211 = 2;
    	a339 = false;
    	a230 = 10;
    	a256 = "f";
    	a300 = false;
    	a395 = false;
    	a324 = a232[7];
    	a383 = a226[5];
    	a358 = a351;
    	a320 = 13;
    	a386 = (((((a386 - 25769) % 61) - -188) - 19576) - -19526);
    	a265 = a293;
    	a225 = a205[4];
    	a296 = a384;
    	a336 = false;
    	a359 = 10;
    	a240 = false;
    	a206 = 5;
    	a75 = "h";
    	a286 = a294[4];
    	a277 = ((((a277 - 21149) + -7734) % 95) + 296);
    	a310 = ((((a310 - 24049) - 506) + -4101) + 51976);
    	a370 = a285;
    	a228 = a292;
    	a338 = 7;
    	a270 = 12;
    	a237 = 7;
    	a235 = a216[4];
    	a368 = false;
    	a239 = a268;
    	a382 = ((((a382 % 12) - 50) / 5) - 39);
    	a378 = false;
    	a373 = false;
    	a269 = "h";
    	a329 = "g";
    	a313 = "e";
    	a398 = 14;
    	a91 = a57; 
    	System.out.println("V");
    } 
    if(((((input.equals(inputs[6]) && ((a338 == 5) && (a295 == a366[4] && ((a57 == 13) && ((355 < a249) && (499 >= a249)))))) && (a359 == 5)) && (a207.equals("g"))) && ((((a75.equals("g")) && ((26 == a137[1]) && cf)) && (59 == a228[3])) && a234 == a372[2]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a122 -= (a122 - 20) < a122 ? 2 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if((!(a129 == a92[3]) || (a59.equals("f")))) {
    	a230 = 4;
    	a201 = ((((48 - -10112) - 10289) * 9) / 10);
    	a310 = (((((a310 + -2219) * 5) * 3) % 20) - -344);
    	a358 = a351;
    	a235 = a216[4];
    	a211 = 2;
    	a225 = a205[7];
    	a134 = true;
    	a333 = (((((a333 / 5) * 5) - 16386) * -1) / 10);
    	a353 = a399;
    	a196 = ((a398 * a338) - 48);
    	a329 = "i";
    	a300 = false;
    	a202 = a217[1];
    	a359 = 9;
    	a277 = ((((((a277 % 95) - -158) * 10) / 9) * 9) / 10);
    	a313 = "h";
    	a265 = a376;
    	a260 = false;
    	a307 = a227[2];
    	a392 = a304;
    	a370 = a311;
    	a237 = 7;
    	a249 = ((((a249 + -12339) % 71) + 449) - -6);
    	a383 = a226[7];
    	a286 = a294[4];
    	a368 = false;
    	a296 = a384;
    	a234 = a372[2];
    	a224 = "g";
    	a395 = false;
    	a361 = "i";
    	a228 = a264;
    	a207 = "g";
    	a218 = "f";
    	a338 = 5;
    	a320 = 7;
    	a256 = "i";
    	a324 = a232[6];
    	a382 = ((((a382 - 6241) % 107) - -164) + 8);
    	a302 = a346[2];
    	a339 = false;
    	a270 = 16;
    	a386 = (((a386 * 5) - -24177) / 5);
    	a336 = false;
    	a373 = false;
    	a75 = "h";
    	a239 = a268;
    	a357 = ((((a357 % 38) - 18) + -1) - -1);
    	a240 = false;
    	a206 = 11;
    	a54 = ((((50 / 5) * 5) / 5) + 310);
    	a378 = false;
    	a276 = a250;
    	a269 = "h";
    	a243 = ((((((a243 - -16864) % 43) + -127) * 5) % 43) + -110);
    	a398 = 15; 
    	}else {
    	a132 = false;
    	a75 = "e";
    	a155 = "g";
    	a77 = ((a338 * a270) - 53);
    	}System.out.println("Y");
    } 
    if((((a329.equals("g")) && (a295 == a366[4] && (input.equals(inputs[5]) && (a207.equals("g"))))) && ((a206 == 6) && ((a338 == 5) && (((148 < a277) && (339 >= a277)) && (((a57 == 13) && ((a75.equals("g")) && (cf && (26 == a137[1])))) && ((316 < a310) && (357 >= a310)))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a395 = true;
    	a269 = "e";
    	a307 = a227[7];
    	a358 = a351;
    	a320 = 11;
    	a173 = "g";
    	a353 = a399;
    	a211 = 6;
    	a370 = a311;
    	a225 = a205[7];
    	a302 = a346[5];
    	a312 = "e";
    	a235 = a216[3];
    	a329 = "h";
    	a218 = "i";
    	a373 = true;
    	a339 = true;
    	a206 = 11;
    	a361 = "i";
    	a243 = ((((((a243 * 5) % 43) + -105) * 5) % 43) + -74);
    	a359 = 10;
    	a172 = ((a57 / a57) - -6);
    	a240 = true;
    	a228 = a292;
    	a382 = (((a382 + 24069) * 1) / 5);
    	a75 = "f";
    	a237 = 9;
    	a392 = a304;
    	a276 = a250;
    	a260 = true;
    	a99 = a26[(a230 + -4)];
    	a313 = "e";
    	a368 = true;
    	a256 = "h";
    	a338 = 7;
    	a300 = true;
    	a386 = (((a386 / 5) + 24358) - -6);
    	a265 = a303;
    	a357 = (((a357 - -23885) * 1) / 5);
    	a249 = ((((a249 - -26957) - -1980) * 1) + -54618);
    	a234 = a372[7];
    	a336 = true;
    	a270 = 15;
    	a333 = ((((a333 % 14) - -162) + 14215) - 14213);
    	a207 = "g";
    	a398 = 17;
    	a310 = ((((a310 % 20) + 328) - -1) + 1);
    	a286 = a294[5];
    	a230 = 5; 
    	System.out.println("R");
    } 
    if(((((26 == a137[1]) && ((a218.equals("g")) && (a234 == a372[2] && (a240 && ((a235 == a216[2] && (a295 == a366[4] && ((a75.equals("g")) && cf))) && a260))))) && (a57 == 13)) && (input.equals(inputs[1]) && a300))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a72 = ((((90 * 10) / -8) * 5) - 11773);
    	a57 = (a211 - -12);
    	a141 = a47[(a230 - -2)]; 
    	System.out.println("V");
    } 
    if(((input.equals(inputs[2]) && ((a57 == 13) && ((110 == a276[2]) && ((cf && (a75.equals("g"))) && a295 == a366[4])))) && ((((316 < a310) && (357 >= a310)) && ((a313.equals("g")) && (((a211 == 3) && ((147 < a333) && (177 >= a333))) && a302 == a346[2]))) && (26 == a137[1])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	cf = false;
    	if(((a59.equals("g")) && (a373 || !a395))) {
    	a75 = "f";
    	a107 = "g";
    	a173 = "g";
    	a172 = a320; 
    	}else {
    	a338 = 10;
    	a240 = true;
    	a235 = a216[7];
    	a392 = a304;
    	a329 = "i";
    	a129 = a92[((a230 * a206) - 29)];
    	a357 = (((a357 / 5) * 5) - -13625);
    	a302 = a346[2];
    	a276 = a253;
    	a225 = a205[0];
    	a286 = a294[0];
    	a307 = a227[4];
    	a125 = a30[(a270 + -5)];
    	a270 = 16;
    	a310 = ((((a310 - -16662) * 10) / 9) * 1);
    	a75 = "f";
    	a370 = a318;
    	a173 = "f";
    	a395 = true;
    	a324 = a232[2];
    	a313 = "h";
    	a361 = "i";
    	a382 = (((a382 - 23027) - 6534) + -350);
    	a230 = 9;
    	}System.out.println("R");
    } 
    if((((a295 == a366[4] && ((a300 && (((((a57 == 13) && cf) && (a75.equals("g"))) && (26 == a137[1])) && (77 == a358[0]))) && (47 == a239[4]))) && a235 == a216[2]) && ((a383 == a226[2] && a324 == a232[2]) && input.equals(inputs[8])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a162 -= (a162 - 20) < a162 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a378 = false;
    	a329 = "h";
    	a265 = a376;
    	a224 = "h";
    	a295 = a366[(a338 + -5)];
    	a392 = a304;
    	a338 = 8;
    	a339 = false;
    	a336 = false;
    	a383 = a226[5];
    	a398 = 14;
    	a320 = 11;
    	a269 = "h";
    	a386 = (((((a386 * 17) / 10) / 5) + -896) + 11515);
    	a296 = a384;
    	a310 = (((((a310 * 5) / 5) + 14119) % 20) - -332);
    	a370 = a318;
    	a333 = (((((a333 * -1) / 10) + -6) - -25046) + -24962);
    	a202 = a217[5];
    	a353 = a399;
    	a75 = "h";
    	a225 = a205[4];
    	a228 = a292;
    	a359 = 6;
    	a276 = a289;
    	a277 = (((((a277 / 5) * 10) / 9) - -28994) + -28932);
    	a286 = a294[3];
    	a368 = false;
    	a211 = 8;
    	a358 = a351;
    	a134 = false;
    	a239 = a268;
    	a201 = ((((9 / 5) * 5) * 9) / 10);
    	a218 = "i";
    	a196 = (a57 + 3); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm145(String input) {
    if((((((a206 == 6) && ((a75.equals("g")) && (a234 == a372[2] && ((cf && (a57 == 13)) && input.equals(inputs[1]))))) && a368) && ((a398 == 12) && ((((31 == a137[0]) && (a230 == 5)) && (a323 == 7)) && (59 == a228[3])))) && a165 == 4790)) {
    	cf = false;
    	a72 = (((((a386 * a386) % 14999) + -25817) / 5) * 5);
    	a141 = a47[((a57 / a57) - -1)];
    	a57 = ((a206 + a323) - -2); 
    	System.out.println("Y");
    } 
    if((((((110 == a276[2]) && ((148 < a277) && (339 >= a277))) && (15 == a353[2])) && a383 == a226[2]) && (((147 < a333) && (177 >= a333)) && ((((a368 && (((31 == a137[0]) && cf) && (a57 == 13))) && (a75.equals("g"))) && (a323 == 7)) && input.equals(inputs[5]))))) {
    	a131 += (a131 + 20) > a131 ? 2 : 0;
    	a169 += (a169 + 20) > a169 ? 1 : 0;
    	a35 += (a35 + 20) > a35 ? 2 : 0;
    	a90 += (a90 + 20) > a90 ? 6 : 0;
    	a110 += (a110 + 20) > a110 ? 2 : 0;
    	a31 -= (a31 - 20) < a31 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 2 : 0;
    	cf = false;
    	a370 = a311;
    	a256 = "f";
    	a395 = true;
    	a324 = a232[((a359 + a323) + -10)];
    	a277 = (((((((a277 * a201) % 14999) * 2) % 95) - -244) / 5) - -145);
    	a300 = false;
    	a243 = (((((a243 * a201) % 11) + -166) + -2) * 1);
    	a302 = a346[(a359 + -3)];
    	a235 = a216[((a211 * a206) + -17)];
    	a382 = (((((((a333 * a386) % 14999) % 107) - -38) * 5) % 107) - 32);
    	a296 = a362;
    	a137 = a189;
    	a201 = ((((((a201 * a249) % 14999) % 93) - 104) - 3539) - -3538);
    	a320 = ((a398 + a270) + -16);
    	a225 = a205[(a270 - 10)];
    	a286 = a294[(a206 - 4)];
    	a312 = "g";
    	a269 = "f";
    	a361 = "g";
    	a310 = (((((((a310 * a382) % 14999) % 77) + 238) - 1) + 25263) + -25260);
    	a154 = ((a359 - a237) + 16);
    	a202 = a217[((a206 + a206) - 10)];
    	a378 = false;
    	a383 = a226[((a206 * a398) - 70)]; 
    	System.out.println("Z");
    } 
    if((((a368 && (((-156 < a243) && (-68 >= a243)) && ((a260 && (a57 == 13)) && a240))) && ((a323 == 7) && ((a230 == 5) && ((a206 == 6) && ((31 == a137[0]) && ((cf && input.equals(inputs[0])) && (a75.equals("g")))))))) && a63 >= 3)) {
    	a51 += (a51 + 20) > a51 ? 1 : 0;
    	cf = false;
    	a370 = a311;
    	a57 = (a211 - -13);
    	a395 = true;
    	a312 = "g";
    	a183 = "h";
    	a278 = a326[a230]; 
    	System.out.println("S");
    } 
    if((((((355 < a249) && (499 >= a249)) && a234 == a372[2]) && (input.equals(inputs[8]) && ((a57 == 13) && ((15 == a353[2]) && (((31 == a137[0]) && (((a338 == 5) && ((a323 == 7) && (cf && (a75.equals("g"))))) && a260)) && (56 == a265[2])))))) && a174 <= 3)) {
    	a164 += (a164 + 20) > a164 ? 1 : 0;
    	cf = false;
    	a313 = "f";
    	a240 = false;
    	a276 = a289;
    	a392 = a257;
    	a75 = "f";
    	a201 = ((((((a201 * a243) / 5) - 17005) / 5) % 93) - 96);
    	a270 = (a211 + 8);
    	a307 = a227[(a211 - 2)];
    	a125 = a30[a323];
    	a357 = ((((((a357 * a277) * 1) % 65) + -121) - 2945) - -2943);
    	a338 = (a57 + -9);
    	a129 = a92[(a323 + -6)];
    	a230 = (a237 + -1);
    	a329 = "f";
    	a173 = "f";
    	a310 = ((((((a310 * a201) % 14999) % 77) - -238) / 5) + 240); 
    	System.out.println("T");
    } 
    if((((a323 == 7) && ((a383 == a226[2] && ((cf && (a57 == 13)) && input.equals(inputs[9]))) && (a270 == 12))) && (((((31 == a137[0]) && (a307 == a227[2] && a234 == a372[2])) && (56 == a265[2])) && (a75.equals("g"))) && ((148 < a277) && (339 >= a277))))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 4 : 0;
    	a56 += (a56 + 20) > a56 ? 2 : 0;
    	a5 += (a5 + 20) > a5 ? 2 : 0;
    	a93 += (a93 + 20) > a93 ? 2 : 0;
    	a94 -= (a94 - 20) < a94 ? 4 : 0;
    	cf = false;
    	a313 = "f";
    	a333 = (((((a333 * a249) % 14999) + -28538) + -331) * 1);
    	a218 = "e";
    	a329 = "e";
    	a225 = a205[(a237 + -5)];
    	a230 = (a237 + -2);
    	a324 = a232[(a359 - a359)];
    	a300 = true;
    	a368 = true;
    	a373 = true;
    	a339 = true;
    	a383 = a226[(a323 + -7)];
    	a211 = (a237 - 3);
    	a392 = a257;
    	a201 = ((((((a201 * a243) + 26723) % 93) - 105) / 5) - 155);
    	a277 = (((((a310 * a249) % 14999) + 3704) - 26533) * 1);
    	a353 = a263;
    	a269 = "f";
    	a75 = "i";
    	a234 = a372[(a359 - 3)];
    	a243 = (((((a333 * a333) % 14999) + -29456) + -80) / 5);
    	a307 = a227[((a338 * a359) + -24)];
    	a386 = (((((a386 * a333) % 14999) + -8820) - 2639) * 1);
    	a270 = (a57 + -3);
    	a286 = a294[((a359 / a359) + 1)];
    	a361 = "e";
    	a16 = true;
    	a378 = true;
    	a265 = a376;
    	a320 = (a237 - -2);
    	a395 = true;
    	a240 = true;
    	a224 = "g";
    	a276 = a250;
    	a235 = a216[(a237 - a237)];
    	a338 = (a359 - 2);
    	a302 = a346[(a323 - 5)];
    	a358 = a348;
    	a228 = a229;
    	a357 = ((((a357 * a310) + -2241) / 5) - 19146);
    	a336 = true;
    	a398 = (a323 - -3);
    	a249 = (((((((a277 * a333) % 14999) + 11100) % 71) - -363) * 10) / 9);
    	a310 = ((((((a310 * a277) % 14999) % 77) - -237) + 0) * 1);
    	a206 = (a359 + -1);
    	a237 = (a359 - 2);
    	a202 = a217[((a359 + a230) + -7)];
    	a207 = "e";
    	a256 = "g";
    	a146 = false;
    	a359 = (a230 - -2);
    	a260 = true;
    	a239 = a242;
    	a73 = (((25 - 26382) * 1) - -26528); 
    	System.out.println("T");
    } 
    if(((((a270 == 12) && ((a269.equals("g")) && (((a57 == 13) && cf) && (31 == a137[0])))) && (((((a75.equals("g")) && ((110 == a276[2]) && ((a323 == 7) && !a339))) && ((148 < a277) && (339 >= a277))) && (a329.equals("g"))) && input.equals(inputs[4]))) && a89 == 9776)) {
    	a5 -= (a5 - 20) < a5 ? 1 : 0;
    	cf = false;
    	a173 = "g";
    	a8 = (a237 + 1);
    	a75 = "f";
    	a172 = (a398 + -10); 
    	System.out.println("Y");
    } 
    if((((110 == a276[2]) && ((59 == a228[3]) && (((a218.equals("g")) && (((50 == a392[4]) && ((((a323 == 7) && cf) && (a57 == 13)) && (a75.equals("g")))) && (31 == a137[0]))) && input.equals(inputs[2])))) && (((316 < a310) && (357 >= a310)) && (77 == a358[0])))) {
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a174 += (a174 + 20) > a174 ? 1 : 0;
    	cf = false;
    	a302 = a346[(a211 - 1)];
    	a235 = a216[(a206 - 4)];
    	a200 = a115[(a398 - 7)];
    	a225 = a205[((a398 * a338) - 58)];
    	a158 = (a57 - 8);
    	a202 = a217[(a338 - 3)];
    	a57 = ((a158 / a158) + 9);
    	a395 = true;
    	a382 = (((((a243 * a333) + -299) % 107) + 164) - -12);
    	a296 = a362;
    	a312 = "g";
    	a361 = "g";
    	a286 = a294[(a323 - 5)];
    	a276 = a253;
    	a324 = a232[(a206 + -4)];
    	a228 = a229;
    	a370 = a318;
    	a320 = ((a338 - a338) - -8); 
    	System.out.println("V");
    } 
    if((((((input.equals(inputs[6]) && cf) && (31 == a137[0])) && (50 == a392[4])) && (a224.equals("g"))) && (((a383 == a226[2] && ((a57 == 13) && ((a240 && (a323 == 7)) && (a75.equals("g"))))) && a378) && a307 == a227[2]))) {
    	a122 -= (a122 - 20) < a122 ? 1 : 0;
    	cf = false;
    	a310 = (((((a310 * a249) % 14999) - 25437) + -3500) / 5);
    	a184 = a157[((a398 * a206) - 69)];
    	a75 = "h";
    	a207 = "e";
    	a307 = a227[((a270 + a320) - 18)];
    	a295 = a366[((a270 + a359) + -13)];
    	a359 = (a57 + -10);
    	a358 = a348;
    	a218 = "e";
    	a378 = true;
    	a339 = true;
    	a269 = "e";
    	a276 = a253;
    	a228 = a229;
    	a202 = a217[((a323 + a320) + -13)];
    	a373 = true;
    	a201 = ((((((a201 * a386) % 14999) % 14900) + -15098) + -2) + 0);
    	a336 = true;
    	a134 = false;
    	a224 = "e";
    	a333 = ((((((a333 * a277) % 14999) - -10863) * 1) * 10) / -9); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm146(String input) {
    if(((input.equals(inputs[0]) && ((77 == a358[0]) && a324 == a232[2])) && ((a230 == 5) && (((((a57 == 13) && ((a75.equals("g")) && ((cf && (a323 == 10)) && (31 == a137[0])))) && (110 == a276[2])) && (a398 == 12)) && (92 == a296[2]))))) {
    	a67 -= (a67 - 20) < a67 ? 4 : 0;
    	cf = false;
    	a256 = "e";
    	a339 = false;
    	a353 = a241;
    	a378 = true;
    	a249 = (((((a249 * a382) % 14999) / 5) + -2008) + -13218);
    	a173 = "f";
    	a75 = "f";
    	a206 = ((a359 + a338) - 3);
    	a228 = a264;
    	a368 = true;
    	a211 = (a237 - 3);
    	a270 = (a359 + 7);
    	a361 = "e";
    	a230 = (a211 + 2);
    	a276 = a289;
    	a207 = "f";
    	a324 = a232[((a338 * a230) - 15)];
    	a320 = ((a398 + a338) - 10);
    	a218 = "g";
    	a8 = (a323 - -1);
    	a240 = false;
    	a225 = a205[(a57 + -12)];
    	a269 = "g";
    	a260 = false;
    	a373 = true;
    	a239 = a299;
    	a329 = "g";
    	a224 = "f";
    	a333 = ((((((a243 * a357) + 18423) + -44436) + -218) % 14) - -169);
    	a395 = false;
    	a358 = a351;
    	a357 = ((((((a357 * a310) % 65) - 121) + -1) + 4694) - 4694);
    	a312 = "g";
    	a313 = "f";
    	a125 = a30[(a8 + -10)];
    	a307 = a227[(a237 + -3)];
    	a296 = a384;
    	a398 = (a338 - -7);
    	a300 = true;
    	a237 = a230;
    	a265 = a303;
    	a336 = true;
    	a235 = a216[((a323 * a359) - 40)];
    	a302 = a346[((a323 - a270) - -2)];
    	a277 = (((((((a277 * a201) % 14999) % 78) + 69) - -21006) + -5874) - 15131); 
    	System.out.println("T");
    } 
    if((((a312.equals("g")) && ((a235 == a216[2] && (47 == a239[4])) && (a57 == 13))) && (((((((355 < a249) && (499 >= a249)) && ((cf && input.equals(inputs[5])) && (31 == a137[0]))) && (a313.equals("g"))) && (a75.equals("g"))) && (a323 == 10)) && (a312.equals("g"))))) {
    	a67 -= (a67 - 20) < a67 ? 1 : 0;
    	cf = false;
    	if((a235 == a216[4] || !(23 == a274[2]))) {
    	a320 = (a230 - -3);
    	a382 = (((((a249 * a357) % 107) + 69) - -25625) + -25624);
    	a201 = (((((((a357 * a357) % 94) + 84) * 5) * 5) % 94) - -82);
    	a243 = ((((((a277 * a382) % 14999) % 43) + -110) + 29982) - 29983);
    	a235 = a216[((a398 * a398) + -144)];
    	a378 = true;
    	a370 = a285;
    	a260 = true;
    	a338 = a237;
    	a373 = false;
    	a154 = (a211 - -9);
    	a359 = (a206 + -1);
    	a300 = true;
    	a234 = a372[((a230 + a323) + -13)];
    	a286 = a294[((a338 * a323) - 48)];
    	a224 = "f";
    	a386 = ((((((((a243 * a249) % 14999) % 61) - -263) + -1) * 5) % 61) - -260);
    	a137 = a189;
    	a202 = a217[(a237 + -3)];
    	a206 = (a398 - 8);
    	a310 = ((((((a333 * a333) % 14999) % 77) + 203) + 19) + -33);
    	a270 = ((a237 * a230) - 13);
    	a265 = a376;
    	a225 = a205[(a398 - 12)];
    	a256 = "e";
    	a392 = a257;
    	a211 = (a237 + -4);
    	a296 = a212;
    	a395 = true;
    	a249 = (((((a333 * a333) % 14999) + 13097) - 28485) * 1);
    	a230 = ((a359 * a57) - 60);
    	a361 = "g";
    	a237 = (a398 - 9); 
    	}else {
    	a286 = a294[(a211 - a211)];
    	a310 = ((((a357 * a357) + -23810) / 5) * 5);
    	a243 = ((((((a357 * a310) % 14999) - -6358) % 14910) - 15088) - 2);
    	a99 = a26[(a57 - 6)];
    	a359 = a211;
    	a228 = a229;
    	a313 = "e";
    	a276 = a253;
    	a239 = a242;
    	a218 = "e";
    	a235 = a216[((a211 * a323) - 30)];
    	a368 = true;
    	a240 = true;
    	a338 = a211;
    	a224 = "e";
    	a370 = a318;
    	a277 = ((((((a277 * a243) % 14999) / 5) / 5) % 78) - -68);
    	a230 = a359;
    	a225 = a205[((a206 + a57) + -19)];
    	a75 = "e";
    	a300 = true;
    	a77 = ((a237 / a237) - -10);
    	a353 = a263;
    	a312 = "e";
    	a307 = a227[(a230 + -3)];
    	a202 = a217[((a211 + a211) + -6)];
    	a382 = ((((((a243 * a357) % 14999) + -4301) % 107) - -69) + -1);
    	a132 = false;
    	a249 = (((((a249 * a243) % 14999) + -10815) * 1) / 5);
    	a398 = (a211 + 8);
    	a207 = "f";
    	a234 = a372[((a338 * a338) + -9)];
    	a206 = (a323 - 6);
    	a270 = a323;
    	a386 = (((((((a357 * a243) % 14999) % 61) - -263) + 1) + 24229) - 24229);
    	a358 = a348;
    	a237 = (a320 - a359);
    	a211 = (a320 - 5);
    	}System.out.println("S");
    } 
}
private  void calculateOutputm11(String input) {
    if((((a312.equals("g")) && (((a359 == 5) && ((147 < a333) && (177 >= a333))) && (110 == a276[2]))) && (a324 == a232[2] && (a240 && (cf && (20 == a137[1])))))) {
    	if((((38 == a370[4]) && ((77 == a358[0]) && (a302 == a346[2] && (((a154 == 10) && cf) && (92 == a296[2]))))) && (((148 < a277) && (339 >= a277)) && ((-12 < a201) && (178 >= a201))))) {
    		calculateOutputm138(input);
    	} 
    	if((((a240 && ((316 < a310) && (357 >= a310))) && (a237 == 5)) && (((-57 < a357) && (20 >= a357)) && ((a338 == 5) && ((50 == a392[4]) && (cf && (a154 == 11))))))) {
    		calculateOutputm139(input);
    	} 
    	if(((cf && (a154 == 12)) && ((((77 == a358[0]) && (((148 < a277) && (339 >= a277)) && (a234 == a372[2] && !a339))) && (a313.equals("g"))) && a395))) {
    		calculateOutputm140(input);
    	} 
    	if((((a368 && (a338 == 5)) && a336) && (((-39 < a382) && (176 >= a382)) && (((92 == a296[2]) && ((a154 == 15) && cf)) && ((201 < a386) && (325 >= a386)))))) {
    		calculateOutputm141(input);
    	} 
    	if(((a286 == a294[2] && ((a154 == 16) && cf)) && (((59 == a228[3]) && (a240 && ((a313.equals("g")) && a260))) && a324 == a232[2]))) {
    		calculateOutputm142(input);
    	} 
    } 
    if((((((26 == a137[1]) && cf) && a336) && (a206 == 6)) && ((15 == a353[2]) && ((((-39 < a382) && (176 >= a382)) && ((-156 < a243) && (-68 >= a243))) && (a207.equals("g")))))) {
    	if((((a218.equals("g")) && (((a256.equals("g")) && ((355 < a249) && (499 >= a249))) && (92 == a296[2]))) && ((a224.equals("g")) && (a368 && (cf && a295 == a366[2]))))) {
    		calculateOutputm143(input);
    	} 
    	if(((((cf && a295 == a366[4]) && a225 == a205[2]) && (a329.equals("g"))) && (((a256.equals("g")) && (a260 && (77 == a358[0]))) && a324 == a232[2]))) {
    		calculateOutputm144(input);
    	} 
    } 
    if((((a207.equals("g")) && ((a206 == 6) && ((31 == a137[0]) && cf))) && ((a270 == 12) && ((15 == a353[2]) && ((a224.equals("g")) && (a313.equals("g"))))))) {
    	if(((!a373 && ((a329.equals("g")) && (a206 == 6))) && ((a240 && ((15 == a353[2]) && ((a323 == 7) && cf))) && ((-12 < a201) && (178 >= a201))))) {
    		calculateOutputm145(input);
    	} 
    	if(((((148 < a277) && (339 >= a277)) && (((a225 == a205[2] && (cf && (a323 == 10))) && a300) && ((355 < a249) && (499 >= a249)))) && (a302 == a346[2] && (a224.equals("g"))))) {
    		calculateOutputm146(input);
    	} 
    } 
}
private  void calculateOutputm152(String input) {
    if(((((((-39 < a382) && (176 >= a382)) && ((a338 == 5) && (((a72 <=  -104 && cf) && (a57 == 15)) && a141 == a47[3]))) && (a75.equals("g"))) && (15 == a353[2])) && (((a307 == a227[2] && !a373) && ((355 < a249) && (499 >= a249))) && input.equals(inputs[2])))) {
    	cf = false;
    	a230 = ((a237 * a237) + -22);
    	a333 = (((((a333 * a72) % 14999) - 3263) * 1) / 5);
    	a382 = ((((a382 * a357) - 11048) - 2188) - 5781);
    	a378 = true;
    	a256 = "e";
    	a361 = "e";
    	a269 = "e";
    	a370 = a318;
    	a75 = "h";
    	a395 = true;
    	a265 = a303;
    	a249 = (((((a249 * a357) % 15076) + -14923) - 0) + -2);
    	a295 = a366[((a57 / a57) + 6)];
    	a201 = ((((a201 * a357) - 16531) - -42159) - 36696);
    	a324 = a232[((a270 * a230) + -34)];
    	a211 = a230;
    	a210 = true;
    	a240 = true;
    	a276 = a253;
    	a392 = a208;
    	a235 = a216[(a398 - 10)];
    	a228 = a229;
    	a134 = false;
    	a218 = "e";
    	a313 = "e";
    	a300 = true;
    	a320 = (a270 + -6);
    	a260 = true;
    	a237 = ((a270 * a398) + -117);
    	a207 = "f";
    	a206 = (a270 - 8);
    	a307 = a227[(a270 + -10)];
    	a234 = a372[(a270 - 12)];
    	a338 = (a398 - 7);
    	a310 = (((((a277 * a386) % 14999) / 5) + -2946) / 5);
    	a296 = a362;
    	a373 = true;
    	a357 = ((((((a357 * a382) % 14999) % 38) - 18) - -1) + -1);
    	a336 = true;
    	a353 = a263;
    	a270 = (a359 + 7); 
    	System.out.println("T");
    } 
    if((((50 == a392[4]) && (a141 == a47[3] && (((cf && (a57 == 15)) && (a75.equals("g"))) && ((-39 < a382) && (176 >= a382))))) && (((((a234 == a372[2] && a72 <=  -104) && (a270 == 12)) && input.equals(inputs[8])) && (56 == a265[2])) && (a230 == 5)))) {
    	a67 -= (a67 - 20) < a67 ? 3 : 0;
    	cf = false;
    	a373 = true;
    	a339 = false;
    	a361 = "e";
    	a338 = (a237 + -1);
    	a224 = "g";
    	a378 = true;
    	a277 = (((((((a333 * a249) % 14999) / 5) - 19363) + 21474) % 95) + 231);
    	a395 = true;
    	a202 = a217[(a230 - 4)];
    	a201 = ((((((a201 * a243) * 1) % 93) + -105) + 20426) + -20426);
    	a265 = a303;
    	a368 = true;
    	a137 = a117;
    	a57 = 13;
    	a243 = (((((((a243 * a382) % 11) + -167) * 5) * 5) % 11) + -165);
    	a358 = a335;
    	a329 = "g";
    	a382 = ((((((a382 * a357) % 12) - 50) + 13817) / 5) + -2802);
    	a225 = a205[(a270 + -10)];
    	a386 = ((((((a333 * a357) / 5) + -4340) / 5) % 61) + 159);
    	a359 = (a206 + -2);
    	a398 = ((a206 - a320) - -14);
    	a323 = (a57 - 3);
    	a269 = "e";
    	a392 = a257;
    	a333 = ((((((a357 * a277) / 5) - 22314) / 5) % 14) - -176);
    	a260 = true;
    	a256 = "e";
    	a234 = a372[((a398 - a398) + 1)];
    	a312 = "g";
    	a239 = a268;
    	a320 = (a230 + 1); 
    	System.out.println("P");
    } 
}
private  void calculateOutputm13(String input) {
    if(((((-12 < a201) && (178 >= a201)) && ((-156 < a243) && (-68 >= a243))) && (((!a373 && ((a72 <=  -104 && cf) && (a269.equals("g")))) && ((147 < a333) && (177 >= a333))) && ((-57 < a357) && (20 >= a357))))) {
    	if(((a378 && (((((92 == a296[2]) && (a313.equals("g"))) && (59 == a228[3])) && (a237 == 5)) && a336)) && (cf && a141 == a47[3]))) {
    		calculateOutputm152(input);
    	} 
    } 
}
private  void calculateOutputm159(String input) {
    if((((((148 < a277) && (339 >= a277)) && (((a75.equals("g")) && (((92 == a296[2]) && a300) && a286 == a294[2])) && a184 == a157[2])) && ((a57 == 16) && (((input.equals(inputs[1]) && (a278 == a326[3] && cf)) && ((-57 < a357) && (20 >= a357))) && (a269.equals("g"))))) && a165 == 4790)) {
    	a168 += (a168 + 20) > a168 ? 4 : 0;
    	cf = false;
    	a72 = ((((36 * -29) / 10) - 11644) - 4894);
    	a141 = a47[((a270 - a270) - -2)];
    	a57 = (a398 - -3); 
    	System.out.println("R");
    } 
    if((((((a224.equals("g")) && ((a75.equals("g")) && (cf && input.equals(inputs[4])))) && (15 == a353[2])) && (((147 < a333) && (177 >= a333)) && (a278 == a326[3] && ((110 == a276[2]) && ((a57 == 16) && (a184 == a157[2] && ((a218.equals("g")) && a286 == a294[2]))))))) && a89 == 9776)) {
    	cf = false;
    	a75 = "f";
    	a173 = "g";
    	a172 = (a237 - 3);
    	a8 = (a237 - -1); 
    	System.out.println("Y");
    } 
    if(((((a361.equals("g")) && (input.equals(inputs[6]) && ((((148 < a277) && (339 >= a277)) && a278 == a326[3]) && a184 == a157[2]))) && !a339) && ((a302 == a346[2] && (a368 && (((a57 == 16) && cf) && (a75.equals("g"))))) && a300))) {
    	a120 -= (a120 - 20) < a120 ? 3 : 0;
    	cf = false;
    	a218 = "e";
    	a224 = "e";
    	a296 = a212;
    	a358 = a348;
    	a295 = a366[(a57 - 12)];
    	a184 = a157[(a398 - 9)];
    	a201 = (((((a333 * a382) % 14999) / 5) / 5) - 13715);
    	a336 = true;
    	a276 = a253;
    	a320 = (a230 - -1);
    	a312 = "e";
    	a75 = "h";
    	a202 = a217[(a211 + -3)];
    	a307 = a227[(a359 - 5)];
    	a310 = ((((((a310 * a249) % 14999) - 23789) * 1) + 25004) * -1);
    	a134 = false;
    	a339 = true;
    	a359 = (a320 - 3);
    	a269 = "e";
    	a373 = true;
    	a333 = ((((((a333 * a310) % 14999) - -14365) % 14976) + -15022) * 1); 
    	System.out.println("S");
    } 
    if((((((a269.equals("g")) && (a269.equals("g"))) && a260) && a278 == a326[3]) && ((110 == a276[2]) && ((a225 == a205[2] && ((a57 == 16) && (((cf && a184 == a157[2]) && (a75.equals("g"))) && (a338 == 5)))) && input.equals(inputs[5]))))) {
    	a131 += (a131 + 20) > a131 ? 2 : 0;
    	a169 += (a169 + 20) > a169 ? 1 : 0;
    	a35 += (a35 + 20) > a35 ? 2 : 0;
    	a90 += (a90 + 20) > a90 ? 6 : 0;
    	a110 += (a110 + 20) > a110 ? 2 : 0;
    	a31 -= (a31 - 20) < a31 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 2 : 0;
    	a122 -= (a122 - 20) < a122 ? 4 : 0;
    	cf = false;
    	a300 = false;
    	a378 = false;
    	a228 = a292;
    	a243 = (((((a277 * a357) % 11) - 167) - -4748) - 4747);
    	a57 = (a338 + 8);
    	a383 = a226[(a270 / a237)];
    	a137 = a189;
    	a313 = "g";
    	a324 = a232[(a320 + -6)];
    	a154 = (a359 - -11);
    	a256 = "f";
    	a277 = (((((((a277 * a382) % 14999) % 95) - -243) - 0) / 5) + 151);
    	a310 = ((((((a310 * a386) % 14999) % 77) + 168) / 5) + 239);
    	a392 = a304;
    	a207 = "g";
    	a202 = a217[((a398 + a230) - 15)];
    	a269 = "f";
    	a235 = a216[(a211 + -2)]; 
    	System.out.println("Z");
    } 
    if(((((a240 && ((a336 && a234 == a372[2]) && (77 == a358[0]))) && input.equals(inputs[9])) && (a75.equals("g"))) && (a278 == a326[3] && (((((a57 == 16) && cf) && a184 == a157[2]) && a336) && (a206 == 6))))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 4 : 0;
    	a56 += (a56 + 20) > a56 ? 2 : 0;
    	a5 += (a5 + 20) > a5 ? 2 : 0;
    	a93 += (a93 + 20) > a93 ? 2 : 0;
    	a94 -= (a94 - 20) < a94 ? 4 : 0;
    	cf = false;
    	a358 = a348;
    	a383 = a226[(a359 - 5)];
    	a243 = (((48 - 11616) + 8710) * 5);
    	a218 = "e";
    	a333 = ((((((a243 * a243) % 14999) - -8293) * 1) * -1) / 10);
    	a339 = true;
    	a296 = a212;
    	a338 = (a57 - 13);
    	a398 = (a320 + 2);
    	a357 = ((((((a357 * a249) % 14906) + -15093) + 23823) * 1) - 23823);
    	a206 = (a320 - 4);
    	a237 = (a320 - 5);
    	a300 = true;
    	a260 = true;
    	a276 = a250;
    	a73 = ((((((((a277 * a382) % 14999) % 54) + 221) * 5) + -8088) % 54) + 271);
    	a310 = (((((((a310 * a357) % 14999) + 11653) * 2) - -6430) % 77) - -238);
    	a225 = a205[(a206 - 4)];
    	a269 = "f";
    	a240 = true;
    	a373 = true;
    	a386 = (((((a333 * a243) % 14999) - 4353) + -17231) / 5);
    	a329 = "e";
    	a353 = a263;
    	a395 = true;
    	a286 = a294[(a338 - 1)];
    	a256 = "g";
    	a324 = a232[((a359 / a359) + -1)];
    	a249 = (((((((a243 * a73) % 14999) - 6913) % 71) - -458) + 4673) - 4647);
    	a312 = "e";
    	a361 = "e";
    	a307 = a227[(a206 + -3)];
    	a336 = true;
    	a146 = false;
    	a224 = "g";
    	a368 = true;
    	a277 = (((((a333 * a333) % 14999) - 28371) + -186) - 1045);
    	a234 = a372[(a230 - 3)];
    	a270 = (a211 + 7);
    	a382 = ((((((a386 * a333) % 14999) - -14133) + -5959) % 12) + -51);
    	a370 = a285;
    	a302 = a346[(a206 - 2)];
    	a230 = (a320 - 5);
    	a211 = (a338 + -1);
    	a265 = a376;
    	a235 = a216[(a320 - 8)];
    	a75 = "i";
    	a239 = a242;
    	a16 = true;
    	a202 = a217[(a398 - 9)];
    	a320 = (a359 + 2);
    	a359 = (a398 - 5); 
    	System.out.println("T");
    } 
    if((((input.equals(inputs[0]) && ((a75.equals("g")) && ((((355 < a249) && (499 >= a249)) && !a373) && (a320 == 8)))) && (((148 < a277) && (339 >= a277)) && (((148 < a277) && (339 >= a277)) && ((a278 == a326[3] && ((cf && (a57 == 16)) && a184 == a157[2])) && a300)))) && a63 >= 3)) {
    	a174 += (a174 + 20) > a174 ? 2 : 0;
    	cf = false;
    	a183 = "h";
    	a392 = a304;
    	a228 = a292;
    	a278 = a326[(a270 + -7)]; 
    	System.out.println("R");
    } 
    if((((a57 == 16) && (a302 == a346[2] && ((((15 == a353[2]) && (a269.equals("g"))) && a278 == a326[3]) && (a211 == 3)))) && (((((input.equals(inputs[2]) && cf) && a184 == a157[2]) && (a269.equals("g"))) && (a75.equals("g"))) && a336))) {
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	cf = false;
    	a378 = true;
    	a201 = (((((((a382 * a310) % 14999) % 94) - -83) - -1) - 16607) - -16607);
    	a324 = a232[(a338 + -3)];
    	a202 = a217[(a230 - 3)];
    	a158 = (a57 - 11);
    	a370 = a318;
    	a200 = a115[((a230 + a158) + -5)];
    	a57 = (a270 + -2);
    	a313 = "g";
    	a392 = a304;
    	a207 = "g";
    	a276 = a253;
    	a235 = a216[(a206 - 4)]; 
    	System.out.println("V");
    } 
    if(((((a218.equals("g")) && ((a278 == a326[3] && ((a224.equals("g")) && ((a206 == 6) && input.equals(inputs[8])))) && a240)) && ((a329.equals("g")) && (!a373 && ((((a75.equals("g")) && cf) && a184 == a157[2]) && (a57 == 16))))) && a174 <= 3)) {
    	a162 -= (a162 - 20) < a162 ? 3 : 0;
    	cf = false;
    	a230 = ((a57 / a359) - -1);
    	a75 = "f";
    	a129 = a92[(a359 - 4)];
    	a307 = a227[(a230 - 3)];
    	a125 = a30[((a237 + a320) + -6)];
    	a357 = (((((a357 * a333) - 9546) % 65) + -84) - -15);
    	a395 = false;
    	a276 = a289;
    	a329 = "f";
    	a338 = (a57 - 12);
    	a382 = (((((((a382 * a249) % 14999) - -7478) - 8424) - 11164) % 12) + -52);
    	a361 = "f";
    	a270 = ((a398 - a359) + 4);
    	a286 = a294[((a338 / a206) - -1)];
    	a240 = false;
    	a173 = "f";
    	a225 = a205[((a230 + a211) - 6)];
    	a310 = ((((((a310 * a277) % 14999) + -1100) % 77) - -238) * 1);
    	a370 = a285;
    	a302 = a346[((a57 / a359) + -2)]; 
    	System.out.println("R");
    } 
}
private  void calculateOutputm164(String input) {
    if((((((201 < a386) && (325 >= a386)) && a278 == a326[7]) && input.equals(inputs[5])) && ((((((((a75.equals("g")) && (cf && (a57 == 16))) && a81 == a167[0]) && (50 == a392[4])) && a202 == a217[2]) && (50 == a392[4])) && ((148 < a277) && (339 >= a277))) && a286 == a294[2]))) {
    	a131 += (a131 + 20) > a131 ? 2 : 0;
    	a169 += (a169 + 20) > a169 ? 1 : 0;
    	a35 += (a35 + 20) > a35 ? 2 : 0;
    	a90 += (a90 + 20) > a90 ? 6 : 0;
    	a110 += (a110 + 20) > a110 ? 2 : 0;
    	a31 -= (a31 - 20) < a31 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 2 : 0;
    	a120 += (a120 + 20) > a120 ? 2 : 0;
    	cf = false;
    	a358 = a335;
    	a57 = (a270 + 1);
    	a228 = a292;
    	a310 = (((((((a310 * a249) % 14999) - -4600) % 77) + 178) * 10) / 9);
    	a276 = a250;
    	a300 = false;
    	a382 = (((((a357 * a386) - 11071) + 31650) % 107) + -32);
    	a243 = ((((((a277 * a357) - -2422) - -10110) - 15621) % 11) + -167);
    	a235 = a216[((a206 - a206) + 1)];
    	a137 = a189;
    	a339 = false;
    	a277 = (((((((a277 * a357) % 95) + 243) + 0) * 5) % 95) - -182);
    	a269 = "f";
    	a378 = false;
    	a256 = "f";
    	a201 = (((((((a201 * a382) % 14999) % 93) - 104) - -16343) - -2145) - 18489);
    	a370 = a311;
    	a154 = (a270 + 4);
    	a312 = "g";
    	a383 = a226[(a230 - 3)]; 
    	System.out.println("Z");
    } 
    if((((((a57 == 16) && (((a81 == a167[0] && cf) && input.equals(inputs[6])) && a278 == a326[7])) && !a373) && (a224.equals("g"))) && ((a237 == 5) && (((a235 == a216[2] && a383 == a226[2]) && (a75.equals("g"))) && !a373)))) {
    	a5 -= (a5 - 20) < a5 ? 2 : 0;
    	cf = false;
    	a296 = a212;
    	a333 = (((((a277 * a277) % 14999) + -17251) * 1) / 5);
    	a373 = true;
    	a218 = "e";
    	a359 = a211;
    	a202 = a217[(a359 - 3)];
    	a276 = a253;
    	a201 = ((((((a201 * a333) % 14999) % 14900) - 15098) - 2) + -1);
    	a320 = ((a57 + a270) + -22);
    	a336 = true;
    	a307 = a227[((a211 - a270) + 9)];
    	a184 = a157[(a57 - 13)];
    	a207 = "e";
    	a224 = "e";
    	a134 = false;
    	a378 = true;
    	a295 = a366[((a206 - a237) + 3)];
    	a75 = "h";
    	a310 = (((((a310 * a357) + -8698) + 18468) * 1) + -18554); 
    	System.out.println("T");
    } 
    if((((((a57 == 16) && (a225 == a205[2] && (a278 == a326[7] && (cf && input.equals(inputs[4]))))) && a378) && (((a207.equals("g")) && ((a75.equals("g")) && ((a383 == a226[2] && (a329.equals("g"))) && a81 == a167[0]))) && a383 == a226[2])) && a89 == 9776)) {
    	a31 -= (a31 - 20) < a31 ? 2 : 0;
    	cf = false;
    	a75 = "f";
    	a173 = "g";
    	a172 = ((a57 + a57) - 30);
    	a8 = ((a270 / a270) + 5); 
    	System.out.println("R");
    } 
    if((((((a75.equals("g")) && (((!a373 && a383 == a226[2]) && a383 == a226[2]) && ((316 < a310) && (357 >= a310)))) && (a211 == 3)) && a81 == a167[0]) && ((((cf && input.equals(inputs[2])) && a278 == a326[7]) && ((148 < a277) && (339 >= a277))) && (a57 == 16)))) {
    	a89 += (a89 + 20) > a89 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a67 -= (a67 - 20) < a67 ? 1 : 0;
    	cf = false;
    	a312 = "g";
    	a57 = (a359 + 5);
    	a358 = a335;
    	a339 = false;
    	a382 = ((((((((a249 * a249) % 14999) % 107) - -51) * 9) / 10) * 10) / 9);
    	a370 = a318;
    	a158 = (a57 - 5);
    	a276 = a253;
    	a269 = "g";
    	a200 = a115[(a320 + -3)]; 
    	System.out.println("V");
    } 
    if((((((-57 < a357) && (20 >= a357)) && (a302 == a346[2] && (input.equals(inputs[0]) && ((148 < a277) && (339 >= a277))))) && ((a260 && (a81 == a167[0] && (a383 == a226[2] && (((cf && a278 == a326[7]) && (a75.equals("g"))) && (a57 == 16))))) && (47 == a239[4]))) && a63 >= 3)) {
    	a63 -= (a63 - 20) < a63 ? 1 : 0;
    	cf = false;
    	a228 = a292;
    	a183 = "h";
    	a370 = a311;
    	a312 = "g";
    	a278 = a326[(a57 - 11)]; 
    	System.out.println("X");
    } 
    if((((((148 < a277) && (339 >= a277)) && (((input.equals(inputs[9]) && cf) && (a57 == 16)) && a278 == a326[7])) && (a207.equals("g"))) && ((a383 == a226[2] && ((((a313.equals("g")) && (a361.equals("g"))) && a81 == a167[0]) && (a75.equals("g")))) && a336))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 4 : 0;
    	a56 += (a56 + 20) > a56 ? 2 : 0;
    	a5 += (a5 + 20) > a5 ? 2 : 0;
    	a93 += (a93 + 20) > a93 ? 2 : 0;
    	a94 -= (a94 - 20) < a94 ? 4 : 0;
    	cf = false;
    	a329 = "e";
    	a270 = (a206 + 4);
    	a201 = ((((((a201 * a310) % 14999) + -10983) / 5) % 93) - 105);
    	a398 = ((a270 / a270) + 9);
    	a218 = "e";
    	a320 = (a211 + 4);
    	a373 = true;
    	a260 = true;
    	a286 = a294[(a57 - 14)];
    	a265 = a376;
    	a361 = "e";
    	a207 = "e";
    	a338 = (a398 + -7);
    	a395 = true;
    	a230 = (a398 + -7);
    	a202 = a217[((a398 * a270) + -99)];
    	a368 = true;
    	a225 = a205[(a398 + -10)];
    	a378 = true;
    	a16 = true;
    	a333 = ((((99 / 5) * 5) * -5) / 10);
    	a269 = "f";
    	a234 = a372[(a57 + -14)];
    	a324 = a232[(a230 - 3)];
    	a310 = ((((((((a310 * a386) % 14999) - -9881) % 77) - -232) * 5) % 77) - -166);
    	a237 = (a359 + -2);
    	a243 = (((66 * 5) - 22567) + -2700);
    	a313 = "f";
    	a240 = true;
    	a392 = a257;
    	a73 = (((4 / 5) / 5) - -243);
    	a235 = a216[(a398 - 10)];
    	a224 = "g";
    	a386 = (((((a333 * a243) % 14999) - 18401) / 5) / 5);
    	a302 = a346[(a206 / a338)];
    	a359 = ((a57 + a270) - 21);
    	a146 = false;
    	a276 = a250;
    	a307 = a227[(a57 + -15)];
    	a75 = "i";
    	a300 = true;
    	a277 = (((((a277 * a333) % 14999) - 12025) - -2698) / 5);
    	a357 = (((((a357 * a249) + -273) % 14906) + -15093) * 1);
    	a296 = a212;
    	a256 = "g";
    	a353 = a263;
    	a249 = ((((((a249 % 71) + 394) * 5) + 596) % 71) + 427);
    	a239 = a242;
    	a336 = true;
    	a383 = a226[(a211 + -3)];
    	a211 = (a206 + -4);
    	a206 = (a338 + 1); 
    	System.out.println("T");
    } 
    if((((a81 == a167[0] && (a278 == a326[7] && (((a75.equals("g")) && cf) && (a57 == 16)))) && ((92 == a296[2]) && ((((a230 == 5) && (a235 == a216[2] && (input.equals(inputs[8]) && ((316 < a310) && (357 >= a310))))) && a395) && a202 == a217[2]))) && a174 <= 3)) {
    	cf = false;
    	a75 = "f";
    	a270 = (a320 + 3);
    	a225 = a205[((a237 - a237) - -1)];
    	a240 = false;
    	a329 = "f";
    	a338 = (a359 - 1);
    	a357 = ((((((a357 * a333) - 9625) % 65) - 90) / 5) + -113);
    	a395 = false;
    	a313 = "f";
    	a361 = "f";
    	a230 = (a359 - 1);
    	a202 = a217[(a206 - 5)];
    	a392 = a257;
    	a129 = a92[(a57 / a57)];
    	a286 = a294[((a320 * a320) + -63)];
    	a235 = a216[(a206 + -5)];
    	a173 = "f";
    	a307 = a227[(a270 + -10)];
    	a125 = a30[(a359 + 2)];
    	a302 = a346[(a338 - 3)];
    	a324 = a232[(a206 - 5)];
    	a201 = ((((((a201 * a310) % 14999) % 93) + -105) - -1) + -2);
    	a310 = (((((a310 * a382) * 1) * 1) % 77) - -241); 
    	System.out.println("X");
    } 
    if((((input.equals(inputs[1]) && (((a278 == a326[7] && ((a57 == 16) && cf)) && a324 == a232[2]) && ((201 < a386) && (325 >= a386)))) && (a302 == a346[2] && (((147 < a333) && (177 >= a333)) && (a81 == a167[0] && ((56 == a265[2]) && ((a270 == 12) && (a75.equals("g")))))))) && a165 == 4790)) {
    	a120 -= (a120 - 20) < a120 ? 1 : 0;
    	cf = false;
    	a72 = (((54 + -20094) - 7407) - 2110);
    	a141 = a47[(a206 + -4)];
    	a57 = (a338 - -10); 
    	System.out.println("Z");
    } 
}
private  void calculateOutputm14(String input) {
    if((((((316 < a310) && (357 >= a310)) && (a278 == a326[3] && cf)) && a336) && ((a336 && ((a270 == 12) && (a256.equals("g")))) && a307 == a227[2]))) {
    	if((((38 == a370[4]) && ((a312.equals("g")) && ((a336 && ((-57 < a357) && (20 >= a357))) && (a270 == 12)))) && ((a184 == a157[2] && cf) && (a218.equals("g"))))) {
    		calculateOutputm159(input);
    	} 
    } 
    if((((a237 == 5) && ((a202 == a217[2] && ((50 == a392[4]) && (((-12 < a201) && (178 >= a201)) && (cf && a278 == a326[7])))) && ((316 < a310) && (357 >= a310)))) && a300)) {
    	if((((a313.equals("g")) && a368) && ((56 == a265[2]) && ((a320 == 8) && (a202 == a217[2] && (((355 < a249) && (499 >= a249)) && (cf && a81 == a167[0]))))))) {
    		calculateOutputm164(input);
    	} 
    } 
}
private  void calculateOutputm171(String input) {
    if(((input.equals(inputs[6]) && ((a240 && a249 <=  152) && (a196 == 10))) && ((((a383 == a226[0] && (((a134 && cf) && a129 == a92[2]) && (a313.equals("e")))) && (a75.equals("h"))) && (a207.equals("e"))) && (a270 == 10)))) {
    	a122 -= (a122 - 20) < a122 ? 2 : 0;
    	cf = false;
    	if((!(a313.equals("h")) || (((a270 == 11) || a132) && a225 == a205[5]))) {
    	a207 = "f";
    	a270 = ((a320 * a398) - 49);
    	a75 = "f";
    	a313 = "f";
    	a269 = "f";
    	a218 = "e";
    	a300 = true;
    	a225 = a205[(a359 - 3)];
    	a173 = "i";
    	a386 = (((((((a386 * a357) % 14999) - 14814) - 96) - -23026) % 61) + 140);
    	a141 = a47[(a320 / a320)];
    	a333 = ((((((a333 * a249) % 14999) - -11946) % 14976) + -15022) - 2);
    	a228 = a229;
    	a42 = (a206 - -4);
    	a206 = (a237 - -2);
    	a383 = a226[(a398 - 10)];
    	a277 = (((((((a277 * a201) % 14999) / 5) / 5) - -5628) % 78) - -58);
    	a260 = false;
    	a234 = a372[((a338 * a230) - 9)];
    	a239 = a242;
    	a240 = false;
    	a320 = (a398 + -3);
    	a276 = a289;
    	a230 = (a270 + -8);
    	a338 = ((a398 * a270) + -107);
    	a359 = ((a237 / a196) - -3);
    	a312 = "e";
    	a237 = ((a398 / a270) - -3);
    	a211 = (a398 + -9);
    	a353 = a263;
    	a373 = true;
    	a398 = (a270 - 1); 
    	}else {

    	}System.out.println("S");
    } 
}
private  void calculateOutputm174(String input) {
    if((((a196 == 12) && (a134 && (((241 < a54) && (402 >= a54)) && cf))) && (a357 <=  -188 && (((3 == a353[2]) && ((a75.equals("h")) && ((a361.equals("e")) && (input.equals(inputs[8]) && ((a320 == 6) && (37 == a392[3])))))) && a225 == a205[0])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	if((((a184 == 8) && (a224.equals("g"))) || !(a141 == 9))) {
    	a57 = (a206 + 12);
    	a75 = "g";
    	a183 = "i";
    	a278 = a326[(a196 - 7)]; 
    	}else {
    	a75 = "g";
    	a256 = "h";
    	a249 = ((((a249 % 14750) - -15249) * 1) + 1);
    	a373 = false;
    	a302 = a346[0];
    	a201 = ((((a201 / 5) % 94) - -130) - 10);
    	a243 = (((((a243 - 0) % 43) - 98) * 9) / 10);
    	a313 = "i";
    	a386 = (((27 / -5) / 5) + -9584);
    	a202 = a217[7];
    	a323 = a398;
    	a353 = a241;
    	a358 = a348;
    	a338 = 7;
    	a277 = ((((((a277 % 78) + 146) * 9) / 10) - 29080) - -29015);
    	a234 = a372[0];
    	a324 = a232[5];
    	a296 = a384;
    	a137 = a117;
    	a368 = false;
    	a310 = ((((a310 - 0) % 15080) - 14919) - 1);
    	a240 = false;
    	a239 = a299;
    	a235 = a216[7];
    	a269 = "g";
    	a211 = 4;
    	a361 = "f";
    	a383 = a226[6];
    	a225 = a205[4];
    	a359 = 8;
    	a276 = a289;
    	a207 = "h";
    	a357 = (((((a357 * -2) / 10) * 10) / 9) * 4);
    	a370 = a318;
    	a329 = "e";
    	a336 = false;
    	a57 = ((a230 - a230) + 13);
    	a382 = ((((a382 * 1) + 18776) / 5) - -10007);
    	a270 = 10;
    	a218 = "f";
    	a224 = "f";
    	a228 = a264;
    	a339 = true;
    	a300 = false;
    	a237 = 10;
    	a307 = a227[7];
    	a320 = 10;
    	a398 = 16;
    	a206 = 11;
    	a378 = false;
    	a333 = ((((a333 % 14976) + -47) + -4925) - 7438);
    	a265 = a376;
    	a395 = false;
    	a312 = "e";
    	a260 = false;
    	a392 = a208;
    	a230 = 4;
    	}System.out.println("T");
    } 
    if(((input.equals(inputs[2]) && ((a302 == a346[0] && (a234 == a372[0] && ((24 == a370[2]) && ((a75.equals("h")) && ((a134 && cf) && ((241 < a54) && (402 >= a54))))))) && (81 == a296[3]))) && (((a196 == 12) && (a338 == 3)) && (24 == a370[2])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a173 = "g";
    	a172 = (a196 - 5);
    	a75 = "f";
    	a99 = a26[(a320 + -6)]; 
    	System.out.println("T");
    } 
    if((((a134 && ((cf && ((241 < a54) && (402 >= a54))) && input.equals(inputs[9]))) && (a312.equals("e"))) && (((a218.equals("e")) && ((a211 == 1) && ((((a196 == 12) && a243 <=  -179) && a373) && (a211 == 1)))) && (a75.equals("h"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a122 -= (a122 - 20) < a122 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if((a339 || !a123)) {
    	a361 = "f";
    	a395 = false;
    	a240 = false;
    	a378 = false;
    	a234 = a372[2];
    	a300 = false;
    	a373 = false;
    	a235 = a216[5];
    	a256 = "i";
    	a211 = 6;
    	a370 = a311;
    	a324 = a232[6];
    	a392 = a257;
    	a307 = a227[4];
    	a134 = false;
    	a265 = a376;
    	a357 = (((a357 - -9410) + -7092) + 27855);
    	a207 = "h";
    	a296 = a384;
    	a260 = false;
    	a310 = (((((a310 % 77) - -237) * 1) - 19646) + 19648);
    	a210 = true;
    	a249 = (((((a249 + 26431) - 9895) * 1) % 14750) - -15249);
    	a353 = a399;
    	a237 = 10;
    	a295 = a366[(a398 - 3)]; 
    	}else {
    	a132 = false;
    	a75 = "e";
    	a129 = a92[(a230 - -2)];
    	a77 = ((a196 + a196) + -12);
    	}System.out.println("X");
    } 
    if(((((a224.equals("e")) && (a243 <=  -179 && (((a75.equals("h")) && (cf && ((241 < a54) && (402 >= a54)))) && (a196 == 12)))) && input.equals(inputs[6])) && (a134 && (a277 <=  -10 && (a234 == a372[0] && ((a218.equals("e")) && (3 == a353[2]))))))) {
    	a164 += (a164 + 20) > a164 ? 2 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	a75 = "g";
    	a57 = a196;
    	a81 = a167[(a338 - 3)];
    	a99 = a26[(a57 + -10)]; 
    	System.out.println("T");
    } 
    if(((((241 < a54) && (402 >= a54)) && ((a196 == 12) && (a134 && (((input.equals(inputs[3]) && cf) && (a75.equals("h"))) && (3 == a353[2]))))) && ((a324 == a232[0] && (a368 && ((a256.equals("e")) && a307 == a227[0]))) && a383 == a226[0]))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a174 += (a174 + 20) > a174 ? 2 : 0;
    	a133 += (a133 + 20) > a133 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a134 = false;
    	a295 = a366[(a237 + 1)];
    	a184 = a157[((a196 + a196) - 21)]; 
    	System.out.println("Y");
    } 
    if((((((37 == a392[3]) && a134) && a382 <=  -65) && a300) && (((a361.equals("e")) && (a249 <=  152 && ((((a196 == 12) && (input.equals(inputs[0]) && cf)) && ((241 < a54) && (402 >= a54))) && (a75.equals("h"))))) && (a361.equals("e"))))) {
    	a164 += (a164 + 20) > a164 ? 2 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	a77 = (a359 + 7);
    	a75 = "e";
    	a132 = false;
    	a1 = a87[(a77 + -7)]; 
    	System.out.println("V");
    } 
    if(((a225 == a205[0] && ((241 < a54) && (402 >= a54))) && ((((a312.equals("e")) && ((a206 == 4) && (((a75.equals("h")) && (((cf && a134) && (a196 == 12)) && a333 <=  -47)) && input.equals(inputs[7])))) && a310 <=  160) && a302 == a346[0]))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	a94 += (a94 + 20) > a94 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a75 = "e";
    	a132 = false;
    	a77 = (a320 - -4);
    	a1 = a87[((a359 - a270) - -9)]; 
    	System.out.println("V");
    } 
    if(((a243 <=  -179 && (a260 && (((a75.equals("h")) && (a134 && (((241 < a54) && (402 >= a54)) && cf))) && a378))) && (((3 == a353[2]) && (((a361.equals("e")) && input.equals(inputs[4])) && (a196 == 12))) && (47 == a265[5])))) {
    	a164 += (a164 + 20) > a164 ? 2 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a75 = "f";
    	a107 = "g";
    	a173 = "g";
    	a172 = ((a206 * a270) - 32); 
    	System.out.println("Y");
    } 
    if(((((a75.equals("h")) && (input.equals(inputs[1]) && (a339 && a234 == a372[0]))) && (81 == a296[3])) && ((((67 == a358[2]) && (((241 < a54) && (402 >= a54)) && (a134 && ((a196 == 12) && cf)))) && (a237 == 3)) && (a312.equals("e"))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 -= (a12 - 20) < a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a178 += (a178 + 20) > a178 ? 4 : 0;
    	cf = false;
    	a398 = 15;
    	a44 = "g";
    	a132 = false;
    	a329 = "h";
    	a234 = a372[6];
    	a300 = true;
    	a286 = a294[2];
    	a75 = "e";
    	a77 = (a206 + 4); 
    	System.out.println("V");
    } 
    if(((((241 < a54) && (402 >= a54)) && ((a75.equals("h")) && ((24 == a370[2]) && ((24 == a370[2]) && (((45 == a228[1]) && a201 <=  -199) && (a206 == 4)))))) && ((a196 == 12) && (((cf && a134) && input.equals(inputs[5])) && (a312.equals("e")))))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a35 -= (a35 - 20) < a35 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a395 = false; 
    	System.out.println("P");
    } 
}
private  void calculateOutputm175(String input) {
    if(((a249 <=  152 && (a243 <=  -179 && ((((a135.equals("e")) && (input.equals(inputs[8]) && ((a75.equals("h")) && ((a196 == 15) && cf)))) && (a206 == 4)) && a134))) && (a300 && (a307 == a227[0] && (47 == a265[5]))))) {
    	a120 += (a120 + 20) > a120 ? 1 : 0;
    	cf = false;
    	a211 = (a398 - 9);
    	a230 = (a196 + -10);
    	a378 = true;
    	a143 = "g";
    	a312 = "e";
    	a368 = true;
    	a265 = a376;
    	a270 = (a359 + 9);
    	a296 = a212;
    	a307 = a227[(a206 - 2)];
    	a398 = a270;
    	a320 = ((a338 / a230) - -8);
    	a218 = "g";
    	a361 = "g";
    	a240 = true;
    	a373 = false;
    	a260 = true;
    	a382 = ((((((((a382 * a249) % 14999) - 9691) % 107) - -69) * 5) % 107) + 69);
    	a57 = (a196 + -4);
    	a324 = a232[(a237 + -1)];
    	a173 = "e";
    	a201 = (((((((a333 * a333) % 14999) % 94) - 1) * 5) % 94) + 82);
    	a300 = true;
    	a329 = "f";
    	a75 = "g";
    	a234 = a372[(a320 + -6)];
    	a333 = (((((((a333 * a357) % 14999) + 13757) % 14) - -163) + -16092) - -16092); 
    	System.out.println("X");
    } 
    if(((((a218.equals("e")) && (a373 && a134)) && a378) && ((a382 <=  -65 && (a286 == a294[0] && ((a75.equals("h")) && (input.equals(inputs[4]) && ((a135.equals("e")) && (cf && (a196 == 15))))))) && a234 == a372[0]))) {
    	a122 -= (a122 - 20) < a122 ? 3 : 0;
    	cf = false;
    	a324 = a232[((a211 - a398) + 9)];
    	a57 = (a398 + 3);
    	a382 = (((((((a382 * a243) % 14999) / 5) - 17944) - -2791) % 107) + 121);
    	a307 = a227[(a57 + -11)];
    	a276 = a250;
    	a75 = "g";
    	a154 = (a359 - -12);
    	a338 = (a320 + -1);
    	a339 = false;
    	a300 = true;
    	a240 = true;
    	a398 = (a196 - 4);
    	a137 = a189;
    	a333 = (((((((a333 * a357) % 14999) * 2) * 1) + 1) % 14) + 163);
    	a218 = "g";
    	a239 = a268;
    	a228 = a292;
    	a359 = ((a211 * a237) - 4); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm176(String input) {
    if(((((a277 <=  -10 && input.equals(inputs[4])) && a395) && a300) && (((((a134 && (((a196 == 15) && cf) && (a75.equals("h")))) && (a135.equals("f"))) && (a207.equals("e"))) && a277 <=  -10) && (a224.equals("e"))))) {
    	cf = false;
    	a270 = (a359 - -8);
    	a338 = (a196 - 10);
    	a207 = "f";
    	a228 = a292;
    	a395 = false;
    	a75 = "g";
    	a300 = true;
    	a339 = false;
    	a137 = a189;
    	a239 = a268;
    	a382 = ((((((a382 * a357) % 14999) / 5) % 107) + 68) + 0);
    	a224 = "f";
    	a359 = ((a237 / a211) - -2);
    	a211 = (a320 + -3);
    	a333 = ((((((a333 * a277) % 14999) % 14) - -148) + 14) - 5);
    	a57 = (a320 - -7);
    	a154 = ((a320 + a57) + -4);
    	a276 = a250;
    	a240 = true;
    	a277 = ((((((a277 * a386) % 14999) % 78) + 68) + 0) - 0); 
    	System.out.println("X");
    } 
    if(((a382 <=  -65 && (a300 && (a134 && (a382 <=  -65 && ((a196 == 15) && ((36 == a239[5]) && (a211 == 1))))))) && ((a361.equals("e")) && ((a75.equals("h")) && ((cf && input.equals(inputs[8])) && (a135.equals("f"))))))) {
    	cf = false;
    	a201 = (((((((a382 * a249) % 14999) % 94) - -82) - 20151) / 5) - -4133);
    	a234 = a372[(a338 + -1)];
    	a395 = false;
    	a224 = "f";
    	a277 = ((((((a277 * a201) % 14999) % 78) + 68) + 0) + 0);
    	a320 = ((a270 * a196) - 142);
    	a358 = a335;
    	a240 = true;
    	a398 = (a270 + 2);
    	a329 = "f";
    	a368 = true;
    	a57 = ((a196 - a196) + 11);
    	a312 = "e";
    	a373 = false;
    	a230 = (a359 + 2);
    	a173 = "e";
    	a260 = true;
    	a143 = "g";
    	a361 = "g";
    	a265 = a376;
    	a333 = ((((((a333 * a357) % 14999) % 14) - -162) + -1) + 0);
    	a300 = true;
    	a207 = "f";
    	a382 = ((((((a382 * a357) % 14999) % 107) - -69) + 1) - 2);
    	a75 = "g";
    	a296 = a212;
    	a270 = ((a398 * a320) - 84); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm177(String input) {
    if((((((a237 == 3) && (a313.equals("e"))) && (81 == a296[3])) && a368) && (((a134 && (((a75.equals("h")) && (input.equals(inputs[5]) && (cf && (a196 == 15)))) && (a135.equals("g")))) && (a211 == 1)) && a235 == a216[0]))) {
    	a122 -= (a122 - 20) < a122 ? 2 : 0;
    	cf = false;
    	a361 = "f";
    	a276 = a250;
    	a235 = a216[(a211 + a211)];
    	a228 = a264;
    	a218 = "f";
    	a128 = (a196 - 3);
    	a240 = false;
    	a368 = false;
    	a296 = a362;
    	a312 = "f";
    	a358 = a348;
    	a339 = false;
    	a386 = (((((a382 * a382) % 14999) - -12706) + 140) - 33135);
    	a286 = a294[((a211 - a206) + 4)];
    	a265 = a293;
    	a333 = ((((((a333 * a310) % 14999) % 96) - -49) + 0) - 0);
    	a237 = ((a320 / a338) + 2);
    	a239 = a242;
    	a243 = (((((((a357 * a357) % 14999) + -23615) * 1) * 1) % 43) - 78);
    	a206 = (a320 - a211);
    	a378 = true;
    	a75 = "f";
    	a249 = ((((((a249 * a277) % 14999) % 101) + 254) - 1) - -1);
    	a359 = a230;
    	a383 = a226[a211];
    	a300 = true;
    	a269 = "g";
    	a382 = ((((((a382 * a201) % 14999) % 107) + -7) - 27) - -55);
    	a202 = a217[(a270 - 11)];
    	a225 = a205[(a206 + -4)];
    	a336 = false;
    	a302 = a346[(a398 - 10)];
    	a141 = a47[(a128 + -6)];
    	a173 = "i";
    	a338 = (a128 + -8);
    	a234 = a372[(a206 + -4)];
    	a373 = false;
    	a313 = "f";
    	a256 = "f";
    	a320 = ((a196 - a211) + -6);
    	a211 = (a196 + -12); 
    	System.out.println("X");
    } 
}
private  void calculateOutputm178(String input) {
    if((((a237 == 3) && (((24 == a370[2]) && a134) && (37 == a392[3]))) && ((a75.equals("h")) && ((a386 <=  77 && ((a312.equals("e")) && (a382 <=  -65 && ((cf && (a196 == 17)) && input.equals(inputs[8]))))) && (a91 == 13))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a94 += (a94 + 20) > a94 ? 2 : 0;
    	cf = false;
    	a75 = "g";
    	a81 = a167[(a398 - 5)];
    	a278 = a326[(a196 - a398)];
    	a57 = ((a196 + a91) - 14); 
    	System.out.println("T");
    } 
    if(((((input.equals(inputs[3]) && a324 == a232[0]) && (a361.equals("e"))) && (a329.equals("e"))) && (a333 <=  -47 && ((a134 && ((a196 == 17) && (((a75.equals("h")) && ((a91 == 13) && cf)) && a260))) && a383 == a226[0])))) {
    	a122 -= (a122 - 20) < a122 ? 2 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a35 += (a35 + 20) > a35 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	a386 = ((((a386 - -14105) / 5) / 5) - 4683);
    	a57 = (a196 + -4);
    	a300 = true;
    	a383 = a226[7];
    	a398 = 15;
    	a256 = "e";
    	a313 = "h";
    	a240 = false;
    	a228 = a229;
    	a207 = "h";
    	a361 = "e";
    	a260 = false;
    	a224 = "e";
    	a392 = a257;
    	a368 = false;
    	a243 = (((((a243 - 0) % 43) + -93) + -15994) - -16001);
    	a269 = "h";
    	a307 = a227[6];
    	a270 = 16;
    	a395 = false;
    	a237 = 9;
    	a338 = 4;
    	a239 = a242;
    	a230 = 3;
    	a359 = 10;
    	a154 = ((a91 / a57) + 15);
    	a75 = "g";
    	a296 = a212;
    	a225 = a205[3];
    	a353 = a241;
    	a286 = a294[0];
    	a265 = a293;
    	a370 = a318;
    	a277 = (((((a277 * 1) % 78) + 125) * 9) / 10);
    	a276 = a289;
    	a312 = "i";
    	a329 = "h";
    	a235 = a216[0];
    	a339 = true;
    	a206 = 11;
    	a382 = (((((a382 % 14967) + -65) / 5) * 10) / 2);
    	a373 = true;
    	a234 = a372[3];
    	a358 = a348;
    	a320 = 10;
    	a336 = false;
    	a211 = 6;
    	a333 = ((((((a333 % 96) + 109) * 9) / 10) - 10764) + 10740);
    	a324 = a232[6];
    	a378 = true;
    	a137 = a189; 
    	System.out.println("S");
    } 
    if((((a91 == 13) && ((cf && (a196 == 17)) && a134)) && ((81 == a296[3]) && ((a230 == 3) && (((a75.equals("h")) && (input.equals(inputs[0]) && (a307 == a227[0] && ((36 == a239[5]) && (a398 == 10))))) && a307 == a227[0]))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	cf = false;
    	a143 = "f";
    	a14 = a79[((a91 / a270) + 3)];
    	a75 = "g";
    	a57 = ((a196 / a91) - -10); 
    	System.out.println("T");
    } 
    if(((((cf && (a75.equals("h"))) && input.equals(inputs[4])) && (a230 == 3)) && ((a206 == 4) && ((67 == a358[2]) && (((a134 && (((a320 == 6) && a307 == a227[0]) && (a196 == 17))) && (a91 == 13)) && (a270 == 10)))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 3 : 0;
    	a94 -= (a94 - 20) < a94 ? 1 : 0;
    	cf = false;
    	a57 = (a359 + 12);
    	a72 = (((98 * 5) + -20794) - -35334);
    	a75 = "g";
    	a17 = (((((a72 * a72) % 14999) - 24989) * 1) + -3618); 
    	System.out.println("Z");
    } 
    if(((((((a91 == 13) && ((a75.equals("h")) && ((a196 == 17) && cf))) && a324 == a232[0]) && a134) && a324 == a232[0]) && ((input.equals(inputs[2]) && ((a256.equals("e")) && (a339 && a300))) && a307 == a227[0]))) {
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a98 += (a98 + 20) > a98 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 2 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	a218 = "g";
    	a64 = false;
    	a357 = (((((39 * 10) / -6) + 19389) + 6991) - 26383);
    	a75 = "i";
    	a146 = false;
    	a202 = a217[7];
    	a302 = a346[5];
    	a73 = (((18 - -495) * 5) + 17841); 
    	System.out.println("T");
    } 
    if((((a196 == 17) && (((a300 && (((98 == a276[2]) && (a75.equals("h"))) && (a230 == 3))) && a234 == a372[0]) && (a224.equals("e")))) && ((a312.equals("e")) && (((a91 == 13) && (cf && input.equals(inputs[6]))) && a134)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a63 -= (a63 - 20) < a63 ? 2 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	cf = false;
    	a373 = false;
    	a202 = a217[2];
    	a249 = ((((51 / 5) - -6594) / 5) + -1093);
    	a243 = (((((a243 % 14910) + -179) * 10) / 9) + -5392);
    	a276 = a250;
    	a265 = a376;
    	a359 = 8;
    	a329 = "i";
    	a260 = true;
    	a312 = "i";
    	a234 = a372[5];
    	a256 = "i";
    	a307 = a227[1];
    	a125 = a30[((a196 + a196) - 33)];
    	a206 = 11;
    	a173 = "f";
    	a339 = true;
    	a235 = a216[3];
    	a218 = "e";
    	a333 = ((((a333 % 14976) + -47) + -10585) * 1);
    	a277 = ((((a277 % 14830) - -15169) - 11521) + 12405);
    	a239 = a268;
    	a357 = (((21 - -19249) + 7832) - -316);
    	a370 = a318;
    	a300 = false;
    	a240 = true;
    	a395 = true;
    	a270 = 17;
    	a302 = a346[5];
    	a398 = 17;
    	a207 = "i";
    	a269 = "h";
    	a228 = a292;
    	a378 = false;
    	a320 = 9;
    	a296 = a212;
    	a211 = 7;
    	a392 = a304;
    	a336 = false;
    	a75 = "f";
    	a224 = "g";
    	a386 = (((((a386 * 1) % 14837) - -15162) + -24827) + 24829);
    	a368 = false;
    	a230 = 10;
    	a338 = 9;
    	a324 = a232[6];
    	a382 = ((((a382 % 14967) - 65) - 8294) / 5);
    	a358 = a348;
    	a237 = 5;
    	a383 = a226[5];
    	a353 = a399;
    	a361 = "h";
    	a8 = ((a196 - a196) + 11); 
    	System.out.println("R");
    } 
    if((((((a206 == 4) && (((((input.equals(inputs[9]) && cf) && (a196 == 17)) && (a75.equals("h"))) && a134) && a333 <=  -47)) && (a91 == 13)) && (a211 == 1)) && ((a336 && a368) && a386 <=  77))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	a31 += (a31 + 20) > a31 ? 3 : 0;
    	cf = false;
    	a296 = a362;
    	a313 = "f";
    	a202 = a217[5];
    	a333 = ((((a333 % 14976) + -47) + -366) + -1549);
    	a249 = (((((3 * 1639) / 10) * 5) + 2264) - 4247);
    	a310 = (((((40 * 10) / 1) - -8488) + -19772) + 36368);
    	a146 = true;
    	a218 = "i";
    	a302 = a346[4];
    	a383 = a226[0];
    	a286 = a294[1];
    	a201 = ((((92 * 5) / 5) * 10) / 5);
    	a357 = ((((81 * 5) * 5) / 5) + -488);
    	a361 = "h";
    	a378 = true;
    	a235 = a216[3];
    	a358 = a348;
    	a75 = "i";
    	a46 = a18;
    	a13 = (((39 + -26) - 22) / 5); 
    	System.out.println("R");
    } 
    if(((((a224.equals("e")) && (((a378 && (47 == a265[5])) && (a91 == 13)) && (a270 == 10))) && (a270 == 10)) && ((a368 && (((cf && a134) && (a75.equals("h"))) && (a196 == 17))) && input.equals(inputs[1])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a89 -= (a89 - 20) < a89 ? 2 : 0;
    	a169 -= (a169 - 20) < a169 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	cf = false;
    	if((a77 == 10)) {
    	a357 = ((((((36 * 10) / 18) * 10) / -9) * 5) + 122);
    	a286 = a294[5];
    	a218 = "f";
    	a75 = "i";
    	a235 = a216[3];
    	a378 = true;
    	a358 = a348;
    	a310 = ((((19 + 22603) / 5) * 10) / 9);
    	a201 = (((76 + 6943) * 4) * 1);
    	a313 = "f";
    	a46 = a18;
    	a383 = a226[0];
    	a333 = (((a333 * 1) / 5) + 28704);
    	a146 = true;
    	a361 = "e";
    	a296 = a212;
    	a302 = a346[4];
    	a249 = ((((95 - -119) * 5) - -21327) + -22179);
    	a202 = a217[7];
    	a13 = (((((36 * 9) / 10) * 10) / 9) * 5); 
    	}else {
    	a237 = 7;
    	a358 = a348;
    	a125 = a30[(a91 + -12)];
    	a353 = a263;
    	a361 = "f";
    	a386 = (((((a386 % 14837) - -15162) * 1) / 5) - -8851);
    	a202 = a217[2];
    	a211 = 3;
    	a324 = a232[7];
    	a249 = ((((75 + 7730) + -7472) - -4437) + -4440);
    	a378 = false;
    	a75 = "f";
    	a207 = "g";
    	a270 = 13;
    	a383 = a226[4];
    	a370 = a311;
    	a382 = (((((a382 + 0) % 14911) + 15087) - 15787) - -22318);
    	a218 = "i";
    	a269 = "e";
    	a265 = a376;
    	a357 = (((76 - 28812) + 28750) - 68);
    	a235 = a216[1];
    	a302 = a346[7];
    	a239 = a268;
    	a230 = 8;
    	a240 = true;
    	a338 = 5;
    	a392 = a304;
    	a329 = "f";
    	a398 = 16;
    	a395 = true;
    	a276 = a250;
    	a336 = false;
    	a312 = "e";
    	a243 = (((((a243 + 25914) % 14910) + -15088) + 24884) - 24885);
    	a339 = true;
    	a234 = a372[5];
    	a277 = ((((((a277 / 5) % 95) + 275) / 5) * 49) / 10);
    	a224 = "e";
    	a228 = a229;
    	a333 = ((((a333 % 14976) + -47) - 13392) - 1095);
    	a296 = a212;
    	a359 = 3;
    	a300 = false;
    	a373 = false;
    	a307 = a227[1];
    	a368 = false;
    	a173 = "f";
    	a320 = 8;
    	a206 = 7;
    	a260 = true;
    	a256 = "h";
    	a8 = (a196 - 6);
    	}System.out.println("V");
    } 
    if(((((((a207.equals("e")) && (a243 <=  -179 && (3 == a353[2]))) && (a91 == 13)) && input.equals(inputs[5])) && a277 <=  -10) && ((a75.equals("h")) && ((a269.equals("e")) && (((a134 && cf) && (a196 == 17)) && a383 == a226[0]))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	a94 -= (a94 - 20) < a94 ? 1 : 0;
    	cf = false;
    	if(((a300 && (38 == a370[4])) && !(a1 == 5))) {
    	a307 = a227[6];
    	a277 = ((((a277 / 5) / 5) - 7289) + 12842);
    	a57 = ((a359 - a320) - -20);
    	a276 = a289;
    	a386 = ((((a386 % 15038) - 14960) - 2) + 0);
    	a265 = a293;
    	a378 = false;
    	a358 = a348;
    	a336 = false;
    	a269 = "h";
    	a211 = 2;
    	a239 = a242;
    	a382 = ((((a382 % 12) - 40) - 17653) + 17649);
    	a40 = a83;
    	a75 = "g";
    	a333 = ((((a333 % 96) - -49) * 5) / 5);
    	a353 = a263;
    	a320 = 6;
    	a240 = false;
    	a25 = a76; 
    	}else {
    	a378 = true;
    	a249 = (((19 + -28726) / 5) / 5);
    	a382 = ((((a382 % 14967) + -65) * 1) - 6574);
    	a230 = 7;
    	a218 = "i";
    	a373 = true;
    	a240 = true;
    	a81 = a167[((a338 * a359) + -2)];
    	a353 = a399;
    	a370 = a318;
    	a395 = true;
    	a300 = true;
    	a398 = 17;
    	a125 = a30[(a206 - -1)];
    	a239 = a268;
    	a383 = a226[3];
    	a206 = 8;
    	a269 = "h";
    	a339 = true;
    	a386 = ((((a386 % 15038) - 14960) - 2) * 1);
    	a256 = "g";
    	a392 = a208;
    	a357 = ((((28 + 237) + -23178) * 10) / -9);
    	a302 = a346[2];
    	a276 = a250;
    	a173 = "f";
    	a207 = "i";
    	a277 = ((((a277 / 5) + -5703) * 10) / -9);
    	a260 = true;
    	a312 = "h";
    	a243 = ((((((a243 % 14910) + -179) / 5) - -21439) * -1) / 10);
    	a336 = true;
    	a333 = (((((a333 % 14976) - 47) + 29003) + 284) - 41776);
    	a320 = 6;
    	a329 = "i";
    	a211 = 6;
    	a75 = "f";
    	a202 = a217[6];
    	a307 = a227[5];
    	a324 = a232[4];
    	a368 = true;
    	a270 = 15;
    	a237 = 5;
    	a265 = a376;
    	a296 = a362;
    	a338 = 6;
    	a228 = a292;
    	a359 = 7;
    	}System.out.println("R");
    } 
    if((((a196 == 17) && (((a134 && ((a91 == 13) && cf)) && input.equals(inputs[7])) && (a75.equals("h")))) && (((37 == a392[3]) && (a336 && ((81 == a296[3]) && (a300 && a260)))) && a324 == a232[0]))) {
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a174 += (a174 + 20) > a174 ? 4 : 0;
    	a133 += (a133 + 20) > a133 ? 3 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	cf = false;
    	if(a102 == a127[1]) {
    	a40 = a83;
    	a277 = (((((a277 * 1) % 14995) - 10) * 10) / 9);
    	a333 = ((((a333 - 0) % 14976) + -47) - 351);
    	a265 = a293;
    	a386 = (((((a386 % 14837) + 15162) / 5) * 5) - -5);
    	a75 = "g";
    	a240 = false;
    	a307 = a227[5];
    	a211 = 2;
    	a269 = "h";
    	a382 = ((((a382 % 12) + -49) + -23605) + 23609);
    	a57 = ((a91 - a320) - -10);
    	a358 = a351;
    	a320 = 13;
    	a276 = a289;
    	a336 = false;
    	a353 = a241;
    	a239 = a299;
    	a378 = false;
    	a25 = a76; 
    	}else {
    	a173 = "f";
    	a210 = true;
    	a75 = "f";
    	a125 = a30[(a91 + -11)];
    	}System.out.println("S");
    } 
}
private  void calculateOutputm16(String input) {
    if((((((3 == a353[2]) && (a230 == 3)) && a386 <=  77) && (a398 == 10)) && (a373 && ((a206 == 4) && (cf && (a196 == 10)))))) {
    	if((((a277 <=  -10 && ((a129 == a92[2] && cf) && a225 == a205[0])) && a368) && ((a312.equals("e")) && ((a269.equals("e")) && a260)))) {
    		calculateOutputm171(input);
    	} 
    } 
    if((((((67 == a358[2]) && ((cf && (a196 == 12)) && a324 == a232[0])) && (47 == a265[5])) && (a338 == 3)) && (a243 <=  -179 && (a211 == 1)))) {
    	if(((a300 && ((a256.equals("e")) && (a359 == 3))) && ((a237 == 3) && (a235 == a216[0] && ((((241 < a54) && (402 >= a54)) && cf) && a310 <=  160))))) {
    		calculateOutputm174(input);
    	} 
    } 
    if(((a249 <=  152 && ((a359 == 3) && a382 <=  -65)) && (((a225 == a205[0] && ((a196 == 15) && cf)) && (a361.equals("e"))) && (47 == a265[5])))) {
    	if(((((36 == a239[5]) && ((a324 == a232[0] && (a361.equals("e"))) && a307 == a227[0])) && a243 <=  -179) && (a225 == a205[0] && (cf && (a135.equals("e")))))) {
    		calculateOutputm175(input);
    	} 
    	if((((a270 == 10) && ((a135.equals("f")) && cf)) && (((((67 == a358[2]) && a300) && (a211 == 1)) && a333 <=  -47) && (a224.equals("e"))))) {
    		calculateOutputm176(input);
    	} 
    	if((((a206 == 4) && (((a218.equals("e")) && (a256.equals("e"))) && a234 == a372[0])) && (((a312.equals("e")) && (cf && (a135.equals("g")))) && a339))) {
    		calculateOutputm177(input);
    	} 
    } 
    if((((((a207.equals("e")) && ((a270 == 10) && (cf && (a196 == 17)))) && (47 == a265[5])) && (a338 == 3)) && ((a269.equals("e")) && (a230 == 3)))) {
    	if((((a277 <=  -10 && (36 == a239[5])) && (a361.equals("e"))) && ((81 == a296[3]) && ((((a91 == 13) && cf) && (a269.equals("e"))) && a307 == a227[0])))) {
    		calculateOutputm178(input);
    	} 
    } 
}
private  void calculateOutputm181(String input) {
    if((((3 == a353[2]) && ((a338 == 3) && ((!a134 && a378) && a225 == a205[0]))) && (((((((a75.equals("h")) && cf) && a295 == a366[0]) && input.equals(inputs[8])) && (a338 == 3)) && (a196 == 16)) && (37 == a392[3])))) {
    	a178 += (a178 + 20) > a178 ? 3 : 0;
    	cf = false;
    	a265 = a376;
    	a57 = (a196 + -1);
    	a75 = "g";
    	a392 = a304;
    	a218 = "g";
    	a336 = true;
    	a211 = (a196 + -13);
    	a378 = true;
    	a72 = (((29 - 4377) * 5) / 5);
    	a353 = a399;
    	a269 = "g";
    	a141 = a47[((a320 - a57) + 12)];
    	a320 = (a237 + 3);
    	a338 = (a57 + -10);
    	a333 = ((((((((a333 * a201) % 14999) % 14) + 159) * 5) + 19183) % 14) + 151);
    	a276 = a250;
    	a228 = a292;
    	a201 = ((((((a201 * a357) % 14999) + -2451) - -7938) % 94) + 83); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm182(String input) {
    if((((a196 == 17) && ((((a75.equals("h")) && (cf && input.equals(inputs[4]))) && a368) && (a207.equals("e")))) && (!a134 && (((a295 == a366[0] && (a234 == a372[0] && (a361.equals("e")))) && a395) && (37 == a392[3]))))) {
    	a168 += (a168 + 20) > a168 ? 2 : 0;
    	cf = false;
    	a339 = false;
    	a225 = a205[(a398 - 10)];
    	a57 = ((a320 + a359) + 4);
    	a224 = "f";
    	a154 = (a230 + 8);
    	a395 = false;
    	a310 = (((((((a310 * a357) % 14999) % 20) - -336) * 5) % 20) + 320);
    	a202 = a217[(a237 + -3)];
    	a329 = "g";
    	a312 = "g";
    	a358 = a335;
    	a336 = true;
    	a368 = true;
    	a228 = a292;
    	a392 = a304;
    	a269 = "g";
    	a302 = a346[(a320 - 4)];
    	a333 = (((((((a333 * a201) % 14999) % 14) + 150) / 5) - -15003) - 14871);
    	a359 = a237;
    	a201 = (((((((a382 * a277) % 14999) % 93) + -104) / 5) * 5) + -2);
    	a382 = (((((a357 * a357) / 5) - -3728) % 107) - -28);
    	a218 = "g";
    	a338 = a237;
    	a207 = "f";
    	a75 = "g";
    	a137 = a189;
    	a276 = a250;
    	a353 = a399;
    	a320 = (a154 - 3); 
    	System.out.println("Z");
    } 
    if(((((input.equals(inputs[2]) && a382 <=  -65) && a310 <=  160) && (a224.equals("e"))) && (((a302 == a346[0] && (!a134 && ((a196 == 17) && (a295 == a366[0] && (cf && (a75.equals("h"))))))) && a225 == a205[0]) && (a320 == 6)))) {
    	a178 += (a178 + 20) > a178 ? 4 : 0;
    	cf = false;
    	a392 = a257;
    	a324 = a232[((a230 / a206) + 1)];
    	a357 = ((((57 * -31) / 10) / 5) + -105);
    	a338 = ((a230 * a230) + -5);
    	a382 = ((((((a382 * a333) % 14999) % 12) - 54) + -1) + 1);
    	a201 = (((((((a201 * a310) % 14999) * 2) % 93) + -104) / 5) + -22);
    	a237 = ((a230 + a338) + -2);
    	a383 = a226[((a206 * a320) + -29)];
    	a300 = false;
    	a207 = "f";
    	a336 = false;
    	a398 = a270;
    	a368 = false;
    	a370 = a285;
    	a225 = a205[(a270 + -10)];
    	a276 = a289;
    	a395 = false;
    	a265 = a293;
    	a211 = (a270 - 9);
    	a218 = "f";
    	a310 = (((((((a357 * a201) % 14999) % 77) - -209) * 5) % 77) - -233);
    	a359 = (a398 - 7);
    	a75 = "f";
    	a373 = false;
    	a296 = a362;
    	a361 = "f";
    	a260 = true;
    	a302 = a346[(a398 + -9)];
    	a240 = false;
    	a312 = "g";
    	a277 = (((86 - 3502) + -9019) + -15718);
    	a307 = a227[(a338 + -4)];
    	a320 = (a196 - 10);
    	a256 = "g";
    	a329 = "f";
    	a234 = a372[(a338 + -3)];
    	a243 = ((((((a357 * a382) / 5) * 5) - 22779) % 43) + -68);
    	a224 = "f";
    	a125 = a30[(a230 - -1)];
    	a202 = a217[(a230 + -2)];
    	a239 = a242;
    	a173 = "f";
    	a333 = ((((((a382 * a357) % 14) + 158) * 5) % 14) + 155);
    	a286 = a294[((a206 / a270) + 1)];
    	a210 = false;
    	a235 = a216[((a359 - a359) - -2)];
    	a339 = false;
    	a230 = (a206 - 1); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm183(String input) {
    if((((a171.equals("f")) && ((a295 == a366[2] && ((a302 == a346[0] && ((((a75.equals("h")) && cf) && input.equals(inputs[8])) && (67 == a358[2]))) && (37 == a392[3]))) && !a134)) && (((36 == a239[5]) && (a312.equals("e"))) && a235 == a216[0]))) {
    	a51 += (a51 + 20) > a51 ? 1 : 0;
    	cf = false;
    	a324 = a232[((a237 + a359) + -6)];
    	a336 = true;
    	a218 = "g";
    	a75 = "g";
    	a269 = "g";
    	a392 = a304;
    	a72 = (((89 + 9037) + -30697) + -4469);
    	a141 = a47[((a320 + a270) - 13)];
    	a228 = a292;
    	a235 = a216[(a270 + -8)];
    	a57 = 15;
    	a265 = a376;
    	a302 = a346[(a359 + -1)];
    	a320 = ((a206 + a338) + -3);
    	a207 = "g";
    	a333 = (((((((a333 * a382) % 14999) + 1002) % 14) + 162) / 5) + 133);
    	a201 = ((((((a201 * a386) % 14999) - -4267) * 1) % 94) - -83);
    	a353 = a399;
    	a276 = a250;
    	a270 = ((a230 / a206) - -12); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm184(String input) {
    if((((((input.equals(inputs[6]) && a324 == a232[0]) && (a230 == 3)) && (a75.equals("h"))) && a333 <=  -47) && ((36 == a239[5]) && (((((cf && !a134) && a295 == a366[2]) && (a171.equals("i"))) && a243 <=  -179) && (a206 == 4))))) {
    	a174 += (a174 + 20) > a174 ? 1 : 0;
    	cf = false;
    	a218 = "g";
    	a224 = "f";
    	a353 = a241;
    	a243 = (((((((a243 * a310) % 14999) / 5) % 11) + -167) + 10575) - 10574);
    	a75 = "f";
    	a269 = "f";
    	a173 = "f";
    	a324 = a232[((a320 * a237) - 17)];
    	a320 = (a359 + 4);
    	a8 = (a338 - -7);
    	a339 = false;
    	a125 = a30[(a237 - 2)];
    	a228 = a264;
    	a237 = (a338 + 1);
    	a276 = a289;
    	a239 = a268;
    	a230 = (a338 + 1);
    	a207 = "g";
    	a378 = false;
    	a206 = (a359 - -2);
    	a358 = a351;
    	a359 = a230;
    	a338 = (a270 - 7); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm189(String input) {
    if(((((a333 <=  -47 && !a134) && a260) && a201 <=  -199) && (((((a295 == a366[4] && (((a75.equals("h")) && cf) && input.equals(inputs[9]))) && (a338 == 3)) && a184 == a157[7]) && a383 == a226[0]) && (a313.equals("e"))))) {
    	a31 += (a31 + 20) > a31 ? 2 : 0;
    	cf = false;
    	a338 = ((a230 * a270) + -40);
    	a276 = a289;
    	a269 = "g";
    	a333 = (((((((a333 * a277) % 14999) * 2) - 0) * 1) % 14) - -161);
    	a206 = ((a230 * a320) + -19);
    	a339 = false;
    	a129 = a92[((a237 / a359) + 6)];
    	a336 = false;
    	a173 = "f";
    	a383 = a226[((a206 - a338) - -1)];
    	a359 = a338;
    	a125 = a30[(a398 + -3)];
    	a202 = a217[(a398 - 8)];
    	a310 = ((((((a310 * a382) % 14999) - -440) / 5) % 77) + 239);
    	a357 = ((((((((a357 * a201) % 14999) / 5) % 65) - 168) * 5) % 65) + -70);
    	a75 = "f";
    	a296 = a362;
    	a302 = a346[(a270 - 9)];
    	a368 = true;
    	a228 = a292;
    	a224 = "f";
    	a358 = a335;
    	a370 = a285;
    	a320 = (a338 - -3);
    	a260 = false;
    	a312 = "f";
    	a313 = "f";
    	a201 = ((((((a201 * a277) % 14999) * 2) % 93) + -105) - 1);
    	a398 = (a237 + 9);
    	a218 = "g";
    	a240 = false;
    	a237 = ((a359 - a338) + 4); 
    	System.out.println("Y");
    } 
    if(((((a240 && (a295 == a366[4] && (((((a224.equals("e")) && a260) && (24 == a370[2])) && a357 <=  -188) && a249 <=  152))) && (a75.equals("h"))) && !a134) && (input.equals(inputs[1]) && (a184 == a157[7] && cf)))) {
    	a162 -= (a162 - 20) < a162 ? 1 : 0;
    	cf = false;
    	a338 = (a320 + -2);
    	a172 = ((a206 * a237) - 8);
    	a240 = false;
    	a373 = false;
    	a173 = "g";
    	a312 = "f";
    	a224 = "f";
    	a370 = a285;
    	a310 = (((((((a310 * a333) % 14999) * 2) % 77) - -238) + 4483) + -4483);
    	a357 = ((((((a357 * a249) % 14999) * 2) % 65) - 121) - 1);
    	a218 = "f";
    	a274 = a290;
    	a75 = "f";
    	a313 = "f";
    	a260 = false;
    	a320 = (a270 + -4); 
    	System.out.println("T");
    } 
}
private  void calculateOutputm193(String input) {
    if((((a201 <=  -199 && ((((((cf && input.equals(inputs[9])) && a295 == a366[7]) && (a75.equals("h"))) && (36 == a239[5])) && a210) && (a329.equals("e")))) && a225 == a205[0]) && (a201 <=  -199 && ((a230 == 3) && !a134)))) {
    	cf = false;
    	a339 = false;
    	a77 = (a206 + 6);
    	a338 = ((a359 / a237) - -4);
    	a1 = a87[(a398 - 4)];
    	a312 = "g";
    	a329 = "g";
    	a336 = true;
    	a386 = ((((((a386 * a382) % 14999) % 61) + 264) + -529) + 527);
    	a224 = "f";
    	a382 = ((((((a382 * a277) % 14999) % 107) - 35) - 2) / 5);
    	a132 = false;
    	a368 = true;
    	a75 = "e";
    	a398 = (a206 - -7);
    	a270 = (a77 + 1); 
    	System.out.println("S");
    } 
}
private  void calculateOutputm17(String input) {
    if(((a201 <=  -199 && ((((cf && a295 == a366[0]) && (3 == a353[2])) && (a359 == 3)) && (a329.equals("e")))) && ((a338 == 3) && (47 == a265[5])))) {
    	if(((((a338 == 3) && ((cf && (a196 == 16)) && (47 == a265[5]))) && (a312.equals("e"))) && (((a211 == 1) && (36 == a239[5])) && (a224.equals("e"))))) {
    		calculateOutputm181(input);
    	} 
    	if((((a312.equals("e")) && (cf && (a196 == 17))) && (a202 == a217[0] && ((24 == a370[2]) && (a373 && ((37 == a392[3]) && (a338 == 3))))))) {
    		calculateOutputm182(input);
    	} 
    } 
    if((((((45 == a228[1]) && ((a359 == 3) && a339)) && (a207.equals("e"))) && (3 == a353[2])) && (a324 == a232[0] && (cf && a295 == a366[2])))) {
    	if(((a302 == a346[0] && (((a359 == 3) && a201 <=  -199) && (47 == a265[5]))) && (((a359 == 3) && ((a171.equals("f")) && cf)) && a386 <=  77))) {
    		calculateOutputm183(input);
    	} 
    	if((((a211 == 1) && (a206 == 4)) && (a324 == a232[0] && ((a338 == 3) && ((a237 == 3) && (a378 && ((a171.equals("i")) && cf))))))) {
    		calculateOutputm184(input);
    	} 
    } 
    if(((a310 <=  160 && ((((81 == a296[3]) && a373) && a202 == a217[0]) && a201 <=  -199)) && ((a218.equals("e")) && (a295 == a366[4] && cf)))) {
    	if((((a312.equals("e")) && (a201 <=  -199 && ((a269.equals("e")) && ((24 == a370[2]) && ((81 == a296[3]) && (a224.equals("e"))))))) && (a184 == a157[7] && cf))) {
    		calculateOutputm189(input);
    	} 
    } 
    if((((a269.equals("e")) && ((cf && a295 == a366[7]) && a368)) && (((a277 <=  -10 && (a270 == 10)) && a386 <=  77) && a382 <=  -65))) {
    	if(((((a312.equals("e")) && (a333 <=  -47 && (a338 == 3))) && (a313.equals("e"))) && (((cf && a210) && (a338 == 3)) && (a230 == 3)))) {
    		calculateOutputm193(input);
    	} 
    } 
}
private  void calculateOutputm195(String input) {
    if(((((a312.equals("e")) && (a211 == 1)) && (45 == a228[1])) && (((input.equals(inputs[6]) && ((a237 == 3) && ((((27 == a46[0]) && (a151 && cf)) && (a218.equals("e"))) && a146))) && (a75.equals("i"))) && a260))) {
    	a31 += (a31 + 20) > a31 ? 1 : 0;
    	cf = false;
    	if(a278 == a326[7]) {
    	a333 = (((((((a333 * a310) % 14999) / 5) + 17186) - 6917) * -1) / 10);
    	a383 = a226[(a398 + -10)];
    	a277 = ((((((a201 * a386) % 14999) - -17701) % 78) - 8) + 21);
    	a75 = "f";
    	a234 = a372[((a237 + a211) + -4)];
    	a300 = true;
    	a324 = a232[a211];
    	a141 = a47[(a230 - 2)];
    	a312 = "e";
    	a230 = (a211 + 2);
    	a313 = "f";
    	a173 = "i";
    	a395 = false;
    	a310 = (((((((a310 * a277) % 14999) * 2) % 77) + 237) + -16437) - -16439);
    	a218 = "e";
    	a398 = ((a338 * a211) - -7);
    	a239 = a242;
    	a237 = a338;
    	a42 = (a359 - -5);
    	a270 = ((a211 - a320) - -17);
    	a373 = true;
    	a307 = a227[(a359 + -2)];
    	a228 = a229;
    	a353 = a263;
    	a260 = false;
    	a225 = a205[((a211 - a206) + 4)];
    	a359 = ((a338 * a338) + -6);
    	a338 = ((a211 * a211) + 2);
    	a211 = 1; 
    	}else {

    	}System.out.println("S");
    } 
}
private  void calculateOutputm196(String input) {
    if(((a249 <=  152 && a202 == a217[0]) && (((-10 < a13) && (203 >= a13)) && (input.equals(inputs[7]) && ((a75.equals("i")) && ((a357 <=  -188 && ((a300 && (a146 && ((35 == a46[2]) && cf))) && (a256.equals("e")))) && (3 == a353[2]))))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a63 -= (a63 - 20) < a63 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 2 : 0;
    	a98 -= (a98 - 20) < a98 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	a146 = false;
    	a16 = false;
    	a73 = ((((((a13 * a13) % 14999) % 54) + 223) * 1) * 1); 
    	System.out.println("R");
    } 
    if((((a398 == 10) && ((a256.equals("e")) && ((a307 == a227[0] && (a373 && (24 == a370[2]))) && (a206 == 4)))) && ((a75.equals("i")) && ((((35 == a46[2]) && (cf && a146)) && input.equals(inputs[1])) && ((-10 < a13) && (203 >= a13)))))) {
    	a165 -= (a165 - 20) < a165 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a141 = a47[2];
    	a173 = "i";
    	a75 = "f";
    	a244 = a363[0]; 
    	System.out.println("T");
    } 
    if((((((-10 < a13) && (203 >= a13)) && ((a312.equals("e")) && ((((cf && (a75.equals("i"))) && (35 == a46[2])) && (a256.equals("e"))) && a357 <=  -188))) && input.equals(inputs[0])) && ((a207.equals("e")) && (((37 == a392[3]) && (a329.equals("e"))) && a146)))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a162 -= (a162 - 20) < a162 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	cf = false;
    	if((a184 == 15)) {
    	a7 = false;
    	a75 = "f";
    	a173 = "g";
    	a172 = ((a270 / a359) - -3); 
    	}else {
    	a201 = (((a201 + 30185) + -24912) + 24919);
    	a218 = "e";
    	a329 = "i";
    	a338 = 7;
    	a386 = ((((a386 % 14837) + 15162) + 1) + 0);
    	a324 = a232[3];
    	a206 = 4;
    	a269 = "e";
    	a353 = a263;
    	a237 = 6;
    	a235 = a216[2];
    	a211 = 8;
    	a300 = true;
    	a277 = (((((a277 - 0) / 5) * 5) % 95) - -317);
    	a75 = "f";
    	a173 = "g";
    	a310 = (((a310 - 0) / 5) - 10225);
    	a307 = a227[6];
    	a249 = ((((a249 % 71) + 428) + 1) + -2);
    	a313 = "h";
    	a358 = a335;
    	a383 = a226[0];
    	a234 = a372[5];
    	a357 = (((((a357 / 5) / 5) * 5) % 38) - 1);
    	a265 = a376;
    	a228 = a229;
    	a395 = true;
    	a105 = "e";
    	a240 = true;
    	a286 = a294[6];
    	a333 = (((((43 * 9) / 10) - -63) * 10) / 9);
    	a270 = 13;
    	a202 = a217[4];
    	a207 = "h";
    	a256 = "e";
    	a224 = "i";
    	a392 = a304;
    	a320 = 10;
    	a336 = true;
    	a368 = true;
    	a339 = true;
    	a239 = a268;
    	a260 = true;
    	a373 = true;
    	a172 = (a359 - -2);
    	a370 = a318;
    	a276 = a253;
    	a398 = 14;
    	a382 = (((a382 / 5) + 16603) - -326);
    	a359 = 5;
    	}System.out.println("R");
    } 
    if((((a286 == a294[0] && (input.equals(inputs[3]) && (((45 == a228[1]) && a395) && (a75.equals("i"))))) && (a313.equals("e"))) && ((a339 && (a201 <=  -199 && (a146 && (cf && ((-10 < a13) && (203 >= a13)))))) && (35 == a46[2])))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 1 : 0;
    	a169 -= (a169 - 20) < a169 ? 3 : 0;
    	a133 += (a133 + 20) > a133 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	cf = false;
    	if((a237 == 5)) {
    	a240 = true;
    	a75 = "f";
    	a235 = a216[7];
    	a373 = false;
    	a392 = a304;
    	a336 = false;
    	a312 = "i";
    	a302 = a346[3];
    	a361 = "f";
    	a207 = "e";
    	a249 = ((((a249 % 14750) + 15249) / 5) * 5);
    	a201 = (((a201 / 5) + -18210) + -4393);
    	a324 = a232[7];
    	a230 = 5;
    	a202 = a217[4];
    	a228 = a292;
    	a310 = ((((a310 * 1) % 15080) - 14919) + -2);
    	a398 = 14;
    	a386 = ((((a386 % 15038) + -14960) - -16844) + -16845);
    	a243 = (((a243 + 0) - -30098) * 1);
    	a269 = "h";
    	a378 = false;
    	a211 = 4;
    	a353 = a399;
    	a218 = "f";
    	a234 = a372[4];
    	a370 = a318;
    	a125 = a30[(a206 + -3)];
    	a224 = "h";
    	a270 = 13;
    	a382 = ((((a382 + 0) - 0) % 14911) - -15087);
    	a265 = a376;
    	a359 = 7;
    	a206 = 7;
    	a339 = true;
    	a276 = a250;
    	a320 = 9;
    	a307 = a227[4];
    	a395 = true;
    	a357 = (((((a357 * 1) % 38) + 1) + 11834) + -11841);
    	a256 = "f";
    	a333 = (((54 - -11340) + -11265) - 150);
    	a277 = ((((a277 + 10399) / 5) % 95) - -243);
    	a313 = "i";
    	a260 = true;
    	a237 = 6;
    	a239 = a242;
    	a300 = false;
    	a338 = 5;
    	a368 = false;
    	a329 = "i";
    	a173 = "f";
    	a286 = a294[3];
    	a8 = 11; 
    	}else {
    	a125 = a30[(a398 - 5)];
    	a75 = "f";
    	a173 = "f";
    	a81 = a167[(a237 - -2)];
    	}System.out.println("R");
    } 
    if(((a146 && ((a313.equals("e")) && ((a75.equals("i")) && ((((-10 < a13) && (203 >= a13)) && cf) && (35 == a46[2]))))) && ((a373 && (((a382 <=  -65 && a234 == a372[0]) && input.equals(inputs[4])) && (36 == a239[5]))) && a357 <=  -188))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a122 -= (a122 - 20) < a122 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a89 -= (a89 - 20) < a89 ? 1 : 0;
    	a12 -= (a12 - 20) < a12 ? 2 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a98 -= (a98 - 20) < a98 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 -= (a90 - 20) < a90 ? 3 : 0;
    	a181 += (a181 + 20) > a181 ? 3 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a395 = true;
    	a357 = (((((a357 + 0) + 0) - -22883) % 38) + -18);
    	a239 = a268;
    	a320 = 6;
    	a382 = ((((a382 % 14911) - -15087) - -4406) * 1);
    	a313 = "e";
    	a201 = ((((a201 % 94) + 117) / 5) - 9);
    	a370 = a318;
    	a173 = "h";
    	a218 = "i";
    	a307 = a227[6];
    	a392 = a304;
    	a310 = ((((a310 % 15080) - 14919) * 1) + -1);
    	a269 = "h";
    	a240 = true;
    	a211 = 3;
    	a270 = 16;
    	a1 = a87[((a398 + a206) - 11)];
    	a353 = a399;
    	a75 = "f";
    	a91 = (a237 - -8); 
    	System.out.println("R");
    } 
    if((((a146 && (a300 && (a339 && (((98 == a276[2]) && a201 <=  -199) && ((-10 < a13) && (203 >= a13)))))) && a336) && (input.equals(inputs[9]) && (((35 == a46[2]) && ((a75.equals("i")) && cf)) && a368)))) {
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a89 += (a89 + 20) > a89 ? 3 : 0;
    	a174 += (a174 + 20) > a174 ? 3 : 0;
    	a133 -= (a133 - 20) < a133 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a156 += (a156 + 20) > a156 ? 1 : 0;
    	a93 += (a93 + 20) > a93 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 1 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	if(((!(a256.equals("e")) && (a134 || (!cf || a383 == a226[3]))) || a184 == a157[1])) {
    	a324 = a232[1];
    	a361 = "f";
    	a240 = false;
    	a249 = (((((a249 % 14750) + 15249) / 5) / 5) - -6639);
    	a224 = "e";
    	a228 = a229;
    	a296 = a384;
    	a170 = (((((a277 * a386) % 14999) - 14986) - 11) - 2);
    	a378 = false;
    	a329 = "i";
    	a395 = false;
    	a81 = a167[(a359 - -2)];
    	a373 = true;
    	a383 = a226[1];
    	a312 = "e";
    	a398 = 10;
    	a300 = false;
    	a307 = a227[7];
    	a239 = a299;
    	a206 = 4;
    	a368 = false;
    	a260 = false;
    	a230 = 6;
    	a302 = a346[5];
    	a338 = 10;
    	a339 = true;
    	a313 = "e";
    	a201 = ((((a201 + 8107) % 14900) - 15098) - 0);
    	a211 = 2;
    	a270 = 16;
    	a310 = (((((a310 / 5) / 5) * 5) % 20) - -337);
    	a265 = a293;
    	a256 = "f";
    	a286 = a294[0];
    	a235 = a216[1];
    	a234 = a372[6];
    	a57 = (a320 + 6);
    	a359 = 3;
    	a75 = "g";
    	a207 = "h";
    	a370 = a285;
    	a386 = (((((a386 * 1) + 12060) / 5) % 61) + 140);
    	a358 = a348;
    	a382 = ((((a382 / 5) % 12) - 43) * 1);
    	a218 = "h";
    	a353 = a241;
    	a202 = a217[1];
    	a320 = 13;
    	a243 = (((a243 / 5) + -7119) + -14147);
    	a357 = (((a357 + 30069) + 49) - -31);
    	a392 = a257;
    	a276 = a289;
    	a225 = a205[5];
    	a336 = false;
    	a237 = 4;
    	a277 = ((((a277 % 14995) - 10) + 2365) - 12751); 
    	}else {
    	a125 = a30[3];
    	a75 = "f";
    	a173 = "f";
    	a397 = 11;
    	}System.out.println("V");
    } 
    if((((35 == a46[2]) && ((a307 == a227[0] && (((a146 && cf) && ((-10 < a13) && (203 >= a13))) && input.equals(inputs[5]))) && (a359 == 3))) && ((a234 == a372[0] && (((a269.equals("e")) && (a207.equals("e"))) && (a75.equals("i")))) && a243 <=  -179))) {
    	a165 -= (a165 - 20) < a165 ? 3 : 0;
    	a63 -= (a63 - 20) < a63 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 2 : 0;
    	a50 -= (a50 - 20) < a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a181 -= (a181 - 20) < a181 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	if(((((a66 || (11 == a353[4])) || !(15 == a353[2])) || a191 == a37[3]) && !(a105.equals("g")))) {
    	a329 = "g";
    	a132 = false;
    	a296 = a384;
    	a240 = false;
    	a333 = ((((95 + 73) * 5) + 4217) - 4889);
    	a383 = a226[2];
    	a378 = false;
    	a324 = a232[6];
    	a16 = true;
    	a225 = a205[2];
    	a75 = "e";
    	a300 = true;
    	a361 = "i";
    	a265 = a376;
    	a358 = a351;
    	a302 = a346[1];
    	a277 = ((((a277 * 1) % 14995) + -10) - 520);
    	a235 = a216[4];
    	a382 = ((((a382 % 14911) + 15087) * 1) - -2587);
    	a77 = ((a230 + a338) + 3); 
    	}else {
    	a249 = ((((a249 % 71) - -428) / 5) - -321);
    	a256 = "e";
    	a237 = 9;
    	a383 = a226[5];
    	a395 = false;
    	a277 = ((((((a277 % 14995) + -10) * 1) - -18307) * -1) / 10);
    	a240 = true;
    	a276 = a250;
    	a392 = a208;
    	a265 = a303;
    	a206 = 6;
    	a373 = true;
    	a398 = 12;
    	a359 = 10;
    	a224 = "g";
    	a338 = 8;
    	a269 = "h";
    	a353 = a399;
    	a307 = a227[0];
    	a357 = ((((a357 * 1) * -2) / 10) + 12646);
    	a339 = true;
    	a368 = true;
    	a370 = a318;
    	a130 = (((((a13 * a13) % 14999) / 5) + 12580) - -5087);
    	a218 = "g";
    	a382 = (((((a382 - 0) - 0) - 0) % 14911) + 15087);
    	a329 = "e";
    	a202 = a217[6];
    	a270 = 10;
    	a75 = "f";
    	a313 = "g";
    	a286 = a294[3];
    	a235 = a216[4];
    	a260 = true;
    	a13 = ((((((a13 * a386) % 14999) / 5) % 60) - -263) - 0);
    	a234 = a372[2];
    	a333 = (((10 + 3337) - -8545) + -4421);
    	a300 = true;
    	a228 = a292;
    	a225 = a205[6];
    	a302 = a346[0];
    	a230 = 5;
    	a211 = 7;
    	a201 = ((((a201 - 0) - 0) % 14900) - 199);
    	a324 = a232[3];
    	a310 = ((((a310 % 20) - -336) - -1) - 1);
    	a312 = "h";
    	a239 = a242;
    	a173 = "e";
    	a243 = ((((a243 * 1) / 5) * 5) + 30170);
    	a207 = "h";
    	a386 = (((((a386 * 1) % 14837) + 15162) - 4150) - -4151);
    	}System.out.println("V");
    } 
    if(((a146 && ((37 == a392[3]) && (((-10 < a13) && (203 >= a13)) && (a336 && ((a270 == 10) && (24 == a370[2])))))) && (a395 && ((a75.equals("i")) && (a307 == a227[0] && (input.equals(inputs[2]) && ((35 == a46[2]) && cf))))))) {
    	a165 += (a165 + 20) > a165 ? 3 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a50 -= (a50 - 20) < a50 ? 2 : 0;
    	a98 += (a98 + 20) > a98 ? 3 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a51 += (a51 + 20) > a51 ? 4 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	a88 += (a88 + 20) > a88 ? 1 : 0;
    	cf = false;
    	a75 = "g";
    	a143 = "i";
    	a69 = "i";
    	a57 = ((a237 + a211) - -7); 
    	System.out.println("V");
    } 
    if(((a310 <=  160 && ((((a302 == a346[0] && ((a240 && a235 == a216[0]) && a146)) && ((-10 < a13) && (203 >= a13))) && input.equals(inputs[8])) && (45 == a228[1]))) && (a324 == a232[0] && (((35 == a46[2]) && cf) && (a75.equals("i")))))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 -= (a165 - 20) < a165 ? 2 : 0;
    	a12 += (a12 + 20) > a12 ? 1 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a90 += (a90 + 20) > a90 ? 3 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 1 : 0;
    	cf = false;
    	if(258 < a72) {
    	a286 = a294[5];
    	a202 = a217[7];
    	a249 = (((((a249 * 1) % 101) - -254) / 5) - -209);
    	a313 = "h";
    	a378 = false;
    	a235 = a216[3];
    	a358 = a351;
    	a218 = "h";
    	a310 = ((((a310 % 15080) + -14919) / 5) - 7170);
    	a302 = a346[4];
    	a383 = a226[4];
    	a361 = "f";
    	a75 = "h";
    	a357 = (((a357 / 5) - 369) - 5623);
    	a134 = true;
    	a296 = a384;
    	a196 = (a398 + 7);
    	a333 = (((36 - -25785) - -916) * 1);
    	a201 = (((((a201 / 5) % 94) + 116) - -25105) + -25088);
    	a91 = ((a196 + a196) - 21); 
    	}else {
    	a370 = a318;
    	a302 = a346[2];
    	a368 = true;
    	a173 = "g";
    	a265 = a303;
    	a224 = "h";
    	a373 = true;
    	a357 = ((((a357 / 5) * 10) / -9) + 7422);
    	a392 = a304;
    	a260 = true;
    	a338 = 6;
    	a395 = true;
    	a320 = 12;
    	a310 = (((((a310 % 20) - -336) * 5) % 20) + 324);
    	a312 = "h";
    	a172 = (a398 - 6);
    	a218 = "e";
    	a75 = "f";
    	a240 = true;
    	a270 = 10;
    	a313 = "e";
    	a274 = a290;
    	}System.out.println("V");
    } 
    if(((((a75.equals("i")) && ((a312.equals("e")) && (a313.equals("e")))) && ((-10 < a13) && (203 >= a13))) && ((((35 == a46[2]) && ((((cf && a146) && input.equals(inputs[6])) && (36 == a239[5])) && a302 == a346[0])) && (a256.equals("e"))) && a260))) {
    	a131 += (a131 + 20) > a131 ? 1 : 0;
    	a165 += (a165 + 20) > a165 ? 2 : 0;
    	a63 -= (a63 - 20) < a63 ? 2 : 0;
    	a133 -= (a133 - 20) < a133 ? 3 : 0;
    	a50 += (a50 + 20) > a50 ? 1 : 0;
    	a56 += (a56 + 20) > a56 ? 1 : 0;
    	a110 += (a110 + 20) > a110 ? 1 : 0;
    	a181 += (a181 + 20) > a181 ? 2 : 0;
    	cf = false;
    	if(((a183.equals("h")) && ((!(a91 == 14) || (a313.equals("f"))) && (a256.equals("f"))))) {
    	a336 = false;
    	a312 = "e";
    	a300 = false;
    	a265 = a303;
    	a230 = 3;
    	a310 = ((((a310 / 5) % 77) - -237) * 1);
    	a249 = (((((a249 + 0) % 14750) - -15249) / 5) - -23662);
    	a339 = true;
    	a357 = (((a357 + 30175) + 6) - -6);
    	a206 = 11;
    	a260 = false;
    	a383 = a226[1];
    	a201 = (((a201 - -30186) - -8) * 1);
    	a370 = a285;
    	a211 = 7;
    	a361 = "f";
    	a277 = ((((a277 - 0) % 14830) + 15169) * 1);
    	a243 = (((a243 * 1) / -5) + 10815);
    	a378 = false;
    	a382 = (((((a382 % 14967) + -65) - 10080) * 10) / 9);
    	a329 = "h";
    	a237 = 6;
    	a302 = a346[4];
    	a256 = "h";
    	a235 = a216[3];
    	a224 = "e";
    	a57 = ((a270 - a359) + 9);
    	a234 = a372[5];
    	a75 = "g";
    	a278 = a326[(a57 - 13)];
    	a373 = true;
    	a398 = 15;
    	a368 = false;
    	a353 = a263;
    	a392 = a304;
    	a276 = a289;
    	a269 = "i";
    	a286 = a294[5];
    	a184 = a157[(a338 - 1)];
    	a307 = a227[3];
    	a218 = "i";
    	a359 = 7;
    	a358 = a351;
    	a386 = ((((a386 % 14837) + 15162) * 1) - -1);
    	a225 = a205[4];
    	a239 = a299;
    	a313 = "i";
    	a395 = false;
    	a338 = 8;
    	a320 = 13;
    	a324 = a232[2];
    	a202 = a217[0];
    	a240 = false;
    	a296 = a212;
    	a270 = 15; 
    	}else {
    	a324 = a232[2];
    	a235 = a216[7];
    	a234 = a372[5];
    	a243 = ((((a243 % 14910) + -179) * 1) - 3794);
    	a368 = true;
    	a237 = 6;
    	a353 = a263;
    	a357 = (((((a357 % 14906) + -188) * 10) / 9) - 58);
    	a336 = true;
    	a370 = a318;
    	a276 = a250;
    	a260 = true;
    	a173 = "f";
    	a269 = "g";
    	a310 = ((((((a310 % 20) - -337) - 2756) * 5) % 20) - -342);
    	a382 = (((((a382 - -370) / 5) * 5) % 14911) + 15087);
    	a320 = 11;
    	a286 = a294[4];
    	a249 = (((a249 / 5) / 5) - 21375);
    	a75 = "f";
    	a207 = "i";
    	a256 = "i";
    	a386 = (((((a386 + 6547) - 4821) - 1265) % 61) + 262);
    	a329 = "i";
    	a129 = a92[(a398 + -5)];
    	a270 = 17;
    	a240 = true;
    	a312 = "i";
    	a383 = a226[0];
    	a307 = a227[5];
    	a218 = "h";
    	a202 = a217[4];
    	a302 = a346[3];
    	a277 = (((((a277 % 14830) - -15169) * 10) / 9) * 1);
    	a296 = a362;
    	a300 = true;
    	a313 = "i";
    	a339 = true;
    	a125 = a30[7];
    	a239 = a299;
    	a373 = true;
    	a206 = 7;
    	a392 = a208;
    	a211 = 1;
    	a338 = 9;
    	a201 = (((((a201 - 0) - 0) + 0) * -9) / 10);
    	a395 = true;
    	a230 = 8;
    	a265 = a293;
    	a359 = 3;
    	a228 = a229;
    	a333 = (((((69 * 5) * 5) + -24238) * -1) / 10);
    	a398 = 10;
    	}System.out.println("R");
    } 
}
private  void calculateOutputm18(String input) {
    if(((a307 == a227[0] && (cf && (27 == a46[0]))) && ((((a277 <=  -10 && a373) && a234 == a372[0]) && a383 == a226[0]) && a225 == a205[0]))) {
    	if((((a270 == 10) && ((a313.equals("e")) && ((3 == a353[2]) && ((a313.equals("e")) && a307 == a227[0])))) && ((a151 && cf) && a333 <=  -47))) {
    		calculateOutputm195(input);
    	} 
    } 
    if((((cf && (35 == a46[2])) && (47 == a265[5])) && (((a329.equals("e")) && ((a324 == a232[0] && (45 == a228[1])) && a339)) && (a207.equals("e"))))) {
    	if((((((-10 < a13) && (203 >= a13)) && cf) && a373) && ((47 == a265[5]) && (a243 <=  -179 && (((a224.equals("e")) && (a398 == 10)) && (a338 == 3)))))) {
    		calculateOutputm196(input);
    	} 
    } 
}
private  void calculateOutputm199(String input) {
    if((((a361.equals("e")) && ((((cf && ((167 < a73) && (277 >= a73))) && !a146) && a16) && (45 == a228[1]))) && ((a373 && (((input.equals(inputs[6]) && a339) && a339) && (a75.equals("i")))) && a383 == a226[0]))) {
    	cf = false;
    	if(((a302 == a346[0] && (a57 == 10)) || !(a320 == 11))) {
    	a382 = (((1 - 7732) - -9203) - 1361);
    	a277 = (((((((a277 * a382) % 14999) % 95) - -243) / 5) / 5) - -288);
    	a296 = a362;
    	a383 = a226[(a338 + -1)];
    	a57 = (a230 + 13);
    	a240 = true;
    	a395 = true;
    	a211 = a338;
    	a206 = (a230 - -3);
    	a218 = "g";
    	a235 = a216[((a359 - a359) + 1)];
    	a333 = ((((((((a333 * a73) % 14999) % 14) + 162) * 5) * 5) % 14) - -157);
    	a310 = (((((((a382 * a277) % 14999) / 5) * 5) / 5) % 20) + 336);
    	a260 = true;
    	a368 = true;
    	a75 = "g";
    	a225 = a205[(a398 - 8)];
    	a307 = a227[(a338 + -1)];
    	a312 = "g";
    	a339 = false;
    	a243 = ((((((a243 * a357) % 14999) % 43) + -138) - -11549) - 11555);
    	a320 = (a359 + 3);
    	a324 = a232[((a338 - a206) + 4)];
    	a357 = (((((((a382 * a310) % 14999) * 2) % 38) + -17) - 12697) + 12696);
    	a336 = true;
    	a230 = (a338 - -2);
    	a278 = a326[(a57 - 13)];
    	a373 = false;
    	a386 = ((((((a386 * a249) % 14999) * 2) + -2) % 61) - -262);
    	a269 = "g";
    	a329 = "g";
    	a361 = "g";
    	a237 = ((a338 + a206) + -4);
    	a358 = a335;
    	a353 = a399;
    	a184 = a157[(a270 + -8)];
    	a270 = (a338 - -9);
    	a370 = a311;
    	a398 = ((a320 / a211) + 10);
    	a239 = a268;
    	a338 = ((a57 * a206) + -91); 
    	}else {
    	a386 = ((((((a386 * a277) % 14999) / 5) % 61) + 138) * 1);
    	a383 = a226[((a206 * a237) - 10)];
    	a361 = "f";
    	a276 = a289;
    	a329 = "f";
    	a333 = (((((((a333 * a357) % 14999) - 10378) % 96) - -51) - -24187) - 24188);
    	a224 = "e";
    	a277 = ((((((a386 * a386) % 14999) - -7875) + -20037) % 95) - -243);
    	a324 = a232[(a206 - 3)];
    	a243 = (((((((a243 * a201) % 14999) % 43) - 150) - -19) * 9) / 10);
    	a218 = "f";
    	a286 = a294[((a270 + a398) - 19)];
    	a312 = "f";
    	a240 = false;
    	a249 = ((((((((a310 * a310) % 14999) % 101) + 254) * 9) / 10) + -17806) + 17734);
    	a338 = (a230 - -1);
    	a207 = "f";
    	a373 = false;
    	a358 = a351;
    	a398 = (a237 + 9);
    	a270 = ((a230 + a230) + 5);
    	a353 = a399;
    	a75 = "f";
    	a302 = a346[(a211 + -1)];
    	a228 = a264;
    	a300 = false;
    	a239 = a268;
    	a357 = ((((((a382 * a73) / 5) % 65) - 60) * 10) / 9);
    	a336 = false;
    	a125 = a30[(a206 - -3)];
    	a339 = false;
    	a129 = a92[5];
    	a173 = "f";
    	a368 = false;
    	a225 = a205[(a230 + -2)];
    	a395 = false;
    	a260 = false;
    	a378 = false;
    	a237 = ((a338 + a338) + -4);
    	a359 = (a320 - 3);
    	a230 = a206;
    	a235 = a216[(a206 - 3)];
    	a206 = (a211 - -3);
    	}System.out.println("Y");
    } 
    if((((((!a146 && cf) && ((167 < a73) && (277 >= a73))) && (67 == a358[2])) && a368) && (((((a373 && (a324 == a232[0] && input.equals(inputs[3]))) && a243 <=  -179) && a243 <=  -179) && a16) && (a75.equals("i"))))) {
    	cf = false;
    	a359 = ((a338 - a206) + 6);
    	a224 = "g";
    	a234 = a372[(a398 - 8)];
    	a202 = a217[(a230 + -2)];
    	a286 = a294[(a270 + -8)];
    	a300 = true;
    	a276 = a250;
    	a256 = "g";
    	a302 = a346[(a270 + -8)];
    	a265 = a376;
    	a249 = ((((((a386 * a243) % 14999) * 2) % 71) + 427) - 1); 
    	System.out.println("T");
    } 
    if((((!a146 && (((a235 == a216[0] && (a243 <=  -179 && (a329.equals("e")))) && (67 == a358[2])) && (a75.equals("i")))) && ((a398 == 10) && ((a16 && (((167 < a73) && (277 >= a73)) && (cf && input.equals(inputs[7])))) && a277 <=  -10))) && a168 <= -13)) {
    	a122 -= (a122 - 20) < a122 ? 1 : 0;
    	cf = false;
    	a361 = "f";
    	a218 = "f";
    	a357 = (((((((a357 * a243) % 14999) + -11157) % 65) - 121) - 19092) - -19090);
    	a239 = a299;
    	a173 = "h";
    	a75 = "f";
    	a395 = false;
    	a270 = (a237 + 8);
    	a353 = a241;
    	a91 = (a206 - -7);
    	a240 = false;
    	a1 = a87[(a91 - 8)]; 
    	System.out.println("Y");
    } 
    if(((a336 && ((a75.equals("i")) && ((!a146 && cf) && a16))) && ((a324 == a232[0] && ((((((167 < a73) && (277 >= a73)) && a395) && input.equals(inputs[8])) && a277 <=  -10) && (45 == a228[1]))) && a378))) {
    	a63 -= (a63 - 20) < a63 ? 1 : 0;
    	cf = false;
    	a286 = a294[1];
    	a359 = 4;
    	a336 = false;
    	a276 = a289;
    	a239 = a268;
    	a302 = a346[((a359 - a359) - -1)];
    	a230 = a359;
    	a260 = false;
    	a338 = a359;
    	a172 = 1;
    	a398 = (a230 + 7);
    	a353 = a241;
    	a361 = "f";
    	a243 = ((((((a243 * a333) % 14999) - -701) % 11) + -176) - 1);
    	a207 = "f";
    	a235 = a216[((a230 + a230) + -7)];
    	a225 = a205[(a237 - 2)];
    	a237 = ((a359 * a359) - 12);
    	a324 = a232[(a270 - 8)];
    	a202 = a217[0];
    	a270 = (a359 - -7);
    	a206 = (a398 + -6);
    	a249 = (((((((a73 * a357) % 14999) - 6970) % 101) + 349) / 5) - -128);
    	a240 = false;
    	a256 = "f";
    	a386 = (((((((a386 * a277) % 14999) * 2) + -3) * 1) % 61) - -140);
    	a234 = a372[((a359 / a359) + -1)];
    	a329 = "f";
    	a75 = "f";
    	a383 = a226[(a320 + -5)];
    	a378 = false;
    	a173 = "g";
    	a368 = false;
    	a300 = false;
    	a211 = (a230 - 3);
    	a224 = "f";
    	a277 = (((((a382 * a382) * 5) % 95) - -180) - -60);
    	a339 = false;
    	a218 = "f";
    	a312 = "f";
    	a357 = (((((((a357 * a310) % 14999) % 65) + -122) - -1) / 5) + -91);
    	a395 = false;
    	a296 = a384;
    	a129 = a92[a172];
    	a265 = a293;
    	a333 = ((((((a333 * a249) % 14999) - 12055) % 96) + 92) - 30); 
    	System.out.println("R");
    } 
}
private  void calculateOutputm19(String input) {
    if((((a378 && a333 <=  -47) && (a207.equals("e"))) && (a386 <=  77 && (a386 <=  77 && ((3 == a353[2]) && (cf && ((167 < a73) && (277 >= a73)))))))) {
    	if((((cf && a16) && (45 == a228[1])) && (a336 && (a243 <=  -179 && (((a312.equals("e")) && a225 == a205[0]) && (a206 == 4)))))) {
    		calculateOutputm199(input);
    	} 
    } 
}



public  void calculateOutput(String input) {
 	cf = true;
    if((((24 == a370[2]) && a260) && ((3 == a353[2]) && ((a269.equals("e")) && ((67 == a358[2]) && ((a230 == 3) && (cf && (a75.equals("e"))))))))) {
    	if((((((24 == a370[2]) && (a378 && (a329.equals("e")))) && a235 == a216[0]) && (a329.equals("e"))) && ((45 == a228[1]) && (cf && a132)))) {
    		calculateOutputm1(input);
    	} 
    	if(((((a269.equals("e")) && (45 == a228[1])) && a310 <=  160) && (((98 == a276[2]) && ((cf && !a132) && (36 == a239[5]))) && (a218.equals("e"))))) {
    		calculateOutputm2(input);
    	} 
    } 
    if(((((-188 < a357) && (-57 >= a357)) && ((160 < a310) && (316 >= a310))) && (((a313.equals("f")) && ((43 == a392[3]) && ((cf && (a75.equals("f"))) && !a240))) && (a270 == 11)))) {
    	if(((!a240 && ((cf && (a173.equals("e"))) && (a269.equals("f")))) && (((-10 < a277) && (148 >= a277)) && (a324 == a232[1] && ((a206 == 5) && !a378))))) {
    		calculateOutputm3(input);
    	} 
    	if((((((28 == a370[0]) && a324 == a232[1]) && ((-199 < a201) && (-12 >= a201))) && (a230 == 4)) && ((((a173.equals("f")) && cf) && ((-65 < a382) && (-39 >= a382))) && (103 == a276[1])))) {
    		calculateOutputm4(input);
    	} 
    	if((((a338 == 4) && ((a218.equals("f")) && (cf && (a173.equals("g"))))) && ((a320 == 7) && (((a224.equals("f")) && (a320 == 7)) && !a368)))) {
    		calculateOutputm5(input);
    	} 
    	if(((((a211 == 2) && ((((a173.equals("h")) && cf) && (43 == a392[3])) && (a218.equals("f")))) && ((-188 < a357) && (-57 >= a357))) && ((a269.equals("f")) && (a269.equals("f"))))) {
    		calculateOutputm6(input);
    	} 
    	if((((43 == a392[3]) && (((-199 < a201) && (-12 >= a201)) && !a260)) && ((!a395 && ((a329.equals("f")) && (cf && (a173.equals("i"))))) && (a224.equals("f"))))) {
    		calculateOutputm7(input);
    	} 
    } 
    if((((a307 == a227[2] && (((a218.equals("g")) && a240) && a240)) && (15 == a353[2])) && (((a75.equals("g")) && cf) && ((-57 < a357) && (20 >= a357))))) {
    	if((((a383 == a226[2] && ((a207.equals("g")) && (a225 == a205[2] && (cf && (a57 == 10))))) && a307 == a227[2]) && (a395 && (a338 == 5)))) {
    		calculateOutputm8(input);
    	} 
    	if((((((-57 < a357) && (20 >= a357)) && (cf && (a57 == 11))) && (a230 == 5)) && (a307 == a227[2] && ((a307 == a227[2] && ((-39 < a382) && (176 >= a382))) && ((-12 < a201) && (178 >= a201)))))) {
    		calculateOutputm9(input);
    	} 
    	if(((a378 && (cf && (a57 == 12))) && ((a230 == 5) && (((148 < a277) && (339 >= a277)) && ((a302 == a346[2] && ((355 < a249) && (499 >= a249))) && ((-156 < a243) && (-68 >= a243))))))) {
    		calculateOutputm10(input);
    	} 
    	if(((!a339 && (a336 && (((59 == a228[3]) && a368) && (47 == a239[4])))) && (((a57 == 13) && cf) && (a329.equals("g"))))) {
    		calculateOutputm11(input);
    	} 
    	if((((a235 == a216[2] && (cf && (a57 == 15))) && (a206 == 6)) && ((((a218.equals("g")) && (110 == a276[2])) && (a320 == 8)) && (92 == a296[2])))) {
    		calculateOutputm13(input);
    	} 
    	if(((a395 && (a395 && ((a256.equals("g")) && ((a329.equals("g")) && (cf && (a57 == 16)))))) && (a336 && (47 == a239[4])))) {
    		calculateOutputm14(input);
    	} 
    } 
    if((((45 == a228[1]) && (((a359 == 3) && (cf && (a75.equals("h")))) && a339)) && ((98 == a276[2]) && (a333 <=  -47 && (a320 == 6))))) {
    	if((((a338 == 3) && ((a237 == 3) && (a134 && cf))) && ((a373 && ((a237 == 3) && a240)) && (45 == a228[1])))) {
    		calculateOutputm16(input);
    	} 
    	if(((a336 && ((((a269.equals("e")) && (67 == a358[2])) && (a218.equals("e"))) && a202 == a217[0])) && ((cf && !a134) && (a224.equals("e"))))) {
    		calculateOutputm17(input);
    	} 
    } 
    if((((a338 == 3) && (cf && (a75.equals("i")))) && ((((a324 == a232[0] && a260) && (a218.equals("e"))) && (36 == a239[5])) && (a398 == 10)))) {
    	if(((a307 == a227[0] && (cf && a146)) && ((a300 && ((a359 == 3) && (a395 && a310 <=  160))) && a277 <=  -10))) {
    		calculateOutputm18(input);
    	} 
    	if(((((a357 <=  -188 && a339) && a240) && (81 == a296[3])) && ((a237 == 3) && (a395 && (!a146 && cf))))) {
    		calculateOutputm19(input);
    	} 
    } 

    errorCheck();
    if(cf)
    	throw new IllegalArgumentException("Current state has no transition for this input!");
}


public static void main(String[] args) throws Exception {
	// init system and input reader
	Problem12 eca = new Problem12();

	// main i/o-loop
	while(true) {
		//read input
		String input = stdin.readLine();

		 if((input.equals("E")) && (input.equals("F")) && (input.equals("A")) && (input.equals("G")) && (input.equals("B")) && (input.equals("H")) && (input.equals("C")) && (input.equals("I")) && (input.equals("J")) && (input.equals("D")))
			throw new IllegalArgumentException("Current state has no transition for this input!");
		try {
			//operate eca engine output = 
			eca.calculateOutput(input);
		} catch(IllegalArgumentException e) {
			System.err.println("Invalid input: " + e.getMessage());
		}
	}
}
}