package learnlib;

import com.google.common.collect.Lists;
import de.learnlib.api.SUL;
import de.learnlib.api.SULException;

import javax.annotation.Nullable;
import java.io.*;
import java.util.List;

/**
 * Simply SUL implementation by running a command line process.
 * Specifically for the RERS challenge (input type is Integer)
 * TODO: make parallel.
 * Created by joshua on 15/07/2016.
 */
public class ProcessSUL implements SUL<Integer, String> {
    private final ProcessBuilder pb;
    private Process process;
    private Writer processInput;
    private BufferedReader processOutput;

    ProcessSUL(String path){
        List<String> command = Lists.newArrayList(path);
        pb = new ProcessBuilder(command);
        pb.redirectErrorStream(true);
    }

    @Override
    public boolean canFork() {
        return false;
    }

    @Override
    public void pre() throws SULException {
        try {
            // System.err.println("SUL: start");
            process = pb.start();
            processInput = new OutputStreamWriter(process.getOutputStream());
            processOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
        } catch (IOException e) {
            throw new SULException(e);
        }
    }

    @Override
    public void post() throws SULException {
        try {
            processInput.close();
            processOutput.close();
            process.destroy();
            process = null;
            processInput = null;
            processOutput = null;
            // System.err.println("SUL: end");
        } catch (IOException e) {
            throw new SULException(e);
        }
    }

    @Nullable
    @Override
    public String step(@Nullable Integer s) throws SULException {
        if(s == null) return null;

        try {
            processInput.write(Integer.toString(s+1));
            processInput.write("\n");
            processInput.flush();

            while(!processOutput.ready()) {
                Thread.sleep(1);
            }

            StringBuilder sb = new StringBuilder();
            while(processOutput.ready()) {
                sb.append(processOutput.readLine());
                sb.append(';');
                Thread.sleep(1);
            }

            // System.err.println("SUL: " + s + " => " + sb);
            return sb.toString();
        } catch (Exception e) {
            throw new SULException(e);
        }
    }
}
