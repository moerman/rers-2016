package learnlib;

import de.learnlib.api.MembershipOracle;
import de.learnlib.api.Query;
import net.automatalib.words.Word;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An membership oracle which looks up words in the cache first. The cache is external to allow for sharing.
 * If the word is not yet in the cache it is delegated to the delegate, and added afterwards. In order to
 * constrain memory usage, the size of the cache is bounded. This class only works for Mealy machines
 *
 * @param <I> Input alphabet
 * @param <O> Output alphabet
 */
public class CachedOracle<I, O> implements MembershipOracle<I, Word<O>> {
    public static class FullQuery<I, O> extends Query<I, Word<O>> {
        private final Query<I, Word<O>> original;
        private Word<O> fullAnswer;

        public FullQuery(Query<I, Word<O>> original) {
            this.original = original;
        }

        public Word<O> getFullAnswer() {
            return fullAnswer;
        }

        @Nonnull
        @Override
        public Word<I> getInput() {
            // getInput gives the concatenated thingy
            return original.getInput();
        }

        @Nonnull
        @Override
        public Word<I> getPrefix() {
            return Word.epsilon();
        }

        @Nonnull
        @Override
        public Word<I> getSuffix() {
            return getInput();
        }

        @Override
        public void answer(@Nullable Word<O> out) {
            fullAnswer = out;
            original.answer(fullAnswer.subWord(original.getPrefix().length()));
        }
    }

    private final Cache<I, O> cache;
    private MembershipOracle<I, Word<O>> delegate;
    private int bound;
    private boolean bounded = false;
    private boolean addNew = true;

    public CachedOracle(Cache<I, O> cache, MembershipOracle<I, Word<O>> delegate, int bound, boolean addNew) {
        this.cache = cache;
        this.delegate = delegate;
        this.bound = bound;
        this.addNew = addNew;
    }

    @Override
    public void processQueries(Collection<? extends Query<I, Word<O>>> collection) {
        // For each query we will look it up, things not present in the cache are left over
        List<FullQuery<I, O>> leftOvers = collection.stream()
                .filter((Query<I, Word<O>> q) -> {
                    // First we are going to look them up, storing output in o
                    Word<O> o = cache.lookup(q.getInput());
                    if (o != null) {
                        // If the query is known. We have the output in o
                        // But for the learners, we only want the output for the suffix!
                        o = o.subWord(q.getPrefix().size(), q.getInput().size());
                        assert o.size() == q.getSuffix().size();
                        // We answer, and filter it out
                        q.answer(o);
                        return false;
                    }
                    // If it was not there, we have to keep it and delegate it
                    return true;
                })
                // We want to make *full* queries to our delegate
                .map(FullQuery::new).collect(Collectors.toList());

        // We then simple delegate the left overs to the real membership oracle
        delegate.processQueries(leftOvers);

        // The remainder of this function adds the leftOvers to the cache.
        // We can skip this if we don't have to insert
        if (!addNew) {
            return;
        }

        // To constraint memory usage, we bound the cache
        // Once bounded, it will remain bounded
        if (bounded) {
            return;
        }

        // Here we will insert them
        // The new answers should be added to the cache
        for (FullQuery<I, O> leftOver : leftOvers) {
            Word<I> input = leftOver.getInput();
            Word<O> output = leftOver.getFullAnswer();
            assert output != null;
            // If we only have output for the suffix part, we might not be able to store the full thing
            // So in that case, we simply ignore it (there is some room for improvement here!)
            if (input.size() != output.size()) {
                System.out.println("WARNING: not correct size");
                continue;
            }
            cache.insert(input, output);
        }

        // A good measure is the number of nodes in the tree
        // (Another way would be to don't insert long sequences, but
        // the current way reflects the usage of sequences better)
        // Unfortunately this function (.asGraph().size()) is SUPER SLOW
        // Which surprises me, because you can simply walk the tree
        // But I made a heuristic avoiding this computation quite often
        if (cache.estimatedSize() > 1.05 * bound) {
            if (cache.size() > bound) {
                bounded = true;
            }
        }
    }
}
