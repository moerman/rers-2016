#inputs [[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T]]
#outputs [[X, Y, Z, U, V, W]]
#0: output W always precedes output X
(! oX WU oW)

#1: input J precedes output V after input O
((false R ! iO) | (true U (iO & (! oV WU iJ))))

#2: input D always precedes output Z, output V
(! (true U (oZ & X (true U oV))) | (! oZ U iD))

#3: input D, input L precedes output V between input B and input C
(false R (! (iB & (true U iC)) | (! oV U (iC | ((iD & ! oV) & X (! oV U iL))))))

#4: output U, output Y responds to input P between input M and input A
(false R (! (iM & (true U iA)) | ((! iP | (! iA U ((oU & ! iA) & X (! iA U oY)))) U iA)))

#5: output X, output U responds to input L after input E
(false R (! iE | (false R (! iL | (oX & X (true U oU))))))

#6: output U precedes output X after input F until output Y
(false R (iF & (! ! oY | (! oX WU (oU | oY)))))

#7: output X, output V responds to input T before input S
(! (true U iS) | ((! iT | (! iS U ((oX & ! iS) & X (! iS U oV)))) U iS))

#8: output U, output Z without output Y always responds to input K
(false R (! iK | (true U ((oU & ! oY) & X (! oY U oZ)))))

#9: output Y responds to input L, input M after output W until input C
(false R (! oW | ((iL & (! X (! iC U iM) | X (! iC U (iM & (true U oY))))) U (iC | (false R (iL & (! X (! iC U iM) | X (! iC U (iM & (true U oY))))))))))

#10: output X precedes output W, output Z after input G
((false R ! iG) | (! iG U (iG & (! (true U (oW & X (true U oZ))) | (! oW U oX)))))

#11: output V, output Y without output Z responds to input D after input K until input O
(false R (! iK | ((! iD | (! iO U (((oV & ! iO) & ! oZ) & X ((! iO & ! oZ) U oY)))) U (iO | (false R (! iD | ((oV & ! oZ) & X (! oZ U oY))))))))

#12: input Q always precedes output X, output Y
(! (true U (oX & X (true U oY))) | (! oX U iQ))

#13: output V responds to input N, input L between output X and input Q
(false R (! (oX & (true U iQ)) | ((iN & (! X (! iQ U iL) | X (! iQ U (iL & (true U oV))))) U iQ)))

#14: input D, input A precedes output X before input G
(! (true U iG) | (! oX U (iG | ((iD & ! oX) & X (! oX U iA)))))

#15: output W responds to input G between input A and input L
(false R (! ((iA & ! iL) & (true U iL)) | ((! iG | (! iL U (oW & ! iL))) U iL)))

#16: input S, input C precedes output W before input K
(! (true U iK) | (! oW U (iK | ((iS & ! oW) & X (! oW U iC)))))

#17: output W, output Y without output Z responds to input J after input F until input K
(false R (! iF | ((! iJ | (! iK U (((oW & ! iK) & ! oZ) & X ((! iK & ! oZ) U oY)))) U (iK | (false R (! iJ | ((oW & ! oZ) & X (! oZ U oY))))))))

#18: input B precedes output Y, output U after output X until input Q
(false R (! oX | ((! ((oY & ! iQ) & X (! iQ U (oU & ! iQ))) U (iQ | iB)) | (false R ! (oY & X (true U oU))))))

#19: output U precedes output X, output Y after input T until input D
(false R (! iT | ((! ((oX & ! iD) & X (! iD U (oY & ! iD))) U (iD | oU)) | (false R ! (oX & X (true U oY))))))

#20: output W, output U responds to input R before input F
(! (true U iF) | ((! iR | (! iF U ((oW & ! iF) & X (! iF U oU)))) U iF))

#21: output Y, output X without output Z responds to input L before input D
(! (true U iD) | ((! iL | (! iD U (((oY & ! iD) & ! oZ) & X ((! iD & ! oZ) U oX)))) U iD))

#22: output W responds to input G, input S before input R
(! (true U iR) | ((iG & (! X (! iR U iS) | X (! iR U (iS & (true U oW))))) U iR))

#23: input N, input L precedes output W before output X
(! (true U oX) | (! oW U (oX | ((iN & ! oW) & X (! oW U iL)))))

#24: output X responds to input T, input M after input N
(false R (! iN | (false R (iT & (! X (true U iM) | X (! iM U (iM & (true U oX))))))))

#25: output V responds to input H after output W until input C
(false R (oW & (! ! iC | ((! iH | (! iC U (oV & ! iC))) WU iC))))

#26: output W, output V always responds to input H
(false R (! iH | (true U (oW & X (true U oV)))))

#27: input M precedes output W, output U after output V
((false R ! oV) | (! oV U (oV & (! (true U (oW & X (true U oU))) | (! oW U iM)))))

#28: output U, output Z responds to input T before input G
(! (true U iG) | ((! iT | (! iG U ((oU & ! iG) & X (! iG U oZ)))) U iG))

#29: output U responds to input O between output V and input G
(false R (! ((oV & ! iG) & (true U iG)) | ((! iO | (! iG U (oU & ! iG))) U iG)))

#30: output W, output V without output Z responds to input D after input C until input T
(false R (! iC | ((! iD | (! iT U (((oW & ! iT) & ! oZ) & X ((! iT & ! oZ) U oV)))) U (iT | (false R (! iD | ((oW & ! oZ) & X (! oZ U oV))))))))

#31: output X, output U responds to input I after input G
(false R (! iG | (false R (! iI | (oX & X (true U oU))))))

#32: input I precedes output V, output Y between input S and input K
(false R (! (iS & (true U iK)) | (! ((oV & ! iK) & X (! iK U (oY & ! iK))) U (iK | iI))))

#33: input O precedes output V, output Z after input T until input L
(false R (! iT | ((! ((oV & ! iL) & X (! iL U (oZ & ! iL))) U (iL | iO)) | (false R ! (oV & X (true U oZ))))))

#34: output X, output W without output V responds to input O betwen input E and input S
(false R (! (iE & (true U iS)) | ((! iO | (! iS U (((oX & ! iS) & ! oV) & X ((! iS & ! oV) U oW)))) U iS)))

#35: input N always precedes output Z, output W
(! (true U (oZ & X (true U oW))) | (! oZ U iN))

#36: output U responds to input C, input M after input K until output Y
(false R (! iK | ((iC & (! X (! oY U iM) | X (! oY U (iM & (true U oU))))) U (oY | (false R (iC & (! X (! oY U iM) | X (! oY U (iM & (true U oU))))))))))

#37: output U, output Z without output Y responds to input Q betwen input D and input O
(false R (! (iD & (true U iO)) | ((! iQ | (! iO U (((oU & ! iO) & ! oY) & X ((! iO & ! oY) U oZ)))) U iO)))

#38: output W always responds to input O
(false R (! iO | (true U oW)))

#39: output Z, output Y responds to input K before input F
(! (true U iF) | ((! iK | (! iF U ((oZ & ! iF) & X (! iF U oY)))) U iF))

#40: output V responds to input E after output W until input K
(false R (oW & (! ! iK | ((! iE | (! iK U (oV & ! iK))) WU iK))))

#41: output U, output Y responds to input J before input R
(! (true U iR) | ((! iJ | (! iR U ((oU & ! iR) & X (! iR U oY)))) U iR))

#42: input H, input T precedes output V after input P
((false R ! iP) | (! iP U (iP & (! (true U oV) | (! oV U ((iH & ! oV) & X (! oV U iT)))))))

#43: input G precedes output Y, output W after input P
((false R ! iP) | (! iP U (iP & (! (true U (oY & X (true U oW))) | (! oY U iG)))))

#44: output W, output Y always responds to input M
(false R (! iM | (true U (oW & X (true U oY)))))

#45: input P always precedes output V, output Y
(! (true U (oV & X (true U oY))) | (! oV U iP))

#46: input H, output W precedes output Z between output Y and input S
(false R (! (oY & (true U iS)) | (! oZ U (iS | ((iH & ! oZ) & X (! oZ U oW))))))

#47: output Z, output Y without output W always responds to input S
(false R (! iS | (true U ((oZ & ! oW) & X (! oW U oY)))))

#48: output Z always responds to input I
(false R (! iI | (true U oZ)))

#49: output Z, output W responds to input K after input L until input C
(false R (! iL | ((! iK | (! iC U ((oZ & ! iC) & X (! iC U oW)))) U (iC | (false R (! iK | (oZ & X (true U oW))))))))

#50: output U responds to input B, input H after output Y until input F
(false R (! oY | ((iB & (! X (! iF U iH) | X (! iF U (iH & (true U oU))))) U (iF | (false R (iB & (! X (! iF U iH) | X (! iF U (iH & (true U oU))))))))))

#51: output X responds to input E, input C after input H until input B
(false R (! iH | ((iE & (! X (! iB U iC) | X (! iB U (iC & (true U oX))))) U (iB | (false R (iE & (! X (! iB U iC) | X (! iB U (iC & (true U oX))))))))))

#52: output X responds to input D, input F before input E
(! (true U iE) | ((iD & (! X (! iE U iF) | X (! iE U (iF & (true U oX))))) U iE))

#53: input B precedes output Y after output W
((false R ! oW) | (true U (oW & (! oY WU iB))))

#54: output X precedes output Z, output Y between input R and input N
(false R (! (iR & (true U iN)) | (! ((oZ & ! iN) & X (! iN U (oY & ! iN))) U (iN | oX))))

#55: input C precedes output V after input S
((false R ! iS) | (true U (iS & (! oV WU iC))))

#56: input D precedes output U, output Y after output Z until input L
(false R (! oZ | ((! ((oU & ! iL) & X (! iL U (oY & ! iL))) U (iL | iD)) | (false R ! (oU & X (true U oY))))))

#57: output Z always responds to input P, input G
(false R (iP & (! X (true U iG) | X (true U (iG & (true U oZ))))))

#58: input A precedes output Y after input M until input T
(false R (iM & (! ! iT | (! oY WU (iA | iT)))))

#59: output Y, output Z without output W always responds to input B
(false R (! iB | (true U ((oY & ! oW) & X (! oW U oZ)))))

#60: input O always precedes output X, output W
(! (true U (oX & X (true U oW))) | (! oX U iO))

#61: input O always precedes output U, output X
(! (true U (oU & X (true U oX))) | (! oU U iO))

#62: output X, output Z responds to input M before input E
(! (true U iE) | ((! iM | (! iE U ((oX & ! iE) & X (! iE U oZ)))) U iE))

#63: output V responds to input F before input I
(! (true U iI) | ((! iF | (! iI U (oV & ! iI))) U iI))

#64: input R, input C precedes output Y between input T and input H
(false R (! (iT & (true U iH)) | (! oY U (iH | ((iR & ! oY) & X (! oY U iC))))))

#65: output Z, output V without output Y responds to input O betwen input K and input C
(false R (! (iK & (true U iC)) | ((! iO | (! iC U (((oZ & ! iC) & ! oY) & X ((! iC & ! oY) U oV)))) U iC)))

#66: output Z, output W without output Y responds to input S before input A
(! (true U iA) | ((! iS | (! iA U (((oZ & ! iA) & ! oY) & X ((! iA & ! oY) U oW)))) U iA))

#67: output U responds to input R, input A after input Q
(false R (! iQ | (false R (iR & (! X (true U iA) | X (! iA U (iA & (true U oU))))))))

#68: output Y, output U always responds to input K
(false R (! iK | (true U (oY & X (true U oU)))))

#69: output U, output X responds to input M after input A
(false R (! iA | (false R (! iM | (oU & X (true U oX))))))

#70: input O precedes output X after output Z
((false R ! oZ) | (true U (oZ & (! oX WU iO))))

#71: input G precedes output Z between output U and input P
(false R (! ((oU & ! iP) & (true U iP)) | (! oZ U (iG | iP))))

#72: output X, output U responds to input Q after input B until input R
(false R (! iB | ((! iQ | (! iR U ((oX & ! iR) & X (! iR U oU)))) U (iR | (false R (! iQ | (oX & X (true U oU))))))))

#73: input B precedes output X between output U and input P
(false R (! ((oU & ! iP) & (true U iP)) | (! oX U (iB | iP))))

#74: input S, input K precedes output V after output Z
((false R ! oZ) | (! oZ U (oZ & (! (true U oV) | (! oV U ((iS & ! oV) & X (! oV U iK)))))))

#75: output V responds to input F, input B after input A until input S
(false R (! iA | ((iF & (! X (! iS U iB) | X (! iS U (iB & (true U oV))))) U (iS | (false R (iF & (! X (! iS U iB) | X (! iS U (iB & (true U oV))))))))))

#76: output X, output Z responds to input N before input S
(! (true U iS) | ((! iN | (! iS U ((oX & ! iS) & X (! iS U oZ)))) U iS))

#77: output X responds to input B, input L after input T
(false R (! iT | (false R (iB & (! X (true U iL) | X (! iL U (iL & (true U oX))))))))

#78: input H always precedes output U
(! oU WU iH)

#79: input A precedes output V, output W after input I until input N
(false R (! iI | ((! ((oV & ! iN) & X (! iN U (oW & ! iN))) U (iN | iA)) | (false R ! (oV & X (true U oW))))))

#80: output Z responds to input P before input G
(! (true U iG) | ((! iP | (! iG U (oZ & ! iG))) U iG))

#81: input R precedes output W, output V after input K until output X
(false R (! iK | ((! ((oW & ! oX) & X (! oX U (oV & ! oX))) U (oX | iR)) | (false R ! (oW & X (true U oV))))))

#82: output U, output Y without output Z responds to input I after input G until output X
(false R (! iG | ((! iI | (! oX U (((oU & ! oX) & ! oZ) & X ((! oX & ! oZ) U oY)))) U (oX | (false R (! iI | ((oU & ! oZ) & X (! oZ U oY))))))))

#83: output W, output U without output Z always responds to input M
(false R (! iM | (true U ((oW & ! oZ) & X (! oZ U oU)))))

#84: output X precedes output W between input I and input N
(false R (! ((iI & ! iN) & (true U iN)) | (! oW U (oX | iN))))

#85: output W, output Y without output U responds to input G after input D
(false R (! iD | (false R (! iG | ((oW & ! oU) & X (! oU U oY))))))

#86: input A precedes output W, output U after input M
((false R ! iM) | (! iM U (iM & (! (true U (oW & X (true U oU))) | (! oW U iA)))))

#87: output Y always responds to input T
(false R (! iT | (true U oY)))

#88: output V, output U without output X responds to input P after input A
(false R (! iA | (false R (! iP | ((oV & ! oX) & X (! oX U oU))))))

#89: output Y responds to input S after input A
(false R (! iA | (false R (! iS | (true U oY)))))

#90: output Z, output W responds to input P between input N and output U
(false R (! (iN & (true U oU)) | ((! iP | (! oU U ((oZ & ! oU) & X (! oU U oW)))) U oU)))

#91: output U responds to input K, input M after input R
(false R (! iR | (false R (iK & (! X (true U iM) | X (! iM U (iM & (true U oU))))))))

#92: output V responds to input P, input M before input G
(! (true U iG) | ((iP & (! X (! iG U iM) | X (! iG U (iM & (true U oV))))) U iG))

#93: output W always responds to input I
(false R (! iI | (true U oW)))

#94: input J precedes output X, output V after input C
((false R ! iC) | (! iC U (iC & (! (true U (oX & X (true U oV))) | (! oX U iJ)))))

#95: output X, output W responds to input S after output U
(false R (! oU | (false R (! iS | (oX & X (true U oW))))))

#96: output Y responds to input F before input I
(! (true U iI) | ((! iF | (! iI U (oY & ! iI))) U iI))

#97: output W responds to input P, input R before input I
(! (true U iI) | ((iP & (! X (! iI U iR) | X (! iI U (iR & (true U oW))))) U iI))

#98: output W, output Z without output X responds to input G after input B
(false R (! iB | (false R (! iG | ((oW & ! oX) & X (! oX U oZ))))))

#99: input T, input C precedes output U between output X and input G
(false R (! (oX & (true U iG)) | (! oU U (iG | ((iT & ! oU) & X (! oU U iC))))))

