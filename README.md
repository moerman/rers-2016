RERS Challenge 2016
===================

In this repo we collect all the stuff we used/created for the RERS challenge.
* challenge: Original files from the RERS challenge.
* report: Contains a textual description of the method(s) used.
* learning: Contains code (java) and results as produce by learnlib. All
  hypotheses were stored and compressed in 7zip archives. Some problems were
  done in multiple round, those have multiple archives of hypotheses.
  See learning/README.md for some statistics on the learning process.
  Please note that the current models are "off-by-one" in their input alphabet
  (not in the output alphabet).
* fuzzing: Contains scripts and results as produced by afl. All fuzzing
  instances (one for each of the challenge's problem) are stored in compressed
  archives, so fuzzing can be resumed at a later point in time (see afl's
  documentation for a how-to). Note that the binaries used for fuzzing are
  different from those used for learning (as they are instrumented and use a
  different function for error-handling), so be sure to use the binaries in
  this directory. Most of afl's traces are post-processed and stored in
  fuzzing/traces.
* models: The final models obtained by learning (still off by one). Also
  contains NuSMV models for problems 1 - 9 (the LTL problems). These are
  obtained by the scripts in postprocessing (and they don't have the off by
  one alphabet issue).
* postprocessing: Contains code to convert the dot files and ltl formulas to
  a format NuSMV understands.
* results: Has the results for all problems.
